<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reportes Puntaje</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!-- <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css"> -->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body >
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="actividad" style="margin-top: 100px;">
			<div class="container bg-light">
				<div class="form-row text-center">
					<div class="col-12">
						<h1 class="titulo">Reportes Puntaje</h1>
					</div>
				</div>
				<div class="form-row">
					
					<div class="form-group col-6">
						<label for="idGimnasio" class="texto">Regional</label>
						<select class="form-control" id="idGimnasio">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="idsede" class="texto">Centro Formación</label>
						<select class="form-control" id="idsede">
						</select>
					</div>

					<?php
						include_once("fechaRango.php");
						?>

					
					
				</div>
				<div class="form-row text-center">
					<div class="form-group col-12">
						<input type="button" class="btn btn-primary" class="Guardar"  id="btnConsultar" value="Consultar" onclick= "Consultar('reportePuntaje')" />
						
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">Sede</th>
							      <th class="bg-success" scope="col"> Nombre Evento</th>
							      <th class="bg-success" scope="col"> Aprendiz </th>

							      <th class="bg-success" scope="col"> Puntaje </th>
							      
							    </tr>
							  </thead>
							  <tbody id="tableBodyActividad">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/reportes/reporte.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>