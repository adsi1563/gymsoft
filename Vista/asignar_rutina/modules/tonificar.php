<div class="container"><center><h1>Tonificar</h1></center></div>
<div id="tonificar"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Elevaciones.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">De pie, coloque los brazos juntos al cuerpo y las rodillas ligeramente flexionadas.Sostenga las mancuernas con las palmas de las manos boca abajo,en un agarre neutro.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/ElevacionesFrontales.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">De pie, coloque los pies alineados a la misma distancia que sus hombros.SOstenga las mancuernas con la palmas de las manos boca abajo,en un agarre neutro.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/ElevacionesLaterales.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Siéntese en un banco recto con la espalda apoyada y sostenga las mancuernas con las manos giradas una a la otra,en un agarre neutro.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Press.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Manténgase de pie y sostenga las pesas con las palmas de las manos boca abajo. Levante las mancuernas hasta la altura de la cabeza,manteniendo los brazos flexionados lateralmente.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/PressSentado.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Levante las mancuernas hasta la altura de los hombros para iniciar el movimiento y luego estire los brazos casi por completo, baje las mancuernas hasta la altura del cuello y repita el movimiento.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Inclinado.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Baje las mancuernas lateralmente llevando los pesos hasta el nivel del tórax, levante las mancuernas de nuevo a la posición inicial y repita el movimiento.</p>
            </div>
          </div>        
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/banco.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Baje las mancuernas en el lateral hasta la altura del asiento, vuelva a la posición inicial y repita el movimiento.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/sentado.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">AJuste el banco a la altura de su pecho, mueva los pegadores hacia adelante hasta que queden lo más cerca posible, vuelva a la posición inicial y repita el movimiento, no deje caer el peso, sostengalo.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Mancuernas.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Levante su cuerpo hasta que los codos queden casi estirados, descienda el cuerpo hasta que su pectoral esté a una distancia de los dedos del suelo.Repita el movimiento.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Bombardero.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Levante el peso de su cuerpo proyectándolo hacia adelante,estire los brazos y eleve el tronco,manteniendo las piernas y la cadera paradas,suba la cadera lo máximo que logre, hasta que el cuerpo forma un v invertido, vuelva a la posición inicial y repita el movimiento.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Fondos.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Coloque sus brazos en una posición de 90 grados y incline su cuerpo hacía delante bajando.</p>
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/banca.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Coloquese las mancuerdas en la altura de su pecho y estirelos hacia arriba, bajelos lentamente, repita este movimiento.</p>
            </div>
          </div>        
        </div>
    </div>
</div>

