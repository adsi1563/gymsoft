<div id="accordion">
  <h3>Delgadez -16 / 18.49</h3>
  <!--STARTER DIV PADRE DELGADEZ-->
  <div>
    <!--starter tabs peso bajo-->
    <div id="peso-bajo">
      <ul>
        <li><a href="#lunes-bajo">Lunes</a></li>
        <li><a href="#martes-bajo">Martes</a></li>
        <li><a href="#miercoles-bajo">Miercoles</a></li>
        <li><a href="#jueves-bajo">Jueves</a></li>
        <li><a href="#viernes-bajo">Viernes</a></li>
      </ul>
      <!--starter rutina del lunes bajo-->
      <div id="lunes-bajo">
        <!--start table lunes bajo-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Flexiones</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=FuDyMYWTgG0">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexiones alterna apertura</td>
              <td>8-10</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Flexiones con mancuernas</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=IZGJJ7w_hMg">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexiones con pies elevados</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=SimJgxfH9Po">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexiones de diamente</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=ILM-0FNHxmM">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de antebrazo con mancuernas en supinación</td>
              <td>8-10</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Curl de antebrazo con mancuernas en plonación</td>
              <td>8-10</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end tabke bajo -->
      </div>
      <!--end div lunes-bajo-->
      <!--start martes bajo-->
      <div id="martes-bajo">
        <!--start table rutina martes bajo-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Groiners</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=AMnnWjBJ6JE">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Peso muerto con mancuernas</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=9oQnzdyU8EY">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Saltar la cuerda</td>
              <td>20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Sentadilla con mancuernas</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=4Ex0B2UMl3E">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Sentadillas con barra</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=feJlfvoFKU8">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Elevación atrás con mancuerna</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=JxS99ffwI50">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Tijeras con piernas altas</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=QNfc-FHe0_Y">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table martes bajo-->
      </div>
      <!--end martes bajo-->
      <!--starter div miercoles bajo-->
      <div id="miercoles-bajo">
        <!--starter table miercoles bajo-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Curl de biceps con barra</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=s66K7xFq-Yc">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl biceps alternos tipo martillo de pie</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=k0o4j4S7Ab0">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de biceps inclinado alterno con mancuernas</td>
              <td>8-10</td>
              <td><a href="">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Curl de bíceps tipo martillo sentado</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=YGpjPvB7j6Y">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de biceps en polea</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=kaZZtJv0B0Y">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de biceps agarre cerrado</td>
              <td>8-10</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Elevaciones frontales de pie con mancuernas</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=vaIORUmalq4">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table miercoles bajo-->
      </div>
      <!--end miercoles bajo-->
      <!--starter jueves bajo-->
      <div id="jueves-bajo">
        <!--starter table jueves bajo-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Bicicleta estática</td>
              <td>15 min</td>
              <td><a href="https://www.youtube.com/watch?v=pc9kMAh6cx8">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Elíptica</td>
              <td>5 min</td>
              <td><a href="https://www.youtube.com/watch?v=Yyx2EnOhRnA">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Groiners</td>
              <td>12-15</td>
              <td><a href="https://www.youtube.com/watch?v=AMnnWjBJ6JE">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Golpes</td>
              <td>13 rep</td>
              <td><a href="https://www.youtube.com/watch?v=LnHvIFV-9as">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Jumping jack</td>
              <td>10 rep</td>
              <td><a href="https://www.youtube.com/watch?v=U2MWgsiA1MU">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Saltar la cuerda</td>
              <td>20 rep</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Trotar</td>
              <td>5 min</td>
              <td><a href="https://www.youtube.com/watch?v=WMGBV3QYJC8">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table jueves bajo-->
      </div>
      <!--end jueves bajo-->
      <!--starter div viernes bajo-->
      <div id="viernes-bajo">
        <!--starter table viernes bajo-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Press de banca inclinada con barra</td>
              <td>7-8</td>
              <td><a href="https://www.youtube.com/watch?v=YzQ51AmzPIc">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Press de banca plana con barra</td>
              <td>7-8</td>
              <td><a href="https://www.youtube.com/watch?v=lEf05Ih9RHg">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexión con brazos cerrados</td>
              <td>8-10</td>
              <td><a href="#">Ver ejercicios</a></td>
            </tr>
            <tr>
              <td>Aperturas en máquina contractora</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=aRAUJ9D_1rI">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Pullover con mancuernas</td>
              <td>8-10</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Remo vertical con mancuernas</td>
              <td>20 rep</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Press sentado con mancuernas</td>
              <td>5 min</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table viernes bajo-->
      </div>
      <!--end viernes bajo-->
    </div>
    <!--end tabs desnutrición-->
  </div>
  <!--END DIV PADRE DELGADEZ-->

  <h3>Peso normal 18.50 / 24.99</h3>
  <!--STARTER DIV PADRE PESO NORMAL-->
  <div>
    <!--starter tabs peso normal-->
    <div id="peso-normal">
      <ul>
        <li><a href="#lunes-normal">Lunes</a></li>
        <li><a href="#martes-normal">Martes</a></li>
        <li><a href="#miercoles-normal">Miercoles</a></li>
        <li><a href="#jueves-normal">Jueves</a></li>
        <li><a href="#viernes-normal">Viernes</a></li>
      </ul>
      <!--starter rutina del lunes normal-->
      <div id="lunes-normal">
        <!--start table lunes normal-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Press de banco plano con barra</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Suend pres</td>
              <td>12-5</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Pulover con mancuernas</td>
              <td>12-15</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Press declinado de pie con polea</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexiones archer</td>
              <td>10-12</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexiones alternas con apertura</td>
              <td>10-12</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Curl de biceps agarre cerrado</td>
              <td>10-15</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end tabke bajo -->
      </div>
      <!--end div lunes-normal-->
      <!--start martes normal-->
      <div id="martes-normal">
        <!--start table rutina martes normal-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Peso muerto estilo sumo con barra</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Peso muerto estilo sumo con una pierna</td>
              <td>12-15</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Peso muerto estilo sumo con mancuerna</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Sentadilla con mancuernas</td>
              <td>10-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Sentadillas frontal con barra</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Prensa de piernas inclinadas</td>
              <td>12-15</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Saltar cuerda</td>
              <td>50 rep</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table martes normal-->
      </div>
      <!--end martes normal-->
      <!--starter div miercoles normal-->
      <div id="miercoles-normal">
        <!--starter table miercoles normal-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Abdominales con peso</td>
              <td>15-20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Abdominales inversos</td>
              <td>12-15</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Abdominales inversos en banco</td>
              <td>15-20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Abdominales sit up punches</td>
              <td>15-20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Cross plank</td>
              <td>15-20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Elevación de rodillas en suspención</td>
              <td>15-20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Superman</td>
              <td>15-20</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table miercoles normal-->
      </div>
      <!--end miercoles normal-->
      <!--starter jueves normal-->
      <div id="jueves-normal">
        <!--starter table jueves normal-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Bicicleta estática</td>
              <td>15 min</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Elíptica</td>
              <td>5 min</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Groiners</td>
              <td>12-15</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Golpes</td>
              <td>13 rep</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Jumping jack</td>
              <td>10 rep</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Saltar la cuerda</td>
              <td>20 rep</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Trotar</td>
              <td>5 min</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table jueves normal-->
      </div>
      <!--end jueves normal-->
      <!--starter div viernes normal-->
      <div id="viernes-normal">
        <!--starter table viernes normal-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Curl de antebrazos con barra en supinación</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de antebrazos con mancuerna en pronación</td>
              <td>12-15</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de antebrazos con mancuernas en supinación</td>
              <td>8-10</td>
              <td><a href="#">Ver ejercicios</a></td>
            </tr>
            <tr>
              <td>Flexión de dedos</td>
              <td>12-15</td>
              <td><a href="#">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Rodillo de antebrazos con barras</td>
              <td>12-15</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Curl de de bíceps con mancuernas sentado</td>
              <td>12-15</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Curl de bíceps alterno tipo martillo</td>
              <td>12-15</td>
              <td><a href="#">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table viernes normal-->
      </div>
      <!--end viernes normal-->
    </div>
    <!--end tabs peso normal-->
  </div>
  <!--END DIV PADRE PESO NORMAL-->


  <h3>Obeso 25.0 / 40.00+</h3>
  <!--STARTER DIV PADRE OBESIDAD-->
  <div>
  <div id="peso-obeso">
      <ul>
        <li><a href="#lunes-obeso">Lunes</a></li>
        <li><a href="#martes-obeso">Martes</a></li>
        <li><a href="#miercoles-obeso">Miercoles</a></li>
        <li><a href="#jueves-obeso">Jueves</a></li>
        <li><a href="#viernes-obeso">Viernes</a></li>
      </ul>
      <!--starter rutina del lunes obeso-->
      <div id="lunes-obeso">
        <!--start table lunes obeso-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Zancada alterna hacia adelante con mancuernas</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=cZOvNi-h14I">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Zancada con saltos</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=2W9lsZe4usY">Ver Ejercicios</a></td>
            </tr>
            <tr>
              <td>Sentadillas con barras</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=feJlfvoFKU8">Ver Ejercicios</a></td>
            </tr>
            <tr>
              <td>Sentadilla frontal con barra</td>
              <td>10-12</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Sentadilla con mancuernas</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=4Ex0B2UMl3E">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Abductores en polea baja</td>
              <td>10-12</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Levantamiento de las piernas del suelo para adutores</td>
              <td>10-15</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table obeso -->
      </div>
      <!--end div lunes-obeso-->
      <!--start martes obeso-->
      <div id="martes-obeso">
        <!--start table rutina martes obeso-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Zancadas con saltos</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=2W9lsZe4usY">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Burpe</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=bthCFHFdbEE">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Bicicleta estatíca</td>
              <td>20 min</td>
              <td><a href="https://www.youtube.com/watch?v=pc9kMAh6cx8">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Golpes</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=LnHvIFV-9as">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Jumping jack</td>
              <td>20 rep</td>
              <td><a href="https://www.youtube.com/watch?v=U2MWgsiA1MU">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Abdominales alternos</td>
              <td>10-12</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Saltar cuerda</td>
              <td>50 rep</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table martes obeso-->
      </div>
      <!--end martes obeso-->
      <!--starter div miercoles obeso-->
      <div id="miercoles-obeso">
        <!--starter table miercoles obeso-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Trotar</td>
              <td>7 min</td>
              <td><a href="https://www.youtube.com/watch?v=WMGBV3QYJC8">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Prensa de piernas inclinada </td>
              <td>10 rep</td>
              <td><a href="https://www.youtube.com/watch?v=wETq1_h1m-o">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Flexiones de pecho</td>
              <td>5-8</td>
              <td><a href="https://www.youtube.com/watch?v=FuDyMYWTgG0">Ver video</a></td>
            </tr>
            <tr>
              <td>Abdutores en polea baja</td>
              <td>8-10</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Abdominales alternos</td>
              <td>10-12</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Abdominales oblicuos en polea</td>
              <td>8-10</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Superman</td>
              <td>12-15</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table miercoles obeso-->
      </div>
      <!--end miercoles obeso-->
      <!--starter jueves obeso-->
      <div id="jueves-obeso">
        <!--starter table jueves obeso-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Saltar cuerda</td>
              <td>40 rep</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Elíptica</td>
              <td>15 min</td>
              <td><a href="https://www.youtube.com/watch?v=Yyx2EnOhRnA">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Caminadora</td>
              <td>15 min</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Groiners</td>
              <td>12 rep</td>
              <td><a href="https://www.youtube.com/watch?v=AMnnWjBJ6JE">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Trotar</td>
              <td>5 min</td>
              <td><a href="https://www.youtube.com/watch?v=WMGBV3QYJC8">Ver Ejercicio</a></td>
            </tr>
            <tr>
              <td>Tijeras con piernas altas</td>
              <td>10 rep</td>
              <td><a href="https://www.youtube.com/watch?v=QNfc-FHe0_Y">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Bicicleta</td>
              <td>15 min</td>
              <td><a href="https://www.youtube.com/watch?v=pc9kMAh6cx8">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table jueves obeso-->
      </div>
      <!--end jueves obeso-->
      <!--starter div viernes obeso-->
      <div id="viernes-obeso">
        <!--starter table viernes obeso-->
        <table class="table table-bordered table-hover">
          <tr>
             <td   class="font-weight-bolder">Ejercicio</td>
            <td class="font-weight-bolder">Repeticiones</td>
             <td class="font-weight-bolder">Ver ejercicio</td>
          </tr>
          <tbody>
            <tr>
              <td>Peso muerto estilo sumo con mancuerna</td>
              <td>10-12</td>
              <td><a href="https://www.youtube.com/watch?v=9oQnzdyU8EY">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Abdominales</td>
              <td>10-12</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Flexion lateral del tronco con manuernas</td>
              <td>10-12</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Rotacón del tronco con bastón</td>
              <td>10-12</td>
              <td><a href="#">En mantenimiento</a></td>
            </tr>
            <tr>
              <td>Plancha abdominal</td>
              <td>12 seg</td>
              <td><a href="#">Mantenimiento</a></td>
            </tr>
            <tr>
              <td>Sentadilla en polea baja</td>
              <td>8-10</td>
              <td><a href="https://www.youtube.com/watch?v=R0ak87tXX6E">Ver ejercicio</a></td>
            </tr>
            <tr>
              <td>Peso muerto</td>
              <td>20</td>
              <td><a href="https://www.youtube.com/watch?v=5hW-frvr0eU">Ver ejercicio</a></td>
            </tr>
          </tbody>
        </table>
        <!--end table viernes obeso-->
      </div>
      <!--end viernes obeso-->
    </div>
  </div>
  <!--END DIV PADRE OBESIDAD-->
</div>