<div class="row">
    <div class="form-group col-12">
        <label for="lblHora">Seleccione la hora</label>
        <select for="selHora" name="selHora" class="form-control">
            <option value="seleccione">Seleccione</option>
            <option value="8_10am">8-10am</option> 
            <option value="4_8pm">4-8pm</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group form-check col-lg-6 col-6 col-mg-6">
        <input type="checkbox" class="form-check-input" id="desactivaEjercicios">
        <label class="form-check-label" for="exampleCheck1">No quiero ver los ejercicios</label>
    </div>
    <div class="form-group form-check col-lg-6 col-6 col-mg-6">
        <input type="checkbox" class="form-check-input" id="desactivaPantalla">
        <label class="form-check-label" for="exampleCheck1">Deseo ver las recomendaciones pantalla completa</label>
    </div>
</div>