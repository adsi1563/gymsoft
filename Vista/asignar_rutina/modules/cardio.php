<div class="container"><center><h1>Cardio</h1></center></div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/ZancadaConBarra.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">De pie, dé un paso largo adelante y apoye el pie en el suelo.Levante el talón del pie opuesto,dejando solamente la punte del pie aapoyando en el suelo.Sostenga las pestillos con las palmas de las manos.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/SaltarLaCuerda.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Varios intervalos son la mejor forma de realizar este ejercicio, muy funcional para bajar grasa,se recomienda utilizar una cuerda acorde a la altura.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/SentadillaConSalto.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Flexione las piernas hasta las rodillas y de un salto elevando todo su cuerpo con los brazos estirados.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/JumpingJack.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Abra las piernas con un leve salto y al mismo tiempo eleve las manos por encima de la cabeza, cirre las piernas con un leve salto y baje los brazos.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Sentadilla.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Flexione las piernas manteniendo el tronco firme,vuelva a la posición inicil y repita el movimiento.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/PesoMuerto.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Flexione las piernas de modo que la barra se aproxime al suelo, vuelva a la posición inicial y repita el movimiento.</p>
            </div>
          </div>        
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/PlanchaLateral.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Acostarse de lado,apoyar el codo en el suelo y mantener las piernas unidas.Apoye la otra mano en la cintura o en la cabeza y levante la cadera .</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Plancha.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Acuéstese en el suelo y apoye el peso del cuerpo con antebrazos y las puntas de los pies.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/FlexionLateral.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Tome una postura derecha, incline el tronco hacia un lado lo máximo que pueda, vuelva a la posición inicial e incline el tronco hacia al lado opuesto.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Bicicleta.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Montese en la bicicleta, apretela lo que más pueda, entre más este pesado hará más efecto en las piernas y gluteos, recomendado para los que estan en sobrepeso.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Encogimiento.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Se acostará de lado en el suelo, deje las piernas levemente flexionadas y coloque el brazo apoyando la nuca, levante el tronco lateralmente, levantando los hombros del suelo.</p>
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Abdominales.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Acuéstese con la espalda apoyada en el suelo,flexione las rodillas y eleve las piernas.Mantenga las manos detras del cuello.</p>
            </div>
          </div>        
        </div>
    </div>
</div>


