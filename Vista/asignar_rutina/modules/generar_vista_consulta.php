<table id="tabla-user-rutina" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Imc</th>
                <th>Tipo de rutina</th>
            </tr>
        </thead>
        <tbody>
        <?php
            require_once '../../../Controlador/asignar_rutina/asignar.c.php';
            require_once '../../../Modelo/asignar_rutina/datos.m.php';

            $data = new AsignarRutina();
            $data-> consultarDatosUsuarioController();
        ?>
            <tr>
                <td>Diego</td>
                <td>22.8</td>
                <td>Ganancia muscular</td>
            </tr>
            <tr>
                <td>Alejandra</td>
                <td>18.32</td>
                <td>Cardio</td>
     
            </tr>
            <tr>
                <td>Dayana</td>
                <td>17.5</td>
                <td>Tonificar</td>
            </tr>
            <tr>
                <td>Mayerli</td>
                <td>19.5</td>
                <td>Tonificar</td>
            </tr>
            <tr>
                <td>Wendy</td>
                <td>17.1</td>
                <td>Ganancia muscular</td>
            </tr>
        
        </tbody>
        <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Tipo de rutina</th>
                <th>IMC</th>
            </tr>
        </tfoot>
    </table>