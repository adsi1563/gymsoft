<div class="container"><center><h1>Ganancia muscular</h1></center></div>
<div id="ganancia-muscular"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Apertur8.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Coloque su codo en su pierna, baje lo más posible que pueda y suba a la misma posición.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Apertura.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Siéntese en un banco inclinado y sostenga las mancuernas encima del tórax con los brazos ligeramente flexionados y las palmas de las manos giradas entre sí.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Apertura2.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Acuéstese en un banco recto y sostenga las pesas por encima del tórax, con los brazos levemente estirados y las palmas de las manos boca abajo, en un agarre neutro.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Apertura3.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Levante su cuerpo hasta que los codos estén casi estirados.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Apertura4.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Levante e impulse su cuerpo hasta que sus manos queden en el aire y descienda entre los brazos mayor en un agarre abierto, impulse de nuevo y vuelva a la posición natural, descienda el cuerpo en un agarre cerrao y repite el movimiento.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Apertura5.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Coloque los pies en un banco o superficie alta, descienda su cuerpo hasta casi tocar el piso, vuelva a la posición anterior, repitalo.</p>
            </div>
          </div>        
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Apertura6.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Coloque sus manos haciendo similitud un diamante, baje hasta casi tocar el pecho al suelo, suba, vuelva hacer este movimiento.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Apertura7.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Levante las mancuernas hasta que los brazos esten casi estirados,baje las mancuernas lentamente hasta la altura del pectoral y repita el movimiento.</p>
      
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Apertura9.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Sientese en el asiento y sostenga una mancuerna en cada mano con las palmas de las manos boca abajo, en un agarre suspendido,levante la mancuerda hasta la altura del pecho.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
<div id="cardio"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-4 col-lg-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="../../img/asignar_rutina/Apertura10.jpeg" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Contraiga el abdomen, estire los brazos hacia abajo y mueva una de las pesas en la dirección de sus hombros baje y repita el mismo movimiento con el otro brazo.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-4 col-lg-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="../../img/asignar_rutina/Apertura11.jpeg" alt="Card image cap">
              <div class="card-body">
              <p class="card-text">Sientese en el asiento y sostenga una mancuerna en cada mano con las palmas de las manos boca abajo, levante la mancuerna hasta la altura del pectoral, vuelva la mancuerna a la posicion inicial y repita el movimiento con el otro brazo.</p>
        </div>
  </div>        
</div>
<div class="col-md-4 col-4 col-lg-4">  
            <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="../../img/asignar_rutina/Apertura12.jpeg" alt="Card image cap">
                  <div class="card-body">
                  <p class="card-text">Manténgase de pie frente a la polea baja. Sostenga la barra con los brazos ligeramente extendidos y las palmas de las manos hacia arriba, en un agarre supinado, levante la barra lentamente hasta la altura de los hombros y vuelva a la posición inicial y repita el movimiento.</p>
            </div>
          </div>        
        </div>
    </div>
</div>
