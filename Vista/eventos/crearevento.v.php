<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Crear Evento Deportivo</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!-- <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css"> -->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="crearEvento" style="margin-top: 100px;">
			<div class="needs-validation container bg-light text-dark" novalidate>
				<center>
					<div class="form-row text-center">
						<div class="col-12">
							<h1 class="titulo">CREAR EVENTO DEPORTIVO</h1>
						</div>
					</div>
				</center>	

				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdCrearEvento" id="hidIdCrearEvento" value=""/>
						</td>
					</div>
					<div class="form-group col-3">
						<label for = "ddlTipoEvento" class="texto">Tipo de Evento </label>
						<select class="form-control" id="ddlTipoEvento">
						</select>
					</div>
					<div class="form-group col-3">
							<label for="txtNombreEvento" class="texto">Nombre Evento</label>
							<input class="form-control" type="text" name="txtNombreEvento" size="15" id="txtNombreEvento" value="" oninput="maxLengthCheck(this)" maxlength="20" onkeypress="return soloLetras(event)"><br>
					</div>
					<div class="form-group col-3">
						<label for="fechaInicio" class="texto">Fecha Inicio</label>
						<input type="date" name="fechaInicio" id="fechaInicio" value="" class="form-control"  onchange="finicio();">
					</div>
					<div class="form-group col-3">
						<label for="fechaFin" class="texto">Fecha Fin</label>
						<input type="date" name="fechaFin" id="fechaFin" value="" class="form-control" onchange="fFin();">
					</div>
					<div class="form-group col-6">
						<label class="texto">Sede</label>
						<select class="form-control" id="idsede">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="departamento" class="texto">Estado</label>
						<select class="form-control" name="estado" id="estado">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
						</select>
					</div>
				</div>
				<div class="form-row text-center">
					<div class="form-group col-12">
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
				</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">TIPO DE EVENTO</th>
							      <th class="bg-success" scope="col">NOMBRE DE EVENTO</th>
							      <th class="bg-success" scope="col">FECHA INICIO</th>
							      <th class="bg-success" scope="col">FECHA FIN</th>
							      <th class="bg-success" scope="col">SEDE</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyCrearEvento">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/eventos/crearevento.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>