<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Gimnasio</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">

	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">

	<?php
		require_once '../../entorno/conexion.php';
		$conn = new Conexion();

		$sentenciaSql = "SELECT * FROM sede WHERE estado='A'";
        $conn->preparar($sentenciaSql);
		$conn->ejecutar();
		$sedes = $conn->obtenerRegistros();

		include "../menu.v.php";

	?>

	<center>
		<form id="gimnasio" style="margin-top: 100px;">
			<div class="needs-validation container bg-light text-dark" novalidate>
				<center>
	
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">GIMNASIO</h1>
						</div>
					</div><br><br>
				</center>
					<div class="form-row">
						<div>
							<td rowspan="2" id="td_lateral_izquierdo">
								<input type="hidden" name="hidIdGimnasio" id ="hidIdGimnasio" value=""/>
							</td>
						</div>
						<div class="form-group col-12">
							<label for="txtNombre" class="texto">Nombre</label>
							<input class="form-control" type="text" name="txtNombre" size="15" id="txtNombre" value="" oninput="maxLengthCheck(this)" maxlength="20" onkeypress="return soloLetras(event)" id="miInput"><br>
						</div>
					</div>


					<div class="form-row">
						<div class="form-group col-6">
							<label for="ddlSede" class="texto">Sede</label>
							<select class="form-control" id="ddlSede">
								<option value="-1">--Seleccione una opción--</option>
								<?php
									foreach($sedes as $sede){
										echo "<option value='" . $sede[0] . "'>" . $sede[1] . "</option>";
									}
								?>
							</select>
						</div>
						<div class="form-group col-6">
							<label for="departamento" class="texto">Estado</label>
							<select class="form-control" name="dep" id="dep">
								<option value="A">Activo</option>
								<option value="I">Inactivo</option>
							</select>
						</div>
					</div>

				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				</center>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">NOMBRE</th>
							      <th class="bg-success" scope="col">SEDE</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyDepartamento">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/gimnasio.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>