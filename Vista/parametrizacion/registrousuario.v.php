<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Registro usuario evento</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">

	<?php
		require_once '../../entorno/conexion.php';
		$conn = new Conexion();

		$sentenciaSql = "SELECT * FROM tipo_identificacion WHERE estado='A'";
        $conn->preparar($sentenciaSql);
		$conn->ejecutar();
		$tipos = $conn->obtenerRegistros();

		include "../menu.v.php";

	?>


	<center>
		<form id="inscripcion_aprendiz" style="margin-top: 100px;">
			<div class="needs-validation container bg-light text-dark" novalidate>
				<center>
	
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">REGISTRO USUARIO EVENTO</h1>
						</div>
					</div><br><br>
				</center>
					<div class="form-row">
						<div>
							<td rowspan="2" id="td_lateral_izquierdo">
								<input type="hidden" name="hidIdInscripcionAprendiz" id ="hidIdInscripcionAprendiz" value=""/>
							</td>
						</div>

					<div class="form-group col-1">
					<?php
						require_once '../../entorno/conexion.php';
						$conn = new Conexion();

						$sentenciaSql = "SELECT * FROM tipo_identificacion WHERE estado='A'";
						$conn->preparar($sentenciaSql);
						$conn->ejecutar();
						$tipo = $conn->obtenerRegistros();
					?>

							<label for="txtTipo" class="texto">Tipo</label>
							<select class="form-control" id="txtTipo">
								<option value="-1"></option>
								<?php
									foreach($tipos as $tipo){
										echo "<option value='" . $tipo[0] . "'>" . $tipo[1] . "</option>";
									}
								?>
							</select>
						</div>



						<div class="form-group col-7">
								<label for="txtNumIdent" class="texto">Numero de identificación</label>
								<input class="form-control" type="text" name="txtNumIdent" size="10" id="txtNumIdent" value="" oninput="maxLengthCheck(this)" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onblur="completarCampos()" autofocus><br>
				
						</div>

						<div class="form-group col-2">
							<label for="txtficha" class="texto">Ficha</label>
							<input class="form-control" type="text" name="txtficha" size="15" id="txtficha" value="" oninput="maxLengthCheck(this)" maxlength="20" onkeypress='return event.charCode >= 48 && event.charCode <= 57' id="txtficha"><br>			
						</div>

						<div class="form-group col-2">
							<label for="departamento" class="texto">Estado</label>
							<select class="form-control" name="dep" id="dep">
								<option value="A">Activo</option>
								<option value="I">Inactivo</option>
							</select>
						</div>

						<div class="form-group col-6">
								<label for="txtNombre" class="texto">Nombre</label>
								<input class="form-control" type="text" name="txtNombre" size="30" id="txtNombre" value="" oninput="maxLengthCheck(this)" maxlength="20" onkeypress="return soloLetras(event)" id="miInput"><br>
						</div>

						<div class="form-group col-6">
								<label for="txtApellido" class="texto">Apellido</label>
								<input class="form-control" type="text" name="txtApellido" size="30" id="txtApellido" value="" oninput="maxLengthCheck(this)" maxlength="20" onkeypress="return soloLetras(event)" id="miInput"><br>
						</div>
					
					</div>


					<div class="form-group col-12">
					<?php
						require_once '../../entorno/conexion.php';
						$conn = new Conexion();

						$sentenciaSql = "SELECT * FROM tipo WHERE estado='A'";
						$conn->preparar($sentenciaSql);
						$conn->ejecutar();
						$eventos = $conn->obtenerRegistros();
					?>

							<label for="ddlEvento" class="texto">Evento</label>
							<select class="form-control" id="ddlEvento">
								<option value="-1">--Seleccione una opción--</option>
								<?php
									foreach($eventos as $evento){
										echo "<option value='" . $evento[0] . "'>" . $evento[1] . "</option>";
									}
								?>
							</select>
						</div>

				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				</center>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
								  <th class="bg-success" scope="col">FICHA</th>
							      <th class="bg-success" scope="col">NOMBRE</th>
								  <th class="bg-success" scope="col">APELLIDO</th>
								  <th class="bg-success" scope="col">IDENTIFICACIÓN</th>
							      <th class="bg-success" scope="col">EVENTO</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>

							    </tr>
							  </thead>
							  <tbody id="tableBodyDepartamento">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/registrousuario.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>