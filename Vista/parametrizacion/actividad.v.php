<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Actividad</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!-- <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css"> -->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="actividad" style="margin-top: 100px;">
			<div class="container bg-light">
				<div class="form-row text-center">
					<div class="col-12">
						<h1 class="titulo">ACTIVIDAD</h1>
					</div>
				</div>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdActividad" id ="hidIdActividad" value=""/>
						</td>
					</div>
					<div class="form-group col-6">
						<label for="idGimnasio" class="texto">Gimnasio</label>
						<select class="form-control" id="idGimnasio">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="idTipo" class="texto">Tipo de Actividad</label>
						<select class="form-control" id="idTipo">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="fechaInicio" class="texto">Fecha de Inicio</label>
						<input type="date" name="fechaInicio" id="fechaInicio" value="" class="form-control" onchange="fInicio();">
					</div>
					<div class="form-group col-6">
						<label for="fechaFin" class="texto">Fecha de Finalización</label>
						<input type="date" name="fechaFin" id="fechaFin" value="" class="form-control" onchange="fFin();">
					</div>
					<div class="form-group col-6">
						<label for="estado" class="texto">Estado</label>
						<select class="form-control" id="estado">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
						</select>
					</div>
				</div>
				<div class="form-row text-center">
					<div class="form-group col-12">
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">GIMNASIO</th>
							      <th class="bg-success" scope="col">TIPO DE ACTIVIDAD</th>
							      <th class="bg-success" scope="col">FECHA INICIO</th>
							      <th class="bg-success" scope="col">FECHA FIN</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyActividad">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/actividad.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>