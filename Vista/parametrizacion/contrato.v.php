<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Contrato</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!--<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
<!-- contrato.js -->
	<script type="text/javascript" src="../../js/parametrizacion/contrato.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>


</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="departamento" style="margin-top: 100px;">
			<div class="container bg-light">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">CONTRATO</h1>
						</div>
					</div><br><br>
				</center>
				<div id="mensaje">
					
				</div>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidAmbiente" id ="hidAmbiente" value=""/>
						</td>
					</div>
					<div class="form-group col-6">
						<label for="codigo" class="texto">Tipo Contrato</label>
						<input class="form-control" type="text" name="txtContrato" size="15" id="txtContrato" value="" oninput="maxLengthCheck(this)" maxlength="50"> <br>
					</div>
					<div class="form-group col-6">
						<label for="finicio" class="texto">Fecha Inicio</label>
						<input class="form-control" type="date" name="finicio" size="30" id="finicio" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput" onchange="fechaInicio();">
					</div>

					<div class="form-group col-6">
						<label for="ffin" class="texto">Fecha Fin</label>
						<input class="form-control" type="date" name="ffin" size="30" id="ffin" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput" onchange="fechaFin();">
					</div>
					<div class="form-group col-6">
						<label for="estado" class="texto">Estado</label>
						<select class="form-control"  name="estado" id="estado">
							<option value="A">Activo</option>	
							<option value="I">Inactivo</option>
						</select>
					</div>
				</div>
				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				</center>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">TIPO CONTRATO</th>
							      <th class="bg-success" scope="col">FECHA INGRESO</th>
							      <th class="bg-success" scope="col">FECHA FIN</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyContrato">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	
</body>
</html>