<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ficha</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!--<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form style="margin-top: 100px;">
			<div class="container bg-light">
				<div class="form-row text-center">
					<div class="col-12">
						<h1 class="titulo">FICHA</h1>
					</div>
				</div>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdFicha" id ="hidIdFicha" value=""/>
						</td>
					</div>
					<div class="form-group col-6">
						<label for="ficha" class="texto">Ficha</label>
						<input class="form-control" type="number" name="ficha" id="ficha" maxlength="30">
					</div>
					<div class="form-group col-6">
						<label for="fechaInicio" class="texto">Fecha Inicio</label>
						<input class="form-control" type="date" name="fechaInicio" id="fechaInicio" onchange="fechaInicioLectiva();">
					</div>
					<div class="form-group col-6">
						<label for="fechaFinLectiva" class="texto">Fecha Fin Lectiva</label>
						<input class="form-control" type="date" name="fechaFinLectiva" id="fechaFinLectiva" value="" onchange="fechaLectiva();">
					</div>
					<div class="form-group col-6">
						<label for="fechaFinProductiva" class="texto">Fecha Fin Productiva</label>
						<input class="form-control" type="date" name="fechaFinProductiva" id="fechaFinProductiva" value="" onchange="fechaProductiva();">
					</div>
					<div class="form-group col-6">
						<label for="Jornada" class="texto">Jornada</label>
						<select class="form-control" id="Jornada">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="Ambiente" class="texto">Ambiente</label>
						<select class="form-control" id="Ambiente">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="ProgramaFormacion" class="texto">Programa de Formación</label>
						<select class="form-control" id="ProgramaFormacion">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="estado" class="texto">Estado</label>
						<select class="form-control" id="estado">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
						</select>
					</div>
				</div>

				<div class="form-row text-center">
					<div class="form-group col-12">
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
					</div>
				</div>
	
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">FICHA</th>
							      <th class="bg-success" scope="col">FECHA INICIO</th>
							      <th class="bg-success" scope="col">FIN LECTIVA</th>
							      <th class="bg-success" scope="col">FIN PRODUCTIVA</th>
							      <th class="bg-success" scope="col">JORNADA</th>
							      <th class="bg-success" scope="col">AMBIENTE</th>
							      <th class="bg-success" scope="col">PROGRAMA FORMACION</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyFicha">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/ficha.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>