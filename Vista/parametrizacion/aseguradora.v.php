<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Aseguradora</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="aseguradora" style="margin-top: 100px;">
			<div class="container bg-light">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">ASEGURADORA</h1>
						</div>
					</div><br><br>
				</center>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdAseguradora" id ="hidIdAseguradora" value=""/>
						</td>
					</div>
					<div class="form-group col-6">
						<label for="nit" class="texto">Nit</label>
						<input class="form-control" type="text" name="txtNit" size="40" id="txtNit" value="" oninput="maxLengthCheck(this)" maxlength="15" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					</div>
					<div class="form-group col-6">
						<label for="razon" class="texto">Razón Social</label>
							<input class="form-control" type="text" name="txtRazon" size="50" id="txtRazon" value="" maxlength="50" onkeypress="return soloLetras(event)" id="miInput">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-6">
						<label for="direccion" class="texto">Dirección</label>
							<input class="form-control" type="text" name="txtDireccion" size="40" id="txtDireccion" value="" maxlength="40">
					</div>
					<div class="form-group col-6">
						<label for="representante" class="texto">Representante Legal</label>
							<input class="form-control" type="text" name="txtRepresentante" size="30" id="txtRepresentante" value="" maxlength="50" onkeypress="return soloLetras(event)">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-12">
						<label for="aseguradora" class="texto">Estado</label>
							<select class="form-control select" name="est" id="est">
								<option>A</option>
								<option>I</option>
							</select>
					</div>
				</div>
				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				</center> 
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">NIT</th>
							      <th class="bg-success" scope="col">RAZÓN SOCIAL</th>
							      <th class="bg-success" scope="col">DIRECCIÓN</th>
							      <th class="bg-success" scope="col">REPRESENTANTE LEGAL</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyDepartamento">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/aseguradora.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>