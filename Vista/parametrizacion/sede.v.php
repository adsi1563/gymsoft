<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sede</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!--<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="sede" style="margin-top: 100px;">
			<div class="container bg-light">
				<div class="form-row text-center">
					<div class="col-12">
						<h1 class="titulo">SEDE</h1>
					</div>
				</div><br><br>

				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdSede" id ="hidIdSede" value=""/>
						</td>
					</div>
					<div class="form-group col-6">
						<label for="nombre" class="texto">Nombre</label>
						<input class="form-control" type="text" name="nombre" size="30" id="nombre" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-6">
						<label for="telefono" class="texto">Teléfono</label>
						<input class="form-control" type="number" name="telefono" size="15" id="telefono" value="">
					</div>
					<div class="form-group col-6">
						<label for="direccion" class="texto">Dirección</label>
						<input class="form-control" type="text" name="direccion" size="30" id="direccion">
					</div>
					<div class="form-group col-6">
						<label for="idCentroFormacion" class="texto">Centro de Formación</label>
						<select class="form-control" id="idCentroFormacion">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="estado" class="texto">Estado</label>
						<select class="form-control" id="estado">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
						</select>
					</div>
				</div>

				<div class="form-row text-center">
					<div class="form-group col-12">
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
						<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
					</div>
				</div>
	
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">SEDE</th>
							      <th class="bg-success" scope="col">NOMBRE</th>
							      <th class="bg-success" scope="col">TELÉFONO</th>
							      <th class="bg-success" scope="col">DIRECCIÓN</th>
							      <th class="bg-success" scope="col">CENTRO DE FORMACIÓN</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodySede">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/sede.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>