<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Aprendiz Ficha</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">

	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>

	<center>
		<form id="aprendiz_ficha" style="margin-top: 100px;">
			<div class="needs-validation container bg-light text-dark" novalidate>
				<center>
	
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">APRENDIZ - FICHA</h1>
						</div>
					</div><br><br>
				</center>
					<div class="form-row" style="margin-left: 25%">
						<div>
							<td rowspan="2" id="td_lateral_izquierdo">
								<input type="hidden" name="hidIAprendizFicha" id ="hidIAprendizFicha" value=""/>
							</td>
						</div>
						<div class="form-group col-4">
							<label for="ddlFicha" class="texto">Ficha</label>
							<select class="form-control" id="ddlFicha">
							</select>
						</div>
						<div class="form-group col-4">
							<label for="Aprendiz" class="texto">Aprendiz</label>
							<input class="form-control" type="text" name="txtAprendiz" id ="txtAprendiz" value="" disabled />
							<input type="hidden" name="hidIAaprendiz" id ="hidIdAprendiz" value=""/>
						</div>
					</div>
				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Siguiente" onclick=""/>
						</div>
					</div>
				</center>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">FICHA</th>
							      <th class="bg-success" scope="col">APRENDIZ</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyDepartamento">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/parametrizacion/aprendiz_ficha.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>