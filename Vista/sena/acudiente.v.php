<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Acudiente</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!--<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="acudiente" style="margin-top: 100px;">
			<div class="container bg-light">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">ACUDIENTE</h1>
						</div>
					</div><br><br>
				</center>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdAcudiente" id ="hidIdAcudiente" value=""/>
						</td>
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Nombre</label>
						<input class="form-control" type="text" name="txtNombre" size="30" id="txtNombre" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-4">
						<label for="apellido" class="texto">Apellido</label>
						<input class="form-control" type="text" name="txtApellido" size="30" id="txtApellido" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-4">
						<label for="telefono" class="texto">Teléfono</label>
						<input class="form-control" type="text" name="txtTelefono" size="15" id="txtTelefono"  value="" oninput="maxLengthCheck(this)" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					</div>
				</div>
				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" name="btnAccion" id="btnAdicionar" value="Siguiente" onclick=""/>
						</div>
					</div>
				</center>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">NOMBRE</th>
							      <th class="bg-success" scope="col">APELLIDO</th>
							      <th class="bg-success" scope="col">TELÉFONO</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyDepartamento">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/sena/acudiente.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>