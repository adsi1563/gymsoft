<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="../../css/global.css">
  <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
  <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
  <link rel="stylesheet" type="text/css" href="../../css/datatables.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/all.css">
  <!-- JAVASCRIPT -->
  <script  type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
  <script  type="text/javascript" src="../../js/bootstrap.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui.js"></script>
  <script type="text/javascript" src="../../js/datatables.min.js"></script>
  <script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
  <script type="text/javascript" src="../../js/pdfmake.min.js"></script>
  <script type="text/javascript" src="../../js/vfs_fonts.js"></script>
  <script type="text/javascript" src="../../js/buttons.html5.js"></script>
  <script type="text/javascript" src="../../js/jszip.min.js"></script>
  <script type="text/javascript" src="../../js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="../../js/buttons.print.min.js"></script>|
  <title>Programa de formacion</title>
</head>
  <?php
    include "../menu.v.php";
  ?>
  <body onload="Enviar('CONSULTAR',null)">
    <form id="accion" name="accion" class="center-block" style="margin-top: 100px;"> 
      <!-- id oculto para obterner consulta -->
      <input type="hidden" name="txtHideid" id="txtHideid" value="">
      <div class="container needs-validation container bg-light text-dark" novalidate>
        <div class="form-row">
          <div class="col-12 text-center">
            <h1 class="titulo">PROGRAMA DE FORMACIÓN</h1><br>
          </div>
        </div>
        <div class="form-row span6">
          <div class="form-group col-6">
            <label for="nombreCodigo" class="texto">Código</label>
            <input class="form-control" type="text" name="txtCodigo" id="txtCodigo"  placeholder="Codigo" oninput="maxLengthCheck(this)" maxlength="11"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'> 
          </div>
          <div class="form-group col-6">
            <label for="nombreDescripcion" class="texto">Descripción</label>
            <input class="form-control" type="text" name="txtDescripcion" id="txtDescripcion" placeholder="Descripcion" maxlength="60" onkeypress="return soloLetras(event)" id="miInput">
          </div>
          </div>
          <div class="form-row">
          <div class="form-group col-6 center-block">
            <label for="cantidadDuracion" class="texto">Duración</label>
            <input class="form-control" type="number" name="txtDuracion" id="txtDuracion" placeholder="Duracion del programa en meses"  oninput="maxLengthCheck(this)" maxlength="50" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
          </div>
          <div class="form-group col-6">
            <label for="estado" class="texto">Nivel del Programa</label>
            <select class="form-control"  name="nivelPrograma" id="nivelPrograma">
            </select>
          </div>
          <div class="form-group col-6">
            <label for="estado" class="texto">Estado</label>
            <select class="form-control"  name="estado" id="estado">
              <option value="A">Activo</option> 
              <option value="I">Inactivo</option>
            </select>
          </div>
        </div>
        <center>
          <div class="form-row">
          <div class="form-group col-12">
           <!--  grupo de botones formularios -->
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
          </div>
        </div>
        </center>
        <div class="container">
            <div class="row">
              <div class="col-12">
                <table class="table" id="resultado">
                  <thead class="thead-dark">
                    <tr class="bg-success">
                      <th class="bg-success" scope="col">CÓDIGO</th>
                      <th class="bg-success" scope="col">DESCRIPCIÓN</th>
                      <th class="bg-success" scope="col">DURACIÓN</th>
                      <th class="bg-success" scope="col">ACCIONES</th>
                    </tr>
                  </thead>
                  <tbody id="tablBodyPrograma">
                    <!--tabla contruida en js  -->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
      <script type="text/javascript" src="../../js/sena/programaformacion.js"></script>
     <!--  <script  src="../../js/global.js"></script> -->
    </form>

</body>
</html>