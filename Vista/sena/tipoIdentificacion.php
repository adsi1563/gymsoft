<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
   <!-- CSS -->
 <link rel="stylesheet" type="text/css" href="../../css/global.css">
  <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">

  <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
  <link rel="stylesheet" type="text/css" href="../../css/datatables.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/all.css">
  <!-- JAVASCRIPT -->
  <script  type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
  <script  type="text/javascript" src="../../js/bootstrap.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui.js"></script>
  <script type="text/javascript" src="../../js/datatables.min.js"></script>
  <script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
  <script type="text/javascript" src="../../js/pdfmake.min.js"></script>
  <script type="text/javascript" src="../../js/vfs_fonts.js"></script>
  <script type="text/javascript" src="../../js/buttons.html5.js"></script>
  <script type="text/javascript" src="../../js/jszip.min.js"></script>
  <script type="text/javascript" src="../../js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="../../js/buttons.print.min.js"></script>|
  <!-- <script  type="text/javascript" src="../js/librerias/validaciones.campos.js">></script> -->
  <title>Tipo Identificacion</title>
</head>
<body onload="Enviar('CONSULTAR',null)">
  <?php
    include "../menu.v.php";
  ?>
  <form id="accion" name="accion" class="center-block" style="margin-top: 100px;">
    <!-- id oculto para obterner consulta -->
    <input type="hidden" name="txtHideid" id="txtHideid" value="">
    <div class="container needs-validation container bg-light text-dark" novalidate>
      <div class="form-row">
        <div class="col-12 text-center">
          <h1 class="font-weight-bold text-dark">TIPO IDENTIFICACIÓN</h1><br>
        </div>
      </div>
      <div class="form-row span6 ">
        <div class="form-group col-6 ">
          <label class="" for="nombreTipoIdentificacion">Tipo Identificación</label>
          <input class="form-control" type="text" name="txtNombreTipoIdentificacion"  placeholder="Ejemplo: CC,TI,CI,TP"  id="txtNombreTipoIdentificacion" maxlength="3" oninput="maxLengthCheck(this)" maxlength="5">
        </div>
        <div class="form-group col-6">
          <label class="font-italic text-dark " for="Descripcion">Descripción</label>
          <input class="form-control" type="text" name="txtDescripcion" id="txtDescripcion" placeholder="Cédula de Ciudadanía" onkeypress="return soloLetras(event)">  
        </div>
      </div>
      <center>
         <div class="form-row">
        <div class="form-group col-12">
         <!--  grupo de botones formularios -->
         <div class="form-row">
          <div class="form-group col-12">
           <!--  grupo de botones formularios -->
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
          </div>
        </div>
      </center>
     <div class="container">
          <div class="row">
            <div class="col-12">
              <table class="table" id="resultado">
                <thead class="thead-dark">
                  <tr class="bg-success">
                    <th class="bg-success"class="" scope="col">TIPO IDENTIFICACIÓN</th>
                    <th class="bg-success" scope="col">DESCRIPCIÓN</th>
                    <th class="bg-success" scope="col">ACCIONES</th>
                  </tr>
                </thead>
                <tbody id="tableIdentificacion">
                  <!--tabla contruida en js  -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </form>
  <script  src="../../js/sena/tipoIdentificacion.js"></script>
  <script  src="../../js/global.js"></script>
</body>
</html>