<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ambiente</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!--<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>

	<script type="text/javascript" src="../../js/sena/ambiente.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>


</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="departamento" style="margin-top: 100px;">
			<div class="container bg-light">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">AMBIENTE</h1>
						</div>
					</div><br><br>
				</center>
				<div id="mensaje">
					
				</div>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidAmbiente" id ="hidAmbiente" value=""/>
						</td>
					</div>
					<div class="form-group col-6">
						<label for="codigo" class="texto">Código</label>
						<input class="form-control" type="text" name="txtCodigo" size="15" id="txtCodigo" value="" oninput="maxLengthCheck(this)" maxlength="3" onkeypress='return event.charCode >= 48 && event.charCode <= 57'><br>
					</div>
					<div class="form-group col-6">
						<label for="nombre" class="texto">Descripción</label>
						<input class="form-control" type="text" name="txtDescripcion" size="30" id="txtDescripcion" value="" maxlength="30" id="miInput">
					</div>

					<div class="form-group col-6">
	                    <label for="ddlEstado" class="texto">Estado</label>
	                    <select class="form-control" name="selEstado" id="selEstado">
	                        <option >--Seleccione--</option>
	                        <option value="A">Activo</option>
	                        <option value="I">Inactivo</option>
	                    </select>
                	</div>
				</div>
				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				</center>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">CÓDIGO</th>
							      <th class="bg-success" scope="col">DESCRIPCIÓN</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyAmbiente">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	
</body>
</html>