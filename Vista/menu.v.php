
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="#" style="margin-left: 10%; margin-right: 40px"><img src="../../img/logo5.png" width="90px"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuPrincipal" aria-controls="menuPrincipal" aria-expanded="false" aria-label="Toggle navigation" style="margin-right: 20%;">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="menuPrincipal">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="../seguridad/principal.v.php">Inicio<span class="sr-only">(current)</span></a>
        </li>
          <?php if ($_SESSION['rol'] ==1 || $_SESSION['rol'] ==2) {
            echo '<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Parametrización
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../parametrizacion/ciudad.v.php">Ciudad</a>
              <a class="dropdown-item" href="../parametrizacion/gimnasio.v.php">Gimnasio</a>
              <a class="dropdown-item" href="../parametrizacion/aseguradora.v.php">Aseguradora</a>
              <a class="dropdown-item" href="../parametrizacion/eps.v.php">Eps</a>
              <a class="dropdown-item" href="../sena/tipoIdentificacion.php">Tipo de Identificación</a>
              <a class="dropdown-item" href="../parametrizacion/sede.v.php">Sede</a>
              <a class="dropdown-item" href="../parametrizacion/regional.v.php">Regional</a>
              <a class="dropdown-item" href="../sena/nivel_programa.v.php">Nivel Programa</a>
              <a class="dropdown-item" href="../centroformacion/centroFormacion.v.php">Centro de formación</a>
              <a class="dropdown-item" href="../parametrizacion/ficha.v.php">Ficha</a>
               <a class="dropdown-item" href="../sena/programa_formacion.php">Programa Formacion</a>
            </div>
          </li>';
          }
          ?>
          <?php if ($_SESSION['rol'] == 1 || $_SESSION['rol'] ==2) {
            echo '<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Inventario
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../inventario/equipo.php">Registro Inventario</a>
              <a class="dropdown-item" href="../inventario/marca.v.php">Marca</a>
            </div>
          </li>';
          }
          ?>

          <?php if ($_SESSION['rol'] ==1) {
            echo '<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Eventos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../eventos/tipoevento.v.php">Clasificación Eventos Deportivos</a>
             <a class="dropdown-item" href="../eventos/crearevento.v.php">Creación Eventos Deportivos</a>
             <a class="dropdown-item" href="../parametrizacion/registrousuario.v.php">Registro usuario evento</a>
             <a class="dropdown-item" href="../eventos/puntaje.v.php">Puntuacion Aprendiz</a>
            </div>
          </li>';
          }
          ?>   

          <?php if ($_SESSION['rol'] ==1 || $_SESSION['rol'] ==2) {
            echo '<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Registros
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../persona/persona.v.php">Persona</a>
              <a class="dropdown-item" href="../registros/RutinaAlimenticia.v.php">Rutina Alimenticia</a>
              <a class="dropdown-item" href="../asignar_rutina/rutina.v.php">Rutinas</a>
            </div>
          </li>';
          }
          ?>
          <?php if ($_SESSION['rol'] ==1) {
            echo '<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Seguridad
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../seguridad/usuario.seguridad.v.php">Usuario</a>
            </div>
          </li>';
          }
          ?>
		  
		  <?php if ($_SESSION['rol'] ==1) {
            echo '<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Reportes
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="../reportes/reportes.v.php">Imc</a>
              <a class="dropdown-item" href="../reportes/reportesEventosD.v.php">Evento Deportivo</a>
              <a class="dropdown-item" href="../reportes/reportesPuntaje.v.php">Puntaje</a>
              <a class="dropdown-item" href="../reportes/reporteParticipacionConteo.v.php">Participación</a>
              <a class="dropdown-item" href="../reportes/reporteAsisGym.v.php">Asistencia Gimnasio</a>
              <a class="dropdown-item" href="../reportes/reporteRutina.v.php">Rutina</a>
            </div>
          </li>';
          }
          ?>
          <li class="nav-item active">
            <a class="btn btn-light action-button" role="button" href="../../index.php">Cerrar Sesion</a>
          </li>
          <li>
            <a class="nav-link">Bienvenido <?php echo $_SESSION['persona'] ?></a>
          </li>
      </ul>
    </div>
  </nav> 