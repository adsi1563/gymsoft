<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Eventos Deportivos</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null);">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="tipo actividad" style="margin-top: 100px;">
			<div class="needs-validation container bg-light text-dark">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">CREACIÓN EVENTOS DEPORTIVOS</h1>
						</div>
					</div><br><br>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdTipoActividad" id ="hidIdTipoActividad" value=""/>
						</td>
					</div>
					<div class="form-group col-9">
						<label for="nombre" class="texto">Nombre</label>
						<input class="form-control" type="text" name="txtNombre" size="30" id="txtNombre" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-3">
						<label for="nombre" class="texto">Tiempo</label>
						<select class="form-control" name="txtTiempo" id="txtTiempo">
							<option value="30 Minutos">30 Minutos</option>	
							<option value="60 Minutos">60 Minutos</option>
							<option value="90 Minutos">90 Minutos</option>
							<option value="120 Minutos">120 Minutos</option>
						</select>
					</div>
					<div class="form-group col-9">
						<label for="nombre" class="texto">Descripción</label>
						<textarea class="form-control" type="text" name="txtDescripcion" id="txtDescripcion" value="" maxlength="300" onkeypress="return soloLetras(event)" id="miInput"></textarea>	
					</div>
					<div class="form-group col-3">
						<label for="estado" class="texto">Estado</label>
						<select class="form-control"  name="estado" id="estado">
							<option value="A">Activo</option>	
							<option value="I">Inactivo</option>
						</select>
					</div>
					<div class="form-group col-6" style="margin-left: 25%">
						<label class="texto">Manual</label>
						<input class="form-control" type="file" name="manual" id="manual" accept="application/pdf">
						<input type="hidden" name="archivo" id ="archivo" value=""/>
					</div>
					<div class="form-group col-6" style="margin-left: 25%">
						<input class="form-control" type="text" name="manual2" id="manual2" disabled>
					</div>
				</div>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
								<table class="table" id="resultado">
								  <thead class="thead-dark">
								    <tr class="bg-success">
									    <th class="bg-success" scope="col">NOMBRE</th>
									    <th class="bg-success" scope="col">DESCRIPCIÓN</th>
									    <th class="bg-success" scope="col">TIEMPO</th>
									    <th class="bg-success" scope="col">ESTADO</th>
									    <th class="bg-success" scope="col">ACCIONES</th> 
									    </tr>
								  </thead>
								  <tbody id="tableBodyTipoActividad">
								   	<!--tabla contruida en js  -->
								  </tbody>
								</table>
							</div>
						</div>
					</div>
				</center>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/tipoactividad/tipoactividad.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>