<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="../../css/global.css">
  <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../../css/datatables.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/all.css">
  <!-- JAVASCRIPT -->
  <script  type="text/javascript"src="../../js/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="../../js/moment-with-locales.min.js"></script>
  <script  type="text/javascript"src="../../js/bootstrap.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui.js"></script>
  <script type="text/javascript" src="../../js/datatables.min.js"></script>
  <script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
  <script type="text/javascript" src="../../js/pdfmake.min.js"></script>
  <script type="text/javascript" src="../../js/vfs_fonts.js"></script>
  <script type="text/javascript" src="../../js/buttons.html5.js"></script>
  <script type="text/javascript" src="../../js/jszip.min.js"></script>
  <script type="text/javascript" src="../../js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="../../js/buttons.print.min.js"></script>|
  <!-- <script  type="text/javascript" src="../js/librerias/validaciones.js">></script> -->
  <title>Equipo inventario</title>
</head>
<body onload="Enviar('CONSULTAR',null)">
  <?php
    include "../menu.v.php";
  ?> 
  <form id="accion" name="accion" class="center-block" style="margin-top: 100px;">
    <!-- id oculto para obterner consulta -->
    <input type="hidden" name="txtHideidequipo" id="txtHideidequipo" value="">
    <div class="containerneeds-validation container bg-light text-dark" novalidate>
      <div class="form-row">
        <div class="col-12 text-center">
          <h1 class="titulo">REGISTRO INVENTARIO</h1><br>
        </div>
      </div>
      <div class="form-row span6 ">
          <div class="form-group col-6">
            <label for="aseguradora" class="texto">Máquina</label>
              <select class="form-control" name="txtNombremaquina" id="txtNombremaquina">
                <option value="">Selecione una Opcion</option>
                <option value="Bicicletas Estáticas">Bicicletas estáticas</option>
                <option value="Bicicletas Elípticas">Bicicletas Elípticas</option>
                <option value="Banco Press">Banco Press "Pesas"</option>
                <option value="Prensa de Piernas">Prensa de piernas</option>
                <option value="Máquina de Dorsales">Máquina de Dorsales</option>
                <option value="Máquinas de Remo">Máquinas de Remo</option>
              </select>
          </div>
          <div class="form-group col-6">
            <label class="texto" for="dateIngreso">Fecha Ingreso</label>
            <input class="form-control" type="date" name="dateIngreso" id="dateIngreso"
             onchange="fechaIngreso();">
          </div>
      </div>
      <div class="form row">
        <div class="form-group col-6">
            <label class="texto">Manual</label>
            <input class="form-control" type="file" name="manual" id="manual" accept="application/pdf">
            <input type="hidden" name="archivo" id ="archivo" value=""/>
        </div>
          <div class="form-group col-6">
            <label for="aseguradora" class="texto">Estado</label>
              <select class="form-control" name="estado" id="estado">
                <option value="A">Activo</option>
                <option value="I">Inactivo</option>
              </select>
          </div>
          <div class="form-group col-6" style="margin-left: 25%">
            <input class="form-control" type="text" name="manual2" id="manual2" disabled>
          </div>
        </div>
           <center>
          <div class="form-row">
            <div class="form-group col-12">
           <!--  grupo de botones formularios -->
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
          </div>
        </div>
        </center>
     <div class="container">
          <div class="row">
            <div class="col-12">
              <table class="table" id="resultado">
                <thead class="thead-dark">
                  <tr class="bg-success">
                    <th class="bg-success" scope="col">NOMBRE DE LA MÁQUINA</th>
                    <th class="bg-success" scope="col">MANUAL USUARIO</th>
                    <th class="bg-success" scope="col">FECHA INGRESO</th>
                    <th class="bg-success" scope="col">ESTADO</th>
                    <th class="bg-success" scope="col">ACCIONES</th>
                  </tr>
                </thead>
                <tbody id="tableEquipo">
                  <!--tabla contruida en js  -->
                </tbody>
              </table>
              </center>
            </div>
          </div>
        </div>
    </div>
  </form>
  <script  src="../../js/inventario/equipo.js"></script>
  <script  src="../../js/global.js"></script>
</body>
</html>