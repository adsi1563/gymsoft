<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Registrar Personal</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<!--<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">-->
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<!--<script type="text/javascript" src="../../js/jquery-ui.js"></script>-->
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<style type="text/css">
[required]{
box-shadow: 0 0 10px 1px #818181;
}
:invalid{
	box-shadow: 0 0 10px 1px red;
} 
</style>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="centroFormacion" style="margin-top: 100px;">
			<div class="needs-validation container bg-light text-dark">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">REGISTRO PERSONAL</h1>
						</div>
					</div>
					<div class="form-row">
					<div class="form-group col-4" style="margin-left: 33%">
						<label for="ciudad" class="texto">Seleccione Rol</label>
						<select class="form-control" name="rol" id="rol" onchange="selectRol();">
						</select>
					</div>
				</div>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdPersona" id ="hidIdPersona" value=""/>
						</td>
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Primer Nombre</label>
						<input class="form-control" type="text" name="txtPrimerNombre" size="30" id="txtPrimerNombre" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Segundo Nombre</label>
						<input class="form-control" type="text" name="txtSegundoNombre" size="30" id="txtSegundoNombre" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Primer Apellido</label>
						<input class="form-control" type="text" name="txtPrimerApellido" size="30" id="txtPrimerApellido" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Segundo Apellido</label>
						<input class="form-control" type="text" name="txtSegundoApellido" size="30" id="txtSegundoApellido" value="" maxlength="30" onkeypress="return soloLetras(event)" id="miInput">
					</div>
					<div class="form-group col-4">
						<label for="ciudad" class="texto">Tipo Identificación</label>
						<select class="form-control"  name="tipoIdentificacion" id="tipoIdentificacion">
						</select>
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Documento Identidad</label>
						<input class="form-control" type="text" name="txtDocumento" size="30" id="txtDocumento" value="" oninput="maxLengthCheck(this)" maxlength="12" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Dirección</label>
						<input class="form-control" type="text" name="txtDireccion" size="30" id="txtDireccion" maxlength="30" value="" >
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Teléfono</label>
						<input class="form-control" type="text" name="txtTelefono" size="30" id="txtTelefono" value="" oninput="maxLengthCheck(this)" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					</div>
					<div class="form-group col-4">
						<label for="nombre" class="texto">Email</label>
						<input class="form-control" type="email" name="txtCorreo" size="30" id="txtCorreo" maxlength="30" value placeholder="ejemplo@dominio.com">
					</div>
					<div class="form-group col-4">
						<label for="ciudad" class="texto">EPS</label>
						<select class="form-control"  name="eps" id="eps">
						</select>
					</div>
					<div class="form-group col-2">
						<label for="nombre" class="texto">Rh</label>
						<select class="form-control"  name="rh" id="rh">
							<option >O+</option>
							<option >O-</option>
							<option >A+</option>
							<option >A-</option>
							<option >B+</option>
							<option >B-</option>	
							<option >AB</option>
							<option >AB+</option>
							<option >AB-</option>
						</select>
					</div>
					<div class="form-group col-4">
						<label for="ciudad" class="texto">Gimnasio</label>
						<select class="form-control"  name="gimnasio" id="gimnasio">
						</select>
					</div>
					<div class="form-group col-2">
						<label for="estado" class="texto">Estado</label>
						<select class="form-control" name="estado" id="estado">
							<option value="A">Activo</option>	
							<option value="I">Inactivo</option>
						</select>
					</div>
					<div class="form-group col-6">
						<label for="condicionPre" class="texto">Condicion pre-existentes</label>
						<select class="form-control" name="condicionPre" id="condicionPre" onchange="enfermedades();">
						</select>
					</div>
					<div class="form-group col-6">
						<label for="condicion" class="texto">Condicion</label>
						<select class="form-control" name="condicion" id="condicion">
						</select>
					</div>
				</div>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Siguiente" onclick=""/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
						</div>
					</div>
				<div class="container" align="center">
					<div class="row">
						<div class="col-12">
								<table class="table table-responsive"" id="resultado">
								  <thead class="thead-dark">
								    <tr class="bg-success">
								      <th class="bg-success" scope="col">NOMBRE</th>
								      <th class="bg-success" scope="col">T. IDENTIFICACIÓN</th>
								      <th class="bg-success" scope="col">IDENTIFICACIÓN</th>
								      <th class="bg-success" scope="col">EPS</th>
								      <th class="bg-success" scope="col">GIMNASIO</th>
								      <th class="bg-success" scope="col">RH</th>
								      <th class="bg-success" scope="col">DIRECCIÓN</th>
								      <th class="bg-success" scope="col">TELÉFONO</th>
								      <th class="bg-success" scope="col">EMAIL</th>
								      <th class="bg-success" scope="col">ACCIONES</th>
								    </tr>
								  </thead>
								  <tbody id="tableBodyPersona">
								   	<!--tabla contruida en js  -->
								  </tbody>
								</table>
							</div>
						</div>
					</div>
				</center>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/persona/persona.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
</html>