<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Imc</title>
	<link rel="stylesheet" type="text/css" href="../../css/global.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/datatables.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../css/all.css">

	<script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../js/bootstrap.js"></script>
	<script type="text/javascript" src="../../js/datatables.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="../../js/pdfmake.js"></script>
	<script type="text/javascript" src="../../js/vfs_fonts.js"></script>
	<script type="text/javascript" src="../../js/jszip.js"></script>
	<script type="text/javascript" src="../../js/buttons.html5.js"></script>
	<script type="text/javascript" src="../../js/buttons.colVis.js"></script>
	<script type="text/javascript" src="../../js/all.js"></script>
	<script type="text/javascript" src="../../js/buttons.print.js"></script>
</head>
<body onload="Enviar('CONSULTAR',null)">
	<?php
		include "../menu.v.php";
	?>
	<center>
		<form id="aseguradora" style="margin-top: 100px;">
			<div class="container bg-light">
				<center>
					<div class="form-row">
						<div class="col-12">
							<h1 class="titulo">INDICE MASA CORPORAL</h1>
						</div>
					</div><br><br>
				</center>
				<div class="form-row">
					<div>
						<td rowspan="2" id="td_lateral_izquierdo">
							<input type="hidden" name="hidIdAseguradora" id ="hidIdImc" value=""/>
						</td>
					</div>
					<div class="form-group col-4">
						<label for="nit" class="texto">Estatura</label>
						<input class="form-control" type="text" name="txtEstatura" size="40" id="txtEstatura" value="" onblur="if(this.value == ''){this.value='0'}" onKeyUp="suma();" placeholder="Estatura En CM">
					</div>
					<div class="form-group col-4">
						<label for="razon" class="texto">Peso</label>
							<input class="form-control" type="text" name="txtPeso" size="50" id="txtPeso" placeholder="Peso en KG" value=""onblur="if(this.value == ''){this.value='0'}"  onKeyUp="suma();" oninput="maxLengthCheck(this)" maxlength="15" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					</div>
					<div class="form-group col-4">
						<label for="direccion" class="texto">Calculo IMC</label>
							<input class="form-control" type="text" name="txtCalculoImc" id="txtCalculoImc"  readonly="readonly" placeholder="Indice De Masa Corporal">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-6">
						<label for="direccion" class="texto">Clasificación al IMC:</label>
							<input class="form-control" type="text" name="txtCalculoImc2" id="txtCalculoImc2"  readonly="readonly" placeholder="Clasificación  de su Imc">
							<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imcmodal">
  						Clasificación IMC
					</button>
					</div>
					<div class="form-group col-6">
						<label for="aseguradora" class="texto">Rutina Alimenticia</label>
						<textarea  class="form-control" name="txtRutAli" id="txtRutAli" readonly="readonly" placeholder="Recomendacion de rutina limenticia"></textarea>
						<!-- <input  type="text" > -->
					</div>	
				</div>
				<center>
					<div class="form-row">
						<div class="form-group col-12">
							<input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Siguiente" onclick=""/>
						</div>
					</div>
				</center> 
				<div class="container">
					<div class="row">
						<div class="col-12">
							<table class="table" id="resultado">
							  <thead class="thead-dark">
							    <tr class="bg-success">
							      <th class="bg-success" scope="col">ESTATURA</th>
							      <th class="bg-success" scope="col">PESO</th>
							      <th class="bg-success" scope="col">IMC</th>
							      <th class="bg-success" scope="col">ESTADO</th>
							      <th class="bg-success" scope="col">ACCIONES</th>
							    </tr>
							  </thead>
							  <tbody id="tableBodyImc">
							   	<!--tabla contruida en js  -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</center>
	<script type="text/javascript" src="../../js/registros/imc.js"></script>
	<script type="text/javascript" src="../../js/global.js"></script>
</body>
<!-- Modal -->
<style type="text/css" media="screen">
.modalCenter{
top: 50%;
transform: translateY(-50%) !important;
}

</style>
<div class="modal fade" id="imcmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalCenter" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Clasificación al IMC</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table align="center" border="3">
 <thead class="thead-dark">
   <tr class="bg-success" align="center">
     <th  scope="col">IMC</th>
     <th  scope="col">Clasificación</th>
   </tr>
 </thead>
 <tbody align="center">
  <tr>
<td>Menor-16.00</td>
  <td> Delgadez Severa</td>
  </tr>
  <tr>
  <td>16.00 - 16.99</td>
  <td> Delgadez moderada</td>
  </tr>
  <tr>
  <td>17.00 - 18.49</td>
  <td> Delgadez aceptable</td>
  </tr>
  <tr>
  <td >18.50 - 24.99</td>
  <td> Peso Normal</td>
  </tr>
  <tr>
  <td>25.00 - 29.99</td>
  <td> Sobrepeso</td>
  </tr>
  <tr>
  <td>30.00 - 34.99</td>
  <td> Obeso: Tipo I</td>
  </tr>
  <tr>
  <td>35.00 - 40.00</td>
  <td> Obeso: Tipo II</td>
  </tr>
  <tr>
  <td>Mayor-40.00</td>
  <td> Obeso: Tipo III</td>
  </tr>
 </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</html>