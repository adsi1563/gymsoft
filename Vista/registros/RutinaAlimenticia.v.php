<?php include '../../entorno/permiso.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
   <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="../../css/global.css">
  <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="../../css/datatables.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/buttons.dataTables.css">
  <link rel="stylesheet" type="text/css" href="../../css/all.css">

  <script type="text/javascript" src="../../js/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui.js"></script>
  <script type="text/javascript" src="../../js/bootstrap.js"></script>
  <script type="text/javascript" src="../../js/datatables.js"></script>
  <script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../../js/dataTables.buttons.js"></script>
  <script type="text/javascript" src="../../js/pdfmake.js"></script>
  <script type="text/javascript" src="../../js/vfs_fonts.js"></script>
  <script type="text/javascript" src="../../js/jszip.js"></script>
  <script type="text/javascript" src="../../js/buttons.html5.js"></script>
  <script type="text/javascript" src="../../js/buttons.colVis.js"></script>
  <script type="text/javascript" src="../../js/all.js"></script>
  <script type="text/javascript" src="../../js/buttons.print.js"></script>
  <!-- <script  type="text/javascript" src="../js/librerias/validaciones.campos.js">></script> -->
  <title>Rutina Alimenticia</title>
</head>
<body onload="Enviar('CONSULTAR',null)">
<?php
    include "../menu.v.php";
  ?> 
  <form id="accion" name="accion" class="center-block" style="margin-top: 100px;">
    <!-- id oculto para obterner consulta -->
    <input type="hidden" name="hidIdRutina" id="hidIdRutina" value="">
    <div class="container needs-validation container bg-light text-dark" novalidate>
      <div class="form-row">
        <div class="col-12 text-center">
          <h1 class="titulo">Rutina Alimenticia</h1><br>
        </div>
      </div>
      <div class="form-row span6 ">
        <div class="form-group col-6 ">
          <label class="" for="nombreRutinaAlimenticia">DESCRIPCIÓN</label>
          <textarea  class="form-control" name="txtRutinaAlimenticia" id="txtRutinaAlimenticia" cols="50" rows="5" placeholder="Ejemplo: Rutina para la semana:
             1) Leche con avena o cereales y plátano en rebanadas." ></textarea>
        </div>
        <div class="form-group col-6">
          <label class="" for="Descripcion">IMC</label>
          <select class="form-control"  name="selectImc" id="selectImc">
            </select>
        </div>
      </div>
      <center>
         <div class="form-row">
        <div class="form-group col-12">
         <!--  grupo de botones formularios -->
         <div class="form-row">
          <div class="form-group col-12">
           <!--  grupo de botones formularios -->
            <!-- <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/> -->
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
            <!-- <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/> -->
            <input type="button" class="btn btn-primary" class="Guardar" name="btnAccion" id="btnConsultar" value="Consultar" onclick="window.location.reload();"/>
          </div>
        </div>
      </center>
     <div class="container">
          <div class="row">
            <div class="col-12">
              <table class="table" id="resultado">
                <thead class="thead-dark">
                  <tr class="bg-success">
                    <th class="bg-success"class="" scope="col">DESCRIPCIÓN</th>
                    <th class="bg-success" scope="col">IMC</th>
                    <th class="bg-success" scope="col">ACCIONES</th>
                  </tr>
                </thead>
                <tbody id="tableRutinaAlimenticia">
                  <!--tabla contruida en js  -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
  </form>
  <script  src="../../js/registros/RutinaAlimenticia.js"></script>
  <script  src="../../js/global.js"></script>
</body>
</html>