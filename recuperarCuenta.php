<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ingreso</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fonts/ionicons.min.css">
    <link rel="stylesheet" href="css/Login-Form-Clean.css">
</head>

<body class="login-clean">
    <div class="centrado">
        <form method="post">
            <h4 class="text-center">Recuperación de Contraseña</h4>
            <div class="illustration" style="margin-top: 15px;"><img src="img/logo4.png" width="150px"></div><input type="hidden" name="tkn1" id="tkn1" value="<?php echo $_GET['tkn'] ?>">
             <input type="hidden" name="tkn2" id="tkn2" value="<?php echo $_GET['tkn2'] ?>">
            <div id="comprueba">
            </div>
        </form>
    </div>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/seguridad/recuperarCuenta.js"></script>
</body>

</html>

