<?php
require 'functions/autoload_class.php';
require 'functions/blog/page.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bienvenido a noticias GymSoft</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/blog/blog.css" rel="stylesheet">

  </head>

  <body>
  <?php include 'navbar.php'; ?>
    <div class="container">

      <div class="page-header master">
        <h1 class="master">Bienvenido a noticias GymSoft </h1>
      </div>
      <div class="row">

        <div class="col-sm-12 blog-main">
           <?php echo getArticle(getId());?>

        </div><!-- /.blog-main -->

      </div><!-- /.row -->

    </div><!-- /.container -->
    <?php include_once('footer.php') ?>
    
  </body>
</html>
