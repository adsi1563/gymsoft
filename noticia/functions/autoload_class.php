<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/gymsoft/noticia/config/config.php';
spl_autoload_register(function ($class) {
  if(in_array($class, NORMAL_CLASS))
    return require DIR . "/gymsoft/noticia/class/$class/$class.class.php";
  elseif (strpos($class, 'Message') !== false)
    require DIR . "/gymsoft/noticia/class/Message/$class.class.php";
  else
    require DIR . "/gymsoft/noticia/class/Article/$class.class.php";
});
