<?php session_start();
	try {
		if($_SESSION['autenticado'] == null){
		    echo "<script>alert('Debe autenticarse para poder acceder al formulario.')</script>";
		    header('Location: ../../index.php');
		    exit();
		}
	} catch (Exception $e) {
		echo "Debe autenticarse para poder acceder al formulario";
	}
?>