<?php
require_once '../../entorno/conexion.php';
require '../../modelo/seguridad/usuario.m.php';
$respuesta = array();
$res = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Usuario();
                $accion->setUsuario($_POST['usuario']);
                $accion->setContrasenia(md5($_POST['contrasenia']));
                //$accion->setEstado($_POST['estado']);
                $accion->setIdpersona($_POST['idPersona']);
                $accion->setRol($_POST['rol']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Usuario();
                $accion->setIdUsuario($_POST['id']);
                $accion->setUsuario($_POST['usuario']);
                $accion->setContrasenia(md5($_POST['contrasenia']));
                $accion->setIdpersona($_POST['idPersona']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
        break;
        case 'ELIMINAR':
            try{
                $accion = new Usuario();
                $accion->setIdUsuario($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Usuario();
                $accion->setIdusuario($_POST['id']);
                $accion->setUsuario($_POST['usuario']);
                $accion->setContrasenia($_POST['contrasenia']);
                $accion->setIdpersona($_POST['idPersona']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idUsuario;
                        $respuesta['usuario'] = $rowBuscar->usuario;
                        $respuesta['contrasenia'] = $rowBuscar->contrasenia;
                        $respuesta['nombre'] = $rowBuscar->Nombre_Persona;
                        //$respuesta['estado'] = $rowBuscar->estado;
                        $respuesta['idPersona'] = $rowBuscar->idPersona;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>                                            
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>                                                                                          
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[3]."</td>
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[4]."</td>
                                            <td>
                                                <input type='button' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'>
                                                <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'>
                                            </td>
                                        </tr>";                            
                        }                                       
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
            }
            //Retornar del retorno
            $respuesta['accion']='CONSULTAR';
            echo json_encode($respuesta);
            break;

        case "INICIAR_SESION":
            $accion = new Usuario();
            $accion->setUsuario($_POST['usuario']);
            $accion->setContrasenia($_POST['contrasenia']);
            $resultado = $accion->iniciarSesion();
            $respuesta = $accion->conn->obtenerObjeto();
            if ($respuesta->contrasenia == md5('') && $respuesta->estado=="A") {   
                    $res['nueva'] = "nueva";
                    echo json_encode($res);
                }else{
                    if($respuesta->contrasenia == md5($_POST["contrasenia"]) && $respuesta->estado=="A"){
                        $_SESSION["iniciarSesion"] = "ok";
                        $_SESSION['idUsuario'] = $respuesta->idUsuario;
                        $_SESSION['autenticado'] = 1;
                        $_SESSION['persona'] = $respuesta->NombrePersona;
                        $_SESSION['rol'] = 1;
                        $_SESSION['usuario'] = $respuesta->usuario;
                        echo json_encode('window.location = "vista/seguridad/principal.v.php";');
                    }else{
                        $res['incorrecto'] = 1;
                        echo json_encode($res);
                    }
                }
        break;

        case 'CONSULTA_PERSONA':
                $accion = new Usuario();
                $resultado = $accion->consultarPersona();
                $respuesta = $accion->conn->obtenerObjeto();
                echo json_encode($respuesta);
        break;

        case 'ACTUALIZAR_CONTRASENIA':
            try{
                $accion = new Usuario();
                $accion->setUsuario($_POST['usuario']);
                $accion->setContrasenia(md5($_POST['contrasenia']));
                $resultado = $accion->actualizar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] = "Error, no fué posible modificar la información, consulte con el administrador.";
            }
                //Respuesta del retorno
                echo json_encode($respuesta);
        break;
    }
}
?>