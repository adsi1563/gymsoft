<?php
    require_once '../../modelo/sena/instructor_ficha.M.php';

    switch($_POST["accion"]){
        case "GUARDAR":
            $datos = array(
                "instructor" => $_POST["instructor"],
                "ficha" => $_POST["ficha"]
            );
            $respuesta = ModeloInstructorFicha::mdlGuardarInstructorFicha($datos);
            if($respuesta == "ok"){
                echo 'alert("Datos guardados");';
            }else{
                echo 'alert("Error al guardar");';
            }
            break;
        case "MODIFICAR":
            $datos = array(
                "instructor" => $_POST["instructor"];
                "ficha" => $_POST["ficha"];
            );
            $respuesta = ModeloInstructorFicha::mdlModificarInstructorFicha($datos);
            if($respuesta == "ok"){
                echo 'alert("Datos guardados");';
            }else{
                echo 'alert("Error al guardar");';
            }
            break;
        case "ELIMINAR":
            $respuesta = ModeloInstructorFicha::mdlEliminarInstructorFicha($_POST["hiddenIdPersona"]);
            if($respuesta == "ok"){
                echo "alert('Datos eliminados');";
            }else{
                echo "alert('Error al eliminar');";
            }
            break;
        case "CONSULTA":
            $respuesta = ModeloInstructorFicha::mdlConsultarTodoInstructorFicha();
            $retorno = "";
            if(isset($respuesta)){
                foreach($respuesta as $dato){
                    $retorno .= "<tr>
                                    <td>".$dato[1]."</td>
                                    <td>".$dato[2]."</td>
                                    <td>
                                        <input type='button' name='editar' class='editar' value='Editar' onclick='consultarPorId(".$dato[0].")'>
                                        <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='eliminar(".$dato[0].")'>
                                    </td>
                                </tr>";
                }
            }else{
                $retorno='No existen datos!!!';
            }
            echo $retorno;
        break;
    }

?>