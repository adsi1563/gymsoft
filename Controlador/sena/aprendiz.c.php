<?php
require_once '../../entorno/conexion.php';
require '../../modelo/sena/aprendiz.m.php';

$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Aprendiz();
                $accion->setUsuario($_POST['usuario']);
                $accion->setImc($_POST['imc']);
                $accion->setAcudiente($_POST['acudiente']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Aprendiz();
                $accion->setIdAprendiz($_POST['id']);
                $accion->setUsuario($_POST['usuario']);
                $accion->setImc($_POST['imc']);
                $accion->setAcudiente($_POST['acudiente']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Aprendiz();
                $accion->setIdAprendiz($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Aprendiz();
                $accion->setIdAprendiz($_POST['id']);
                $accion->setUsuario($_POST['usuario']);
                $accion->setImc($_POST['imc']);
                $accion->setAcudiente($_POST['acudiente']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idAprendiz;
                        $respuesta['idusuario'] = $rowBuscar->idUsuario;
                        $respuesta['idimc'] = $rowBuscar->idImc;
                        $respuesta['idacudiente'] = $rowBuscar->idAcudiente;
                        //$respuesta['estado'] = $rowBuscar->estado;
                        $respuesta['nombreAcudiente'] = $rowBuscar->nombreAcudiente;
                        $respuesta['nombrePersona'] = $rowBuscar->nombrePersona;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table id='tabla1'>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>                                            
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[5]."</label></td>
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[7]."</label></td> 
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[6]."</label></td>                                                                                            
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[4]."</td>
                                            <td>
                                                <input type='button' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'>
                                                <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'>
                                            </td>
                                        </tr>";                            
                        }
                        $retorno.="</table>";                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
            }
            //Retornar del retorno
            $respuesta['accion']='CONSULTAR';
            echo json_encode($respuesta);
            break;

        case 'CONSULTA_IMC':
                $accion = new Aprendiz();
                $resultado = $accion->consultarImc();
                $respuesta = $accion->conn->obtenerObjeto();         
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_USUARIO':
                $accion = new Aprendiz();
                $resultado = $accion->consultarUsuario();
                $respuesta = $accion->conn->obtenerObjeto();         
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_ACUDIENTE':
                $accion = new Aprendiz();
                $resultado = $accion->consultarAcudiente();
                $respuesta = $accion->conn->obtenerObjeto();         
        echo json_encode($respuesta);
        break;
    }
}
?>