<?php
    require '../../modelo/sena/aprendiz.m.php';
    require_once '../../entorno/conexion.php';

    $acudiente = $_GET["acudiente"]; 
    $usuario = $_GET["usuario"]; 

    if ($acudiente != null) {
        $respuesta = Aprendiz::readAcudiente($acudiente);
        foreach ($respuesta as $key => $value) {
                $personas[] = array(
                    "value" => $value[0],
                    "label" => $value[1]
                );
            }
        echo json_encode($personas);
    }

    if ($usuario != null) {
        $respuesta = Aprendiz::readUsuario($usuario);
        foreach ($respuesta as $key => $value) {
                $personas[] = array(
                    "value" => $value[0],
                    "label" => $value[1]
                );
            }
        echo json_encode($personas);
    }
?>