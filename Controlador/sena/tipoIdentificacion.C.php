<?php
require_once '../../entorno/conexion.php';
require_once '../../modelo/sena/tipoIdentificacion.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setDescripcion($_POST['Descripcion']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdtipoidentificacion($_POST['id']);
                $accion->setTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setDescripcion($_POST['Descripcion']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                 $accion->setIdtipoidentificacion($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdtipoidentificacion($_POST['id']);
                $accion->setTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setDescripcion($_POST['Descripcion']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros ===1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idTipoIdentificacion;
                        $respuesta['tipoIdentificacion'] = $rowBuscar->tipo;
                        $respuesta['Descripcion'] = $rowBuscar->descripcion;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= " <tr>
                                            <td><label id='tipoIdentificacion".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>                                                                
                                            <td><label id='Descripcion".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                       </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "prueba";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;
    }
}
?>
