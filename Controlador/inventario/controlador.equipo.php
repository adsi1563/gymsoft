<?php
require_once '../../entorno/conexion.php';
require '../../modelo/inventario/equipo.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setNombremaquina($_POST['nombremaquina']);
                $accion->setManual(str_replace(" ","_",$_POST['manual']));
                $accion->setFechaingreso($_POST['fechaingreso']);
                $accion->setEstado($_POST['estado']);
                if ($resultado = $accion->agregar()) {
                    echo "exito";
                } else {
                    echo "fallo";
                }
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.". $e->getMessage();
                }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdequipo($_POST['id']);
                $accion->setNombremaquina($_POST['nombremaquina']);
                $accion->setFechaingreso($_POST['fechaingreso']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                 $accion->setIdequipo($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdequipo($_POST['id']);
                $accion->setNombremaquina($_POST['nombremaquina']);
                $accion->setFechaingreso($_POST['fechaingreso']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros ===1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idEquipo;
                        $respuesta['nombremaquina'] = $rowBuscar->nombre;
                        $respuesta['manual'] = $rowBuscar->descripcion;
                        $respuesta['fechaingreso'] = $rowBuscar->fechaIngreso;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table class='table' id='resultado'>
                                    <thead class='thead-dark'>
                                        
                                    </thead>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tbody id='tableEquipo'>
                                            <td><label id='nombremaquina".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>
                                             <td><a target='_blank' href='../../documentos/".$rowConsulta[2]."'>".$rowConsulta[2]."</a></td>  
            
                                            <td><label id='fecha_ingreso".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[4]."</label></td>
                                               <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                       </tbody>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "prueba fallida";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;
    }
}
?>
