<?php
require_once '../../entorno/conexion.php';
require '../../modelo/centroformacion/centroformacion.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setNombre($_POST['nombre']);
                $accion->setEstado($_POST['Estado']);
                $accion->setIdAseguradora($_POST['aseguradora']);
                $accion->setIdRegional($_POST['regional']);
                $accion->setIdCiudad($_POST['ciudad']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.".$e->getMessage();
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdCentroFormacion($_POST['id']);
                $accion->setNombre($_POST['nombre']);
                $accion->setEstado($_POST['Estado']);
                $accion->setIdAseguradora($_POST['aseguradora']);
                $accion->setIdRegional($_POST['regional']);
                $accion->setIdCiudad($_POST['ciudad']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdCentroFormacion($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdCentroFormacion($_POST['id']);
                $accion->setNombre($_POST['nombre']);
                $accion->setEstado($_POST['Estado']);
                $accion->setEstado($_POST['aseguradora']);
                $accion->setIdRegional($_POST['regional']);
                $accion->setIdCiudad($_POST['ciudad']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idCentroFormacion;
                        $respuesta['nombre'] = $rowBuscar->nombre;
                        $respuesta['estado'] = $rowBuscar->estado;
                        $respuesta['aseguradora'] = $rowBuscar->idAseguradora;
                        $respuesta['regional'] = $rowBuscar->idRegional;
                        $respuesta['ciudad'] = $rowBuscar->idCiudad;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table class='table' id='resultado'>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>

                                            <td><label id='nombre".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>
                                            <td><label id='ciudad".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>
                                            <td><label id='aseguradora".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>
                                            <td><label id='regional".$rowConsulta[0]."'>".$rowConsulta[4]."</label></td>  
                                            <td><label id='Estado".$rowConsulta[0]."'>".$rowConsulta[5]."</label></td>                                                            
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_ASEGURADORA':
                $accion = new Accion();
                $resultado = $accion->consultarAseguradora();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[2].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_REGIONAL':
                $accion = new Accion();
                $resultado = $accion->consultarRegional();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_CIUDAD':
                $accion = new Accion();
                $resultado = $accion->consultarCiudad();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[2].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;
    }
}
?>
