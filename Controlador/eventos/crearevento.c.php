<?php 

require_once '../../entorno/conexion.php';
require '../../modelo/eventos/crearevento.m.php';

$respuesta = array();
if (isset ($_POST['accion'])){
	switch($_POST['accion']){
		case 'ADICIONAR':
			try{
				$accion= new Accion();
				$accion->setIdTipoEvento($_POST['tipoEvento']);
				$accion->setNombreEvento($_POST['nombreEvento']);
				$accion->setFechaInicio($_POST['fechaInicio']);
                $accion->setFechaFin($_POST['fechaFin']);
                $accion->setSede($_POST['sede']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                $respuesta['respuesta'] = "La información se adicionó correctamente.";

			}catch(Exception $e){
				$respuesta['respuesta'] = "Error, no fue posible adicionar la información, consulte con el administrador.".$e->getMessage();

			}
			 //Respuesta del retorno
            $respuesta['accion']='ADICIONAR'; 
            echo json_encode($respuesta);			
			break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdCrearEvento($_POST['idCrearEvento']);
                $accion->setIdTipoEvento($_POST['tipoEvento']);
                $accion->setNombreEvento($_POST['nombreEvento']);
                $accion->setFechaInicio($_POST['fechaInicio']);
                $accion->setFechaFin($_POST['fechaFin']);
                $accion->setSede($_POST['sede']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;

		case 'ELIMINAR':
			try{
				$accion = new Accion();
				$accion->setIdCrearEvento($_POST['idCrearEvento']);
				$resultado = $accion->eliminar();

				$respuesta['respuesta'] = "La información se eliminó correctamente.";
			}catch (Exception $e){
				$respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();
			}
			//Respuesta del retorno
            $respuesta['accion']='ELIMINAR'; 
            echo json_encode($respuesta);
			break;

		 case 'Consulta_tipo_evento': 
                $accion = new Accion();
                $resultado = $accion->consultarTipo();
               
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.utf8_encode($dato[1]).'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
            echo json_encode($respuesta);
            break;

             case 'Consulta_sede':
                $accion = new Accion();
                $resultado = $accion->consultarSede();
               
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
            echo json_encode($respuesta);
            break;

            case 'AgregarTipo':
                try{
					$accion= new Accion();
					$accion->setDescripcion($_POST['descripcion']);		
	                $resultado = $accion->agregarTipoEvento();

	                $respuesta['respuesta'] = "La información se adicionó correctamente.";

				}catch(Exception $e){
					$respuesta['respuesta'] = "Error, no fue posible adicionar la información, consulte con el administrador.";

				}
				 //Respuesta del retorno
	            $respuesta['accion']='ADICIONAR'; 
	            echo json_encode($respuesta);			
			break;
			
		case 'CONSULTAR':
			try{
				$accion= new Accion();
				$accion->setIdCrearEvento($_POST['idCrearEvento']);
				$accion->setIdTipoEvento($_POST['tipoEvento']);
				$accion->setNombreEvento($_POST['nombreEvento']);
				$accion->setFechaInicio($_POST['fechaInicio']);
                $accion->setFechaFin($_POST['fechaFin']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;
                if ($numeroRegistros === 1) {
                	if ($rowBuscar = $accion->conn->obtenerObjeto()){
                		$respuesta['idCrearEvento'] = $rowBuscar->idCrearEvento;
                        $respuesta['tipoEvento'] = $rowBuscar->idTipoEvento;
                		$respuesta['nombreEvento'] = $rowBuscar->nombreEvento;
                		$respuesta['fechaInicio'] = $rowBuscar->fechaInicio;
                        $respuesta['fechaFin'] = $rowBuscar->fechaCulminacion;
                        $respuesta['sede'] = $rowBuscar->idSede;
                        $respuesta['estado'] = $rowBuscar->estado;
                	}
                }else{
                	if (isset($resultado)) {
                		$retorno = "";
                		foreach ($accion->conn->obtenerRegistros() AS $rowConsulta){
                				/*
                				
                				*/
                				$retorno .= "<tr>
												<td>".utf8_encode($rowConsulta[11])."</td>
												<td>" . $rowConsulta[2] . "</td>
												<td>" . $rowConsulta[3] . "</td>
												<td>" . $rowConsulta[4] . "</td>
                                                <td>" . $rowConsulta[12] . "</td>
												<td>" . $rowConsulta[6] . "</td>
												<td>
													<button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>
													<button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
												</td>
											</tr>";
                			
                	}; 
                	$respuesta['tablaRegistro'] = $retorno;
                }else{
                	$retorno .="";
                	$respuesta['tablaRegistro']='No existen datos';
                } }

			}catch(Exception $e){
				echo "hola";
			}
			//Retornar del retorno
			$respuesta['accion']='CONSULTAR';
			echo json_encode($respuesta);
			break;
	}
}

?>