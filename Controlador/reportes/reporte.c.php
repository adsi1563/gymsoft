<?php
require_once '../../entorno/conexion.php';
require '../../modelo/reportes/reporte.m.php';
//require_once 'nueva.php';

$respuesta = array();


if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        
        case 'Consulta_Centro_form':
                $accion = new Accion();
                $resultado = $accion->consultarCentroF();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'ConsultaSede':
                $idCentro = $_POST['idCentro'];
                $accion = new Accion();
                $resultado = $accion->consultarSedeIds($idCentro);

                $retorno='<option value="">Seleccione..</option>';
                foreach ($resultado as $key) {
                   $retorno .= '<option value="'.$key['idCentroFormacion'].'">'.$key['nombre'].'</option>';
                }
                $respuesta=$retorno;
                
        echo json_encode($respuesta);
        break;

        case 'Consulta':
                $idsede = $_POST['idGimnasio'];
                $fi = $_POST['fechaInicio'];
                $ff = $_POST['fechaFin'];
                $item = 1;
                $accion = new Accion();

                $accion->setFechaini($fi);
                $accion->setFechafin($ff);

                $res = $accion->consultar($item, $idsede);

                if(isset($res)){
                    $retorno="<table class='table' id='resultado'>";
                    foreach ($res as $key) {
                        $retorno.= "<tr>
                                <td><label>". $key['sede'] ."</label></td>                                                                
                                <td><label>".$key['suma']."</label></td>

                                <td><label>".$key['fe']."</label></td>

                                </tr>";
                    } // fon for

                    $respuesta['tablaRegistro']=$retorno;
                }

                //$respuesta['tabla'] = $res;
                $respuesta['accion']='Consulta'; 
        echo json_encode($respuesta);
        break;

        case 'reporteED':
                $idsede = $_POST['idGimnasio'];
                $fi = $_POST['fechaInicio'];
                $ff = $_POST['fechaFin'];
                $item = 2;
                $accion = new Accion();

                $accion->setFechaini($fi);
                $accion->setFechafin($ff);

                $res = $accion->consultar($item, $idsede);

                if(isset($res)){
                    $retorno="<table class='table' id='resultado'>"; //fechafin
                    foreach ($res as $key) {
                        $retorno.= "<tr>
                                <td><label>". $key['sedes'] ."</label></td>                                                                
                                <td><label>".$key['nombre']."</label></td>

                                <td><label>".$key['fi']."</label></td>
                                <td><label>".$key['fechafin']."</label></td>
                                </tr>";
                    } // fon for

                    $respuesta['tablaRegistro']=$retorno;
                }

                //$respuesta['tabla'] = $res;
                $respuesta['accion']='reporteED'; 
        echo json_encode($respuesta);
        break;

        case 'reportePuntaje': // reportePartipar
                $idsede = $_POST['idGimnasio'];
                $fi = $_POST['fechaInicio'];
                $ff = $_POST['fechaFin'];
                $item = 3;
                $accion = new Accion();

                $accion->setFechaini($fi);
                $accion->setFechafin($ff);

                $res = $accion->consultar($item, $idsede);

                if(isset($res)){
                    $retorno="<table class='table' id='resultado'>";
                    foreach ($res as $key) {
                        $retorno.= "<tr>
                                <td><label>". $key['sedes'] ."</label></td>                                                                
                                <td><label>".$key['nombre']."</label></td>
                                <td><label>".$key['nombreCom']."</label></td>

                                <td><label>".$key['punta']."</label></td>

                                </tr>";
                    } // fin for

                    $respuesta['tablaRegistro']=$retorno;
                }

                //$respuesta['tabla'] = $res;
                $respuesta['accion']='reportePuntaje'; 
        echo json_encode($respuesta);
        break;

        case 'reportePartipar': // 
                $idsede = $_POST['idGimnasio'];
                $fi = $_POST['fechaInicio'];
                $ff = $_POST['fechaFin'];
                $item = 4;
                $accion = new Accion();

                $accion->setFechaini($fi);
                $accion->setFechafin($ff);

                $res = $accion->consultar($item, $idsede);

                if(isset($res)){
                    $retorno="<table class='table' id='resultado'>";
                    foreach ($res as $key) {
                        $retorno.= "<tr>
                                <td><label>". $key['fichas'] ."</label></td>                                                                
                                <td><label>".$key['conteo']."</label></td>
                                
                                </tr>";
                    } // fin for

                    $respuesta['tablaRegistro']=$retorno;
                }

                //$respuesta['tabla'] = $res;
                $respuesta['accion']='reportePartipar'; 
        echo json_encode($respuesta);
        break;

        case 'reporteAsisGym': // reporteDesGym
                // cod
                consul('reporteAsisGym', 5);
        break;

        case 'reporteDesGym': // reporteRutina
                // cod
                consul('reporteDesGym', 6);
        break;

        case 'reporteRutina':
                $idsede = $_POST['idGimnasio'];
                $fi = $_POST['fechaInicio'];
                $ff = $_POST['fechaFin'];
                $item = 7;
                $accion = new Accion();

                $accion->setFechaini($fi);
                $accion->setFechafin($ff);

                $res = $accion->consultar($item, $idsede);

                if(isset($res)){
                    $retorno="<table class='table' id='resultado'>";
                    foreach ($res as $key) {
                        $retorno.= "<tr>
                                <td><label>". $key['iden'] ."</label></td>                                                                
                                <td><label>".$key['nombre']."</label></td>

                                <td><label>".$key['conteo']."</label></td>
                                
                                </tr>";
                    } // fin for

                    $respuesta['tablaRegistro']=$retorno;
                }

                //$respuesta['tabla'] = $res;
                $respuesta['accion']='reporteRutina'; 
        echo json_encode($respuesta);
        break;
    }
}

     function consul($var, $it)
    {
                $idsede = $_POST['idGimnasio'];
                $fi = $_POST['fechaInicio'];
                $ff = $_POST['fechaFin'];
                
                $accion = new Accion();

                $accion->setFechaini($fi);
                $accion->setFechafin($ff);

                $res = $accion->consultar($it, $idsede);

                if(isset($res)){
                    $retorno="<table class='table' id='resultado'>";
                    foreach ($res as $key) {
                        $retorno.= "<tr>
                                <td><label>". $key['gim'] ."</label></td>                                                                
                                <td><label>".$key['conteo']."</label></td>

                                <td><label>". evaluar($key['fechas'])."</label></td>
                                
                                </tr>";
                    } // fin for

                    $respuesta['tablaRegistro']=$retorno;
                }

                //$respuesta['tabla'] = $res;
                $respuesta['accion']= $var; 
                echo json_encode($respuesta);
    }

  function evaluar($value)
    {
    switch ($value) {
        case 1:
            return "enero";
            break;
         case 2:
            return "febrero";
            break;
            
         case 3:
            return "marzo";
            break;
            
         case 4:
            return "abril";
            break;
            
          case 5:
            return "mayo";
            break; 

        case 6:
            return "junio";
            break;
            
         case 7:
            return "julio";
            break;
            
          case 8:
            return "agosto";
            break;

        case 9:
            return "septiembre";
            break;

        case 10:
            return "octubre";
            break;

        case 11:
            return "noviembre";
            break;

        case 12:
            return "diciembre";
            break;               
        default:
            # code...
            break;
     }
    }



?>
