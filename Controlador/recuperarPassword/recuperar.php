
<?php
require_once '../../entorno/conexion.php';
require '../../modelo/persona/persona.m.php';
$respuesta = array();
$salida = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'CONSULTAR_EMAIL':

        $accion= new Accion();
        $accion->setCorreo($_POST['email']);
        $resultado = $accion->consultarEmail();
        $respuesta = $accion->conn->obtenerObjeto();

        if(isset($respuesta)){                                 
           if ($respuesta->email == $_POST['email']) {
                    try {

                        $email_to = $respuesta->email;

                        $email_subject = "Solicutud de recuperacion de contraseña GymSoft";
                        $email_message = "Estimado(a): ".$respuesta->Nombre."\n\n";
                        $email_message .= "Para restabecer la contraseña por favor ingrese al siguiente link: \n\n";
                        $email_message .= "http://localhost/GymSoft/recuperarCuenta.php?tkn=".$respuesta->token."&tkn2=".base64_encode($respuesta->usuario)."\n\n";
                        $email_message .= "Si tiene alguna duda contactese con soporte tecnico, \n\n";
                        $email_message .= "Muchas gracias \n\n";
                        $email_message .= "Equipo GymSoft \n\n";
                        
                        mail($email_to, $email_subject, $email_message);

                        $salida['respuesta']= "Mensaje enviado correctamente, Por favor revise su bandeja de correo no deseado.";   
                    } catch (Exception $e) {
                        $salida['respuesta'] = 'Error en: '.$e->getMessage();  
                    }
                }     
        }else{
             $salida['respuesta']='El correo no existe en el sistema, por favor verifique la informacion.';
        }
        echo json_encode($salida);
        break;

        case 'ACTUALIZAR_CONTRASENIA':
            try{
                $accion = new Accion();
                $accion->setToken2(base64_decode($_POST['token2']));
                $accion->setContrasenia(md5($_POST['contrasenia']));
                $resultado = $accion->actualizar();
                    $respuesta['respuesta'] = '<div class="form-group"><h6 class="text-center">La informacion fue actualizada correctamente, ingrese sesión nuevamente.</h6></div>
                            <div class="form-group"><button class="btn btn-primary btn-block" type="button" id="regresar">Regresar</button></div>';
                }catch(Exception $e){
                    $respuesta['respuesta'] = "Error, no fué posible modificar la información, consulte con el administrador.";
            }
                //Respuesta del retorno
                echo json_encode($respuesta);
        break;

        case 'CONSULTA_USUARIO':
                $accion = new Accion();
                $accion->setToken1($_POST['token1']);
                $accion->setToken2(base64_decode($_POST['token2']));
                $resultado = $accion->consultarUsuario();
                $respuesta = $accion->conn->obtenerObjeto();
                
                if (isset($respuesta)) {
                    if ($respuesta->usuario == base64_decode($_POST['token2']) && $respuesta->token == $_POST['token1']){
                       $token = substr(md5(uniqid()), 0, 20);
                       $accion = new Accion();
                       $accion->setToken1($token);
                       $accion->setToken2(base64_decode($_POST['token2']));
                       $resultado = $accion->actualizarToken();
                       $salida['respuesta'] = '<div class="form-group"><input class="form-control" type="password" name="txtContrasenia2" placeholder="Contraseña nueva" id="txtContrasenia2" onkeypress=""></div>
                           <div class="form-group"><input class="form-control" type="password" name="txtContrasenia3" placeholder="Repetir contraseña nueva" id="txtContrasenia3" disabled></div>
                            <div class="" id="verificacion"></div>
                            <div class="form-group"><button class="btn btn-primary btn-block" type="button" disabled id="cambiar">Cambiar</button></div>';
                        }
                    }else{
                        $salida['respuesta'] = '<div class="form-group"><h6 class="text-center">La sesión ah finalizado o el enlace ya no esta disponible, por favor realice el proceso nuevamente.</h6></div>
                            <div class="form-group"><button class="btn btn-primary btn-block" type="button" id="regresar">Regresar</button></div>';
                }
                echo json_encode($salida);
        break;
    }
}