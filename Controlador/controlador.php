<?php
    require_once '../../modelo/seguridad/persona.M.php';

    switch($_POST["accion"]){
        case "GUARDAR":
            $datos = array(
                "primerNombre" => $_POST["primerNombre"],
                "segundoNombre" => $_POST["segundoNombre"],
                "primerApellido" => $_POST["primerApellido"],
                "segundoApellido" => $_POST["segundoApellido"],
                "documento" => $_POST["documento"],
                "telefono" => $_POST["telefono"],
                "email" => $_POST["email"],
                "estado" => $_POST["estado"]
            );
            $respuesta = ModeloPersona::mdlGuardarPersona($datos);
            if($respuesta == "ok"){
                echo 'alert("Datos guardados");';
            }else{
                echo 'alert("Error al guardar");';
            }
            break;
        case "MODIFICAR":
            $datos = array(
                "primerNombre" => $_POST["primerNombre"],
                "segundoNombre" => $_POST["segundoNombre"],
                "primerApellido" => $_POST["primerApellido"],
                "segundoApellido" => $_POST["segundoApellido"],
                "documento" => $_POST["documento"],
                "telefono" => $_POST["telefono"],
                "email" => $_POST["email"],
                "estado" => $_POST["estado"],
                "hiddenIdPersona" => $_POST["hiddenIdPersona"]
            );
            $respuesta = ModeloPersona::mdlModificarPersona($datos);
            if($respuesta == "ok"){
                echo 'alert("Datos guardados");';
            }else{
                echo 'alert("Error al guardar");';
            }
            break;
        case "ELIMINAR":
            $respuesta = ModeloPersona::mdlEliminarPersona($_POST["hiddenIdPersona"]);
            if($respuesta == "ok"){
                echo "alert('Datos eliminados');";
            }else{
                echo "alert('Error al eliminar');";
            }
            break;
        case "CONSULTA":
            $respuesta = ModeloPersona::mdlConsultarTodoPersona();
            $retorno = "";
            if(isset($respuesta)){
                foreach($respuesta as $dato){
                    $retorno .= "<tr>
                                    <td>".$dato[1]."</td>
                                    <td>".$dato[2]."</td>
                                    <td>".$dato[3]."</td>
                                    <td>".$dato[4]."</td>
                                    <td>".$dato[5]."</td>
                                    <td>".$dato[6]."</td>
                                    <td>".$dato[7]."</td>
                                    <td>".$dato[9]."</td>
                                    <td>
                                        <input type='button' name='editar' class='editar' value='Editar' onclick='consultarPorId(".$dato[0].")'>
                                        <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='eliminar(".$dato[0].")'>
                                    </td>
                                </tr>";
                }
            }else{
                $retorno='No existen datos!!!';
            }
            echo $retorno;
            break;
        case "CONSULTA_ID":
            $respuesta = ModeloPersona::mdlConsultarPersonaPorId($_POST["idPersona"]);
            echo json_encode($respuesta);
            break;
    }

?>