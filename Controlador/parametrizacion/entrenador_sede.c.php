<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/entrenador_sede.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setIdEntrenador($_POST['entrenador']);
                $accion->setSede($_POST['sede']);
                $accion->setHoraInicio($_POST['horaInicio']);
                $accion->setHoraFin($_POST['horaFin']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdEntrenadorSede($_POST['id']);
                $accion->setIdEntrenador($_POST['entrenador']);
                $accion->setSede($_POST['sede']);
                $accion->setHoraInicio($_POST['horaInicio']);
                $accion->setHoraFin($_POST['horaFin']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                $respuesta['respuesta'] = "La información se modificó correctamente.";
            }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdEntrenadorSede($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdEntrenadorSede($_POST['id']);
                $accion->setIdEntrenador($_POST['entrenador']);
                $accion->setSede($_POST['sede']);
                $accion->setHoraInicio($_POST['horaInicio']);
                $accion->setHoraFin($_POST['horaFin']);
                //$accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idEntrenadorSede;
                        $respuesta['entrenador'] = $rowBuscar->idEntrenador;
                        $respuesta['persona'] = $rowBuscar->Nombre_Persona;
                        $respuesta['sede'] = $rowBuscar->idSede;
                        $respuesta['horaInicio'] = $rowBuscar->horaInicio;
                        $respuesta['horaFin'] = $rowBuscar->horaFin;
                        //$respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table class='table' id='resultado'>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>

                                            <td><label id='persona".$rowConsulta[0]."'>".$rowConsulta[7]."</label></td>                                                   
                                            <td><label id='sede".$rowConsulta[0]."'>".$rowConsulta[8]."</label></td>
                                            <td><label id='hora_inicio".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>
                                            <td><label id='hora_fin".$rowConsulta[0]."'>".$rowConsulta[4]."</label></td>
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[5]."</label></td>
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_SEDE':
                $accion = new Accion();
                $resultado = $accion->consultarSede();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

         case 'CONSULTA_ENTRENADOR':
                $accion = new Accion();
                $resultado = $accion->consultarEntrenador();
                $respuesta = $accion->conn->obtenerObjeto();         
        echo json_encode($respuesta);
        break;
    }
}
?>
