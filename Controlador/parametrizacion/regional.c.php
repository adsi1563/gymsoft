<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/regional.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Regional();
                $accion->setNombre($_POST['nombre']);
                $accion->setCodigo($_POST['codigo']);
                $accion->setDescripcion($_POST['descripcion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Regional();
                $accion->setIdRegional($_POST['id']);
                $accion->setNombre($_POST['nombre']);
                $accion->setCodigo($_POST['codigo']);
                $accion->setDescripcion($_POST['descripcion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Regional();
                $accion->setIdRegional($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Regional();
                $accion->setIdRegional($_POST['id']);
                $accion->setNombre($_POST['nombre']);
                $accion->setCodigo($_POST['codigo']);
                $accion->setDescripcion($_POST['descripcion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idRegional;
                        $respuesta['nombre'] = $rowBuscar->nombre;
                        $respuesta['codigo'] = $rowBuscar->codigo;
                        $respuesta['descripcion'] = $rowBuscar->descripcion;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table id='tabla1'>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>                                            
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td> 
                                            <td><label id='accion".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>                                                                                            
                                            <td><label id='estado".$rowConsulta[0]."'>".($rowConsulta[4] == 'A' ? 'Activo' : 'Inactivo')."</td>
                                            <td>
                                                <input type='button' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'>
                                                <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'>
                                            </td>
                                        </tr>";                            
                        }
                        $retorno.="</table>";                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
            }
            //Retornar del retorno
            $respuesta['accion']='CONSULTAR';
            echo json_encode($respuesta);
            break;
        }
    }




?>