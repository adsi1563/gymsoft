<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/registrousuario.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setUsuarioevento($_POST['usuarioevento']);
                $accion->setTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setNumeroIdentificacion($_POST['numeroIdentificacion']);
                $accion->setNombreAprendiz($_POST['nombreAprendiz']);
                $accion->setApellidoAprendiz($_POST['apellidoAprendiz']);
                $accion->setFicha($_POST['ficha']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                $respuesta['respuesta'] = "La información se adicionó correctamente.";
            }catch(Exception $e){
                $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

            //Respuesta del retorno
            $respuesta['accion']='ADICIONAR'; 
            echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdInscripcionAprendiz($_POST['id']);
                $accion->setUsuarioevento($_POST['usuarioevento']);
                $accion->setTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setNumeroIdentificacion($_POST['numeroIdentificacion']);
                $accion->setNombreAprendiz($_POST['nombreAprendiz']);
                $accion->setApellidoAprendiz($_POST['apellidoAprendiz']);
                $accion->setFicha($_POST['ficha']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                $respuesta['respuesta'] = "La información se modificó correctamente.";
            }catch(Exception $e){
                $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

            //Respuesta del retorno
            $respuesta['accion']='MODIFICAR'; 
            echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdInscripcionAprendiz($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
            }catch(Exception $e){
                $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
            }

            //Respuesta del retorno
            $respuesta['accion']='ELIMINAR'; 
            echo json_encode($respuesta);
            break;
        
            case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdInscripcionAprendiz($_POST['id']);
                $accion->setUsuarioevento($_POST['usuarioevento']);
                $accion->setTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setNumeroIdentificacion($_POST['numeroIdentificacion']);
                $accion->setNombreAprendiz($_POST['nombreAprendiz']);
                $accion->setApellidoAprendiz($_POST['apellidoAprendiz']);
                $accion->setFicha($_POST['ficha']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;



                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idInscripcionAprendiz;
                        $respuesta['usuarioevento'] = $rowBuscar->idCrearEvento;
                        $respuesta['tipoIdentificacion'] = $rowBuscar->idTipoIdentificacion;
                        $respuesta['numeroIdentificacion'] = $rowBuscar->numeroIdentificacion;
                        $respuesta['nombreAprendiz'] = $rowBuscar->nombreAprendiz;
                        $respuesta['apellidoAprendiz'] = $rowBuscar->apellidoAprendiz;
                        $respuesta['ficha'] = $rowBuscar->ficha;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno = "";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            /*
                                Nombre
                                idCiudad
                                idSede
                                Estado
                            */
                            $retorno .= "<tr>
                                            <td>" . $rowConsulta[6] . "</td>
                                            <td>" . $rowConsulta[4] . "</td>
                                            <td>" . $rowConsulta[5] . "</td>
                                            <td>" . $rowConsulta[3] . "</td>
                                            <td>" . $rowConsulta[1] . "</td>
                                            <td>" . $rowConsulta[7] . "</td>
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;

        case 'completarCampos':
                $accion = new Accion();
                $accion->setNumeroIdentificacion($_POST['identificacion']);
                $resultado = $accion->ConsultarPersona();
                $respuesta = $accion->conn->obtenerObjeto();
        echo json_encode($respuesta);
        break;

    }
}
?>
