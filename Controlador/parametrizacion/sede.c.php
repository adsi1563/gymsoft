<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/sede.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setIdSede($_POST['idSede']);
                $accion->setNombre($_POST['nombre']);
                $accion->setTelefono($_POST['telefono']);
                $accion->setDireccion($_POST['direccion']);
                $accion->setIdCentroFormacion($_POST['idCentroFormacion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.".$e->getMessage();
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion= new Accion();
                $accion->setIdSede($_POST['idSede']);
                $accion->setNombre($_POST['nombre']);
                $accion->setTelefono($_POST['telefono']);
                $accion->setDireccion($_POST['direccion']);
                $accion->setIdCentroFormacion($_POST['idCentroFormacion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdSede($_POST['idSede']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdSede($_POST['idSede']);
                $accion->setNombre($_POST['nombre']);
                $accion->setTelefono($_POST['telefono']);
                $accion->setDireccion($_POST['direccion']);
                $accion->setIdCentroFormacion($_POST['idCentroFormacion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['idSede'] = $rowBuscar->idSede;
                        $respuesta['nombre'] = $rowBuscar->nombre;
                        $respuesta['telefono'] = $rowBuscar->telefono;
                        $respuesta['direccion'] = $rowBuscar->direccion;
                        $respuesta['idCentroFormacion'] = $rowBuscar->idCentroFormacion;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>

                                            <td><label id='idSede".$rowConsulta[0]."'>".$rowConsulta[0]."</label></td>                                                                
                                            <td><label id='nombre".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>
                                            <td><label id='telefono".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>
                                            <td><label id='direccion".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>
                                            <td><label id='idCentroFormacion".$rowConsulta[0]."'>".$rowConsulta[10]."</label></td>
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[5]."</label></td>
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_CENTRO_FORMACION':
                $accion = new Accion();
                $resultado = $accion->consultarCentroFormacion();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;
    }
}
?>
