<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/ficha.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setFicha($_POST['ficha']);
                $accion->setFechaInicio($_POST['fechaInicio']);
                $accion->setFechaFinLectiva($_POST['fechaFinLectiva']);
                $accion->setFechaFinProductiva($_POST['fechaFinProductiva']);
                $accion->setIdJornada($_POST['Jornada']);
                $accion->setIdAmbiente($_POST['Ambiente']);
                $accion->setIdProgramaFormacion($_POST['ProgramaFormacion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.".$e->getMessage();
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion= new Accion();
                $accion->setIdFicha($_POST['id']);
                $accion->setFicha($_POST['ficha']);
                $accion->setFechaInicio($_POST['fechaInicio']);
                $accion->setFechaFinLectiva($_POST['fechaFinLectiva']);
                $accion->setFechaFinProductiva($_POST['fechaFinProductiva']);
                $accion->setIdJornada($_POST['Jornada']);
                $accion->setIdAmbiente($_POST['Ambiente']);
                $accion->setIdProgramaFormacion($_POST['ProgramaFormacion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdFicha($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdFicha($_POST['id']);
                $accion->setFicha($_POST['ficha']);
                $accion->setFechaInicio($_POST['fechaInicio']);
                $accion->setFechaFinLectiva($_POST['fechaFinLectiva']);
                $accion->setFechaFinProductiva($_POST['fechaFinProductiva']);
                $accion->setIdJornada($_POST['Jornada']);
                $accion->setIdAmbiente($_POST['Ambiente']);
                $accion->setIdProgramaFormacion($_POST['ProgramaFormacion']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idFicha;
                        $respuesta['ficha'] = $rowBuscar->ficha;
                        $respuesta['fechaInicio'] = $rowBuscar->fechaInicio;
                        $respuesta['fechaFinLectiva'] = $rowBuscar->fechaFinLectiva;
                        $respuesta['fechaFinProductiva'] = $rowBuscar->fechaFinProductiva;
                        $respuesta['Jornada'] = $rowBuscar->idJornada;
                        $respuesta['Ambiente'] = $rowBuscar->idAmbiente;
                        $respuesta['ProgramaFormacion'] = $rowBuscar->idProgramaFormacion;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>

                                            <td><label id='ficha".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>                                                                
                                            <td><label id='fechaInicio".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>
                                            <td><label id='fechaFinLectiva".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>
                                            <td><label id='fechaFinProductiva".$rowConsulta[0]."'>".$rowConsulta[4]."</label></td>
                                            <td><label id='Jornada".$rowConsulta[0]."'>".$rowConsulta[13]."</label></td>
                                            <td><label id='Ambiente".$rowConsulta[0]."'>".$rowConsulta[14]."</label></td>
                                            <td><label id='ProgramaFormacion".$rowConsulta[0]."'>".$rowConsulta[15]."</label></td>
                                            <td><label id='estado".$rowConsulta[0]."'>".$rowConsulta[8]."</label></td>
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_JORNADA':
                $accion = new Accion();
                $resultado = $accion->consultarJornada();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_AMBIENTE':
                $accion = new Accion();
                $resultado = $accion->consultarAmbiente();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[2].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_PROGRAMA':
                $accion = new Accion();
                $resultado = $accion->consultarProrama();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[2].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;
    }
}
?>
