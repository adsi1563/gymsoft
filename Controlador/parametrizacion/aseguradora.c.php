<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/aseguradora.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setNit($_POST['nit']);
                $accion->setRazon($_POST['razon']);
                $accion->setDireccion($_POST['direccion']);
                $accion->setRepresentante($_POST['representante']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdAseguradora($_POST['id']);
                $accion->setNit($_POST['nit']);
                $accion->setRazon($_POST['razon']);
                $accion->setDireccion($_POST['direccion']);
                $accion->setRepresentante($_POST['representante']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdAseguradora($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdAseguradora($_POST['id']);
                $accion->setNit($_POST['nit']);
                $accion->setRazon($_POST['razon']);
                $accion->setDireccion($_POST['direccion']);
                $accion->setRepresentante($_POST['representante']);
                $accion->setEstado($_POST['estado']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idAseguradora;
                        $respuesta['nit'] = $rowBuscar->nit;
                        $respuesta['razon'] = $rowBuscar->razonSocial;
                        $respuesta['direccion'] = $rowBuscar->direccion;
                        $respuesta['representante'] = $rowBuscar->representanteLegal;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>                                            
                                            <td><label id='nit".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>                                                                                             
                                            <td><label id='razon".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>
                                            <td><label id='direccion".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>                                                                                             
                                            <td><label id='representante".$rowConsulta[0]."'>".$rowConsulta[4]."</label></td>
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;
    }
}
?>
