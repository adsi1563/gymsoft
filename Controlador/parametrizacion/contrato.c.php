<?php
require_once '../../entorno/conexion.php';
require '../../modelo/parametrizacion/contrato.m.php';
$respuesta = array();


if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setTipoContrato($_POST['tipoC']);
                $accion->setFechaInicio($_POST['fechaI']);
                $accion->setFechaFin($_POST['fechaF']);
                $accion->setEstado($_POST['estado']);

                if ($resultado = $accion->agregar()) {
                    echo "exito";
                } else {
                    echo "fallo";
                }
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.". $e->getMessage();
            	}

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdContrato($_POST['id']);
                $accion->setTipoContrato($_POST['tipoC']);
                $accion->setFechaInicio($_POST['fechaI']);
                $accion->setFechaFin($_POST['fechaF']);
                $accion->setEstado($_POST['estado']);

                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
                }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdContrato($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                
                $accion->setIdContrato($_POST['id']);
                $accion->setTipoContrato($_POST['tipoC']);
                $accion->setFechaInicio($_POST['fechaI']);
                $accion->setFechaFin($_POST['fechaF']);
                $accion->setEstado($_POST['estado']);

                $resultado = $accion->consultar();


                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();


                //var_dump($numeroRegistros); return 1;
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idContrato;
                        $respuesta['tipoC'] = $rowBuscar->tipoContrato;
                        $respuesta['codigo'] = $rowBuscar->fechaIngreso;
                        $respuesta['nombre'] = $rowBuscar->fechaFinalizacion;
                        $respuesta['estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table class='table' id='resultado'>";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>

                                            <td><label id='tipoCont".$rowConsulta[0]."'>".$rowConsulta[1]."</label></td>                                                                
                                            <td><label id='fechaIn".$rowConsulta[0]."'>".$rowConsulta[2]."</label></td>

                                            <td><label id='fechaFin".$rowConsulta[0]."'>".$rowConsulta[3]."</label></td>
                                            <td><label id='fechaFin".$rowConsulta[0]."'>".$rowConsulta[4]."</label></td>

                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo $e->getMessage();
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;
    }
}
?>
