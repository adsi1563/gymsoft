<?php
require_once '../../entorno/conexion.php';
require '../../modelo/persona/persona.m.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $accion= new Accion();
                $accion->setPrimerNombre($_POST['PrimerNombre']);
                $accion->setSegundoNombre($_POST['SegundoNombre']);
                $accion->setPrimerApellido($_POST['PrimerApellido']);
                $accion->setSegundoApellido($_POST['SegundoApellido']);
                $accion->setDocumento($_POST['Documento']);
                $accion->setRh($_POST['Rh']);
                $accion->setDireccion($_POST['Direccion']);
                $accion->setTelefono($_POST['Telefono']);
                $accion->setCorreo($_POST['Correo']);
                $accion->setEstado($_POST['Estado']);
                $accion->setIdTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setIdGimasio($_POST['gimnasio']);
                $accion->setIdEps($_POST['eps']);
                $accion->setCondicion($_POST['condicion']);
                $resultado = $accion->agregar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.".$e->getMessage();
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $accion = new Accion();
                $accion->setIdPersona($_POST['id']);
                $accion->setPrimerNombre($_POST['PrimerNombre']);
                $accion->setSegundoNombre($_POST['SegundoNombre']);
                $accion->setPrimerApellido($_POST['PrimerApellido']);
                $accion->setSegundoApellido($_POST['SegundoApellido']);
                $accion->setDocumento($_POST['Documento']);
                $accion->setRh($_POST['Rh']);
                $accion->setDireccion($_POST['Direccion']);
                $accion->setTelefono($_POST['Telefono']);
                $accion->setCorreo($_POST['Correo']);
                $accion->setIdTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setIdGimasio($_POST['gimnasio']);
                $accion->setIdEps($_POST['eps']);
                $resultado = $accion->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $accion = new Accion();
                $accion->setIdPersona($_POST['id']);
                $resultado = $accion->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.".$e->getCode();                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $accion->setIdPersona($_POST['id']);
                $accion->setPrimerNombre($_POST['PrimerNombre']);
                $accion->setSegundoNombre($_POST['SegundoNombre']);
                $accion->setPrimerApellido($_POST['PrimerApellido']);
                $accion->setSegundoApellido($_POST['SegundoApellido']);
                $accion->setDocumento($_POST['Documento']);
                $accion->setRh($_POST['Rh']);
                $accion->setDireccion($_POST['Direccion']);
                $accion->setTelefono($_POST['Telefono']);
                $accion->setCorreo($_POST['Correo']);
                $accion->setIdTipoIdentificacion($_POST['tipoIdentificacion']);
                $accion->setIdGimasio($_POST['gimnasio']);
                $accion->setIdEps($_POST['eps']);
                $resultado = $accion->consultar();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idPersona;
                        $respuesta['PrimerNombre'] = $rowBuscar->primerNombre;
                        $respuesta['SegundoNombre'] = $rowBuscar->segundoNombre;
                        $respuesta['PrimerApellido'] = $rowBuscar->primerApellido;
                        $respuesta['SegundoApellido'] = $rowBuscar->segundoApellido;
                        $respuesta['Documento'] = $rowBuscar->identificacion;
                        $respuesta['Rh'] = $rowBuscar->rh;
                        $respuesta['Direccion'] = $rowBuscar->direccion;
                        $respuesta['Telefono'] = $rowBuscar->telefono;
                        $respuesta['Correo'] = $rowBuscar->email;
                        $respuesta['Eps'] = $rowBuscar->idEps;
                        $respuesta['Gimnasio'] = $rowBuscar->idGimnasio;
                        $respuesta['TipoIdentificacion'] = $rowBuscar->idTipoIdentificacion;
                        $respuesta['Estado'] = $rowBuscar->estado;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="";
                        foreach($accion->conn->obtenerRegistros() AS $rowConsulta){
                            $retorno .= "<tr>

                                            <td><label id='nombreCompleto".$rowConsulta[0]."'>".$rowConsulta[1]." ".$rowConsulta[2]." ".$rowConsulta[3]." ".$rowConsulta[4]."</label></td>  
                                            <td><label id='tipoIdentificacion".$rowConsulta[0]."'>".$rowConsulta[10]."</label></td>
                                            <td><label id='Documento".$rowConsulta[0]."'>".$rowConsulta[5]."</label></td> 
                                            <td><label id='eps".$rowConsulta[0]."'>".$rowConsulta[12]."</label></td>
                                            <td><label id='gimnasio".$rowConsulta[0]."'>".$rowConsulta[11]."</label></td>
                                            <td><label id='Rh".$rowConsulta[0]."'>".$rowConsulta[6]."</label></td>
                                            <td><label id='Direccion".$rowConsulta[0]."'>".$rowConsulta[7]."</label></td> 
                                            <td><label id='Telefono".$rowConsulta[0]."'>".$rowConsulta[8]."</label></td> 
                                            <td><label id='Correo".$rowConsulta[0]."'>".$rowConsulta[9]."</label></td>                                                    
                                            <td>
                                                <button type='button' class='btn btn-primary' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'><img src='../../img/edit.svg' class='iconos'></button>

                                                <button type='button' class='btn btn-danger' name='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'><img src='../../img/delete.svg' class='iconos'></button>
                                            </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
        }
        //Retornar del retorno
        $respuesta['accion']='CONSULTAR';
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_TIPO_IDENTIFICACION':
                $accion = new Accion();
                $resultado = $accion->consultarIdentificacion();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[2].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_EPS':
                $accion = new Accion();
                $resultado = $accion->consultarEps();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_GIMNASIO':
                $accion = new Accion();
                $resultado = $accion->consultarGimnasio();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_ROL':
                $accion = new Accion();
                $resultado = $accion->consultarRol();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.$dato[1].'</option>';                            
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_CONDICIONES':
                $accion = new Accion();
                $resultado = $accion->consultarCondiciones();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.utf8_encode($dato[1]).'</option>';                      
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;

        case 'CONSULTA_ENFERMEDADES':
                $accion = new Accion();
                $accion->setIdCondicion($_POST['idCondicion']);
                $resultado = $accion->consultarEnfermedades();
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                $numeroRegistros;
                    if(isset($resultado)){
                        $retorno='<option value="">Seleccione..</option>';
                        foreach($accion->conn->obtenerRegistros() AS $dato){
                            $retorno .= '<option value="'.$dato[0].'">'.utf8_encode($dato[1]).'</option>';                      
                        }                                        
                        $respuesta=$retorno;
                        
                    }else{
                        $respuesta='No existen datos!!!';
                    }
        echo json_encode($respuesta);
        break;
    }
}
?>
