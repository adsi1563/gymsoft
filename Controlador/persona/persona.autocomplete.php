<?php
    require '../../modelo/persona/persona.m.php';
    require_once '../../entorno/conexion.php';
    $termino = $_GET["term"]; 

    $respuesta = Accion::readAutocompletar($termino);
    foreach ($respuesta as $key => $value) {
            $personas[] = array(
                "value" => $value[0],
                "label" => $value[1]
            );
        }
    echo json_encode($personas);
?>