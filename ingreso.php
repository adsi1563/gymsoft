<?php  
  session_start();
  session_destroy();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ingreso</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fonts/ionicons.min.css">
    <link rel="stylesheet" href="css/Login-Form-Clean.css">
</head>

<body class="login-clean">
    <div class="centrado">
        <form method="post">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><img src="img/logo4.png" width="150px"></div>
            <div class="form-group"><input class="form-control" type="text" name="txtUsuario" placeholder="Usuario" id="txtUsuario"></div>
            <div class="form-group"><input class="form-control" type="password" name="txtContrasenia" placeholder="Contraseña" id="txtContrasenia"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="button" onclick="iniciar()">Ingresar</button></div>
            <div class="form-group text-center"><a href="#" id="recuperar" name="recuperar">¿Olvidaste tu contraseña?</a></div>
        </form>
    </div>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/seguridad/ingreso.js"></script>

    <!-- MODAL Nueva Contraseña-->
    <div class="modal fade" id="nuevaContra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Actualizar Contraseña</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group"><input class="form-control" type="password" name="txtContrasenia2" placeholder="Contraseña nueva" id="txtContrasenia2" onkeypress=""></div>
            <div class="form-group"><input class="form-control" type="password" name="txtContrasenia3" placeholder="Repetir contraseña nueva" id="txtContrasenia3" disabled></div>
            <div class="" id="verificacion"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="actualizar" name="actualizar" disabled>Guardar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- MODAL Recuperar-->
    <div class="modal fade" id="modalEnviar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Recuperar Contraseña</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group"><h5>Por favor escriba su correo electronico: </h5></div>
            <div class="form-group"><input class="form-control" type="email" name="emailRecuperar" placeholder="Correo Electronico" id="emailRecuperar"></div>
            <div class="" id="verificacionCorreo"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="enviarEmail" name="enviarEmail" disabled>Enviar</button>
          </div>
        </div>
      </div>
    </div>
</body>

</html>

