<?php 
require 'noticia/functions/autoload_class.php';
require 'noticia/functions/blog/inicio.php';
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Noticias</title>

    <link href="noticia/css/bootstrap.min.css" rel="stylesheet">
    <link href="noticia/css/blog/blog.css" rel="stylesheet">

  </head>

  <body>
  <?php include 'noticia/navbar.php'; ?>
    <div class="container">

      <div class="page-header master">
        <h1 class="master">Bienvenido a noticias GymSoft </h1>
      </div>
      <div class="row">

        <div class="col-sm-8 blog-main">
           <?php echo getArticles();?>

          <nav>
            <ul class="pager">
              <li><a href="#">Anterior</a></li>
              <li><a href="#">Siguiente</a></li>
            </ul>
          </nav>

        </div><!-- /.blog-main -->
        <?php include_once('noticia/aside.php') ?>
      </div><!-- /.row -->

    </div><!-- /.container -->
    <?php include_once('noticia/footer.php') ?>
    
  </body>
</html>
