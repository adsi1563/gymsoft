-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-06-2019 a las 02:51:52
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_formativo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutina_alimenticia`
--

CREATE TABLE `rutina_alimenticia` (
  `idRutina` int(12) NOT NULL,
  `rutina` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `imcMenor` float NOT NULL,
  `imcMayor` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rutina_alimenticia`
--

INSERT INTO `rutina_alimenticia` (`idRutina`, `rutina`, `imcMenor`, `imcMayor`) VALUES
(1, 'Jugos y adicionar leche en polvo para utilizarlo como suplemento, comer alimentos  magros(pollo, pescado, atun, queso, arroz, frijoles), en el desayuno comer frutas y huevos, en las horas de las ma?ana poder comer yogurt o jugos que tengan leche con galletas o pan integral, noche algo de comida con una fruta y jugo.', 0, 15.99),
(2, 'Jugos en leche (leche en polvo para utilizarlo como suplemento) tambien puedes tomarlo en chocolates y tinto, comer 5 comidas al dia con cantidad proporcionada ejemplo ensalada y verduras,alimentos magros(pollo, pescado, atun, queso, arroz, frijoles)para bajar la comida puedes tomar jugos en agua y agregar miel, ingerir frutas y vegetales.\r\n', 16, 16.99),
(3, 'Desayuno : Jugos con leche, comer moderados alimen', 17, 18.49),
(4, 'comer 3 comidas al d?a con cantidad proporcionada ejemplo ensalada y verduras,alimentos	)para bajar la comida puedes tomar jugos en agua y agregar miel, ingerir frutas y vegetales, en las noches comer solo jugos y alimentos integrales.', 18.5, 24.99),
(7, '1. L?cteos: La leche y los yogures ser?n desnatados; los quesos, magros. Se aconsejan 2-3 raciones al d?a para asegurar un aporte adecuado de calcio.\r\n2. Carnes y pescados: Se retirar? la grasa visible antes de la cocci?n,en el caso de las aves, quitar toda la piel. Evitaremos alimentos ricos en grasa saturada, como los embutidos y el tocino. Se recomienda consumir pescado al menos tres veces a la semana.\r\n3. Frutas y verduras: Elige frutas crudas de consistencia firme, evitando las piezas cocid', 25, 29.99);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `rutina_alimenticia`
--
ALTER TABLE `rutina_alimenticia`
  ADD PRIMARY KEY (`idRutina`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `rutina_alimenticia`
--
ALTER TABLE `rutina_alimenticia`
  MODIFY `idRutina` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
