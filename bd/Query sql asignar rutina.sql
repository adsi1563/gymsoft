-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-06-2019 a las 21:12:41
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `GymSoft`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `idActividad` int(11) NOT NULL,
  `idGimnasio` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`idActividad`, `idGimnasio`, `idTipo`, `fechaInicio`, `fechaFin`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, 1, '2019-05-22', '2019-05-31', 'A', 1, 1, '2019-05-23 01:30:14', '2019-05-23 01:30:14'),
(2, 2, 2, '2019-05-23', '2019-05-28', 'A', 1, 1, '2019-05-23 01:28:34', '2019-05-23 01:28:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acudiente`
--

CREATE TABLE `acudiente` (
  `idAcudiente` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acudiente`
--

INSERT INTO `acudiente` (`idAcudiente`, `nombre`, `apellido`, `telefono`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Dilmer', 'Ramirez', '3023909182', 'A', 1, 1, '2019-05-22 23:58:51', '2019-05-22 23:56:16'),
(2, 'Liseth', 'Monroy', '3136100713', 'A', 1, 1, '2019-05-22 23:57:25', '2019-05-22 23:57:25');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `acudienteauto`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `acudienteauto` (
`idAcudiente` int(11)
,`nombreAcudiente` varchar(61)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambiente`
--

CREATE TABLE `ambiente` (
  `idAmbiente` int(11) NOT NULL,
  `codigo` int(5) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ambiente`
--

INSERT INTO `ambiente` (`idAmbiente`, `codigo`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, '209', 'A', 1, 1, '2019-05-13 20:17:07', '2019-05-13 20:17:07'),
(2, 2, '210', 'A', 1, 1, '2019-05-13 20:19:53', '2019-05-13 20:19:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz`
--

CREATE TABLE `aprendiz` (
  `idAprendiz` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idAcudiente` int(11) NOT NULL,
  `idImc` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `aprendiz`
--

INSERT INTO `aprendiz` (`idAprendiz`, `idUsuario`, `idAcudiente`, `idImc`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 3, 2, 1, 'A', 1, 1, '2019-05-23 21:10:48', '2019-05-23 21:10:48'),
(2, 5, 1, 2, 'A', 1, 1, '2019-05-23 21:11:01', '2019-05-23 21:11:01');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `aprendizauto`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `aprendizauto` (
`idAprendiz` int(11)
,`nombrePersona` varchar(123)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz_ficha`
--

CREATE TABLE `aprendiz_ficha` (
  `idAprendizFicha` int(11) NOT NULL,
  `idFicha` int(11) NOT NULL,
  `idAprendiz` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aprendiz_ficha`
--

INSERT INTO `aprendiz_ficha` (`idAprendizFicha`, `idFicha`, `idAprendiz`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, 1, 'A', 1, 1, '2019-05-23 23:05:28', '2019-05-23 23:05:28'),
(2, 2, 2, 'A', 1, 1, '2019-05-23 23:05:36', '2019-05-23 23:05:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aseguradora`
--

CREATE TABLE `aseguradora` (
  `idAseguradora` int(11) NOT NULL,
  `nit` varchar(15) NOT NULL,
  `razonSocial` varchar(50) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `representanteLegal` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aseguradora`
--

INSERT INTO `aseguradora` (`idAseguradora`, `nit`, `razonSocial`, `direccion`, `representanteLegal`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(3, '2664514', 'Super Tienda familiar', 'cra 1 # 22-16', 'Luz Mary Villegas', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '36304705', 'Boutique andrea', 'Clle 25A # 32-29', 'Andrea Caquimboo', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '2147483647', 'Helados Colombina', 'Toma 10 A', 'Cielo Correa', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '860026', 'ALLIANZ SEGUROS S.A.', 'Carrera 13A No. 29-24', 'David Alejandro Colmenares Spence', 'A', 1, 1, '2019-05-13 16:41:22', '2019-05-13 16:41:22'),
(10, '8002561619', 'ARL SURA', 'Calle 49A No. 63-55 Piso 9', 'Ivan Ignacio Zuluaga Latorre', 'A', 1, 1, '2019-05-13 17:02:43', '2019-05-13 17:02:43');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `autocompletar`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `autocompletar` (
`idPersona` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_formacion`
--

CREATE TABLE `centro_formacion` (
  `idCentroFormacion` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `idAseguradora` int(11) NOT NULL,
  `idRegional` int(11) NOT NULL,
  `idCiudad` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `centro_formacion`
--

INSERT INTO `centro_formacion` (`idCentroFormacion`, `nombre`, `idAseguradora`, `idRegional`, `idCiudad`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(2, 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS', 10, 1, 30, 'A', 1, 1, '2019-05-13 19:06:22', '2019-05-13 19:06:22'),
(3, 'CENTRO DE FORMACIÃ“N AGROINDUSTRIAL', 7, 1, 30, 'A', 1, 1, '2019-05-13 19:07:19', '2019-05-13 19:07:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `idCiudad` int(11) NOT NULL,
  `codigo` int(4) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `idDepartamento` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaModificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idCiudad`, `codigo`, `nombre`, `idDepartamento`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(11, 210, 'Mexico', 1, 'I', 1, 1, '0000-00-00', '0000-00-00'),
(25, 102, 'Dorada', 11, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(26, 128, 'Cartagena', 31, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(27, 134, 'Barranquilla', 8, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(29, 145, 'cali', 3, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(30, 200, 'Neiva', 1, 'A', 1, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE `contrato` (
  `idContrato` int(11) NOT NULL,
  `tipoContrato` varchar(30) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `fechaFinalizacion` date NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contrato`
--

INSERT INTO `contrato` (`idContrato`, `tipoContrato`, `fechaIngreso`, `fechaFinalizacion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, '1 aÃ±o', '2019-05-01', '2020-05-01', 'A', 1, 1, '2019-05-17 23:39:09', '2019-05-17 23:39:09'),
(2, '3 meses', '2019-05-01', '2019-08-31', 'A', 1, 1, '2019-05-17 23:39:59', '2019-05-17 23:39:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `idDepartamento` int(11) NOT NULL,
  `codigo` int(3) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`idDepartamento`, `codigo`, `nombre`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 110, 'Huila', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 'NariÃ±o', 'A', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 3, 'Cundinamarca', 'A', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 4, 'Meta', 'A', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 953, 'Caqueta', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 5, 'Antioquia', 'A', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 197, 'Balle', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 598, 'Santander', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 1, 'Huila', 'A', 1, 1, '2019-05-09 00:25:20', '2019-05-09 00:25:20'),
(31, 6, 'AtlÃ¡ntico', 'A', 1, 1, '2019-05-13 18:58:02', '2019-05-13 18:58:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenador`
--

CREATE TABLE `entrenador` (
  `idEntrenador` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entrenador`
--

INSERT INTO `entrenador` (`idEntrenador`, `idPersona`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 2, 'A', 1, 1, '2019-05-22 18:37:55', '2019-05-22 18:37:55'),
(2, 4, 'A', 1, 1, '2019-05-22 18:38:07', '2019-05-22 18:38:07');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `entrenadorvista`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `entrenadorvista` (
`idEntrenador` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenador_sede`
--

CREATE TABLE `entrenador_sede` (
  `idEntrenadorSede` int(11) NOT NULL,
  `idEntrenador` int(11) NOT NULL,
  `idSede` int(11) NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entrenador_sede`
--

INSERT INTO `entrenador_sede` (`idEntrenadorSede`, `idEntrenador`, `idSede`, `horaInicio`, `horaFin`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(7, 1, 1, '08:00:00', '10:00:00', 'A', 1, 1, '2019-05-22 22:01:24', '2019-05-22 22:01:24'),
(8, 2, 2, '18:00:00', '20:00:00', 'A', 1, 1, '2019-05-22 22:43:21', '2019-05-22 22:43:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE `eps` (
  `idEps` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `eps`
--

INSERT INTO `eps` (`idEps`, `nombre`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(2, 'Medimas', 'A', 1, 1, '2019-05-13 19:59:59', '2019-05-13 19:45:54'),
(3, 'NuevaEps', 'A', 1, 1, '2019-05-13 20:02:47', '2019-05-13 20:02:31'),
(4, 'Salud Total', 'A', 1, 1, '2019-05-13 20:04:07', '2019-05-13 20:04:07'),
(5, 'Sanitas', 'A', 1, 1, '2019-05-19 22:28:47', '2019-05-19 22:28:47'),
(6, 'Comfamiliar', 'A', 1, 1, '2019-05-19 22:52:02', '2019-05-19 22:52:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `idEquipo` int(11) NOT NULL,
  `idGimnasio` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `serial` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaIngreso` date NOT NULL,
  `idMarca` int(11) NOT NULL,
  `idManualUsuario` int(11) DEFAULT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha`
--

CREATE TABLE `ficha` (
  `idFicha` int(11) NOT NULL,
  `ficha` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinLectiva` date NOT NULL,
  `fechaFinProductiva` date NOT NULL,
  `idJornada` int(11) NOT NULL,
  `idAmbiente` int(11) NOT NULL,
  `idProgramaFormacion` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ficha`
--

INSERT INTO `ficha` (`idFicha`, `ficha`, `fechaInicio`, `fechaFinLectiva`, `fechaFinProductiva`, `idJornada`, `idAmbiente`, `idProgramaFormacion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1563193, '2017-12-11', '2019-06-12', '2019-12-13', 3, 1, 2, 'A', 1, 1, '2019-05-21 19:13:16', '2019-05-21 18:50:28'),
(2, 1563194, '2018-06-12', '2019-12-11', '2020-06-12', 1, 2, 2, 'A', 1, 1, '2019-05-21 18:51:39', '2019-05-21 18:51:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gimnasio`
--

CREATE TABLE `gimnasio` (
  `idGimnasio` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `idSede` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gimnasio`
--

INSERT INTO `gimnasio` (`idGimnasio`, `nombre`, `idSede`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Gimnasio Industria', 1, 'A', 1, 1, '2019-05-17 19:36:25', '2019-05-17 19:36:25'),
(2, 'Gimnasio Comercio', 2, 'A', 1, 1, '2019-05-17 19:38:44', '2019-05-17 19:38:44'),
(3, 'Gimnasio Tecnoparque', 3, 'A', 1, 1, '2019-05-17 19:42:23', '2019-05-21 16:43:31'),
(4, 'Agroindustrial', 1, 'A', 1, 1, '2019-05-21 16:39:38', '2019-05-21 16:39:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imc`
--

CREATE TABLE `imc` (
  `idImc` int(11) NOT NULL,
  `estatura` float NOT NULL,
  `peso` float NOT NULL,
  `calculoImc` float NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imc`
--

INSERT INTO `imc` (`idImc`, `estatura`, `peso`, `calculoImc`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 176, 81, 26.1493, 'A', 1, 1, '2019-05-23 17:41:56', '2019-05-23 17:41:56'),
(2, 170, 70, 24.2215, 'A', 1, 1, '2019-05-23 17:44:02', '2019-05-23 17:44:02'),
(3, 1.7, 90, 40, 'A', 0, 0, '2019-06-04 01:24:35', '2019-06-04 01:24:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor`
--

CREATE TABLE `instructor` (
  `idInstructor` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idContrato` int(11) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCracion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `instructor`
--

INSERT INTO `instructor` (`idInstructor`, `idUsuario`, `estado`, `idContrato`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCracion`, `fechaModificacion`) VALUES
(2, 3, 'A', 1, 1, 3, '2019-05-21 15:25:02', '2019-05-21 15:25:02'),
(3, 5, 'A', 2, 1, 1, '2019-05-21 15:25:55', '2019-05-21 15:25:55');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `instructordeficha`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `instructordeficha` (
`idInstructor` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `instructornombre`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `instructornombre` (
`idUsuario` int(11)
,`idPersona` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor_ficha`
--

CREATE TABLE `instructor_ficha` (
  `idInstructorFicha` int(11) NOT NULL,
  `idFicha` int(11) NOT NULL,
  `idInstructor` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `instructor_ficha`
--

INSERT INTO `instructor_ficha` (`idInstructorFicha`, `idFicha`, `idInstructor`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, 2, 'A', 1, 1, '2019-05-21 23:25:22', '2019-05-22 00:51:45'),
(2, 2, 3, 'A', 1, 1, '2019-05-21 23:29:05', '2019-05-21 23:29:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE `jornada` (
  `idJornada` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `jornada`
--

INSERT INTO `jornada` (`idJornada`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'MaÃ±ana', 'A', 1, 1, '2019-05-21 15:55:37', '2019-05-21 15:55:37'),
(2, 'Tarde', 'A', 1, 1, '2019-05-21 15:55:43', '2019-05-21 15:55:43'),
(3, 'Noche', 'A', 1, 1, '2019-05-21 16:06:09', '2019-05-21 16:06:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manual_usuario`
--

CREATE TABLE `manual_usuario` (
  `idManualUsuario` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idMarca` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idMarca`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Adidas', 'A', 1, 1, '2019-05-17 18:24:43', '2019-05-17 18:24:43'),
(2, 'Bkool', 'A', 1, 1, '2019-05-17 18:25:00', '2019-05-17 18:25:00'),
(3, '', '', 1, 1, '2019-05-20 21:59:23', '2019-05-20 21:59:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_programa`
--

CREATE TABLE `nivel_programa` (
  `idNivelPrograma` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `nivel_programa`
--

INSERT INTO `nivel_programa` (`idNivelPrograma`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'TÃ©cnico', 'A', 1, 1, '2019-05-13 20:53:08', '2019-05-13 20:53:08'),
(2, 'TecnÃ³logo', 'A', 1, 1, '2019-05-13 20:53:40', '2019-05-13 20:53:40'),
(4, 'Complementario', 'A', 1, 1, '2019-05-13 21:49:42', '2019-05-13 21:49:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `primerNombre` varchar(30) NOT NULL,
  `segundoNombre` varchar(30) NOT NULL,
  `primerApellido` varchar(30) NOT NULL,
  `segundoApellido` varchar(30) NOT NULL,
  `identificacion` varchar(11) NOT NULL,
  `rh` varchar(4) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idGimnasio` int(11) NOT NULL,
  `idEps` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idPersona`, `primerNombre`, `segundoNombre`, `primerApellido`, `segundoApellido`, `identificacion`, `rh`, `direccion`, `telefono`, `email`, `idTipoIdentificacion`, `idGimnasio`, `idEps`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Fabian', 'Camilo', 'Cano', 'Clavijo', '1016010678', 'A+', 'Calle 56 #8-51p', '3155458449', 'fccano87@misena.edu.co', 1, 1, 3, '', 1, 1, '2019-05-19 22:54:04', '2019-05-19 22:54:04'),
(2, 'Sergio', 'Eduardo', 'Mosquera', 'Villegas', '1117818286', 'A+', 'Calle 25A sur #32-29', '3192120941', 'semosquera68@misena.edu.co', 1, 1, 5, '', 1, 1, '2019-05-19 22:29:04', '2019-05-19 22:29:04'),
(4, 'Maria de', 'Los Angles', 'Cabrera', 'Perdomo', '1075313263', 'O+', 'Calle 21 #53-84', '3214388190', 'mdcabrera36@misena.edu.co', 1, 1, 6, '', 1, 1, '2019-05-19 22:55:48', '2019-05-19 22:55:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa_formacion`
--

CREATE TABLE `programa_formacion` (
  `idProgramaFormacion` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `duracion` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `idNivelPrograma` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `programa_formacion`
--

INSERT INTO `programa_formacion` (`idProgramaFormacion`, `codigo`, `descripcion`, `duracion`, `idNivelPrograma`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(2, 1, ' AnÃ¡lisis y Desarrollo de Sistemas de InformaciÃ³n', '24', 2, 'A', 1, 1, '2019-05-13 23:32:29', '2019-05-13 21:40:08'),
(3, 2, 'Multimedia', '12', 1, 'A', 1, 1, '2019-05-13 21:40:29', '2019-05-13 21:40:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regional`
--

CREATE TABLE `regional` (
  `idRegional` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo` int(20) NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `regional`
--

INSERT INTO `regional` (`idRegional`, `nombre`, `codigo`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'SENA Huila', 100, 'Zona Andina', 'A', 1, 1, '2019-05-13 17:11:16', '2019-05-13 17:18:12'),
(2, 'SENA Cundinamarca', 101, 'Zona Andina', 'A', 1, 1, '2019-05-13 17:11:40', '2019-05-13 17:19:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idRol`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Administrador', 'A', 1, 1, '2019-05-17 21:08:50', '2019-05-17 21:08:50'),
(2, 'Aprendiz', 'A', 1, 1, '2019-05-17 21:08:57', '2019-05-17 21:08:57'),
(3, 'Instructor', 'A', 1, 1, '2019-05-17 21:09:04', '2019-05-17 21:09:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutina`
--

CREATE TABLE `rutina` (
  `idRutina` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutina_usuario`
--

CREATE TABLE `rutina_usuario` (
  `id` int(11) NOT NULL,
  `idUsuario` varchar(200) NOT NULL,
  `hora` varchar(20) NOT NULL,
  `rutina` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rutina_usuario`
--

INSERT INTO `rutina_usuario` (`id`, `idUsuario`, `hora`, `rutina`) VALUES
(1, 'fccano87', '4_8pm', 'tonificar'),
(2, 'fccano87', 'seleccione', 'cardio'),
(3, 'fccano87', 'seleccione', 'cardio'),
(4, 'fccano87', 'seleccione', 'cardio'),
(5, 'fccano87', 'seleccione', 'cardio'),
(6, 'fccano87', 'seleccione', 'cardio'),
(7, 'fccano87', 'seleccione', 'cardio'),
(8, 'fccano87', 'seleccione', 'cardio'),
(9, 'fccano87', 'seleccione', 'cardio'),
(10, 'fccano87', 'seleccione', 'cardio'),
(11, 'fccano87', 'seleccione', 'cardio'),
(12, 'diego', '4_8pm', 'ganancia_muscular'),
(13, 'diego', '4_8pm', 'ganancia_muscular'),
(14, 'diego', '4_8pm', 'ganancia_muscular'),
(15, 'diego', '4_8pm', 'ganancia_muscular'),
(16, 'diego', '4_8pm', 'ganancia_muscular'),
(17, 'diego', '4_8pm', 'ganancia_muscular'),
(18, 'diego', '4_8pm', 'ganancia_muscular'),
(19, 'diego', '4_8pm', 'ganancia_muscular'),
(20, 'diego', '4_8pm', 'ganancia_muscular'),
(21, 'diego', '4_8pm', 'ganancia_muscular'),
(22, 'diego', '4_8pm', 'ganancia_muscular'),
(23, 'diego', '4_8pm', 'ganancia_muscular'),
(24, 'diego', '4_8pm', 'ganancia_muscular'),
(25, 'diego', '4_8pm', 'ganancia_muscular'),
(26, 'diego', '4_8pm', 'ganancia_muscular'),
(27, 'diego', '4_8pm', 'ganancia_muscular'),
(28, 'diego', '4_8pm', 'ganancia_muscular'),
(29, 'diego', '4_8pm', 'ganancia_muscular'),
(30, 'diego', '4_8pm', 'ganancia_muscular'),
(31, 'diego', '4_8pm', 'ganancia_muscular'),
(32, 'diego', '4_8pm', 'ganancia_muscular'),
(33, 'diego', '4_8pm', 'ganancia_muscular'),
(34, 'fccano87', 'seleccione', 'seleccione'),
(35, 'fccano87', 'seleccione', 'seleccione'),
(36, 'fccano87', 'seleccione', 'seleccione'),
(37, 'fccano87', 'seleccione', 'seleccione'),
(38, 'fccano87', 'seleccione', 'seleccione'),
(39, 'fccano87', 'seleccione', 'seleccione'),
(40, 'fccano87', 'seleccione', 'seleccione'),
(41, 'fccano87', 'seleccione', 'seleccione'),
(42, 'fccano87', 'seleccione', 'seleccione'),
(43, 'fccano87', 'seleccione', 'seleccione'),
(44, 'fccano87', 'seleccione', 'seleccione'),
(45, 'fccano87', 'seleccione', 'seleccione'),
(46, 'fccano87', 'seleccione', 'seleccione'),
(47, 'fccano87', 'seleccione', 'seleccione'),
(48, 'fccano87', 'seleccione', 'seleccione'),
(49, 'fccano87', 'seleccione', 'seleccione'),
(50, 'fccano87', 'seleccione', 'seleccione'),
(51, 'fccano87', 'seleccione', 'seleccione'),
(52, 'fccano87', 'seleccione', 'seleccione'),
(53, 'fccano87', 'seleccione', 'seleccione'),
(54, 'fccano87', 'seleccione', 'seleccione'),
(55, 'fccano87', 'seleccione', 'seleccione'),
(56, 'fccano87', 'seleccione', 'seleccione'),
(57, 'diego', '8_10am', 'tonificar'),
(58, 'semosquera', 'seleccione', 'ganancia_muscular'),
(59, 'semosquera', 'seleccione', 'ganancia_muscular'),
(60, 'semosquera', 'seleccione', 'ganancia_muscular'),
(61, 'semosquera', 'seleccione', 'ganancia_muscular'),
(62, 'semosquera', 'seleccione', 'ganancia_muscular'),
(63, 'semosquera', 'seleccione', 'ganancia_muscular'),
(64, 'semosquera', 'seleccione', 'ganancia_muscular'),
(65, 'semosquera', 'seleccione', 'ganancia_muscular'),
(66, 'semosquera', 'seleccione', 'ganancia_muscular'),
(67, 'semosquera', 'seleccione', 'ganancia_muscular'),
(68, 'semosquera', 'seleccione', 'ganancia_muscular'),
(69, 'semosquera', 'seleccione', 'ganancia_muscular'),
(70, 'semosquera', 'seleccione', 'ganancia_muscular'),
(71, 'semosquera', 'seleccione', 'ganancia_muscular'),
(72, 'semosquera', 'seleccione', 'ganancia_muscular'),
(73, 'semosquera', 'seleccione', 'ganancia_muscular'),
(74, 'semosquera', 'seleccione', 'ganancia_muscular'),
(75, 'semosquera', 'seleccione', 'ganancia_muscular'),
(76, 'semosquera', 'seleccione', 'ganancia_muscular'),
(77, 'semosquera', 'seleccione', 'ganancia_muscular'),
(78, 'semosquera', 'seleccione', 'ganancia_muscular'),
(79, 'semosquera', 'seleccione', 'ganancia_muscular'),
(80, 'semosquera', 'seleccione', 'ganancia_muscular'),
(81, 'semosquera', 'seleccione', 'ganancia_muscular'),
(82, 'semosquera', 'seleccione', 'ganancia_muscular'),
(83, 'semosquera', 'seleccione', 'ganancia_muscular'),
(84, 'semosquera', 'seleccione', 'ganancia_muscular'),
(85, 'semosquera', 'seleccione', 'ganancia_muscular'),
(86, 'semosquera', 'seleccione', 'ganancia_muscular'),
(87, 'semosquera', 'seleccione', 'ganancia_muscular'),
(88, 'semosquera', 'seleccione', 'ganancia_muscular'),
(89, 'semosquera', 'seleccione', 'ganancia_muscular'),
(90, 'semosquera', 'seleccione', 'ganancia_muscular'),
(91, 'semosquera', 'seleccione', 'ganancia_muscular'),
(92, 'semosquera', 'seleccione', 'ganancia_muscular'),
(93, 'semosquera', 'seleccione', 'ganancia_muscular'),
(94, 'semosquera', 'seleccione', 'ganancia_muscular'),
(95, 'semosquera', 'seleccione', 'ganancia_muscular'),
(96, 'semosquera', 'seleccione', 'ganancia_muscular'),
(97, 'semosquera', 'seleccione', 'ganancia_muscular'),
(98, 'semosquera', 'seleccione', 'ganancia_muscular'),
(99, 'semosquera', 'seleccione', 'ganancia_muscular'),
(100, 'semosquera', 'seleccione', 'ganancia_muscular'),
(101, 'semosquera', 'seleccione', 'ganancia_muscular'),
(102, 'diego', 'seleccione', 'tonificar'),
(103, 'diego', 'seleccione', 'tonificar'),
(104, 'diego', 'seleccione', 'tonificar'),
(105, 'semosquera', 'seleccione', 'cardio'),
(106, 'semosquera', 'seleccione', 'cardio'),
(107, 'fccano87', 'seleccione', 'seleccione'),
(108, 'fccano87', 'seleccione', 'seleccione'),
(109, 'fccano87', 'seleccione', 'seleccione'),
(110, 'fccano87', 'seleccione', 'seleccione'),
(111, 'fccano87', 'seleccione', 'seleccione'),
(112, 'fccano87', 'seleccione', 'seleccione'),
(113, 'fccano87', 'seleccione', 'seleccione'),
(114, 'fccano87', 'seleccione', 'seleccione'),
(115, 'fccano87', 'seleccione', 'seleccione'),
(116, 'diego', 'seleccione', 'ganancia_muscular'),
(117, 'diego', '4_8pm', 'tonificar'),
(118, 'semosquera', 'seleccione', 'cardio'),
(119, 'diego', 'seleccione', 'cardio'),
(120, 'diego', 'seleccione', 'cardio'),
(121, 'diego', 'seleccione', 'cardio'),
(122, 'diego', 'seleccione', 'cardio'),
(123, 'diego', 'seleccione', 'cardio'),
(124, 'diego', 'seleccione', 'cardio'),
(125, 'diego', 'seleccione', 'cardio'),
(126, 'diego', 'seleccione', 'cardio'),
(127, 'diego', 'seleccione', 'cardio'),
(128, 'diego', 'seleccione', 'cardio'),
(129, 'diego', 'seleccione', 'cardio'),
(130, 'diego', 'seleccione', 'cardio'),
(131, 'diego', 'seleccione', 'cardio'),
(132, 'diego', 'seleccione', 'cardio'),
(133, 'diego', 'seleccione', 'cardio'),
(134, 'diego', 'seleccione', 'cardio'),
(135, 'diego', 'seleccione', 'cardio'),
(136, 'diego', 'seleccione', 'cardio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sede`
--

CREATE TABLE `sede` (
  `idSede` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` int(10) NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `idCentroFormacion` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `sede`
--

INSERT INTO `sede` (`idSede`, `nombre`, `telefono`, `direccion`, `idCentroFormacion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Industrial', 8764401, 'Carrera 9 No. 68 - 50', 2, 'A', 1, 1, '2019-05-21 16:50:53', '2019-05-21 16:50:53'),
(2, 'Comercio', 8757040, 'Kra. 5 No. 16 - 16', 2, 'A', 1, 1, '2019-05-13 19:29:11', '2019-05-13 19:29:11'),
(3, 'TecnoParque', 8670078, 'Diagonal 20 NÂº 38 -16', 2, 'A', 1, 1, '2019-05-21 16:43:03', '2019-05-21 16:43:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(80) NOT NULL,
  `tiempoActividad` varchar(30) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaModificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idTipo`, `nombre`, `descripcion`, `tiempoActividad`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'TOTAL TRAINING', 'TONIFICA Y PIERDE GRASA DE FORMA GLOBAL', '60 Minutos', 'A', 1, 1, '2019-05-22', '2019-05-22'),
(2, 'ESPALDA SANA', 'TRABAJO ESPECÃFICO PARA ALIVIAR Y PREVENIR LOS DOLORES DE ESPALDA', '60 Minutos', 'A', 1, 1, '2019-05-22', '2019-05-22'),
(3, 'SPIN', 'LA MÃS EXIGENTE DE LAS MODALIDADES, PON TUS PIERNAS A PRUEBA', '60 Minutos', 'A', 1, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_identificacion`
--

CREATE TABLE `tipo_identificacion` (
  `idTipoIdentificacion` int(11) NOT NULL,
  `tipo` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo_identificacion`
--

INSERT INTO `tipo_identificacion` (`idTipoIdentificacion`, `tipo`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'CC', 'CÃ©dula de CiudadanÃ­a', 'A', 1, 1, '2019-05-13 16:28:57', '2019-05-13 16:28:57'),
(2, 'TI', 'Tarjeta de Identidad', 'A', 1, 1, '2019-05-13 16:16:38', '2019-05-13 16:16:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `usuario` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasenia` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idImc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `usuario`, `contrasenia`, `estado`, `idPersona`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `idImc`) VALUES
(3, 'fccano87', '25d55ad283aa400af464c76d713c07ad', 'A', 1, 1, 1, '2019-05-20 00:19:25', '2019-05-20 00:19:25', 1),
(5, 'semosquera', '25d55ad283aa400af464c76d713c07ad', 'A', 2, 1, 1, '2019-05-20 00:20:05', '2019-05-20 00:20:05', 2),
(6, 'diego', '25d55ad283aa400af464c76d713c07ad', 'A', 4, 1, 1, '2019-05-24 00:23:41', '2019-05-24 00:23:41', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_rol`
--

CREATE TABLE `usuario_rol` (
  `idUsuarioRol` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idRol` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_rol`
--

INSERT INTO `usuario_rol` (`idUsuarioRol`, `idUsuario`, `idRol`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 6, 2, 'A', 1, 1, '2019-05-24 00:30:24', '2019-05-24 00:30:24'),
(2, 5, 3, 'A', 1, 1, '2019-05-24 00:30:35', '2019-05-24 00:30:35');

-- --------------------------------------------------------

--
-- Estructura para la vista `acudienteauto`
--
DROP TABLE IF EXISTS `acudienteauto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `acudienteauto`  AS  (select `acudiente`.`idAcudiente` AS `idAcudiente`,concat(`acudiente`.`nombre`,' ',`acudiente`.`apellido`) AS `nombreAcudiente` from `acudiente`) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `aprendizauto`
--
DROP TABLE IF EXISTS `aprendizauto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aprendizauto`  AS  (select `aprendiz`.`idAprendiz` AS `idAprendiz`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `nombrePersona` from ((`aprendiz` join `usuario` on((`aprendiz`.`idUsuario` = `usuario`.`idUsuario`))) join `persona` on((`usuario`.`idPersona` = `persona`.`idPersona`)))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `autocompletar`
--
DROP TABLE IF EXISTS `autocompletar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `autocompletar`  AS  (select `persona`.`idPersona` AS `idPersona`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from `persona`) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `entrenadorvista`
--
DROP TABLE IF EXISTS `entrenadorvista`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `entrenadorvista`  AS  (select `entrenador`.`idEntrenador` AS `idEntrenador`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from (`entrenador` join `persona` on((`persona`.`idPersona` = `entrenador`.`idPersona`)))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `instructordeficha`
--
DROP TABLE IF EXISTS `instructordeficha`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `instructordeficha`  AS  (select `instructor`.`idInstructor` AS `idInstructor`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from ((`instructor` join `usuario` on((`usuario`.`idUsuario` = `instructor`.`idUsuario`))) join `persona` on((`persona`.`idPersona` = `usuario`.`idPersona`)))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `instructornombre`
--
DROP TABLE IF EXISTS `instructornombre`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `instructornombre`  AS  (select `usuario`.`idUsuario` AS `idUsuario`,`usuario`.`idPersona` AS `idPersona`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from (`usuario` join `persona` on((`usuario`.`idPersona` = `persona`.`idPersona`)))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`idActividad`),
  ADD KEY `id_gimnasio` (`idGimnasio`),
  ADD KEY `id_tipo` (`idTipo`),
  ADD KEY `id_fecha` (`fechaInicio`);

--
-- Indices de la tabla `acudiente`
--
ALTER TABLE `acudiente`
  ADD PRIMARY KEY (`idAcudiente`);

--
-- Indices de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`idAmbiente`);

--
-- Indices de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD PRIMARY KEY (`idAprendiz`),
  ADD KEY `id_usuario` (`idUsuario`),
  ADD KEY `id_persona` (`idAcudiente`),
  ADD KEY `id_imc` (`idImc`);

--
-- Indices de la tabla `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  ADD PRIMARY KEY (`idAprendizFicha`),
  ADD KEY `id_ficha` (`idFicha`),
  ADD KEY `id_aprendiz` (`idAprendiz`);

--
-- Indices de la tabla `aseguradora`
--
ALTER TABLE `aseguradora`
  ADD PRIMARY KEY (`idAseguradora`),
  ADD UNIQUE KEY `nit` (`nit`);

--
-- Indices de la tabla `centro_formacion`
--
ALTER TABLE `centro_formacion`
  ADD PRIMARY KEY (`idCentroFormacion`),
  ADD KEY `id_aseguradora` (`idAseguradora`),
  ADD KEY `id_ciudad` (`idCiudad`),
  ADD KEY `id_regional` (`idRegional`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idCiudad`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `id_departamento` (`idDepartamento`);

--
-- Indices de la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`idContrato`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`idDepartamento`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `entrenador`
--
ALTER TABLE `entrenador`
  ADD PRIMARY KEY (`idEntrenador`),
  ADD KEY `idPersona` (`idPersona`);

--
-- Indices de la tabla `entrenador_sede`
--
ALTER TABLE `entrenador_sede`
  ADD PRIMARY KEY (`idEntrenadorSede`),
  ADD KEY `idEntrenador` (`idEntrenador`),
  ADD KEY `idSede` (`idSede`);

--
-- Indices de la tabla `eps`
--
ALTER TABLE `eps`
  ADD PRIMARY KEY (`idEps`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`idEquipo`),
  ADD KEY `id_gimnasio` (`idGimnasio`),
  ADD KEY `id_marca` (`idMarca`),
  ADD KEY `idManualUsuario` (`idManualUsuario`);

--
-- Indices de la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`idFicha`),
  ADD KEY `id_jornada` (`idJornada`),
  ADD KEY `id_ambiente` (`idAmbiente`),
  ADD KEY `id_programa_formacion` (`idProgramaFormacion`);

--
-- Indices de la tabla `gimnasio`
--
ALTER TABLE `gimnasio`
  ADD PRIMARY KEY (`idGimnasio`),
  ADD KEY `id_sede` (`idSede`);

--
-- Indices de la tabla `imc`
--
ALTER TABLE `imc`
  ADD PRIMARY KEY (`idImc`);

--
-- Indices de la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`idInstructor`),
  ADD KEY `id_usuario` (`idUsuario`),
  ADD KEY `id_persona` (`estado`),
  ADD KEY `idContrato` (`idContrato`);

--
-- Indices de la tabla `instructor_ficha`
--
ALTER TABLE `instructor_ficha`
  ADD PRIMARY KEY (`idInstructorFicha`),
  ADD KEY `id_ficha` (`idFicha`),
  ADD KEY `id_instructor` (`idInstructor`);

--
-- Indices de la tabla `jornada`
--
ALTER TABLE `jornada`
  ADD PRIMARY KEY (`idJornada`);

--
-- Indices de la tabla `manual_usuario`
--
ALTER TABLE `manual_usuario`
  ADD PRIMARY KEY (`idManualUsuario`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idMarca`);

--
-- Indices de la tabla `nivel_programa`
--
ALTER TABLE `nivel_programa`
  ADD PRIMARY KEY (`idNivelPrograma`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD KEY `id_tipo_identificacion` (`idTipoIdentificacion`),
  ADD KEY `id_gimnasio` (`idGimnasio`),
  ADD KEY `id_eps` (`idEps`);

--
-- Indices de la tabla `programa_formacion`
--
ALTER TABLE `programa_formacion`
  ADD PRIMARY KEY (`idProgramaFormacion`),
  ADD KEY `id_nivel_programa` (`idNivelPrograma`);

--
-- Indices de la tabla `regional`
--
ALTER TABLE `regional`
  ADD PRIMARY KEY (`idRegional`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `rutina_usuario`
--
ALTER TABLE `rutina_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sede`
--
ALTER TABLE `sede`
  ADD PRIMARY KEY (`idSede`),
  ADD KEY `id_centro_formacion` (`idCentroFormacion`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indices de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  ADD PRIMARY KEY (`idTipoIdentificacion`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD UNIQUE KEY `idImc` (`idImc`) USING BTREE,
  ADD KEY `Id_Persona` (`idPersona`);

--
-- Indices de la tabla `usuario_rol`
--
ALTER TABLE `usuario_rol`
  ADD PRIMARY KEY (`idUsuarioRol`),
  ADD KEY `id_usuario` (`idUsuario`),
  ADD KEY `id_rol` (`idRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `idActividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `acudiente`
--
ALTER TABLE `acudiente`
  MODIFY `idAcudiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ambiente`
--
ALTER TABLE `ambiente`
  MODIFY `idAmbiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  MODIFY `idAprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  MODIFY `idAprendizFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `aseguradora`
--
ALTER TABLE `aseguradora`
  MODIFY `idAseguradora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `centro_formacion`
--
ALTER TABLE `centro_formacion`
  MODIFY `idCentroFormacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `contrato`
--
ALTER TABLE `contrato`
  MODIFY `idContrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `idDepartamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `entrenador`
--
ALTER TABLE `entrenador`
  MODIFY `idEntrenador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `entrenador_sede`
--
ALTER TABLE `entrenador_sede`
  MODIFY `idEntrenadorSede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `eps`
--
ALTER TABLE `eps`
  MODIFY `idEps` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `equipo`
--
ALTER TABLE `equipo`
  MODIFY `idEquipo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ficha`
--
ALTER TABLE `ficha`
  MODIFY `idFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `gimnasio`
--
ALTER TABLE `gimnasio`
  MODIFY `idGimnasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `imc`
--
ALTER TABLE `imc`
  MODIFY `idImc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `instructor`
--
ALTER TABLE `instructor`
  MODIFY `idInstructor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `instructor_ficha`
--
ALTER TABLE `instructor_ficha`
  MODIFY `idInstructorFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `jornada`
--
ALTER TABLE `jornada`
  MODIFY `idJornada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `manual_usuario`
--
ALTER TABLE `manual_usuario`
  MODIFY `idManualUsuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nivel_programa`
--
ALTER TABLE `nivel_programa`
  MODIFY `idNivelPrograma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `programa_formacion`
--
ALTER TABLE `programa_formacion`
  MODIFY `idProgramaFormacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `regional`
--
ALTER TABLE `regional`
  MODIFY `idRegional` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rutina_usuario`
--
ALTER TABLE `rutina_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT de la tabla `sede`
--
ALTER TABLE `sede`
  MODIFY `idSede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  MODIFY `idTipoIdentificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuario_rol`
--
ALTER TABLE `usuario_rol`
  MODIFY `idUsuarioRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `actividad_ibfk_1` FOREIGN KEY (`idGimnasio`) REFERENCES `gimnasio` (`idGimnasio`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `actividad_ibfk_2` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idTipo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD CONSTRAINT `aprendiz_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendiz_ibfk_2` FOREIGN KEY (`idImc`) REFERENCES `imc` (`idImc`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendiz_ibfk_3` FOREIGN KEY (`idAcudiente`) REFERENCES `acudiente` (`idAcudiente`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  ADD CONSTRAINT `aprendiz_ficha_ibfk_1` FOREIGN KEY (`idAprendiz`) REFERENCES `aprendiz` (`idAprendiz`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendiz_ficha_ibfk_2` FOREIGN KEY (`idFicha`) REFERENCES `ficha` (`idFicha`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `centro_formacion`
--
ALTER TABLE `centro_formacion`
  ADD CONSTRAINT `centro_formacion_ibfk_1` FOREIGN KEY (`idCiudad`) REFERENCES `ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `centro_formacion_ibfk_2` FOREIGN KEY (`idRegional`) REFERENCES `regional` (`idRegional`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `centro_formacion_ibfk_3` FOREIGN KEY (`idAseguradora`) REFERENCES `aseguradora` (`idAseguradora`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`idDepartamento`) REFERENCES `departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `entrenador`
--
ALTER TABLE `entrenador`
  ADD CONSTRAINT `entrenador_ibfk_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `entrenador_sede`
--
ALTER TABLE `entrenador_sede`
  ADD CONSTRAINT `entrenador_sede_ibfk_1` FOREIGN KEY (`idEntrenador`) REFERENCES `entrenador` (`idEntrenador`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `entrenador_sede_ibfk_2` FOREIGN KEY (`idSede`) REFERENCES `sede` (`idSede`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`idMarca`) REFERENCES `marca` (`idMarca`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `equipo_ibfk_2` FOREIGN KEY (`idGimnasio`) REFERENCES `gimnasio` (`idGimnasio`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `equipo_ibfk_3` FOREIGN KEY (`idManualUsuario`) REFERENCES `manual_usuario` (`idManualUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD CONSTRAINT `ficha_ibfk_1` FOREIGN KEY (`idJornada`) REFERENCES `jornada` (`idJornada`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_ibfk_2` FOREIGN KEY (`idAmbiente`) REFERENCES `ambiente` (`idAmbiente`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_ibfk_3` FOREIGN KEY (`idProgramaFormacion`) REFERENCES `programa_formacion` (`idProgramaFormacion`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `gimnasio`
--
ALTER TABLE `gimnasio`
  ADD CONSTRAINT `gimnasio_ibfk_1` FOREIGN KEY (`idSede`) REFERENCES `sede` (`idSede`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD CONSTRAINT `instructor_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `instructor_ibfk_2` FOREIGN KEY (`idContrato`) REFERENCES `contrato` (`idContrato`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `instructor_ficha`
--
ALTER TABLE `instructor_ficha`
  ADD CONSTRAINT `instructor_ficha_ibfk_1` FOREIGN KEY (`idInstructor`) REFERENCES `instructor` (`idInstructor`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `instructor_ficha_ibfk_2` FOREIGN KEY (`idFicha`) REFERENCES `ficha` (`idFicha`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipo_identificacion` (`idTipoIdentificacion`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`idEps`) REFERENCES `eps` (`idEps`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `persona_ibfk_3` FOREIGN KEY (`idGimnasio`) REFERENCES `gimnasio` (`idGimnasio`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `programa_formacion`
--
ALTER TABLE `programa_formacion`
  ADD CONSTRAINT `programa_formacion_ibfk_1` FOREIGN KEY (`idNivelPrograma`) REFERENCES `nivel_programa` (`idNivelPrograma`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `sede`
--
ALTER TABLE `sede`
  ADD CONSTRAINT `sede_ibfk_1` FOREIGN KEY (`idCentroFormacion`) REFERENCES `centro_formacion` (`idCentroFormacion`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_rol`
--
ALTER TABLE `usuario_rol`
  ADD CONSTRAINT `usuario_rol_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_rol_ibfk_2` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
