-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2019 at 08:57 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyectoformativo`
--

-- --------------------------------------------------------

--
-- Table structure for table `actividad`
--

CREATE TABLE `actividad` (
  `idActividad` int(11) NOT NULL,
  `idGimnasio` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `actividad`
--

INSERT INTO `actividad` (`idActividad`, `idGimnasio`, `idTipo`, `fechaInicio`, `fechaFin`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, 1, '2019-05-22', '2019-05-31', 'A', 1, 1, '2019-05-23 01:30:14', '2019-05-23 01:30:14'),
(2, 2, 2, '2019-05-23', '2019-05-28', 'A', 1, 1, '2019-05-23 01:28:34', '2019-05-23 01:28:34'),
(3, 1, 1, '2019-05-23', '2019-05-23', 'A', 1, 1, '2019-05-24 03:35:27', '2019-05-24 03:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `acudiente`
--

CREATE TABLE `acudiente` (
  `idAcudiente` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acudiente`
--

INSERT INTO `acudiente` (`idAcudiente`, `nombre`, `apellido`, `telefono`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Dilmer', 'Ramirez', '3023909182', 'A', 1, 1, '2019-05-22 23:58:51', '2019-05-22 23:56:16'),
(2, 'Liseth', 'Monroy', '3136100713', 'A', 1, 1, '2019-05-22 23:57:25', '2019-05-22 23:57:25'),
(4, 'Misael ', 'Nieto', '3178690284', 'A', 1, 1, '2019-06-07 08:23:58', '2019-06-07 08:23:58'),
(5, 'Esperanza', 'Perafan Melo', '3164728851', 'A', 1, 1, '2019-06-07 08:33:17', '2019-06-07 08:33:17'),
(6, 'martha', 'Borrero', '3167900931', 'A', 1, 1, '2019-06-10 19:23:13', '2019-06-10 19:23:13');

-- --------------------------------------------------------

--
-- Stand-in structure for view `acudienteauto`
-- (See below for the actual view)
--
CREATE TABLE `acudienteauto` (
`idAcudiente` int(11)
,`nombreAcudiente` varchar(61)
);

-- --------------------------------------------------------

--
-- Table structure for table `ambiente`
--

CREATE TABLE `ambiente` (
  `idAmbiente` int(11) NOT NULL,
  `codigo` int(5) NOT NULL,
  `descripcion` varchar(50) CHARACTER SET latin1 NOT NULL,
  `estado` enum('A','I') CHARACTER SET latin1 NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `ambiente`
--

INSERT INTO `ambiente` (`idAmbiente`, `codigo`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, '209', 'A', 1, 1, '2019-05-13 20:17:07', '2019-05-13 20:17:07'),
(2, 2, '210', 'A', 1, 1, '2019-05-13 20:19:53', '2019-05-13 20:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `aprendiz`
--

CREATE TABLE `aprendiz` (
  `idAprendiz` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idAcudiente` int(11) NOT NULL,
  `idImc` int(11) NOT NULL,
  `estado` enum('A','I') CHARACTER SET latin1 NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `aprendiz`
--

INSERT INTO `aprendiz` (`idAprendiz`, `idUsuario`, `idAcudiente`, `idImc`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 7, 2, 1, 'A', 1, 1, '2019-05-23 21:10:48', '2019-05-23 21:10:48'),
(2, 5, 1, 2, 'A', 1, 1, '2019-05-23 21:11:01', '2019-05-23 21:11:01'),
(3, 6, 2, 1, 'A', 1, 1, '2019-06-02 05:39:54', '2019-06-02 05:39:54'),
(4, 12, 1, 3, 'A', 1, 1, '2019-06-08 03:28:54', '2019-06-08 03:28:54'),
(5, 14, 6, 6, 'A', 1, 1, '2019-06-10 19:23:43', '2019-06-10 19:23:43');

-- --------------------------------------------------------

--
-- Stand-in structure for view `aprendizauto`
-- (See below for the actual view)
--
CREATE TABLE `aprendizauto` (
`idAprendiz` int(11)
,`nombrePersona` varchar(123)
);

-- --------------------------------------------------------

--
-- Table structure for table `aprendiz_ficha`
--

CREATE TABLE `aprendiz_ficha` (
  `idAprendizFicha` int(11) NOT NULL,
  `idFicha` int(11) NOT NULL,
  `idAprendiz` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aprendiz_ficha`
--

INSERT INTO `aprendiz_ficha` (`idAprendizFicha`, `idFicha`, `idAprendiz`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, 1, 'A', 1, 1, '2019-05-23 23:05:28', '2019-05-23 23:05:28'),
(2, 2, 2, 'A', 1, 1, '2019-05-23 23:05:36', '2019-05-23 23:05:36'),
(3, 4, 3, 'A', 1, 1, '2019-06-02 05:41:01', '2019-06-02 05:41:01'),
(4, 1, 5, 'A', 1, 1, '2019-06-10 19:23:57', '2019-06-10 19:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `articulo`
--

CREATE TABLE `articulo` (
  `articulo_id` int(4) NOT NULL,
  `categoria_id` int(4) DEFAULT NULL,
  `autor` varchar(40) NOT NULL,
  `titulo` varchar(40) NOT NULL,
  `contenido` text NOT NULL,
  `fecha` date NOT NULL,
  `img` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aseguradora`
--

CREATE TABLE `aseguradora` (
  `idAseguradora` int(11) NOT NULL,
  `nit` varchar(15) NOT NULL,
  `razonSocial` varchar(50) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `representanteLegal` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aseguradora`
--

INSERT INTO `aseguradora` (`idAseguradora`, `nit`, `razonSocial`, `direccion`, `representanteLegal`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(3, '2664514', 'Super Tienda familiar', 'cra 1 # 22-16', 'Luz Mary Villegas', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '36304705', 'Boutique andrea', 'Clle 25A # 32-29', 'Andrea Caquimboo', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '2147483647', 'Helados Colombina', 'Toma 10 A', 'Cielo Correa', 'I', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '860026', 'ALLIANZ SEGUROS S.A.', 'Carrera 13A No. 29-24', 'David Alejandro Colmenares Spence', 'A', 1, 1, '2019-05-13 16:41:22', '2019-05-13 16:41:22'),
(10, '8002561619', 'ARL SURA', 'Calle 49A No. 63-55 Piso 9', 'Ivan Ignacio Zuluaga Latorre', 'A', 1, 1, '2019-05-13 17:02:43', '2019-05-13 17:02:43');

-- --------------------------------------------------------

--
-- Table structure for table `asistenciagimnasio`
--

CREATE TABLE `asistenciagimnasio` (
  `idAsis` bigint(20) NOT NULL,
  `idGim` int(11) DEFAULT NULL,
  `idPer` int(11) DEFAULT NULL,
  `fechaAsis` date DEFAULT NULL,
  `ina` int(1) DEFAULT NULL,
  `idUsuarioC` int(11) DEFAULT NULL,
  `idUsuarioM` int(11) DEFAULT NULL,
  `fechaC` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaM` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `asistenciagimnasio`
--

INSERT INTO `asistenciagimnasio` (`idAsis`, `idGim`, `idPer`, `fechaAsis`, `ina`, `idUsuarioC`, `idUsuarioM`, `fechaC`, `fechaM`) VALUES
(1, 1, 10, '2019-06-05', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(2, 1, 2, '2019-06-06', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(3, 2, 10, '2019-06-07', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(4, 1, 4, '2019-06-05', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(5, 1, 4, '2019-06-07', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(6, 1, 4, '2019-06-10', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(7, 1, 10, '2019-06-14', NULL, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33'),
(8, 2, 4, '2019-07-11', 1, NULL, NULL, '2019-06-05 19:08:33', '2019-06-05 19:08:33');

-- --------------------------------------------------------

--
-- Stand-in structure for view `autocompletar`
-- (See below for the actual view)
--
CREATE TABLE `autocompletar` (
`idPersona` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `categoria_id` int(11) NOT NULL,
  `categoria` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `centro_formacion`
--

CREATE TABLE `centro_formacion` (
  `idCentroFormacion` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `idAseguradora` int(11) NOT NULL,
  `idRegional` int(11) NOT NULL,
  `idCiudad` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centro_formacion`
--

INSERT INTO `centro_formacion` (`idCentroFormacion`, `nombre`, `idAseguradora`, `idRegional`, `idCiudad`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(2, 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS', 10, 1, 30, 'A', 1, 1, '2019-05-13 19:06:22', '2019-05-13 19:06:22'),
(3, 'CENTRO DE FORMACIÃ“N AGROINDUSTRIAL', 7, 1, 30, 'A', 1, 1, '2019-05-13 19:07:19', '2019-05-13 19:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `ciudad`
--

CREATE TABLE `ciudad` (
  `idCiudad` int(11) NOT NULL,
  `codigo` int(4) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `idDepartamento` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaModificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ciudad`
--

INSERT INTO `ciudad` (`idCiudad`, `codigo`, `nombre`, `idDepartamento`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(11, 210, 'Mexico', 1, 'I', 1, 1, '0000-00-00', '0000-00-00'),
(25, 102, 'Dorada', 11, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(26, 128, 'Cartagena', 31, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(27, 134, 'Barranquilla', 8, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(29, 145, 'cali', 3, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(30, 200, 'Neiva', 1, 'A', 1, 1, '0000-00-00', '0000-00-00'),
(31, 105, '105 O 1 = 1', 3, 'A', 1, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `condicion_preexistente`
--

CREATE TABLE `condicion_preexistente` (
  `idEnfermedad` int(11) NOT NULL,
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idGrupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `condicion_preexistente`
--

INSERT INTO `condicion_preexistente` (`idEnfermedad`, `descripcion`, `idGrupo`) VALUES
(0, 'Ninguno', 11),
(1, 'Cardiopatía coronaria', 1),
(2, 'Insuficiencia cardíaca', 1),
(3, 'Arritmias', 1),
(4, 'Válvulas cardíacas', 1),
(5, 'Arteriopatía periférica', 1),
(6, 'Presión arterial alta (hipertensión)', 1),
(7, 'Cerebrovascular', 1),
(8, 'Cardiopatía congínita', 1),
(9, 'Artritis reumatoide', 2),
(10, 'Lupus eritematoso sistémico', 2),
(11, 'Celiaquía', 2),
(12, 'Polimialgia reumática', 2),
(13, 'Esclerosis múltiple', 2),
(14, 'Espondilitis anquilosante', 2),
(15, 'Diabetes mellitus tipo 1', 2),
(16, 'Vasculitis', 2),
(17, 'Arteritis de células gigantes', 2),
(18, 'Asma', 2),
(19, 'Cardiopulmonares', 3),
(20, 'Hepáticos', 3),
(21, 'pulmonares', 3),
(22, 'Médula ósea', 3),
(23, 'Cardíacos', 3),
(24, 'Distrofia muscular', 4),
(25, 'Parkinson', 4),
(26, 'Lesiones en la múdula espinal', 4),
(27, 'Epilepsia', 4),
(28, 'Tumores cerebrales', 4),
(29, 'Meningitis', 4),
(32, 'Artritis Reumatoide', 5),
(33, 'Bursitis', 5),
(34, 'Cervicoartrosis', 5),
(35, 'Espondilosis cervical', 5),
(36, 'Fibromialgia', 5),
(37, 'Gota', 5),
(38, 'Hernia de Disco', 5),
(39, 'Osteoartritis', 5),
(40, 'Osteoporosis', 5),
(41, 'Sacroileitis', 5),
(42, 'Raquitismo', 5),
(43, 'Tendinitis de hombro', 5),
(44, 'Túnel Carpal', 5),
(45, 'Túnel Tarsal', 5),
(46, 'Leucemias', 5),
(47, 'Síndrome depresivos', 6),
(48, 'Demencias', 6),
(49, 'Neurosis grave', 6),
(50, 'Psicosis', 6),
(51, 'Adicción', 6),
(52, 'Bulimia', 6),
(53, 'Anorexia', 6),
(54, 'Autismo', 6),
(55, 'Ulcera péptica o gástrica', 7),
(56, 'Síndrome de Malabsorción\r\n', 7),
(57, 'Cirrosis hepática y sus concomitantes\r\n', 7),
(58, 'Pancreatitis crónicas\r\n', 7),
(59, 'Enfermedad inflamatoria intestinal\r\n', 7),
(60, 'Enfermedad biliar no resuelta\r\n', 7),
(61, 'Hepatitis crónicas\r\n', 7),
(62, 'Enfermedades renales con compromiso de parénquima\r\n', 8),
(63, 'Enfermedades obstructivas del árbol urinario', 8),
(64, 'Insuficiencia renal crónica (en plan de diálisis)', 8),
(65, 'Enfermedad pulmonar obstructiva crónica (EPOC)', 9),
(66, 'Asma', 9),
(67, 'Enfermedad fibroquística', 9),
(68, 'Hipertensión pulmonar', 9),
(69, 'Fibrosis pulmonar', 9),
(70, 'Enfermedades endocrinas descompensadas\r\n', 10),
(71, 'Diabetes insulino dependiente\r\n', 10),
(72, 'Diabetes no insulino dependiente con compromiso de órgano blanco\r\n', 10);

-- --------------------------------------------------------

--
-- Table structure for table `contrato`
--

CREATE TABLE `contrato` (
  `idContrato` int(11) NOT NULL,
  `tipoContrato` varchar(30) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `fechaFinalizacion` date NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contrato`
--

INSERT INTO `contrato` (`idContrato`, `tipoContrato`, `fechaIngreso`, `fechaFinalizacion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, '1 aÃ±o', '2019-05-01', '2020-05-01', 'A', 1, 1, '2019-05-17 23:39:09', '2019-05-17 23:39:09'),
(2, '3 meses', '2019-05-01', '2019-08-31', 'A', 1, 1, '2019-05-17 23:39:59', '2019-05-17 23:39:59');

-- --------------------------------------------------------

--
-- Table structure for table `crear_evento`
--

CREATE TABLE `crear_evento` (
  `idCrearEvento` int(11) NOT NULL,
  `idTipoEvento` int(11) NOT NULL,
  `nombreEvento` varchar(30) CHARACTER SET utf8 NOT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaCulminacion` date DEFAULT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `crear_evento`
--

INSERT INTO `crear_evento` (`idCrearEvento`, `idTipoEvento`, `nombreEvento`, `fechaInicio`, `fechaCulminacion`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(3, 3, 'Apertura', '2019-06-05', '2019-07-04', 'A', '2019-06-05 19:18:58', '2019-06-05 19:19:05', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `idDepartamento` int(11) NOT NULL,
  `codigo` int(3) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`idDepartamento`, `codigo`, `nombre`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 91, 'Amazonas', 'A', 1, 1, '2019-06-03 13:23:00', '2019-06-03 13:24:00'),
(2, 5, 'Antioquia', 'A', 1, 1, '2019-06-03 13:19:29', '2019-06-03 13:19:29'),
(3, 81, 'Arauca', 'A', 1, 1, '2019-06-03 13:21:33', '2019-06-03 13:21:33'),
(4, 8, 'Atlantico', 'A', 1, 1, '2019-06-03 13:26:23', '2019-06-03 13:26:23'),
(5, 11, 'Bogota D.C', 'A', 1, 1, '2019-06-03 13:28:13', '2019-06-03 13:28:13'),
(6, 13, 'Bolivar', 'A', 1, 1, '2019-06-03 13:29:49', '2019-06-03 13:29:49'),
(7, 15, 'Boyaca', 'A', 1, 1, '2019-06-03 13:30:41', '2019-06-03 13:30:41'),
(8, 17, 'Caldas', 'A', 1, 1, '2019-06-03 13:31:15', '2019-06-03 13:31:15'),
(9, 18, 'Caqueta', 'A', 1, 1, '2019-06-03 13:31:40', '2019-06-03 13:31:40'),
(10, 85, 'Casanare', 'A', 1, 1, '2019-06-03 13:32:14', '2019-06-03 13:32:14'),
(11, 19, 'Cauca', 'A', 1, 1, '2019-06-03 13:35:27', '2019-06-03 13:35:27'),
(12, 20, 'Cesar', 'A', 1, 1, '2019-06-03 13:36:03', '2019-06-03 13:36:03'),
(13, 27, 'Choco', 'A', 1, 1, '2019-06-03 13:36:37', '2019-06-03 13:36:37'),
(14, 23, 'Cordoba', 'A', 1, 1, '2019-06-03 13:37:06', '2019-06-03 13:37:06'),
(15, 25, 'Cundinamarca', 'A', 1, 1, '2019-06-03 13:37:41', '2019-06-03 13:37:41'),
(16, 94, 'Guainia', 'A', 1, 1, '2019-06-03 13:38:13', '2019-06-03 13:38:13'),
(17, 95, 'Guaviare', 'A', 1, 1, '2019-06-03 13:38:50', '2019-06-03 13:38:50'),
(18, 41, 'Huila', 'A', 1, 1, '2019-06-03 13:25:10', '2019-06-03 13:25:50'),
(19, 44, 'La Guajira', 'A', 1, 1, '2019-06-03 13:39:47', '2019-06-03 13:39:47'),
(20, 47, 'Magdalena', 'A', 1, 1, '2019-06-03 13:40:17', '2019-06-03 13:40:17'),
(21, 50, 'Meta', 'A', 1, 1, '2019-06-03 13:40:44', '2019-06-03 13:40:44'),
(22, 52, 'Narino', 'A', 1, 1, '2019-06-03 13:41:15', '2019-06-03 13:41:15'),
(23, 54, 'Norte de Santander', 'A', 1, 1, '2019-06-03 13:41:45', '2019-06-03 13:41:45'),
(24, 86, 'Putumayo', 'A', 1, 1, '2019-06-03 13:42:16', '2019-06-03 13:42:16'),
(25, 63, 'Quindio', 'A', 1, 1, '2019-06-03 13:43:58', '2019-06-03 13:43:58'),
(26, 66, 'Risaralda', 'A', 1, 1, '2019-06-03 13:44:27', '2019-06-03 13:44:27'),
(27, 88, 'San Andres', 'A', 1, 1, '2019-06-03 13:44:55', '2019-06-03 13:44:55'),
(28, 68, 'Santander', 'A', 1, 1, '2019-06-03 13:45:30', '2019-06-03 13:45:30'),
(29, 70, 'Sucre', 'A', 1, 1, '2019-06-03 13:45:57', '2019-06-03 13:45:57'),
(30, 73, 'Tolima', 'A', 1, 1, '2019-06-03 13:46:22', '2019-06-03 13:46:22'),
(31, 76, 'Valle del Cauca', 'A', 1, 1, '2019-06-03 13:46:52', '2019-06-03 13:46:52'),
(32, 97, 'Vaupes', 'A', 1, 1, '2019-06-03 13:47:29', '2019-06-03 13:47:29'),
(33, 99, 'Vichada', 'A', 1, 1, '2019-06-03 13:47:54', '2019-06-03 13:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `entrenador`
--

CREATE TABLE `entrenador` (
  `idEntrenador` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entrenador`
--

INSERT INTO `entrenador` (`idEntrenador`, `idPersona`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 2, 'A', 1, 1, '2019-05-22 18:37:55', '2019-05-22 18:37:55'),
(2, 4, 'A', 1, 1, '2019-05-22 18:38:07', '2019-05-22 18:38:07'),
(3, 14, 'A', 1, 1, '2019-06-10 21:13:00', '2019-06-10 21:13:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `entrenadorvista`
-- (See below for the actual view)
--
CREATE TABLE `entrenadorvista` (
`idEntrenador` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Table structure for table `entrenador_sede`
--

CREATE TABLE `entrenador_sede` (
  `idEntrenadorSede` int(11) NOT NULL,
  `idEntrenador` int(11) NOT NULL,
  `idSede` int(11) NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entrenador_sede`
--

INSERT INTO `entrenador_sede` (`idEntrenadorSede`, `idEntrenador`, `idSede`, `horaInicio`, `horaFin`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 3, 2, '09:00:00', '03:00:00', 'A', 1, 1, '2019-06-10 21:21:42', '2019-06-10 21:21:42'),
(2, 2, 1, '12:00:00', '17:00:00', 'A', 1, 1, '2019-06-10 21:23:23', '2019-06-10 21:23:23'),
(3, 3, 2, '08:12:00', '14:00:00', 'A', 1, 1, '2019-06-10 21:24:28', '2019-06-10 21:24:28');

-- --------------------------------------------------------

--
-- Table structure for table `eps`
--

CREATE TABLE `eps` (
  `idEps` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `categoria` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `eps`
--

INSERT INTO `eps` (`idEps`, `nombre`, `categoria`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(7, 'Medimas', 'Contributivo', 'A', 1, 1, '2019-06-10 20:51:24', '2019-06-05 03:34:00'),
(8, 'Asmet Salud', 'Subsidiado', 'A', 1, 1, '2019-06-10 20:52:07', '2019-06-05 03:34:11'),
(9, 'Cafesalud Contributivo', 'Contributivo', 'A', 1, 1, '2019-06-10 20:53:22', '2019-06-05 04:21:37'),
(10, 'Ecopetrol', 'Exceptuada', 'A', 1, 1, '2019-06-10 20:54:34', '2019-06-10 20:54:34'),
(11, 'Emcosalud', 'Exceptuada', 'A', 1, 1, '2019-06-10 20:55:05', '2019-06-10 20:55:05'),
(12, 'Sanitas', 'Contributivo', 'A', 1, 1, '2019-06-10 20:55:54', '2019-06-10 20:55:54'),
(13, 'Coomeva', 'Contributivo', 'A', 1, 1, '2019-06-10 20:57:17', '2019-06-10 20:57:17'),
(14, 'Aic', 'Subsidiado', 'A', 1, 1, '2019-06-10 20:57:17', '2019-06-10 20:57:17'),
(15, 'Ecoopsos', 'Subsidiado', 'A', 1, 1, '2019-06-10 20:57:42', '2019-06-10 20:57:42'),
(16, 'Nueva EPS', 'Contributivo', 'A', 1, 1, '2019-06-10 20:58:08', '2019-06-10 20:58:08'),
(17, 'Mallamas ', 'Subsidiado', 'A', 1, 1, '2019-06-10 20:58:33', '2019-06-10 20:58:33'),
(18, 'Ejercito', 'Exceptuada', 'A', 1, 1, '2019-06-10 20:59:37', '2019-06-10 20:59:37'),
(19, 'Comparta', 'Contributivo', 'A', 1, 1, '2019-06-10 21:00:17', '2019-06-10 21:00:17'),
(20, 'Comfamiliar', 'Subsidiado', 'A', 1, 1, '2019-06-10 21:01:20', '2019-06-10 21:01:20'),
(21, 'Cafesalud Subsidiado', 'Subsidiado', 'A', 1, 1, '2019-06-10 21:02:58', '2019-06-10 21:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `equipo`
--

CREATE TABLE `equipo` (
  `idEquipo` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaIngreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `equipo`
--

INSERT INTO `equipo` (`idEquipo`, `nombre`, `descripcion`, `fechaIngreso`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(21, 'Bicicletas ElÃ­pticas', 'Carta_Aprendiz.pdf', '2019-06-05 05:00:00', 'A', 1, 1, '2019-06-06 00:52:34', '2019-06-06 00:52:34'),
(22, 'Bicicletas ElÃ­pticas', 'Carta_Aprendiz.pdf', '2019-06-05 05:00:00', 'A', 1, 1, '2019-06-06 01:28:27', '2019-06-06 01:28:27'),
(23, 'Bicicletas estÃ¡ticas', 'BICICLETA_MAGNETICA_TE20407.pdf', '2019-06-05 05:00:00', 'A', 1, 1, '2019-06-06 01:30:56', '2019-06-06 01:30:56'),
(24, 'Banco press', 'BICICLETA_MAGNETICA_TE20407.pdf', '2019-06-05 05:00:00', 'A', 1, 1, '2019-06-06 01:36:09', '2019-06-06 01:36:09'),
(25, 'Banco press', 'Carta_Aprendiz.pdf', '2019-06-05 05:00:00', 'A', 1, 1, '2019-06-06 01:39:22', '2019-06-06 01:39:22');

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `idEvento` int(11) NOT NULL,
  `idSede` int(11) DEFAULT NULL,
  `idTipo` int(11) DEFAULT NULL,
  `nombreEvento` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fechaI` datetime DEFAULT NULL,
  `fechaF` datetime DEFAULT NULL,
  `idUsuarioC` int(11) DEFAULT NULL,
  `idUsuarioM` int(11) DEFAULT NULL,
  `fechaC` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaM` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `evento`
--

INSERT INTO `evento` (`idEvento`, `idSede`, `idTipo`, `nombreEvento`, `fechaI`, `fechaF`, `idUsuarioC`, `idUsuarioM`, `fechaC`, `fechaM`) VALUES
(1, 2, 1, 'apertura', '2019-06-04 03:24:29', '2019-06-07 03:24:34', 1, 1, '2019-06-05 19:05:29', '2019-06-05 19:05:29'),
(2, 2, 2, 'final', '2019-07-09 03:24:55', '2019-06-14 03:24:59', 1, 1, '2019-06-05 19:05:29', '2019-06-05 19:05:29'),
(3, 1, 1, 'recreacional', '2019-08-08 03:25:35', '2019-06-14 03:25:40', 1, 1, '2019-06-05 19:05:29', '2019-06-05 19:05:29');

-- --------------------------------------------------------

--
-- Table structure for table `ficha`
--

CREATE TABLE `ficha` (
  `idFicha` int(11) NOT NULL,
  `ficha` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinLectiva` date NOT NULL,
  `fechaFinProductiva` date NOT NULL,
  `idJornada` int(11) NOT NULL,
  `idAmbiente` int(11) NOT NULL,
  `idProgramaFormacion` int(11) NOT NULL,
  `idSede` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `ficha`
--

INSERT INTO `ficha` (`idFicha`, `ficha`, `fechaInicio`, `fechaFinLectiva`, `fechaFinProductiva`, `idJornada`, `idAmbiente`, `idProgramaFormacion`, `idSede`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1563193, '2017-12-11', '2019-06-12', '2019-12-13', 3, 1, 2, 1, 'A', 1, 1, '2019-06-08 04:13:59', '2019-05-21 18:50:28'),
(2, 1563194, '2018-06-12', '2019-12-11', '2020-06-12', 1, 2, 2, 2, 'A', 1, 1, '2019-06-08 04:14:31', '2019-05-21 18:51:39'),
(4, 1105465, '2019-06-10', '2019-06-26', '2019-06-17', 3, 1, 3, 2, 'A', 1, 1, '2019-06-02 10:38:57', '2019-06-02 10:38:57'),
(5, 1201224, '2019-06-03', '2019-06-10', '2019-06-03', 3, 1, 2, 2, 'A', 1, 1, '2019-06-03 10:58:08', '2019-06-03 10:58:08');

-- --------------------------------------------------------

--
-- Table structure for table `gimnasio`
--

CREATE TABLE `gimnasio` (
  `idGimnasio` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `idSede` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gimnasio`
--

INSERT INTO `gimnasio` (`idGimnasio`, `nombre`, `idSede`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Gimnasio Industria', 1, 'A', 1, 1, '2019-05-17 19:36:25', '2019-05-17 19:36:25'),
(2, 'Gimnasio Comercio', 2, 'A', 1, 1, '2019-05-17 19:38:44', '2019-05-17 19:38:44'),
(3, 'Gimnasio Tecnoparque', 3, 'A', 1, 1, '2019-05-17 19:42:23', '2019-05-21 16:43:31'),
(4, 'Agroindustrial', 1, 'A', 1, 1, '2019-05-21 16:39:38', '2019-05-21 16:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `grupo_enfermedades`
--

CREATE TABLE `grupo_enfermedades` (
  `idGrupo` int(11) NOT NULL,
  `descripcion` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupo_enfermedades`
--

INSERT INTO `grupo_enfermedades` (`idGrupo`, `descripcion`) VALUES
(1, 'Cardiovasculares'),
(2, 'Enfermedades inmunodeficiencia '),
(3, 'Postransplantes '),
(4, 'Neurología '),
(5, 'Osteoarticulares '),
(6, 'Psiquiátricas '),
(7, 'Digestivas '),
(8, 'Enfermedades urinarias '),
(9, 'Enfermedades del aparato respiratorio '),
(10, 'Enfermedades endocrinas '),
(11, 'Ninguno');

-- --------------------------------------------------------

--
-- Table structure for table `imc`
--

CREATE TABLE `imc` (
  `idImc` int(11) NOT NULL,
  `estatura` float NOT NULL,
  `peso` float NOT NULL,
  `calculoImc` float NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imc`
--

INSERT INTO `imc` (`idImc`, `estatura`, `peso`, `calculoImc`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 176, 81, 26.1493, 'A', 1, 1, '2019-05-23 17:41:56', '2019-05-23 17:41:56'),
(2, 170, 70, 24.2215, 'A', 1, 1, '2019-05-23 17:44:02', '2019-05-23 17:44:02'),
(3, 180, 67, 20.679, 'A', 1, 1, '2019-06-07 03:12:56', '2019-06-07 03:12:56'),
(4, 175, 90, 29.3878, 'A', 1, 1, '2019-06-07 03:24:20', '2019-06-07 03:24:20'),
(5, 184, 66, 19.4943, 'A', 1, 1, '2019-06-07 03:33:41', '2019-06-07 03:33:41'),
(6, 170, 80, 27.68, 'A', 1, 1, '2019-06-10 19:23:34', '2019-06-10 19:23:34');

-- --------------------------------------------------------

--
-- Table structure for table `imccontrol`
--

CREATE TABLE `imccontrol` (
  `idImcC` int(11) NOT NULL,
  `idAprendiz` int(11) DEFAULT NULL,
  `imc` float DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioC` int(11) DEFAULT NULL,
  `idUsuarioM` int(11) DEFAULT NULL,
  `fechaC` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaM` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `imccontrol`
--

INSERT INTO `imccontrol` (`idImcC`, `idAprendiz`, `imc`, `fecha`, `idUsuarioC`, `idUsuarioM`, `fechaC`, `fechaM`) VALUES
(1, 1, 20.2, '2019-06-03 03:50:31', NULL, NULL, '2019-06-05 19:04:14', '2019-06-05 19:04:14'),
(2, 1, 20.5, '2019-06-05 03:50:51', NULL, NULL, '2019-06-05 19:04:14', '2019-06-05 19:04:14'),
(3, 2, 20.6, '2019-07-09 03:51:11', NULL, NULL, '2019-06-05 19:04:14', '2019-06-05 19:04:14'),
(4, 2, 20.9, '2019-07-10 03:51:32', NULL, NULL, '2019-06-05 19:04:14', '2019-06-05 19:04:14'),
(5, 3, 20.1, '2019-07-18 03:51:50', NULL, NULL, '2019-06-05 19:04:14', '2019-06-05 19:04:14'),
(6, 3, 20.7, '2019-07-18 03:52:18', NULL, NULL, '2019-06-05 19:04:14', '2019-06-05 19:04:14');

-- --------------------------------------------------------

--
-- Table structure for table `inscripcion_aprendiz`
--

CREATE TABLE `inscripcion_aprendiz` (
  `idInscripcionAprendiz` int(11) NOT NULL,
  `idCrearEvento` int(11) NOT NULL,
  `idTipoIdentificacion` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `numeroIdentificacion` varchar(15) CHARACTER SET utf8 NOT NULL,
  `nombreAprendiz` varchar(30) CHARACTER SET utf8 NOT NULL,
  `apellidoAprendiz` varchar(30) CHARACTER SET utf8 NOT NULL,
  `ficha` varchar(10) CHARACTER SET utf8 NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `inscripcion_aprendiz`
--

INSERT INTO `inscripcion_aprendiz` (`idInscripcionAprendiz`, `idCrearEvento`, `idTipoIdentificacion`, `numeroIdentificacion`, `nombreAprendiz`, `apellidoAprendiz`, `ficha`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(3, 3, '1', '103214758', 'Juan Andres', 'Perdomo Lizcano', '784536', 'A', '2019-06-06 00:20:38', '2019-06-06 00:20:45', 1, 0),
(5, 3, '1', '7717022', 'LUIS FERNANDO', 'CUELLAR SANCHEZ', '1501561', 'A', '2019-06-11 13:37:24', '2019-06-11 13:37:24', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE `instructor` (
  `idInstructor` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idContrato` int(11) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCracion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`idInstructor`, `idUsuario`, `estado`, `idContrato`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCracion`, `fechaModificacion`) VALUES
(2, 1, 'A', 1, 1, 3, '2019-05-21 15:25:02', '2019-05-21 15:25:02'),
(3, 5, 'A', 2, 1, 1, '2019-05-21 15:25:55', '2019-05-21 15:25:55'),
(4, 15, 'A', 1, 7, 1, '2019-06-10 19:28:35', '2019-06-10 19:28:35');

-- --------------------------------------------------------

--
-- Stand-in structure for view `instructordeficha`
-- (See below for the actual view)
--
CREATE TABLE `instructordeficha` (
`idInstructor` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `instructornombre`
-- (See below for the actual view)
--
CREATE TABLE `instructornombre` (
`idUsuario` int(11)
,`idPersona` int(11)
,`Nombre_Completo` varchar(123)
);

-- --------------------------------------------------------

--
-- Table structure for table `instructor_ficha`
--

CREATE TABLE `instructor_ficha` (
  `idInstructorFicha` int(11) NOT NULL,
  `idFicha` int(11) NOT NULL,
  `idInstructor` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor_ficha`
--

INSERT INTO `instructor_ficha` (`idInstructorFicha`, `idFicha`, `idInstructor`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 1, 2, 'A', 1, 1, '2019-05-21 23:25:22', '2019-05-22 00:51:45'),
(2, 2, 3, 'A', 1, 1, '2019-05-21 23:29:05', '2019-05-21 23:29:05'),
(3, 1, 4, 'A', 1, 1, '2019-06-10 19:28:44', '2019-06-10 19:28:44');

-- --------------------------------------------------------

--
-- Table structure for table `jornada`
--

CREATE TABLE `jornada` (
  `idJornada` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jornada`
--

INSERT INTO `jornada` (`idJornada`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'MaÃ±ana', 'A', 1, 1, '2019-05-21 15:55:37', '2019-05-21 15:55:37'),
(2, 'Tarde', 'A', 1, 1, '2019-05-21 15:55:43', '2019-05-21 15:55:43'),
(3, 'Noche', 'A', 1, 1, '2019-05-21 16:06:09', '2019-05-21 16:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `manual_usuario`
--

CREATE TABLE `manual_usuario` (
  `idManualUsuario` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manual_usuario`
--

INSERT INTO `manual_usuario` (`idManualUsuario`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'EK201231139963.pdf', 'A', 1, 1, '2019-06-02 20:10:28', '2019-06-02 20:10:28'),
(2, 'Sergio_Eduardo_Mosquera.pdf', 'A', 1, 1, '2019-06-02 20:13:17', '2019-06-02 20:13:17');

-- --------------------------------------------------------

--
-- Table structure for table `marca`
--

CREATE TABLE `marca` (
  `idMarca` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marca`
--

INSERT INTO `marca` (`idMarca`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Adidas', 'A', 1, 1, '2019-05-17 18:24:43', '2019-05-17 18:24:43'),
(2, 'Bkool', 'A', 1, 1, '2019-05-17 18:25:00', '2019-05-17 18:25:00'),
(3, 'AAA', 'A', 1, 1, '2019-05-20 21:59:23', '2019-05-20 21:59:23'),
(4, 'torpellino', 'A', 1, 1, '2019-05-28 16:56:56', '2019-05-28 16:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `nivel_programa`
--

CREATE TABLE `nivel_programa` (
  `idNivelPrograma` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `nivel_programa`
--

INSERT INTO `nivel_programa` (`idNivelPrograma`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'TÃ©cnico', 'A', 1, 1, '2019-05-13 20:53:08', '2019-05-13 20:53:08'),
(2, 'TecnÃ³logo', 'A', 1, 1, '2019-05-13 20:53:40', '2019-05-13 20:53:40'),
(4, 'Complementario', 'A', 1, 1, '2019-05-13 21:49:42', '2019-05-13 21:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `participante`
--

CREATE TABLE `participante` (
  `idParticipante` int(11) NOT NULL,
  `idAprendiz` int(11) DEFAULT NULL,
  `idEvento` int(11) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `idUsuarioC` int(11) DEFAULT NULL,
  `idUsuarioM` int(11) DEFAULT NULL,
  `fechaC` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaM` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `participante`
--

INSERT INTO `participante` (`idParticipante`, `idAprendiz`, `idEvento`, `puntaje`, `idUsuarioC`, `idUsuarioM`, `fechaC`, `fechaM`) VALUES
(7, 2, 1, 25, 1, 1, '2019-06-08 03:02:11', '2019-06-08 03:02:11'),
(8, 2, 2, 100, 1, 1, '2019-06-08 03:02:11', '2019-06-08 03:02:11'),
(9, 3, 2, 22, 1, 1, '2019-06-08 03:02:45', '2019-06-08 03:02:45'),
(10, 3, 2, 23, 1, 1, '2019-06-08 03:02:45', '2019-06-08 03:02:45'),
(11, 4, 1, 10, 1, 1, '2019-06-07 19:29:07', '2019-06-08 19:29:07');

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `primerNombre` varchar(30) NOT NULL,
  `segundoNombre` varchar(30) NOT NULL,
  `primerApellido` varchar(30) NOT NULL,
  `segundoApellido` varchar(30) NOT NULL,
  `identificacion` varchar(11) NOT NULL,
  `rh` varchar(4) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `idGimnasio` int(11) NOT NULL,
  `idEps` int(11) NOT NULL,
  `idCondicion` int(11) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`idPersona`, `primerNombre`, `segundoNombre`, `primerApellido`, `segundoApellido`, `identificacion`, `rh`, `direccion`, `telefono`, `email`, `idTipoIdentificacion`, `idGimnasio`, `idEps`, `idCondicion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Fabian', 'Camilo', 'Cano', 'Clavijo', '1016010678', 'A+', 'Calle 56 #8-51p', '3155458449', 'fccano87@misena.edu.co', 1, 1, 3, 0, 'A', 1, 1, '2019-06-07 03:30:23', '2019-06-07 03:30:23'),
(2, 'Sergio', 'Eduardo', 'Mosquera', 'Villegas', '1117818286', 'A+', 'Calle 25A sur #32-29', '3192120941', 'semosquera68@misena.edu.co', 1, 1, 5, 0, 'A', 1, 1, '2019-06-07 03:30:20', '2019-06-07 03:30:20'),
(4, 'Maria de', 'Los Angles', 'Cabrera', 'Perdomo', '1075313263', 'O+', 'Calle 21 #53-84', '3214388190', 'mdcabrera36@misena.edu.co', 1, 1, 6, 0, 'A', 1, 1, '2019-06-07 03:30:18', '2019-06-07 03:30:18'),
(7, 'Julian', 'Mauricio', 'Medina', 'Almonacid', '1075310854', 'O+', 'Calle 48 #19-69', '3105590838', 'jmmedina45@misena.edu.co', 1, 1, 7, 0, 'A', 1, 1, '2019-06-07 02:46:15', '2019-06-07 02:46:15'),
(8, 'Duvan', 'David', 'Dominguez', 'Falla', '1075303266', 'O+', 'Calle 20a sur #25-17', '3178690284', 'dddominguez6@misena.edu.co', 1, 1, 7, 0, 'A', 1, 1, '2019-06-07 03:23:22', '2019-06-07 03:23:22'),
(9, 'Jose', 'Luis', 'Gracia', 'Perafan', '1075312403', 'O-', 'calle 38#8-38', '3228086958', 'joseluisgraciaperafan@hotmail.com', 1, 1, 7, 0, 'A', 1, 1, '2019-06-07 03:32:40', '2019-06-07 03:32:40'),
(10, 'Alberto', '', 'Avila', 'Montenegro', '7709008', 'A+', 'Calle 85', '3103166666', 'aavila88@misena.edu.co', 1, 1, 7, 0, 'A', 1, 1, '2019-06-07 08:30:23', '2019-06-07 08:30:23'),
(11, 'Luis ', 'Alberto', 'Charry', 'Borrego', '11111111111', 'A+', 'cra 9# 12-09', '3112798345', 'lacharry@gmail.com', 1, 1, 9, 0, 'A', 1, 1, '2019-06-10 19:22:39', '2019-06-10 19:22:39'),
(12, 'Jesus', 'Ariel', 'Gonzales', 'Bonilla', '22222222222', 'AB', 'Tercer Milenio', '3152709910', 'jesus@gmail.com', 1, 2, 8, 0, 'A', 1, 1, '2019-06-10 19:28:21', '2019-06-10 19:28:21'),
(13, 'Juan', 'Camilo', 'Castro', 'Cuellar', '33333333333', 'B+', 'terminal', '3139085', 'juan@gmail.com', 1, 3, 8, 0, 'A', 1, 1, '2019-06-10 20:31:14', '2019-06-10 20:31:14'),
(14, 'Willintong', 'Arcenio', 'Rodriguez', 'Navarro', '123456789', 'AB', 'centro', '3167900931', 'willy@gmail.com', 1, 2, 9, 0, 'A', 1, 1, '2019-06-10 21:12:47', '2019-06-10 21:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `programa_formacion`
--

CREATE TABLE `programa_formacion` (
  `idProgramaFormacion` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `duracion` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `idNivelPrograma` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `programa_formacion`
--

INSERT INTO `programa_formacion` (`idProgramaFormacion`, `codigo`, `descripcion`, `duracion`, `idNivelPrograma`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(2, 1, ' AnÃ¡lisis y Desarrollo de Sistemas de InformaciÃ³n', '24', 2, 'A', 1, 1, '2019-05-13 23:32:29', '2019-05-13 21:40:08'),
(3, 2, 'Multimedia', '12', 1, 'A', 1, 1, '2019-05-13 21:40:29', '2019-05-13 21:40:29');

-- --------------------------------------------------------

--
-- Table structure for table `puntuacion`
--

CREATE TABLE `puntuacion` (
  `idPuntuacion` int(11) NOT NULL,
  `idInscripcionAprendiz` int(11) NOT NULL,
  `idEvento` int(11) NOT NULL,
  `participa` enum('S','N') COLLATE utf8_spanish2_ci NOT NULL,
  `puntaje` int(3) NOT NULL,
  `totalPuntaje` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `puntuacion`
--

INSERT INTO `puntuacion` (`idPuntuacion`, `idInscripcionAprendiz`, `idEvento`, `participa`, `puntaje`, `totalPuntaje`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(3, 3, 3, 'S', 5, 5, 'A', '2019-06-06 00:21:41', '2019-06-06 00:21:46', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `regional`
--

CREATE TABLE `regional` (
  `idRegional` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo` int(20) NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `regional`
--

INSERT INTO `regional` (`idRegional`, `nombre`, `codigo`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'SENA Huila.', 100, 'Zona Andina', 'A', 1, 1, '2019-05-13 17:11:16', '2019-06-03 14:35:46'),
(2, 'SENA Cundinamarca', 101, 'Zona Andina..', 'A', 1, 1, '2019-05-13 17:11:40', '2019-05-28 19:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `rol`
--

CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rol`
--

INSERT INTO `rol` (`idRol`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Administrador', 'A', 1, 1, '2019-05-17 21:08:50', '2019-05-17 21:08:50'),
(2, 'Lider Bienestar', 'A', 1, 1, '2019-05-17 21:08:57', '2019-05-17 21:08:57'),
(3, 'Profesional Apoyo Gimnasio', 'A', 1, 1, '2019-05-17 21:09:04', '2019-05-17 21:09:04'),
(4, 'Profesional Apoyo Deportivo', 'A', 1, 1, '2019-06-03 14:50:00', '2019-06-03 14:50:00'),
(5, 'Aprendiz', 'A', 1, 1, '2019-06-03 14:50:55', '2019-06-03 14:50:55'),
(6, 'Instructor', 'A', 1, 1, '2019-06-10 19:18:45', '2019-06-10 19:18:45');

-- --------------------------------------------------------

--
-- Table structure for table `rutina`
--

CREATE TABLE `rutina` (
  `idRutina` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rutina`
--

INSERT INTO `rutina` (`idRutina`, `nombre`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(3, 'tonificar', 1, 1, '2019-06-11 05:11:17', '2019-06-12 05:00:00'),
(4, 'bajar de peso', 1, 1, '2019-06-12 05:00:00', '2019-06-12 05:00:00'),
(5, 'hipertrofia', 1, 1, '2019-06-12 05:00:00', '2019-06-12 05:00:00'),
(6, 'mejorar rendimiento', 1, 1, '2019-06-12 05:00:00', '2019-06-12 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rutina_alimenticia`
--

CREATE TABLE `rutina_alimenticia` (
  `idRutina` int(12) NOT NULL,
  `rutina` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `imcMenor` float NOT NULL,
  `imcMayor` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rutina_alimenticia`
--

INSERT INTO `rutina_alimenticia` (`idRutina`, `rutina`, `imcMenor`, `imcMayor`) VALUES
(1, 'Jugos y adicionar leche en polvo para utilizarlo como suplemento, comer alimentos  magros(pollo, pescado, atun, queso, arroz, frijoles), en el desayuno comer frutas y huevos, en las horas de las ma?ana poder comer yogurt o jugos que tengan leche con galletas o pan integral, noche algo de comida con una fruta y jugo.', 0, 15.99),
(2, 'Jugos en leche (leche en polvo para utilizarlo como suplemento) tambien puedes tomarlo en chocolates y tinto, comer 5 comidas al dia con cantidad proporcionada ejemplo ensalada y verduras,alimentos magros(pollo, pescado, atun, queso, arroz, frijoles)para bajar la comida puedes tomar jugos en agua y agregar miel, ingerir frutas y vegetales.\r\n', 16, 16.99),
(3, 'Desayuno : Jugos con leche, comer moderados alimen', 17, 18.49),
(4, 'comer 3 comidas al d?a con cantidad proporcionada ejemplo ensalada y verduras,alimentos	)para bajar la comida puedes tomar jugos en agua y agregar miel, ingerir frutas y vegetales, en las noches comer solo jugos y alimentos integrales.', 18.5, 24.99),
(7, '1. L?cteos: La leche y los yogures ser?n desnatados; los quesos, magros. Se aconsejan 2-3 raciones al d?a para asegurar un aporte adecuado de calcio.\r\n2. Carnes y pescados: Se retirar? la grasa visible antes de la cocci?n,en el caso de las aves, quitar toda la piel. Evitaremos alimentos ricos en grasa saturada, como los embutidos y el tocino. Se recomienda consumir pescado al menos tres veces a la semana.\r\n3. Frutas y verduras: Elige frutas crudas de consistencia firme, evitando las piezas cocid', 25, 29.99);

-- --------------------------------------------------------

--
-- Table structure for table `rutina_usuario`
--

CREATE TABLE `rutina_usuario` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `hora` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `idRutina` int(11) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `rutina_usuario`
--

INSERT INTO `rutina_usuario` (`id`, `idUsuario`, `hora`, `idRutina`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(3, 5, '8-10', 3, 1, 1, '2019-05-12 05:00:00', '2019-05-12 05:00:00'),
(4, 5, '8-10', 4, 1, 1, '2019-06-12 05:00:00', '2019-06-12 05:00:00'),
(5, 5, '8-10', 5, 1, 1, '2019-06-18 05:00:00', '2019-06-18 05:00:00'),
(6, 6, '10-12', 3, 1, 1, '2019-06-14 05:00:00', '2019-06-14 05:00:00'),
(7, 6, '10-12', 4, 1, 1, '2019-07-14 05:00:00', '2019-07-14 05:00:00'),
(8, 6, '10-12', 5, 1, 1, '2019-08-14 05:00:00', '2019-08-14 05:00:00'),
(9, 12, '13-15', 4, 1, 1, '2019-06-14 05:00:00', '2019-06-14 05:00:00'),
(10, 13, '13-15', 4, 1, 1, '2019-06-14 05:00:00', '2019-06-14 05:00:00'),
(11, 14, '10-12', 4, 1, 1, '2019-07-14 05:00:00', '2019-07-14 05:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sede`
--

CREATE TABLE `sede` (
  `idSede` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` int(10) NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `idCentroFormacion` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `sede`
--

INSERT INTO `sede` (`idSede`, `nombre`, `telefono`, `direccion`, `idCentroFormacion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Industrial', 8764401, 'Carrera 9 No. 68 - 50', 2, 'A', 1, 1, '2019-05-21 16:50:53', '2019-05-21 16:50:53'),
(2, 'Comercio', 8757040, 'Kra. 5 No. 16 - 16', 2, 'A', 1, 1, '2019-05-13 19:29:11', '2019-05-13 19:29:11'),
(3, 'TecnoParque', 8670078, 'Diagonal 20 NÂº 38 -16', 2, 'A', 1, 1, '2019-05-21 16:43:03', '2019-05-21 16:43:03');

-- --------------------------------------------------------

--
-- Table structure for table `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(80) DEFAULT NULL,
  `tiempoActividad` varchar(30) DEFAULT NULL,
  `estado` enum('A','I') NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `fechaModificacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo`
--

INSERT INTO `tipo` (`idTipo`, `nombre`, `descripcion`, `tiempoActividad`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'MicroFutbol', 'Un equipo por ambiente.', '30 Minutos', 'A', 1, 1, '0000-00-00', '0000-00-00'),
(2, 'Ajedres', 'dos personas por ficha', '90 Minutos', 'A', 0, 0, '0000-00-00', '0000-00-00'),
(3, 'natacion', NULL, NULL, 'A', 1, 1, '2019-06-12', '2019-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_evento`
--

CREATE TABLE `tipo_evento` (
  `idTipoEvento` int(11) NOT NULL,
  `descripcion` varchar(20) CHARACTER SET utf8 NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `tipo_evento`
--

INSERT INTO `tipo_evento` (`idTipoEvento`, `descripcion`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(3, 'Fútbol Sala', 'A', '2019-06-05 18:50:27', '2019-06-05 18:50:33', 1, 0),
(4, 'Atletismo', 'A', '2019-06-05 19:00:37', '2019-06-05 19:00:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipo_identificacion`
--

CREATE TABLE `tipo_identificacion` (
  `idTipoIdentificacion` int(11) NOT NULL,
  `tipo` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `tipo_identificacion`
--

INSERT INTO `tipo_identificacion` (`idTipoIdentificacion`, `tipo`, `descripcion`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'CC', 'Cedula de Ciudadania', 'A', 1, 1, '2019-06-03 13:57:09', '2019-05-13 16:28:57'),
(2, 'TI', 'Tarjeta de Identidad', 'A', 1, 1, '2019-05-13 16:16:38', '2019-05-13 16:16:38'),
(3, 'RC', 'Registro Civil', 'A', 1, 1, '2019-06-03 13:57:53', '2019-06-03 13:57:53'),
(4, 'CE', 'Cedula Extranjera', 'A', 1, 1, '2019-06-03 14:07:35', '2019-06-03 14:07:35'),
(5, 'NIP', 'Numero  Identificacion Personal', 'A', 1, 1, '2019-06-03 14:09:08', '2019-06-03 14:09:08'),
(6, 'NUIP', 'Numero Unico de Identificacion Personal', 'A', 1, 1, '2019-06-03 14:10:06', '2019-06-03 14:10:06'),
(7, 'DNI', 'Documento Nacional de Identidad', 'A', 1, 1, '2019-06-03 14:12:12', '2019-06-03 14:12:12'),
(8, 'PIP', 'Permiso de Ingreso y Permanencia', 'A', 1, 1, '2019-06-03 14:15:04', '2019-06-03 14:15:04');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `usuario` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasenia` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` enum('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idRol` int(11) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `usuario`, `contrasenia`, `estado`, `idPersona`, `idRol`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, '1016010678', '25d55ad283aa400af464c76d713c07ad', 'A', 1, 1, 1, 1, '2019-06-07 02:53:12', '2019-06-07 02:53:12'),
(5, '7709008', '25d55ad283aa400af464c76d713c07ad', 'A', 10, 1, 1, 1, '2019-05-20 00:20:05', '2019-05-20 00:20:05'),
(6, '1075313263', '25d55ad283aa400af464c76d713c07ad', 'A', 4, 1, 1, 1, '2019-05-24 00:23:41', '2019-05-24 00:23:41'),
(7, '1117818286', '25d55ad283aa400af464c76d713c07ad', 'A', 2, 1, 1, 1, '2019-06-07 02:54:24', '2019-06-07 02:54:24'),
(11, '1075310854', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 7, 5, 1, 1, '2019-06-07 03:11:02', '2019-06-07 03:11:02'),
(12, '1075303266', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 8, 5, 1, 1, '2019-06-07 03:23:28', '2019-06-07 03:23:28'),
(13, '1075312403', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 9, 5, 1, 1, '2019-06-07 03:32:48', '2019-06-07 03:32:48'),
(14, '11111111111', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 11, 5, 1, 1, '2019-06-10 19:22:46', '2019-06-10 19:22:46'),
(15, '22222222222', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 12, 6, 1, 1, '2019-06-10 19:28:25', '2019-06-10 19:28:25'),
(20, '33333333333', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 13, 4, 1, 1, '2019-06-10 20:31:20', '2019-06-10 20:31:20'),
(21, '123456789', 'd41d8cd98f00b204e9800998ecf8427e', 'A', 14, 3, 1, 1, '2019-06-10 21:12:53', '2019-06-10 21:12:53');

-- --------------------------------------------------------

--
-- Structure for view `acudienteauto`
--
DROP TABLE IF EXISTS `acudienteauto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `acudienteauto`  AS  (select `acudiente`.`idAcudiente` AS `idAcudiente`,concat(`acudiente`.`nombre`,' ',`acudiente`.`apellido`) AS `nombreAcudiente` from `acudiente`) ;

-- --------------------------------------------------------

--
-- Structure for view `aprendizauto`
--
DROP TABLE IF EXISTS `aprendizauto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aprendizauto`  AS  (select `aprendiz`.`idAprendiz` AS `idAprendiz`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `nombrePersona` from ((`aprendiz` join `usuario` on((`aprendiz`.`idUsuario` = `usuario`.`idUsuario`))) join `persona` on((`usuario`.`idPersona` = `persona`.`idPersona`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `autocompletar`
--
DROP TABLE IF EXISTS `autocompletar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `autocompletar`  AS  (select `persona`.`idPersona` AS `idPersona`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from `persona`) ;

-- --------------------------------------------------------

--
-- Structure for view `entrenadorvista`
--
DROP TABLE IF EXISTS `entrenadorvista`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `entrenadorvista`  AS  (select `entrenador`.`idEntrenador` AS `idEntrenador`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from (`entrenador` join `persona` on((`persona`.`idPersona` = `entrenador`.`idPersona`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `instructordeficha`
--
DROP TABLE IF EXISTS `instructordeficha`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `instructordeficha`  AS  (select `instructor`.`idInstructor` AS `idInstructor`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from ((`instructor` join `usuario` on((`usuario`.`idUsuario` = `instructor`.`idUsuario`))) join `persona` on((`persona`.`idPersona` = `usuario`.`idPersona`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `instructornombre`
--
DROP TABLE IF EXISTS `instructornombre`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `instructornombre`  AS  (select `usuario`.`idUsuario` AS `idUsuario`,`usuario`.`idPersona` AS `idPersona`,concat(`persona`.`primerNombre`,' ',`persona`.`segundoNombre`,' ',`persona`.`primerApellido`,' ',`persona`.`segundoApellido`) AS `Nombre_Completo` from (`usuario` join `persona` on((`usuario`.`idPersona` = `persona`.`idPersona`)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`idActividad`),
  ADD KEY `id_gimnasio` (`idGimnasio`),
  ADD KEY `id_tipo` (`idTipo`),
  ADD KEY `id_fecha` (`fechaInicio`);

--
-- Indexes for table `acudiente`
--
ALTER TABLE `acudiente`
  ADD PRIMARY KEY (`idAcudiente`);

--
-- Indexes for table `ambiente`
--
ALTER TABLE `ambiente`
  ADD PRIMARY KEY (`idAmbiente`);

--
-- Indexes for table `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD PRIMARY KEY (`idAprendiz`),
  ADD KEY `id_usuario` (`idUsuario`),
  ADD KEY `id_persona` (`idAcudiente`),
  ADD KEY `id_imc` (`idImc`);

--
-- Indexes for table `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  ADD PRIMARY KEY (`idAprendizFicha`),
  ADD KEY `id_ficha` (`idFicha`),
  ADD KEY `id_aprendiz` (`idAprendiz`);

--
-- Indexes for table `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`articulo_id`),
  ADD KEY `categoria_id` (`categoria_id`);

--
-- Indexes for table `aseguradora`
--
ALTER TABLE `aseguradora`
  ADD PRIMARY KEY (`idAseguradora`),
  ADD UNIQUE KEY `nit` (`nit`);

--
-- Indexes for table `asistenciagimnasio`
--
ALTER TABLE `asistenciagimnasio`
  ADD PRIMARY KEY (`idAsis`),
  ADD KEY `idGim` (`idGim`),
  ADD KEY `idPer` (`idPer`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoria_id`);

--
-- Indexes for table `centro_formacion`
--
ALTER TABLE `centro_formacion`
  ADD PRIMARY KEY (`idCentroFormacion`),
  ADD KEY `id_aseguradora` (`idAseguradora`),
  ADD KEY `id_ciudad` (`idCiudad`),
  ADD KEY `id_regional` (`idRegional`);

--
-- Indexes for table `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idCiudad`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `id_departamento` (`idDepartamento`);

--
-- Indexes for table `condicion_preexistente`
--
ALTER TABLE `condicion_preexistente`
  ADD PRIMARY KEY (`idEnfermedad`),
  ADD KEY `idGrupo` (`idGrupo`);

--
-- Indexes for table `contrato`
--
ALTER TABLE `contrato`
  ADD PRIMARY KEY (`idContrato`);

--
-- Indexes for table `crear_evento`
--
ALTER TABLE `crear_evento`
  ADD PRIMARY KEY (`idCrearEvento`),
  ADD KEY `idTipoEvento` (`idTipoEvento`);

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`idDepartamento`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `entrenador`
--
ALTER TABLE `entrenador`
  ADD PRIMARY KEY (`idEntrenador`),
  ADD KEY `idPersona` (`idPersona`);

--
-- Indexes for table `entrenador_sede`
--
ALTER TABLE `entrenador_sede`
  ADD PRIMARY KEY (`idEntrenadorSede`),
  ADD KEY `idEntrenador` (`idEntrenador`),
  ADD KEY `idSede` (`idSede`);

--
-- Indexes for table `eps`
--
ALTER TABLE `eps`
  ADD PRIMARY KEY (`idEps`);

--
-- Indexes for table `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`idEquipo`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`idEvento`),
  ADD KEY `idSede` (`idSede`),
  ADD KEY `idTipo` (`idTipo`);

--
-- Indexes for table `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`idFicha`),
  ADD KEY `id_jornada` (`idJornada`),
  ADD KEY `id_ambiente` (`idAmbiente`),
  ADD KEY `id_programa_formacion` (`idProgramaFormacion`),
  ADD KEY `idSede` (`idSede`);

--
-- Indexes for table `gimnasio`
--
ALTER TABLE `gimnasio`
  ADD PRIMARY KEY (`idGimnasio`),
  ADD KEY `id_sede` (`idSede`);

--
-- Indexes for table `grupo_enfermedades`
--
ALTER TABLE `grupo_enfermedades`
  ADD PRIMARY KEY (`idGrupo`);

--
-- Indexes for table `imc`
--
ALTER TABLE `imc`
  ADD PRIMARY KEY (`idImc`);

--
-- Indexes for table `imccontrol`
--
ALTER TABLE `imccontrol`
  ADD PRIMARY KEY (`idImcC`),
  ADD KEY `idAprendiz` (`idAprendiz`);

--
-- Indexes for table `inscripcion_aprendiz`
--
ALTER TABLE `inscripcion_aprendiz`
  ADD PRIMARY KEY (`idInscripcionAprendiz`),
  ADD KEY `idCrearEvento` (`idCrearEvento`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`idInstructor`),
  ADD KEY `id_usuario` (`idUsuario`),
  ADD KEY `id_persona` (`estado`),
  ADD KEY `idContrato` (`idContrato`);

--
-- Indexes for table `instructor_ficha`
--
ALTER TABLE `instructor_ficha`
  ADD PRIMARY KEY (`idInstructorFicha`),
  ADD KEY `id_ficha` (`idFicha`),
  ADD KEY `id_instructor` (`idInstructor`);

--
-- Indexes for table `jornada`
--
ALTER TABLE `jornada`
  ADD PRIMARY KEY (`idJornada`);

--
-- Indexes for table `manual_usuario`
--
ALTER TABLE `manual_usuario`
  ADD PRIMARY KEY (`idManualUsuario`);

--
-- Indexes for table `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idMarca`);

--
-- Indexes for table `nivel_programa`
--
ALTER TABLE `nivel_programa`
  ADD PRIMARY KEY (`idNivelPrograma`);

--
-- Indexes for table `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`idParticipante`),
  ADD KEY `idAprendiz` (`idAprendiz`),
  ADD KEY `idEvento` (`idEvento`);

--
-- Indexes for table `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD KEY `id_tipo_identificacion` (`idTipoIdentificacion`),
  ADD KEY `id_gimnasio` (`idGimnasio`),
  ADD KEY `id_eps` (`idEps`),
  ADD KEY `idCondicion` (`idCondicion`);

--
-- Indexes for table `programa_formacion`
--
ALTER TABLE `programa_formacion`
  ADD PRIMARY KEY (`idProgramaFormacion`),
  ADD KEY `id_nivel_programa` (`idNivelPrograma`);

--
-- Indexes for table `puntuacion`
--
ALTER TABLE `puntuacion`
  ADD PRIMARY KEY (`idPuntuacion`),
  ADD KEY `idEvento` (`idEvento`);

--
-- Indexes for table `regional`
--
ALTER TABLE `regional`
  ADD PRIMARY KEY (`idRegional`);

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indexes for table `rutina`
--
ALTER TABLE `rutina`
  ADD PRIMARY KEY (`idRutina`);

--
-- Indexes for table `rutina_alimenticia`
--
ALTER TABLE `rutina_alimenticia`
  ADD PRIMARY KEY (`idRutina`);

--
-- Indexes for table `rutina_usuario`
--
ALTER TABLE `rutina_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`idRutina`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indexes for table `sede`
--
ALTER TABLE `sede`
  ADD PRIMARY KEY (`idSede`),
  ADD KEY `id_centro_formacion` (`idCentroFormacion`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indexes for table `tipo_evento`
--
ALTER TABLE `tipo_evento`
  ADD PRIMARY KEY (`idTipoEvento`);

--
-- Indexes for table `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  ADD PRIMARY KEY (`idTipoIdentificacion`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `Id_Persona` (`idPersona`),
  ADD KEY `idRol` (`idRol`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actividad`
--
ALTER TABLE `actividad`
  MODIFY `idActividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `acudiente`
--
ALTER TABLE `acudiente`
  MODIFY `idAcudiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ambiente`
--
ALTER TABLE `ambiente`
  MODIFY `idAmbiente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `aprendiz`
--
ALTER TABLE `aprendiz`
  MODIFY `idAprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  MODIFY `idAprendizFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `articulo`
--
ALTER TABLE `articulo`
  MODIFY `articulo_id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aseguradora`
--
ALTER TABLE `aseguradora`
  MODIFY `idAseguradora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `asistenciagimnasio`
--
ALTER TABLE `asistenciagimnasio`
  MODIFY `idAsis` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoria_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `centro_formacion`
--
ALTER TABLE `centro_formacion`
  MODIFY `idCentroFormacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `contrato`
--
ALTER TABLE `contrato`
  MODIFY `idContrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `crear_evento`
--
ALTER TABLE `crear_evento`
  MODIFY `idCrearEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `departamento`
--
ALTER TABLE `departamento`
  MODIFY `idDepartamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `entrenador`
--
ALTER TABLE `entrenador`
  MODIFY `idEntrenador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `entrenador_sede`
--
ALTER TABLE `entrenador_sede`
  MODIFY `idEntrenadorSede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eps`
--
ALTER TABLE `eps`
  MODIFY `idEps` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `equipo`
--
ALTER TABLE `equipo`
  MODIFY `idEquipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `evento`
--
ALTER TABLE `evento`
  MODIFY `idEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ficha`
--
ALTER TABLE `ficha`
  MODIFY `idFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gimnasio`
--
ALTER TABLE `gimnasio`
  MODIFY `idGimnasio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `imc`
--
ALTER TABLE `imc`
  MODIFY `idImc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `imccontrol`
--
ALTER TABLE `imccontrol`
  MODIFY `idImcC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inscripcion_aprendiz`
--
ALTER TABLE `inscripcion_aprendiz`
  MODIFY `idInscripcionAprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `idInstructor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `instructor_ficha`
--
ALTER TABLE `instructor_ficha`
  MODIFY `idInstructorFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jornada`
--
ALTER TABLE `jornada`
  MODIFY `idJornada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `manual_usuario`
--
ALTER TABLE `manual_usuario`
  MODIFY `idManualUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `marca`
--
ALTER TABLE `marca`
  MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nivel_programa`
--
ALTER TABLE `nivel_programa`
  MODIFY `idNivelPrograma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `participante`
--
ALTER TABLE `participante`
  MODIFY `idParticipante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `programa_formacion`
--
ALTER TABLE `programa_formacion`
  MODIFY `idProgramaFormacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `puntuacion`
--
ALTER TABLE `puntuacion`
  MODIFY `idPuntuacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `regional`
--
ALTER TABLE `regional`
  MODIFY `idRegional` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rol`
--
ALTER TABLE `rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rutina`
--
ALTER TABLE `rutina`
  MODIFY `idRutina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rutina_usuario`
--
ALTER TABLE `rutina_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sede`
--
ALTER TABLE `sede`
  MODIFY `idSede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipo_evento`
--
ALTER TABLE `tipo_evento`
  MODIFY `idTipoEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  MODIFY `idTipoIdentificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `actividad_ibfk_1` FOREIGN KEY (`idGimnasio`) REFERENCES `gimnasio` (`idGimnasio`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `actividad_ibfk_2` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idTipo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD CONSTRAINT `aprendiz_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendiz_ibfk_2` FOREIGN KEY (`idImc`) REFERENCES `imc` (`idImc`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendiz_ibfk_3` FOREIGN KEY (`idAcudiente`) REFERENCES `acudiente` (`idAcudiente`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `aprendiz_ficha`
--
ALTER TABLE `aprendiz_ficha`
  ADD CONSTRAINT `aprendiz_ficha_ibfk_1` FOREIGN KEY (`idAprendiz`) REFERENCES `aprendiz` (`idAprendiz`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `aprendiz_ficha_ibfk_2` FOREIGN KEY (`idFicha`) REFERENCES `ficha` (`idFicha`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `articulo_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`);

--
-- Constraints for table `asistenciagimnasio`
--
ALTER TABLE `asistenciagimnasio`
  ADD CONSTRAINT `asistenciagimnasio_ibfk_1` FOREIGN KEY (`idGim`) REFERENCES `gimnasio` (`idGimnasio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asistenciagimnasio_ibfk_2` FOREIGN KEY (`idPer`) REFERENCES `persona` (`idPersona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `centro_formacion`
--
ALTER TABLE `centro_formacion`
  ADD CONSTRAINT `centro_formacion_ibfk_1` FOREIGN KEY (`idCiudad`) REFERENCES `ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `centro_formacion_ibfk_2` FOREIGN KEY (`idRegional`) REFERENCES `regional` (`idRegional`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `centro_formacion_ibfk_3` FOREIGN KEY (`idAseguradora`) REFERENCES `aseguradora` (`idAseguradora`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`idDepartamento`) REFERENCES `departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `condicion_preexistente`
--
ALTER TABLE `condicion_preexistente`
  ADD CONSTRAINT `condicion_preexistente_ibfk_1` FOREIGN KEY (`idGrupo`) REFERENCES `grupo_enfermedades` (`idGrupo`);

--
-- Constraints for table `crear_evento`
--
ALTER TABLE `crear_evento`
  ADD CONSTRAINT `crear_evento_ibfk_1` FOREIGN KEY (`idTipoEvento`) REFERENCES `tipo_evento` (`idTipoEvento`) ON UPDATE CASCADE;

--
-- Constraints for table `entrenador`
--
ALTER TABLE `entrenador`
  ADD CONSTRAINT `entrenador_ibfk_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `entrenador_sede`
--
ALTER TABLE `entrenador_sede`
  ADD CONSTRAINT `entrenador_sede_ibfk_1` FOREIGN KEY (`idEntrenador`) REFERENCES `entrenador` (`idEntrenador`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `entrenador_sede_ibfk_2` FOREIGN KEY (`idSede`) REFERENCES `sede` (`idSede`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`idSede`) REFERENCES `sede` (`idSede`),
  ADD CONSTRAINT `evento_ibfk_2` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idTipo`);

--
-- Constraints for table `ficha`
--
ALTER TABLE `ficha`
  ADD CONSTRAINT `ficha_ibfk_1` FOREIGN KEY (`idJornada`) REFERENCES `jornada` (`idJornada`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_ibfk_2` FOREIGN KEY (`idAmbiente`) REFERENCES `ambiente` (`idAmbiente`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_ibfk_3` FOREIGN KEY (`idProgramaFormacion`) REFERENCES `programa_formacion` (`idProgramaFormacion`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ficha_ibfk_4` FOREIGN KEY (`idSede`) REFERENCES `sede` (`idSede`);

--
-- Constraints for table `gimnasio`
--
ALTER TABLE `gimnasio`
  ADD CONSTRAINT `gimnasio_ibfk_1` FOREIGN KEY (`idSede`) REFERENCES `sede` (`idSede`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `imccontrol`
--
ALTER TABLE `imccontrol`
  ADD CONSTRAINT `imccontrol_ibfk_1` FOREIGN KEY (`idAprendiz`) REFERENCES `aprendiz` (`idAprendiz`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inscripcion_aprendiz`
--
ALTER TABLE `inscripcion_aprendiz`
  ADD CONSTRAINT `inscripcion_aprendiz_ibfk_1` FOREIGN KEY (`idCrearEvento`) REFERENCES `crear_evento` (`idCrearEvento`);

--
-- Constraints for table `instructor`
--
ALTER TABLE `instructor`
  ADD CONSTRAINT `instructor_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `instructor_ibfk_2` FOREIGN KEY (`idContrato`) REFERENCES `contrato` (`idContrato`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `instructor_ficha`
--
ALTER TABLE `instructor_ficha`
  ADD CONSTRAINT `instructor_ficha_ibfk_1` FOREIGN KEY (`idInstructor`) REFERENCES `instructor` (`idInstructor`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `instructor_ficha_ibfk_2` FOREIGN KEY (`idFicha`) REFERENCES `ficha` (`idFicha`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `participante`
--
ALTER TABLE `participante`
  ADD CONSTRAINT `participante_ibfk_1` FOREIGN KEY (`idAprendiz`) REFERENCES `aprendiz` (`idAprendiz`),
  ADD CONSTRAINT `participante_ibfk_2` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`idEvento`);

--
-- Constraints for table `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipo_identificacion` (`idTipoIdentificacion`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`idEps`) REFERENCES `eps` (`idEps`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `persona_ibfk_3` FOREIGN KEY (`idGimnasio`) REFERENCES `gimnasio` (`idGimnasio`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `persona_ibfk_4` FOREIGN KEY (`idCondicion`) REFERENCES `condicion_preexistente` (`idEnfermedad`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `programa_formacion`
--
ALTER TABLE `programa_formacion`
  ADD CONSTRAINT `programa_formacion_ibfk_1` FOREIGN KEY (`idNivelPrograma`) REFERENCES `nivel_programa` (`idNivelPrograma`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `rutina_usuario`
--
ALTER TABLE `rutina_usuario`
  ADD CONSTRAINT `rutina_usuario_ibfk_1` FOREIGN KEY (`idRutina`) REFERENCES `rutina` (`idRutina`),
  ADD CONSTRAINT `rutina_usuario_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`);

--
-- Constraints for table `sede`
--
ALTER TABLE `sede`
  ADD CONSTRAINT `sede_ibfk_1` FOREIGN KEY (`idCentroFormacion`) REFERENCES `centro_formacion` (`idCentroFormacion`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
