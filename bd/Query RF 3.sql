/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.38-MariaDB : Database - proyectoformativo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`proyectoformativo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `proyectoformativo`;

/*Table structure for table `tipo_evento` */

DROP TABLE IF EXISTS `tipo_evento`;

CREATE TABLE `tipo_evento` (
  `idTipoEvento` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(20) CHARACTER SET utf8 NOT NULL,
  `estado` ENUM('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` INT(11) NOT NULL,
  `idUsuarioModificacion` INT(11) NOT NULL,
  PRIMARY KEY (`idTipoEvento`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `tipo_evento` */

INSERT  INTO `tipo_evento`(`idTipoEvento`,`descripcion`,`estado`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`) VALUES (3,'Fútbol Sala','A','2019-06-05 13:50:27','2019-06-05 13:50:33',1,0),(4,'Atletismo','A','2019-06-05 14:00:37','2019-06-05 14:00:37',1,1);


/*Table structure for table `crear_evento` */

DROP TABLE IF EXISTS `crear_evento`;

CREATE TABLE `crear_evento` (
  `idCrearEvento` INT(11) NOT NULL AUTO_INCREMENT,
  `idTipoEvento` INT(11) NOT NULL,
  `nombreEvento` VARCHAR(30) CHARACTER SET utf8 NOT NULL,
  `fechaInicio` DATE DEFAULT NULL,
  `fechaCulminacion` DATE DEFAULT NULL,
  `estado` ENUM('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` INT(11) NOT NULL,
  `idUsuarioModificacion` INT(11) NOT NULL,
  PRIMARY KEY (`idCrearEvento`),
  KEY `idTipoEvento` (`idTipoEvento`),
  CONSTRAINT `crear_evento_ibfk_1` FOREIGN KEY (`idTipoEvento`) REFERENCES `tipo_evento` (`idTipoEvento`) ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `crear_evento` */

INSERT  INTO `crear_evento`(`idCrearEvento`,`idTipoEvento`,`nombreEvento`,`fechaInicio`,`fechaCulminacion`,`estado`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`) VALUES (3,3,'Apertura','2019-06-05','2019-07-04','A','2019-06-05 14:18:58','2019-06-05 14:19:05',1,0);

/*Table structure for table `inscripcion_aprendiz` */

DROP TABLE IF EXISTS `inscripcion_aprendiz`;

CREATE TABLE `inscripcion_aprendiz` (
  `idInscripcionAprendiz` INT(11) NOT NULL AUTO_INCREMENT,
  `idCrearEvento` INT(11) NOT NULL,
  `idTipoIdentificacion` INT(11) NOT NULL,
  `numeroIdentificaion` VARCHAR(15) CHARACTER SET utf8 NOT NULL,
  `nombreAprendiz` VARCHAR(30) CHARACTER SET utf8 NOT NULL,
  `aprellidoAprendiz` VARCHAR(30) CHARACTER SET utf8 NOT NULL,
  `ficha` VARCHAR(10) CHARACTER SET utf8 NOT NULL,
  `estado` ENUM('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` INT(11) NOT NULL,
  `idUsuarioModificacion` INT(11) NOT NULL,
  PRIMARY KEY (`idInscripcionAprendiz`),
  KEY `inscripcion_aprendiz_ibfk_1` (`idCrearEvento`),
  KEY `inscripcion_aprendiz_ibfk_2` (`idTipoIdentificacion`),
  CONSTRAINT `inscripcion_aprendiz_ibfk_1` FOREIGN KEY (`idCrearEvento`) REFERENCES `crear_evento` (`idCrearEvento`) ON UPDATE CASCADE,
  CONSTRAINT `inscripcion_aprendiz_ibfk_2` FOREIGN KEY (`idTipoIdentificacion`) REFERENCES `tipo_identificacion` (`idTipoIdentificacion`) ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `inscripcion_aprendiz` */

INSERT  INTO `inscripcion_aprendiz`(`idInscripcionAprendiz`,`idCrearEvento`,`idTipoIdentificacion`,`numeroIdentificaion`,`nombreAprendiz`,`aprellidoAprendiz`,`ficha`,`estado`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`) VALUES (3,3,1,'103214758','Juan Andres','Perdomo Lizcano','784536','A','2019-06-05 14:20:38','2019-06-05 14:20:45',1,0);

/*Table structure for table `puntuacion` */

DROP TABLE IF EXISTS `puntuacion`;

CREATE TABLE `puntuacion` (
  `idPuntuacion` INT(11) NOT NULL AUTO_INCREMENT,
  `idInscripcionAprendiz` INT(11) NOT NULL,
  `idEvento` INT(11) NOT NULL,
  `participa` ENUM('S','N') COLLATE utf8_spanish2_ci NOT NULL,
  `puntaje` INT(3) NOT NULL,
  `totalPuntaje` INT(11) NOT NULL,
  `estado` ENUM('A','I') COLLATE utf8_spanish2_ci NOT NULL,
  `fechaCreacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaModificacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` INT(11) NOT NULL,
  `idUsuarioModificacion` INT(11) NOT NULL,
  PRIMARY KEY (`idPuntuacion`),
  KEY `puntuacion_ibfk_1` (`idInscripcionAprendiz`),
  KEY `idEvento` (`idEvento`),
  CONSTRAINT `puntuacion_ibfk_1` FOREIGN KEY (`idInscripcionAprendiz`) REFERENCES `inscripcion_aprendiz` (`idInscripcionAprendiz`) ON UPDATE CASCADE,
  CONSTRAINT `puntuacion_ibfk_2` FOREIGN KEY (`idEvento`) REFERENCES `inscripcion_aprendiz` (`idCrearEvento`) ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `puntuacion` */

INSERT  INTO `puntuacion`(`idPuntuacion`,`idInscripcionAprendiz`,`idEvento`,`participa`,`puntaje`,`totalPuntaje`,`estado`,`fechaCreacion`,`fechaModificacion`,`idUsuarioCreacion`,`idUsuarioModificacion`) VALUES (3,3,3,'S',5,5,'A','2019-06-05 14:21:41','2019-06-05 14:21:46',1,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;