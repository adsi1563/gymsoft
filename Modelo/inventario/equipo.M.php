<?php
class Accion{
    
    private $idequipo;
    private $nombremaquina;
    private $manual;
    private $fechaingreso;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $estado;
    public  $conn=null;
    
  /**
     * @return mixed
     */
    public function getIdequipo()
    {
        return $this->idequipo;
    }

    /**
     * @param mixed $idequipo
     *
     * @return self
     */
    public function setIdequipo($idequipo)
    {
        $this->idequipo = $idequipo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombremaquina()
    {
        return $this->nombremaquina;
    }

    /**
     * @param mixed $nombremaquina
     *
     * @return self
     */
    public function setNombremaquina($nombremaquina)
    {
        $this->nombremaquina = $nombremaquina;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getManual()
    {
        return $this->manual;
    }

    /**
     * @param mixed $manual
     *
     * @return self
     */
    public function setManual($manual)
    {
        $this->manual = $manual;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaingreso()
    {
        return $this->fechaingreso;
    }

    /**
     * @param mixed $fechaingreso
     *
     * @return self
     */
    public function setFechaingreso($fechaingreso)
    {
        $this->fechaingreso = $fechaingreso;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO equipo 
                                (nombre, descripcion, fechaIngreso, estado, idUsuarioCreacion, idUsuarioModificacion)
                             VALUES 
                                ('$this->nombremaquina', '$this->manual','$this->fechaingreso', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE 
                                equipo 
                            SET nombre = '$this->nombremaquina',
                                descripcion = '$this->manual',
                                fechaIngreso = '$this->fechaingreso',
                                estado = '$this->estado',
                        WHERE   idEquipo = $this->idequipo  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
       $sentenciaSql = "UPDATE  
                            equipo
                        SET
                            estado='I'
                        WHERE 
                            idEquipo = $this->idequipo";     
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idEquipo
                                , nombre
                                , descripcion
                                , fechaIngreso
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 equipo $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idequipo !=''){
            $condicion=$condicion.$whereAnd."equipo.idEquipo  = $this->idequipo";
            $whereAnd = ' AND ';
            
        }      
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idequipo);
        unset($this->nombremaquina);
        unset($this->descripcion);
        unset($this->fechaingreso);
        unset($this->fechaCreacion);
        unset($this->estado);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }

   
}

?>
