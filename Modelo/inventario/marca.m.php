<?php
class Accion{
    
    private $idMarca;
    private $nombre;
    private $descripcion;
    private $marca;
    private $tipoElemento;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    /**
     * @return mixed
     */
    public function getIdMarca()
    {
        return $this->idMarca;
    }

    /**
     * @param mixed $idMarca
     *
     * @return self
     */
    public function setIdMarca($idMarca)
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of marca
     */ 
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set the value of marca
     *
     * @return  self
     */ 
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get the value of tipoElemento
     */ 
    public function getTipoElemento()
    {
        return $this->tipoElemento;
    }

    /**
     * Set the value of tipoElemento
     *
     * @return  self
     */ 
    public function setTipoElemento($tipoElemento)
    {
        $this->tipoElemento = $tipoElemento;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO marca 
                                (nombre, descripcion, marca, tipoElemento, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->nombre', '$this->descripcion','$this->marca'. '$this->tipoElemento' '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE marca 
                            SET nombre = '$this->nombre', 
                            descripcion = '$this->descripcion',
                            marca = '$this->marca', 
                            tipoElemento = '$this->tipoElemento',  
                            estado = '$this->estado' 
                        WHERE idMarca = $this->idMarca  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            marca
                        SET
                            estado='I'
                        WHERE 
                            idMarca = $this->idMarca";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idMarca
                                , nombre
                                , descripcion
                                , marca
                                , tipoElemento
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 marca $condicion                    
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    

    public function consultarNombre(){

        $sentenciaSql = "SELECT * FROM equipo WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idMarca !=''){
            $condicion=$condicion.$whereAnd." marca.idMarca  = $this->idMarca";
            $whereAnd = ' AND ';
            
        }else{
            if($this->descripcion !=''){
                $condicion=$condicion.$whereAnd." marca.marca  like '$this->descripcion'";
                $whereAnd = ' AND ';            
            }
            // if($this->estado!=''){          
            //             $condicion=$condicion.$whereAnd." marca.estado = 'A'";
            //             $whereAnd = ' AND ';                    
            // }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idMarca);
        unset($this->nombre);
        unset($this->descripcion);
        unset($this->marca);
        unset($this->tipoElemento);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }

}
?>
