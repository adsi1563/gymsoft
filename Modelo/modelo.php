<?php
    require_once '../../entorno/conexion.php';
    session_start();
    class ModeloPersona{
        static public function mdlConsultarTodoPersona(){
            $stmt = new Conexion();
            $stmt->preparar("SELECT * FROM persona WHERE id_persona ");
            $stmt->ejecutar();
            if($stmt->obtenerNumeroRegistros() > 0){
                return $stmt->obtenerRegistros();
            }
        }
        static public function mdlConsultarPersonaPorId($idPersona){
            $stmt = new Conexion();
            $stmt->preparar("SELECT * FROM persona WHERE id_persona = ".$idPersona);
            $stmt->ejecutar();
            if($stmt->obtenerNumeroRegistros() > 0){
                return $stmt->obtenerRow();
            }
        }
        static public function mdlGuardarPersona($datos){
            $sql = "INSERT INTO persona
                        (primer_nombre,
                        segundo_nombre,
                        primer_apellido,
                        segundo_apellido,
                        documento,
                        telefono,
                        email,
                        estado,
                        id_usuario_creacion,
                        id_usuario_modificacion,
                        fecha_creacion,
                        fecha_modificacion)
                    VALUES ('".$datos["primerNombre"]."',
                             '".$datos["segundoNombre"]."',
                             '".$datos["primerApellido"]."',
                             '".$datos["segundoApellido"]."',
                             '".$datos["documento"]."',
                             '".$datos["telefono"]."',
                             '".$datos["email"]."',
                            '".$datos["estado"]."',
                            ".$_SESSION["idUsuario"].",
                            ".$_SESSION["idUsuario"].",
                            (SELECT NOW()),
                            (SELECT NOW()));";
            $stmt = new Conexion();
            $stmt->preparar($sql);
            if($stmt->ejecutar()){
                return "ok";
            }else{
                return "error";
            }
            $stmt = null;
        }
        static public function mdlModificarPersona($datos){
            $stmt = new Conexion();
            $stmt->preparar("UPDATE persona
                            SET primer_nombre = '".$datos['primerNombre']."',
                            segundo_nombre = '".$datos['segundoNombre']."',
                            primer_apellido = '".$datos['primerApellido']."',
                            segundo_apellido = '".$datos['segundoApellido']."',
                            documento = '".$datos['documento']."',
                            telefono= '".$datos['telefono']."',
                            email = '".$datos['email']."',
                            estado = '".$datos["estado"]."',
                            id_usuario_modificacion = ".$datos["hiddenIdPersona"].",
                            fecha_modificacion = (SELECT NOW())
                            WHERE id_persona = ".$datos["hiddenIdPersona"]."
                            ");
            if($stmt->ejecutar()){
                return "ok";
            }else{
                return "error";
            }
            $stmt = null;
        }
        static public function mdlEliminarPersona($idEliminar){
            $stmt = new Conexion();
            $stmt->preparar("DELETE FROM persona WHERE id_persona = ".$idEliminar);
            if($stmt->ejecutar()){
                return "ok";
            }else{
                return "error";
            }
        }
    }
?>