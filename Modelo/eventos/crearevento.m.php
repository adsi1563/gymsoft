<?php 
	class Accion{

		private $idCrearEvento;
		private $idTipoEvento;
		private $nombreEvento;
		private $fechaInicio;
	    private $fechaFin;
	    private $estado;
	    private $sede;
	    private $fechaCreacion;
	    private $fechaModificacion;
	    private $idUsuarioCreacion;
	    private $idUsuarioModificacion;
	    public  $conn;
	    private $descripcion;

	    public function getSede()
	    {
	        return $this->sede;
	    }

	    /**
	     * @param mixed $sede
	     *
	     * @return self
	     */
	    public function setSede($sede)
	    {
	        $this->sede = $sede;

	        return $this;
	    }
	     public function getDescripcion()
	    {
	        return $this->descripcion;
	    }

	    /**
	     * @param mixed $descripcion
	     *
	     * @return self
	     */
	    public function setDescripcion($descripcion)
	    {
	        $this->descripcion = $descripcion;

	        return $this;
	    }
	    /**
	     * @return mixed
	     */
	    public function getIdCrearEvento()
	    {
	        return $this->idCrearEvento;
	    }

	    /**
	     * @param mixed $idCrearEvento
	     *
	     * @return self
	     */
	    public function setIdCrearEvento($idCrearEvento)
	    {
	        $this->idCrearEvento = $idCrearEvento;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getIdTipoEvento()
	    {
	        return $this->idTipoEvento;
	    }

	    /**
	     * @param mixed $idTipoEvento
	     *
	     * @return self
	     */
	    public function setIdTipoEvento($idTipoEvento)
	    {
	        $this->idTipoEvento = $idTipoEvento;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getNombreEvento()
	    {
	        return $this->nombreEvento;
	    }

	    /**
	     * @param mixed $nombreEvento
	     *
	     * @return self
	     */
	    public function setNombreEvento($nombreEvento)
	    {
	        $this->nombreEvento = $nombreEvento;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFechaInicio()
	    {
	        return $this->fechaInicio;
	    }

	    /**
	     * @param mixed $fechaInicio
	     *
	     * @return self
	     */
	    public function setFechaInicio($fechaInicio)
	    {
	        $this->fechaInicio = $fechaInicio;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFechaFin()
	    {
	        return $this->fechaFin;
	    }

	    /**
	     * @param mixed $fechaFin
	     *
	     * @return self
	     */
	    public function setFechaFin($fechaFin)
	    {
	        $this->fechaFin = $fechaFin;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getEstado()
	    {
	        return $this->estado;
	    }

	    /**
	     * @param mixed $estado
	     *
	     * @return self
	     */
	    public function setEstado($estado)
	    {
	        $this->estado = $estado;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFechaCreacion()
	    {
	        return $this->fechaCreacion;
	    }

	    /**
	     * @param mixed $fechaCreacion
	     *
	     * @return self
	     */
	    public function setFechaCreacion($fechaCreacion)
	    {
	        $this->fechaCreacion = $fechaCreacion;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFechaModificacion()
	    {
	        return $this->fechaModificacion;
	    }

	    /**
	     * @param mixed $fechaModificacion
	     *
	     * @return self
	     */
	    public function setFechaModificacion($fechaModificacion)
	    {
	        $this->fechaModificacion = $fechaModificacion;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getIdUsuarioCreacion()
	    {
	        return $this->idUsuarioCreacion;
	    }

	    /**
	     * @param mixed $idUsuarioCreacion
	     *
	     * @return self
	     */
	    public function setIdUsuarioCreacion($idUsuarioCreacion)
	    {
	        $this->idUsuarioCreacion = $idUsuarioCreacion;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getIdUsuarioModificacion()
	    {
	        return $this->idUsuarioModificacion;
	    }

	    /**
	     * @param mixed $idUsuarioModificacion
	     *
	     * @return self
	     */
	    public function setIdUsuarioModificacion($idUsuarioModificacion)
	    {
	        $this->idUsuarioModificacion = $idUsuarioModificacion;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getConn()
	    {
	        return $this->conn;
	    }

	    /**
	     * @param mixed $conn
	     *
	     * @return self
	     */
	    public function setConn($conn)
	    {
	        $this->conn = $conn;

	        return $this;
	    }
	

	public function __construct(){$this->conn = new Conexion();}

	public function agregar(){
		$sentenciaSql = "INSERT INTO crear_evento
				            (idTipoEvento,
				             nombreEvento,
				             fechaInicio,
				             fechaCulminacion,
				             estado,
				             idSede,
				             idUsuarioCreacion,
				             idUsuarioModificacion)
						VALUES ('$this->idTipoEvento',
						    '$this->nombreEvento',
						    '$this->fechaInicio',
						    '$this->fechaFin',
						    '$this->estado',
						    '$this->sede',
						    '1',
						    '1');";
		$this->conn->preparar($sentenciaSql);
		$this->conn->ejecutar();
		return true;
	}
	
	public function modificar(){
		$sentenciaSql = "UPDATE crear_evento
						SET 
						idTipoEvento = '$this->idTipoEvento',
						nombreEvento = '$this->nombreEvento',
						fechaInicio = '$this->fechaInicio',
						fechaCulminacion = '$this->fechaFin',
						idSede = '$this->sede',
						estado = '$this->estado',
						idUsuarioModificacion = '1'
						WHERE idCrearEvento = '$this->idCrearEvento'";
		$this->conn->preparar($sentenciaSql);
		$this->conn->ejecutar();
	}

	public function eliminar(){
		$sentenciaSql = "UPDATE crear_evento SET estado='I'	WHERE idCrearEvento = " . $this->idCrearEvento;
		$this->conn->preparar($sentenciaSql);
		$this->conn->ejecutar();
	}

	public function consultar(){
		$condicion = $this->obtenerCondicion();
		$sentenciaSql = "SELECT crear_evento.*, 
								tipo_evento.descripcion AS 'descripcionEvento',
								sede.nombre AS sede
								FROM crear_evento
								INNER JOIN tipo_evento ON tipo_evento.idTipoEvento = crear_evento.idTipoEvento
								INNER JOIN sede ON sede.idSede = crear_evento.idSede $condicion";
		$this->conn->preparar($sentenciaSql);
		$this->conn->ejecutar();
		return true;
	}

	private function obtenerCondicion(){
		$whereAnd = " WHERE ";
		$condicion = "";

		if ($this->idCrearEvento != ''){
			$condicion=$condicion.$whereAnd." crear_evento.idCrearEvento = $this->idCrearEvento ";
			$whereAnd = ' AND ';
		}
		return $condicion;
	}

	public function consultarTipo(){

        $sentenciaSql = "SELECT * FROM tipo_evento";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarSede(){

        $sentenciaSql = "SELECT * FROM sede";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

	public function __destruct(){
		unset ($this->idCrearEvento);
		unset ($this->idTipoEvento);
		unset ($this->nombreEvento);
		unset ($this->fechaInicio);
	    unset ($this->fechaFin);
	    unset ($this->estado);
	    unset ($this->fechaCreacion);
	    unset ($this->fechaModificacion);
	    unset ($this->idUsuarioCreacion);
	    unset ($this->idUsuarioModificacion);
	    unset ($this->conn);
	} }

?>