<?php
class Accion{
    
    private $idAseguradora;
    private $nit;
    private $razon;
    private $direccion;
    private $representante;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idAseguradora
    public function getIdAseguradora(){return $this->idAseguradora;}
    public function setIdAseguradora($idAseguradora){$this->idAseguradora = $idAseguradora;}
    
    //nit
    public function getNit(){return $this->nit;}
    public function setNit($nit){$this->nit = $nit;}
    
    //razon
    public function getRazon(){return $this->razon;}
    public function setRazon($razon){$this->razon = $razon;}

     //direccion
     public function getDireccion(){return $this->direccion;}
     public function setDireccion($direccion){$this->direccion = $direccion;}
     
     //representante
     public function getRepresentante(){return $this->representante;}
     public function setRepresentante($representante){$this->representante = $representante;}

     //Estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO aseguradora 
                                (nit, razonSocial, direccion, representanteLegal, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->nit', '$this->razon', '$this->direccion', '$this->representante', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE aseguradora 
                            SET nit = '$this->nit', 
                                razonSocial = '$this->razon',
                                direccion = '$this->direccion', 
                                representanteLegal = '$this->representante',
                                estado = '$this->estado'
                        WHERE idAseguradora = $this->idAseguradora
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            aseguradora
                        SET
                            estado='I'
                        WHERE 
                            idAseguradora = $this->idAseguradora";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idAseguradora
                                , nit
                                , razonSocial
                                , direccion
                                , representanteLegal
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 aseguradora $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAseguradora !=''){
            $condicion=$condicion.$whereAnd." aseguradora.idAseguradora  = $this->idAseguradora";
            $whereAnd = ' AND ';
            
        }else{
            if($this->nit !=''){
                $condicion=$condicion.$whereAnd." aseguradora.nit  like '$this->nit'";
                $whereAnd = ' AND ';            
            }
            if($this->razon!=''){          
                        $condicion=$condicion.$whereAnd." aseguradora.razonSocial = '%$this->razon%'";
                        $whereAnd = ' AND ';                    
            }
            if($this->direccion!=''){          
                $condicion=$condicion.$whereAnd." aseguradora.direccion = '%$this->direcccion%'";
                $whereAnd = ' AND ';                    
            }
            if($this->representante!=''){          
                $condicion=$condicion.$whereAnd." aseguradora.representanteLegal = '%$this->representante%'";
                $whereAnd = ' AND ';                    
            }
            if($this->estado!=''){          
                        $condicion=$condicion.$whereAnd." aseguradora.estado = 'A'";
                        $whereAnd = ' AND ';                    
            }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idAseguradora);
        unset($this->nit);
        unset($this->razon);
        unset($this->direccion);
        unset($this->representante);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
