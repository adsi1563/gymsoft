<?php
class Accion{
    
    private $idEps;
    private $estado;
    private $nombre;
    private $categoria;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    // idEps
    /**
     * @return mixed
     */
    public function getIdEps()
    {
        return $this->idEps;
    }

    /**
     * @param mixed $idEps
     *
     * @return self
     */
    public function setIdEps($idEps)
    {
        $this->idEps = $idEps;

        return $this;
    }

    // estado


    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }


    //categoria
    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     *
     * @return self
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    
    //Nombre
    public function getNombre(){return $this->nombre;}
    public function setNombre($nombre){$this->nombre = $nombre;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
       // var_dump($this->estado); return 1;


        $sentenciaSql = "
                             INSERT INTO eps 
                                (nombre, categoria, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->nombre','$this->categoria', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE eps 
                            SET nombre = '$this->nombre',
                                categoria = '$this->categoria', 
                                estado = '$this->estado' 
                        WHERE idEps = $this->idEps   
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            eps
                        SET
                            estado = 'I'
                        WHERE 
                            idEps = $this->idEps";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }

    
    public function consultar(){
        
        $condiciona = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idEps
                                , nombre
                                , categoria
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 eps  $condiciona				
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        
        $whereAnd = " WHERE ";

        $condicion = "";

        if($this->idEps !=''){

            
            $condicion=$condicion.$whereAnd." eps.idEps  = $this->idEps";
            $whereAnd = ' AND ';
            
        }else if($this->estado !='' && $this->nombre!='') {


            if($this->estado !=''){
                $condicion=$condicion.$whereAnd." eps.estado  like '$this->estado'";
                $whereAnd = ' AND ';            
            }
            if($this->nombre!=''){          
                        $condicion=$condicion.$whereAnd." eps.nombre = '%$this->nombre%'";
                        $whereAnd = ' AND ';                    
            }
        }  else {

            $condicion = "";
        }     
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idEps);
        unset($this->estado);
        unset($this->categoria);
        unset($this->nombre);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }

    

}
?>
