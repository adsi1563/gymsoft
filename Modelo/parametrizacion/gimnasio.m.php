<?php
class Accion{
    
    private $idGimnasio;
    private $nombre;
    private $idCiudad;
    private $idSede;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;

    public  $conn=null;

    /**
     * Get the value of idGimnasio
     */ 
    public function getIdGimnasio()
    {
        return $this->idGimnasio;
    }

    /**
     * Set the value of idGimnasio
     *
     * @return  self
     */ 
    public function setIdGimnasio($idGimnasio)
    {
        $this->idGimnasio = $idGimnasio;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of idCiudad
     */ 
    public function getIdCiudad()
    {
        return $this->idCiudad;
    }

    /**
     * Set the value of idCiudad
     *
     * @return  self
     */ 
    public function setIdCiudad($idCiudad)
    {
        $this->idCiudad = $idCiudad;

        return $this;
    }

    /**
     * Get the value of idSede
     */ 
    public function getIdSede()
    {
        return $this->idSede;
    }

    /**
     * Set the value of idSede
     *
     * @return  self
     */ 
    public function setIdSede($idSede)
    {
        $this->idSede = $idSede;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of fechaCreacion
     */ 
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set the value of fechaCreacion
     *
     * @return  self
     */ 
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get the value of fechaModificacion
     */ 
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set the value of fechaModificacion
     *
     * @return  self
     */ 
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get the value of idUsuarioCreacion
     */ 
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set the value of idUsuarioCreacion
     *
     * @return  self
     */ 
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get the value of idUsuarioModificacion
     */ 
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * Set the value of idUsuarioModificacion
     *
     * @return  self
     */ 
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }
    

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "INSERT INTO `gimnasio`
                            (`nombre`,
                            `idSede`,
                            `estado`,
                            `fechaCreacion`,
                            `fechaModificacion`,
                            `idUsuarioCreacion`,
                            `idUsuarioModificacion`)
                    VALUES ('" . $this->nombre . "',
                        '". $this->idSede . "',
                        '". $this->estado . "',
                        NOW(),
                        NOW(),
                        '1',
                        '1');";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE `gimnasio`
                        SET `nombre` = '" . $this->nombre . "',
                        `idSede` = '" . $this->idSede . "',
                        `estado` = '" . $this->estado . "',
                        `fechaModificacion` = NOW(),
                        `idUsuarioModificacion` = '1'
                        WHERE idGimnasio = " . $this->idGimnasio;
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function eliminar(){
        $sentenciaSql = "UPDATE gimnasio SET estado='I' WHERE idGimnasio = " . $this->idGimnasio;
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT gimnasio.*, 
                                sede.nombre AS 'nombreSede' 
                        FROM gimnasio 
                        INNER JOIN sede ON sede.idSede = gimnasio.idSede
                        $condicion ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idGimnasio !=''){
            $condicion=$condicion.$whereAnd." gimnasio.idGimnasio  = $this->idGimnasio";
            $whereAnd = ' AND ';
            
        }else{
            if($this->nombre !=''){
                $condicion=$condicion.$whereAnd." gimnasio.descripcion  like '$this->nombre'";
                $whereAnd = ' AND ';            
            }
            // if($this->estado!=''){          
            //             $condicion=$condicion.$whereAnd." gimnasio.estado = 'A'";
            //             $whereAnd = ' AND ';                    
            // }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idGimnasio);
        unset($this->nombre);
        unset($this->idCiudad);
        unset($this->idSede);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
