<?php
class Accion{
    
    private $idAprendiz_ficha;
    private $idFicha;
    private $idAprendiz;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;

    public  $conn=null;

    /**
     * Get the value of idGimnasio
     */ 
    public function getIdAprendiz_ficha()
    {
        return $this->idAprendiz_ficha;
    }

    /**
     * Set the value of id_aprendiz_ficha
     *
     * @return  self
     */ 
    public function setIdAprendiz_ficha($idAprendiz_ficha)
    {
        $this->idAprendiz_ficha = $idAprendiz_ficha;

        return $this;
    }

    /**
     * Get the value of idFicha
     */ 
    public function getIdFicha()
    {
        return $this->idFicha;
    }

    /**
     * Set the value of idFicha
     *
     * @return  self
     */ 
    public function setIdFicha($idFicha)
    {
        $this->idFicha = $idFicha;

        return $this;
    }

    /**
     * Get the value of idAprendiz
     */ 
    public function getIdAprendiz()
    {
        return $this->idAprendiz;
    }

    /**
     * Set the value of idAprendiz
     *
     * @return  self
     */ 
    public function setIdAprendiz($idAprendiz)
    {
        $this->idAprendiz = $idAprendiz;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of fechaCreacion
     */ 
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set the value of fechaCreacion
     *
     * @return  self
     */ 
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get the value of fechaModificacion
     */ 
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set the value of fechaModificacion
     *
     * @return  self
     */ 
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get the value of idUsuarioCreacion
     */ 
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set the value of idUsuarioCreacion
     *
     * @return  self
     */ 
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get the value of idUsuarioModificacion
     */ 
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * Set the value of idUsuarioModificacion
     *
     * @return  self
     */ 
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }
    

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "INSERT INTO aprendiz_ficha
                            ( idFicha,
                              idAprendiz,
                              estado,
                              idUsuarioCreacion,
                              idUsuarioModificacion)
                    VALUES ('$this->idFicha',
                            '$this->idAprendiz',
                            'A',
                             1,
                             1);";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE `aprendiz_ficha`
                        SET 
                        idFicha = '$this->idFicha',
                        idAprendiz = '$this->idAprendiz',
                        estado = '$this->estado',
                        idUsuarioModificacion = 1
                        WHERE idAprendizFicha = '$this->idAprendiz_ficha'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function eliminar(){
        $sentenciaSql = "UPDATE aprendiz_ficha SET estado='I' WHERE idAprendizFicha = " . $this->idAprendiz_ficha;
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT aprendiz_ficha.idAprendizFicha, 
                                aprendiz_ficha.idFicha,
                                aprendiz_ficha.idAprendiz,
                                aprendiz_ficha.estado,
                                CONCAT(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS nombreCompleto,
                                ficha.ficha
                        FROM aprendiz_ficha
                        INNER JOIN ficha ON ficha.idFicha = aprendiz_ficha.idFicha 
                        INNER JOIN aprendiz ON aprendiz.idAprendiz = aprendiz_ficha.idAprendiz
                        INNER JOIN usuario ON aprendiz.idUsuario = usuario.idUsuario
                        INNER JOIN persona ON persona.idPersona = usuario.idPersona
                        $condicion ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAprendiz_ficha !=''){
            $condicion=$condicion.$whereAnd." aprendiz_ficha.idAprendizFicha  = $this->idAprendiz_ficha";
            $whereAnd = ' AND ';   
        }
        return $condicion;
           
    }

    public function consultarFicha(){

        $sentenciaSql = "SELECT idFicha, ficha FROM ficha WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarAprendiz(){
        $sentenciaSql = "SELECT     
                            aprendiz.idAprendiz,
                            CONCAT(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS nombreCompleto
                        FROM aprendiz
                            INNER JOIN usuario ON aprendiz.idUsuario = usuario.idUsuario
                            INNER JOIN persona ON persona.idPersona = usuario.idPersona
                        WHERE aprendiz.idAprendiz=(SELECT MAX(idAprendiz) FROM aprendiz)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idAprendiz_ficha);
        unset($this->idFicha);
        unset($this->idAprendiz);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
