<?php
class Accion{
    
    private $idEntrenadorSede;
    private $idEntrenador;
    private $sede;
    private $horaInicio;
    private $horaFin;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idEntrenadorSede
    public function getIdEntrenadorSede(){return $this->idEntrenadorSede;}
    public function setIdEntrenadorSede($idEntrenadorSede){$this->idEntrenadorSede = $idEntrenadorSede;}
    
    //Nombre
    public function getIdEntrenador(){return $this->idEntrenador;}
    public function setIdEntrenador($idEntrenador){$this->idEntrenador = $idEntrenador;}

    //Sede
    public function getSede(){return $this->sede;}
    public function setSede($sede){$this->sede = $sede;}

    //horaInicio
    public function getHoraInicio(){return $this->horaInicio;}
    public function setHoraInicio($horaInicio){$this->horaInicio = $horaInicio;}

    //horaFin
    public function getHoraFin(){return $this->horaFin;}
    public function setHoraFin($horaFin){$this->horaFin = $horaFin;}

    //Estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion){ $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($idUsuarioCreacion) { $this->idUsuarioCreacion =$idUsuarioCreacion;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($idUsuarioModificacion) { $this->idUsuarioModificacion =$idUsuarioModificacion;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                         INSERT INTO entrenador_sede
                            (idEntrenador, idSede, horaInicio, horaFin, estado, idUsuarioCreacion, idUsuarioModificacion) 
                         VALUES 
                            ('$this->idEntrenador', '$this->sede', '$this->horaInicio', '$this->horaFin', 'A',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE entrenador_sede
                            SET idEntrenador = '$this->idEntrenador', 
                                idSede = '$this->sede', 
                                horaInicio = '$this->horaInicio', 
                                horaFin = '$this->horaFin', 
                                estado = '$this->estado' 
                            WHERE idEntrenadorSede = $this->idEntrenadorSede 
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            entrenador_sede
                        SET
                            estado = 'I'
                        WHERE 
                            idEntrenadorSede = $this->idEntrenadorSede";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT 
                                entrenador_sede.idEntrenadorSede
                                , entrenador_sede.idEntrenador
                                , entrenador_sede.idSede
                                , entrenador_sede.horaInicio
                                , entrenador_sede.horaFin
                                , entrenador_sede.estado
                                , entrenador.idPersona
                                , CONCAT(persona.primerNombre,' ', persona.segundoNombre,' ',persona.primerApellido,' ', persona.segundoApellido) AS Nombre_Persona
                                , sede.nombre
                            FROM
                                entrenador_sede
                            INNER JOIN entrenador ON entrenador_sede.idEntrenador = entrenador.idEntrenador
                            INNER JOIN persona ON persona.idPersona = entrenador.idPersona
                            INNER JOIN sede ON sede.idSede = entrenador_sede.idSede
                                $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idEntrenadorSede !=''){
            $condicion=$condicion.$whereAnd." entrenador_sede.idEntrenadorSede
              = $this->idEntrenadorSede";
            $whereAnd = ' AND ';
            
        }       
        return $condicion;      
    }

    public function consultarSede(){

        $sentenciaSql = "SELECT * FROM sede WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

     public function consultarEntrenador(){
        $sentenciaSql = "SELECT entrenador.idEntrenador, CONCAT(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS nombreCompleto FROM entrenador INNER JOIN persona ON persona.idPersona = entrenador.idPersona WHERE entrenador.idEntrenador=(SELECT MAX(idEntrenador) FROM entrenador)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    public function __destruct() {
        unset($this->idEntrenadorSede);
        unset($this->idEntrenador);
        unset($this->sede);
        unset($this->horaInicio);
        unset($this->horaFin);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->con);
    }
}
?>  