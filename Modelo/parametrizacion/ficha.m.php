<?php
class Accion{
    
    private $idFicha;
    private $ficha;
    private $fechaInicio;
    private $fechaFinLectiva;
    private $fechaFinProductiva;
    private $idJornada;
    private $idAmbiente;
    private $idProgramaFormacion;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;


    /**
     * @return mixed
     */
    public function getIdFicha()
    {
        return $this->idFicha;
    }

    /**
     * @param mixed $idFicha
     *
     * @return self
     */
    public function setIdFicha($idFicha)
    {
        $this->idFicha = $idFicha;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFicha()
    {
        return $this->ficha;
    }

    /**
     * @param mixed $ficha
     *
     * @return self
     */
    public function setFicha($ficha)
    {
        $this->ficha = $ficha;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * @param mixed $fechaInicio
     *
     * @return self
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaFinLectiva()
    {
        return $this->fechaFinLectiva;
    }

    /**
     * @param mixed $fechaFinLectiva
     *
     * @return self
     */
    public function setFechaFinLectiva($fechaFinLectiva)
    {
        $this->fechaFinLectiva = $fechaFinLectiva;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaFinProductiva()
    {
        return $this->fechaFinProductiva;
    }

    /**
     * @param mixed $fechaFinProductiva
     *
     * @return self
     */
    public function setFechaFinProductiva($fechaFinProductiva)
    {
        $this->fechaFinProductiva = $fechaFinProductiva;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdJornada()
    {
        return $this->idJornada;
    }

    /**
     * @param mixed $idJornada
     *
     * @return self
     */
    public function setIdJornada($idJornada)
    {
        $this->idJornada = $idJornada;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdAmbiente()
    {
        return $this->idAmbiente;
    }

    /**
     * @param mixed $idAmbiente
     *
     * @return self
     */
    public function setIdAmbiente($idAmbiente)
    {
        $this->idAmbiente = $idAmbiente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdProgramaFormacion()
    {
        return $this->idProgramaFormacion;
    }

    /**
     * @param mixed $idProgramaFormacion
     *
     * @return self
     */
    public function setIdProgramaFormacion($idProgramaFormacion)
    {
        $this->idProgramaFormacion = $idProgramaFormacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                         INSERT INTO ficha 
                            (ficha, fechaInicio, fechaFinLectiva, fechaFinProductiva, idJornada, idAmbiente, idProgramaFormacion, estado, idUsuarioCreacion, idUsuarioModificacion) 
                         VALUES 
                            ('$this->ficha', '$this->fechaInicio', '$this->fechaFinLectiva', '$this->fechaFinProductiva', '$this->idJornada', '$this->idAmbiente', '$this->idProgramaFormacion', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE ficha 
                            SET ficha = '$this->ficha',
                                fechaInicio = '$this->fechaInicio',
                                fechaFinLectiva = '$this->fechaFinLectiva',
                                fechaFinProductiva = '$this->fechaFinProductiva',
                                idJornada = '$this->idJornada',
                                idAmbiente = '$this->idAmbiente',
                                idProgramaFormacion = '$this->idProgramaFormacion',
                                estado = '$this->estado',
                                idUsuarioModificacion = 1
                        WHERE idFicha = '$this->idFicha' 
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            ficha
                        SET
                            estado = 'I'
                        WHERE 
                            idFicha = $this->idFicha";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                ficha.*,
                                jornada.descripcion AS jornada,
                                ambiente.descripcion AS ambiente,
                                programa_formacion.descripcion AS programa
                            FROM
                                 ficha 
                            INNER JOIN jornada ON jornada.idJornada = ficha.idJornada
                            INNER JOIN ambiente ON ambiente.idAmbiente = ficha.idAmbiente
                            INNER JOIN programa_formacion ON programa_formacion.idProgramaFormacion = ficha.idProgramaFormacion 
                            $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idFicha !=''){
            $condicion=$condicion.$whereAnd." ficha.idFicha  = $this->idFicha";
            $whereAnd = ' AND ';
        }       
        return $condicion;
           
    }

    public function consultarJornada(){

        $sentenciaSql = "SELECT * FROM jornada WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarAmbiente(){

        $sentenciaSql = "SELECT * FROM ambiente WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarProrama(){

        $sentenciaSql = "SELECT * FROM programa_formacion WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idFicha);
        unset($this->ficha);
        unset($this->fechaInicio);
        unset($this->fechaFinLectiva);
        unset($this->fechaFinProductiva);
        unset($this->idJornada);
        unset($this->idAmbiente);
        unset($this->idProgramaFormacion);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>