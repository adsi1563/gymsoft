<?php
class Accion{
    
    private $idInstructor_ficha;
    private $idFicha;
    private $idInstructor;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;

    public  $conn=null;

    /**
     * Get the value of idGimnasio
     */ 
    public function getIdInstructor_ficha()
    {
        return $this->idInstructor_ficha;
    }

    /**
     * Set the value of id_aprendiz_ficha
     *
     * @return  self
     */ 
    public function setIdInstructor_ficha($idInstructor_ficha)
    {
        $this->idInstructor_ficha = $idInstructor_ficha;

        return $this;
    }

    /**
     * Get the value of idFicha
     */ 
    public function getIdFicha()
    {
        return $this->idFicha;
    }

    /**
     * Set the value of idFicha
     *
     * @return  self
     */ 
    public function setIdFicha($idFicha)
    {
        $this->idFicha = $idFicha;

        return $this;
    }

    /**
     * Get the value of idAprendiz
     */ 
    public function getIdInstructor()
    {
        return $this->idInstructor;
    }

    /**
     * Set the value of idAprendiz
     *
     * @return  self
     */ 
    public function setIdInstructor($idInstructor)
    {
        $this->idInstructor = $idInstructor;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of fechaCreacion
     */ 
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set the value of fechaCreacion
     *
     * @return  self
     */ 
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get the value of fechaModificacion
     */ 
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set the value of fechaModificacion
     *
     * @return  self
     */ 
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get the value of idUsuarioCreacion
     */ 
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set the value of idUsuarioCreacion
     *
     * @return  self
     */ 
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get the value of idUsuarioModificacion
     */ 
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * Set the value of idUsuarioModificacion
     *
     * @return  self
     */ 
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }
    

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "INSERT INTO instructor_ficha
                            (idFicha,
                            idInstructor,
                            estado,
                            idUsuarioCreacion,
                            idUsuarioModificacion)
                    VALUES ('$this->idFicha',
                        '$this->idInstructor',
                        'A',
                        1,
                        1);";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE instructor_ficha
                        SET 
                            idFicha = '$this->idFicha',
                            idInstructor = '$this->idInstructor',
                            estado = '$this->estado',
                            fechaModificacion = NOW()
                        WHERE idInstructorFicha = '$this->idInstructor_ficha'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function eliminar(){
        $sentenciaSql = "UPDATE instructor_ficha SET estado='I' 
                        WHERE idInstructorFicha = '$this->idInstructor_ficha'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT instructor_ficha.*,
                           ficha.ficha,
                           CONCAT(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS nombreCompleto
                        FROM 
                            instructor_ficha
                        INNER JOIN ficha ON instructor_ficha.idFicha = ficha.idFicha
                        INNER JOIN instructor ON instructor.idInstructor = instructor_ficha.idInstructor
                        INNER JOIN usuario ON instructor.idUsuario = usuario.idUsuario
                        INNER JOIN persona ON persona.idPersona = usuario.idPersona
                        $condicion ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idInstructor_ficha !=''){
            $condicion=$condicion.$whereAnd." instructor_ficha.idInstructorFicha  = $this->idInstructor_ficha";
            $whereAnd = ' AND ';
            
        }  
        return $condicion;        
    }

    public function consultarFicha(){

        $sentenciaSql = "SELECT * FROM ficha WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarInstructor(){
        $sentenciaSql = "SELECT     
                            instructor.idInstructor,
                            CONCAT(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS nombreCompleto
                        FROM instructor
                            INNER JOIN usuario ON instructor.idUsuario = usuario.idUsuario
                            INNER JOIN persona ON persona.idPersona = usuario.idPersona
                        WHERE instructor.idInstructor=(SELECT MAX(idInstructor) FROM instructor)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idAprendiz_ficha);
        unset($this->idFicha);
        unset($this->idAprendiz);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
