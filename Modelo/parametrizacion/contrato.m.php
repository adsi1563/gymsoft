<?php
class Accion{
    
    private $idContrato;
    private $tipoContrato;
    private $fechaInicio;
    private $fechaFin;
    private $fechaCreacion;
    private $estado;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //tipoContrato
    public function getTipoContrato(){ return $this->tipoContrato;}
    public function setTipoContrato($tipoContrato) { $this->tipoContrato =$tipoContrato;}



    /**
     * @return mixed
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * @param mixed $fechaInicio
     *
     * @return self
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * @param mixed $fechaFin
     *
     * @return self
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdContrato()
    {
        return $this->idContrato;
    }

    /**
     * @param mixed $idContrato
     *
     * @return self
     */
    public function setIdContrato($idContrato)
    {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO contrato 
                                (tipoContrato, fechaIngreso, fechaFinalizacion, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->tipoContrato', '$this->fechaInicio', '$this->fechaFin','$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE contrato
                            SET tipoContrato = '$this->tipoContrato', 
                                fechaIngreso = '$this->fechaInicio',
                                fechaFinalizacion = '$this->fechaFin',
                                estado = '$this->estado',
                                idUsuarioModificacion = 1
                        WHERE idContrato = '$this->idContrato'";       
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            contrato
                        SET
                            estado = 'I'
                        WHERE 
                            idContrato = $this->idContrato";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condiciones = $this->obtenerCondicion();

        //var_dump($condiciones); return 1;
        $sentenciaSql = "SELECT
                                idContrato
                                , tipoContrato
                                , fechaIngreso
                                , fechaFinalizacion
                                , estado
                                , fechaModificacion
                                , idUsuarioModificacion
                                
                            FROM
                                 contrato $condiciones					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idContrato !=''){
            $condicion=$condicion.$whereAnd." contrato.idContrato  = $this->idContrato";
            $whereAnd = ' AND ';
            
        }else if($this->tipoContrato !='' && $this->fechaInicio !='') {
            if($this->tipoContrato !=''){
                $condicion=$condicion.$whereAnd." contrato.tipoContrato  like '$this->tipoContrato'";
                $whereAnd = ' AND ';            
            }
            // if($this->fechaInicio !=''){          
            //             $condicion=$condicion.$whereAnd." contrato.fechaInicio = '%$this->fechaInicio%'";
            //             $whereAnd = ' AND ';                    
            // }
        } else { $condicion = "";   }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idDepartamento);
        unset($this->codigo);
        unset($this->nombre);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
