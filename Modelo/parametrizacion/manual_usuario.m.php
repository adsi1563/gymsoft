<?php
class Accion{
    
    private $idManual;
    private $manual;
    private $estado;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    /**
     * @return mixed
     */
    public function getIdManual()
    {
        return $this->idManual;
    }

    /**
     * @param mixed $idManual
     *
     * @return self
     */
    public function setIdManual($idManual)
    {
        $this->idManual = $idManual;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getManual()
    {
        return $this->manual;
    }

    /**
     * @param mixed $manual
     *
     * @return self
     */
    public function setManual($manual)
    {
        $this->manual = $manual;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO manual_usuario 
                                (descripcion, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->manual','$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE manual_usuario
                            SET estado = '$this->estado',
                                idUsuarioModificacion = 1
                        WHERE idManualUsuario = '$this->idManual'";       
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            manual_usuario
                        SET
                            estado = 'I'
                        WHERE 
                            idManualUsuario = $this->idManual";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condiciones = $this->obtenerCondicion();

        $sentenciaSql = "SELECT
                                idManualUsuario
                                , descripcion
                                , estado
                                , fechaModificacion
                                , idUsuarioModificacion
                                
                            FROM
                                 manual_usuario $condiciones					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idManual !=''){
            $condicion=$condicion.$whereAnd." manual_usuario.idManualUsuario  = $this->idManual";
            $whereAnd = ' AND ';   
        }     
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idManual);
        unset($this->manual);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>