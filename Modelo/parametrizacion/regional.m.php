<?php
class Regional{
    
    private $idregional;
    private $nombre;
    private $codigo;
    private $descripcion;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    public function getIdregional(){
		return $this->idregional;
	}

	public function setIdregional($idregional){
		$this->idregional = $idregional;
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getCodigo(){
		return $this->codigo;
	}

	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}

	public function getEstado(){
		return $this->estado;
	}

	public function setEstado($estado){
		$this->estado = $estado;
	}

	public function getFechaCreacion(){
		return $this->fechaCreacion;
	}

	public function setFechaCreacion($fechaCreacion){
		$this->fechaCreacion = $fechaCreacion;
	}

	public function getFechaModificacion(){
		return $this->fechaModificacion;
	}

	public function setFechaModificacion($fechaModificacion){
		$this->fechaModificacion = $fechaModificacion;
	}

	public function getIdUsuarioCreacion(){
		return $this->idUsuarioCreacion;
	}

	public function setIdUsuarioCreacion($idUsuarioCreacion){
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}

	public function getIdUsuarioModificacion(){
		return $this->idUsuarioModificacion;
	}

	public function setIdUsuarioModificacion($idUsuarioModificacion){
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO regional
                                (nombre,
                                codigo,
                                descripcion,
                                estado,
                                idUsuarioCreacion,
                                idUsuarioModificacion,
                                fechaCreacion,
                                fechaModificacion) 
                             VALUES 
                                ('$this->nombre', 
                                '$this->codigo', 
                                '$this->descripcion', 
                                '$this->estado',
                                1,
                                1,
                                NOW(),
                                NOW());
                        ";
                        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE regional 
                            SET nombre = '$this->nombre',
                                codigo = '$this->codigo',
                                descripcion = '$this->descripcion',
                                estado = '$this->estado',
                                idUsuarioModificacion = 1,
                                fechaModificacion = NOW()
                        WHERE idRegional = $this->idregional  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            regional
                        SET
                            estado='I' 
                        WHERE 
                            idRegional = $this->idregional";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idRegional
                                , nombre
                                , codigo
                                , descripcion
                                , estado
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                                , fechaCreacion
                                , fechaModificacion
                            FROM
                                 regional $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idregional !=''){
            $condicion=$condicion.$whereAnd." regional.idRegional  = $this->idregional";
            $whereAnd = ' AND ';
            
        }else{
            if($this->nombre !=''){
                $condicion=$condicion.$whereAnd." regional.nombre  like '%$this->nombre%'";
                $whereAnd = ' AND ';            
            }
            // if($this->estado!=''){          
            //             $condicion=$condicion.$whereAnd." regional.estado = '$this->estado'";
            //             $whereAnd = ' AND ';                    
            // }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idregional);
        unset($this->nombre);
        unset($this->codigo);
        unset($this->descripcion);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>