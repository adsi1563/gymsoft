<?php
class Accion{
    
    private $idActividad;
    private $idGimnasio;
    private $idTipo;
    private $fechaInicio;
    private $fechaFin;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

     /**
     * @return mixed
     */
    public function getIdActividad()
    {
        return $this->idActividad;
    }

    /**
     * @param mixed $idActividad
     *
     * @return self
     */
    public function setIdActividad($idActividad)
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdGimnasio()
    {
        return $this->idGimnasio;
    }

    /**
     * @param mixed $idGimnasio
     *
     * @return self
     */
    public function setIdGimnasio($idGimnasio)
    {
        $this->idGimnasio = $idGimnasio;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * @param mixed $idTipo
     *
     * @return self
     */
    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * @param mixed $fechaInicio
     *
     * @return self
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * @param mixed $fechaFin
     *
     * @return self
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO actividad 
                                (idGimnasio, idTipo, estado, fechaInicio, fechaFin, idUsuarioCreacion, idUsuarioModificacion, fechaCreacion, fechaModificacion) 
                             VALUES 
                                ('$this->idGimnasio', '$this->idTipo', '$this->estado', '$this->fechaInicio', '$this->fechaFin',1,1,NOW(),NOW())
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE actividad 
                            SET idGimnasio = '$this->idGimnasio', 
                                idTipo = '$this->idTipo',
                                estado = '$this->estado',
                                fechaInicio = '$this->fechaInicio',
                                fechaFin = '$this->fechaFin',
                                idUsuarioModificacion = 1,
                                fechaModificacion = NOW()
                        WHERE idActividad = '$this->idActividad'  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            actividad
                        SET 
                            estado = 'I'
                        WHERE 
                            idActividad = $this->idActividad";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                actividad.idActividad,
                                actividad.idGimnasio,
                                actividad.idTipo,
                                actividad.fechaInicio,
                                actividad.fechaFin,
                                actividad.estado,
                                tipo.nombre AS nombreTipo,
                                gimnasio.nombre AS nombreGimnasio
                            FROM
                                 actividad 
                            INNER JOIN tipo ON tipo.idTipo = actividad.idTipo
                            INNER JOIN gimnasio ON gimnasio.idGimnasio = actividad.idGimnasio
                            $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idActividad !=''){
            $condicion=$condicion.$whereAnd." actividad.idActividad  = $this->idActividad";
            $whereAnd = ' AND ';
        }       
        return $condicion;      
    }

    public function consultarGimnasio(){

        $sentenciaSql = "SELECT * FROM gimnasio WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarTipo(){

        $sentenciaSql = "SELECT idTipo,nombre FROM tipo WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idActividad);
        unset($this->idGimnasio);
        unset($this->idTipo);
        unset($this->fechaInicio);
        unset($this->fechaFin);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
