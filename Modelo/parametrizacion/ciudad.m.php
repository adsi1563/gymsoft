<?php
class Accion{
    
    private $idCiudad;
    private $codigo;
    private $nombre;
    private $departamento;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idCiudad
    public function getIdCiudad(){return $this->idCiudad;}
    public function setIdCiudad($idCiudad){$this->idCiudad = $idCiudad;}
    
    //Codigo
    public function getCodigo(){return $this->codigo;}
    public function setCodigo($codigo){$this->codigo = $codigo;}
    
    //Nombre
    public function getNombre(){return $this->nombre;}
    public function setNombre($nombre){$this->nombre = $nombre;}

    //Id_Departamento
    public function getDepartamento(){return $this->departamento;}
    public function setDepartamento($departamento){$this->departamento = $departamento;}

    //Estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO ciudad 
                                (codigo, nombre, idDepartamento, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->codigo', '$this->nombre', '$this->departamento', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE ciudad 
                            SET codigo = '$this->codigo', 
                                nombre = '$this->nombre',
                                idDepartamento = '$this->departamento',
                                estado = '$this->estado'
                        WHERE idCiudad = $this->idCiudad  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            ciudad
                        SET
                            estado='I'
                        WHERE 
                            idCiudad = $this->idCiudad";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                ciudad.*,
                                departamento.nombre AS nombreDepartamento
                            FROM
                                 ciudad 
                            INNER JOIN departamento ON ciudad.idDepartamento = departamento.idDepartamento
                            $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idCiudad !=''){
            $condicion=$condicion.$whereAnd." ciudad.idCiudad  = $this->idCiudad";
            $whereAnd = ' AND ';
            
        }else{
            if($this->codigo !=''){
                $condicion=$condicion.$whereAnd." ciudad.codigo  like '$this->codigo'";
                $whereAnd = ' AND ';            
            }
            if($this->nombre!=''){          
                        $condicion=$condicion.$whereAnd." ciudad.nombre = '%$this->nombre%'";
                        $whereAnd = ' AND ';                    
            }
            if($this->departamento!=''){          
                $condicion=$condicion.$whereAnd." ciudad.idDepartamento = '%$this->departamento%'";
                $whereAnd = ' AND ';                    
            }
            if($this->estado!=''){          
                        $condicion=$condicion.$whereAnd." ciudad.estado = 'A'";
                        $whereAnd = ' AND ';                    
            }
        }       
        return $condicion;
           
    }
    
    public function consultardepa(){

        $sentenciaSql = "SELECT * FROM `departamento` WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function __destruct() {
        unset($this->idCiudad);
        unset($this->codigo);
        unset($this->nombre);
        unset($this->departamento);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
        unset($this->stmt);
    }
}
?>
