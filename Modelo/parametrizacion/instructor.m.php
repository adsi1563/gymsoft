<?php
session_start();
class Accion{
    
    private $idInstructor;
    private $hiddenUsuario;
    private $contrato;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    /**
     * @return mixed
     */
    public function getIdInstructor()
    {
        return $this->idInstructor;
    }

    /**
     * @param mixed $idInstructor
     *
     * @return self
     */
    public function setIdInstructor($idInstructor)
    {
        $this->idInstructor = $idInstructor;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHiddenUsuario()
    {
        return $this->hiddenUsuario;
    }

    /**
     * @param mixed $hiddenUsuario
     *
     * @return self
     */
    public function setHiddenUsuario($hiddenUsuario)
    {
        $this->hiddenUsuario = $hiddenUsuario;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * @param mixed $contrato
     *
     * @return self
     */
    public function setContrato($contrato)
    {
        $this->contrato = $contrato;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO instructor 
                                (idUsuario, estado, idContrato, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->hiddenUsuario', 'A', '$this->contrato' , ".$_SESSION['idUsuario'].",1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE instructor 
                            SET idUsuario = '$this->hiddenUsuario', 
                                estado = '$this->estado',
                                idContrato = '$this->contrato',
                                idUsuarioModificacion = ".$_SESSION['idUsuario']."
                        WHERE idInstructor = $this->idInstructor  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            instructor
                        SET
                            estado='I'
                        WHERE 
                            idInstructor = $this->idInstructor";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT instructor.*,
                                usuario.usuario,
                                concat(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS Nombre_Completo,
                                contrato.tipoContrato
                            FROM
                                 instructor
                            INNER JOIN usuario ON usuario.idUsuario = instructor.idUsuario
                            INNER JOIN  persona ON persona.idpersona = usuario.idPersona
                            INNER JOIN contrato ON contrato.idContrato = instructor.idContrato 
                            $condicion
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idInstructor !=''){
            $condicion=$condicion.$whereAnd." instructor.idInstructor  = $this->idInstructor";
            $whereAnd = ' AND ';
        }       
        return $condicion;
    }

    public function consultarContrato(){

        $sentenciaSql = "SELECT * FROM contrato WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

   
    public function consultarUsuario(){

        $sentenciaSql = "SELECT idUsuario,usuario FROM usuario WHERE idUsuario=(SELECT MAX(idUsuario) FROM usuario)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    public function __destruct() {
        unset($this->idMarca);
        unset($this->descripcion);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
