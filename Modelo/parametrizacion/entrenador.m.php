<?php

class Accion{

	private $id_entrenador;
	private $hiddenIdPersona;
	private $estado;
	private $fecha_creacion;
    private $fecha_modificacion;
    private $id_usuario_creacion;
    private $id_usuario_modificacion;
    public  $conn=null;

    //entrenador
    public function getId_entrenador(){return $this->id_entrenador;}
    public function setId_entrenador($id_entrenador){$this->id_entrenador = $id_entrenador;}
    
    //Nombre
    public function getHiddenIdPersona(){return $this->hiddenIdPersona;}
    public function setHiddenIdPersona($hiddenIdPersona){$this->hiddenIdPersona = $hiddenIdPersona;}

    //Estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFecha_creacion(){ return $this->fecha_creacion;}
    public function setFecha_creacion($fecha_creacion) { $this->fecha_creacion =$fecha_creacion;}
    
    //fechaModificacion
    public function getFecha_modificacion(){ return $this->fecha_modificacion;}
    public function setFecha_modificacion($fecha_modificacion) { $this->fecha_modificacion =$fecha_modificacion;}
    
    //idUsuarioCreacion
    public function getId_usuario_creacion(){ return $this->id_usuario_creacion;}
    public function setId_usuario_creacion($id_usuario_creacion) { $this->id_usuario_creacion =$id_usuario_modificacion;}
    
    //idUsuarioModificacion
    public function getId_usuario_modificacion(){ return $this->id_usuario_modificacion;}
    public function setId_usuario_modificacion($id_usuario_modificacion) { $this->id_usuario_modificacion =$id_usuario_modificacion;}
    //
    
    public function __construct() {$this->conn = new Conexion();}

    public function agregar(){
    	 $sentenciaSql = "
                          INSERT INTO entrenador
                             (idPersona, estado, idUsuarioCreacion, idUsuarioModificacion)
                          VALUES 
                             ('$this->hiddenIdPersona', 'A', 1, 1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function modificar(){
        $sentenciaSql = "UPDATE entrenador
                            SET 
                                idPersona = '$this->hiddenIdPersona', 
                                estado = '$this->estado' 
                        WHERE idEntrenador = $this->id_entrenador
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }

    public function eliminar(){
        $sentenciaSql = "UPDATE  
                            entrenador
                        SET
                            estado='I'
                        WHERE 
                            idEntrenador = $this->id_entrenador";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }

    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                entrenador.idEntrenador,
                                entrenador.idPersona,
                                entrenador.estado,
                                CONCAT(persona.primerNombre,' ', persona.segundoNombre,' ',persona.primerApellido,' ', persona.segundoApellido) AS Nombre_Persona
                            FROM
                                 entrenador 
                            INNER JOIN persona ON entrenador.idPersona = persona.idPersona
                            $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;    
    }

    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->id_entrenador !=''){
            $condicion=$condicion.$whereAnd." entrenador.idEntrenador  = $this->id_entrenador";
            $whereAnd = ' AND ';
        }       
        return $condicion;
           
    }

    public function consultarPersona(){

        $sentenciaSql = "SELECT idPersona,CONCAT(primerNombre,' ',segundoNombre,' ',primerApellido,' ',segundoApellido) AS NombreCompleto FROM persona WHERE idPersona=(SELECT MAX(idPersona) FROM persona)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
}

?>