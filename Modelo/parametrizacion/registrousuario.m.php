<?php
class Accion{
    
    private $idInscripcionAprendiz;
    private $usuarioevento;
    private $tipoIdentificacion;
    private $numeroIdentificacion;
    private $nombreAprendiz;
    private $apellidoAprendiz;
    private $ficha;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    
    /**
     * @return mixed
     */
    public function getIdInscripcionAprendiz()
    {
        return $this->idInscripcionAprendiz;
    }

    /**
     * @param mixed $idInscripcionAprendiz
     *
     * @return self
     */
    public function setIdInscripcionAprendiz($idInscripcionAprendiz)
    {
        $this->idInscripcionAprendiz = $idInscripcionAprendiz;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsuarioevento()
    {
        return $this->usuarioevento;
    }

    /**
     * @param mixed $usuarioevento
     *
     * @return self
     */
    public function setUsuarioevento($usuarioevento)
    {
        $this->usuarioevento = $usuarioevento;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTipoIdentificacion()
    {
        return $this->tipoIdentificacion;
    }

    /**
     * @param mixed $tipoIdentificacion
     *
     * @return self
     */
    public function setTipoIdentificacion($tipoIdentificacion)
    {
        $this->tipoIdentificacion = $tipoIdentificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumeroIdentificacion()
    {
        return $this->numeroIdentificacion;
    }

    /**
     * @param mixed $numeroIdentificacion
     *
     * @return self
     */
    public function setNumeroIdentificacion($numeroIdentificacion)
    {
        $this->numeroIdentificacion = $numeroIdentificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreAprendiz()
    {
        return $this->nombreAprendiz;
    }

    /**
     * @param mixed $nombreAprendiz
     *
     * @return self
     */
    public function setNombreAprendiz($nombreAprendiz)
    {
        $this->nombreAprendiz = $nombreAprendiz;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getApellidoAprendiz()
    {
        return $this->apellidoAprendiz;
    }

    /**
     * @param mixed $apellidoAprendiz
     *
     * @return self
     */
    public function setApellidoAprendiz($apellidoAprendiz)
    {
        $this->apellidoAprendiz = $apellidoAprendiz;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFicha()
    {
        return $this->ficha;
    }

    /**
     * @param mixed $ficha
     *
     * @return self
     */
    public function setFicha($ficha)
    {
        $this->ficha = $ficha;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    
    

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "INSERT INTO inscripcion_aprendiz
                            (idCrearEvento,
                            idTipoIdentificacion,
                            numeroIdentificacion,
                            nombreAprendiz,
                            apellidoAprendiz,
                            ficha,
                            estado,
                            fechaCreacion,
                            fechaModificacion,
                            idUsuarioCreacion,
                            idUsuarioModificacion)
                    VALUES 
                            ('$this->usuarioevento',
                            '$this->tipoIdentificacion',
                            '$this->numeroIdentificacion',
                            '$this->nombreAprendiz',
                            '$this->apellidoAprendiz',
                            '$this->ficha',
                            '$this->estado',
                            NOW(),
                            NOW(),
                            1,
                            1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE `inscripcion_aprendiz`
                            SET `idCrearEvento` = '$this->usuarioevento',
                                `idTipoIdentificacion` = '$this->tipoIdentificacion',
                                `numeroIdentificacion` = '$this->numeroIdentificacion',
                                `nombreAprendiz` = '$this->nombreAprendiz',
                                `apellidoAprendiz` = '$this->apellidoAprendiz',
                                `ficha` = '$this->ficha',
                                `estado` = '$this->estado',
                                `fechaModificacion` = NOW(),
                                `idUsuarioModificacion` = 1
                        WHERE idInscripcionAprendiz = $this->idInscripcionAprendiz
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function eliminar(){
        $sentenciaSql = "UPDATE inscripcion_aprendiz 
                         SET estado='I' 
                         WHERE idInscripcionAprendiz = " . $this->idInscripcionAprendiz;
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();         
    }


    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                             idInscripcionAprendiz
                             ,idCrearEvento
                            ,idTipoIdentificacion
                            ,numeroIdentificacion
                            ,nombreAprendiz
                            ,apellidoAprendiz
                            ,ficha
                            ,estado
                            ,fechaCreacion
                            ,fechaModificacion
                            ,idUsuarioCreacion
                            ,idUsuarioModificacion
                            FROM
                                 inscripcion_aprendiz $condicion                    
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idInscripcionAprendiz !=''){
            $condicion=$condicion.$whereAnd." inscripcion_aprendiz.idInscripcionAprendiz  = $this->idInscripcionAprendiz";
            $whereAnd = ' AND ';
        }     
        return $condicion;  
    }

    public function autocompletar($term){
        $stmt = new Conexion();
        $stmt->preparar("SELECT identificacion
                            FROM
                            persona WHERE identificacion LIKE CONCAT('".$term."','%')");
        $stmt->ejecutar();
        return $stmt->obtenerRegistros();
        $stmt = null;
    }
    
    public function ConsultarPersona(){

        $sentenciaSql = "SELECT * FROM persona WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }  

    public function __destruct() {
        unset($this->idInscripcionAprendiz);
        unset($this->idCrearEvento);
        unset($this->idTipoIdentificacion);
        unset($this->numeroIdentificacion);
        unset($this->nombreAprendiz);
        unset($this->apellidoAprendiz);
        unset($this->ficha);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
