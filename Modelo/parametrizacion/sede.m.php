<?php
class Accion{
    
    private $idSede;
    private $nombre;
    private $telefono;
    private $direccion;
    private $idCentroFormacion;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;


    /**
     * @return mixed
     */
    public function getIdSede()
    {
        return $this->idSede;
    }

    /**
     * @param mixed $idSede
     *
     * @return self
     */
    public function setIdSede($idSede)
    {
        $this->idSede = $idSede;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     *
     * @return self
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     *
     * @return self
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdCentroFormacion()
    {
        return $this->idCentroFormacion;
    }

    /**
     * @param mixed $idCentroFormacion
     *
     * @return self
     */
    public function setIdCentroFormacion($idCentroFormacion)
    {
        $this->idCentroFormacion = $idCentroFormacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                         INSERT INTO sede 
                            (nombre, telefono, direccion, idCentroFormacion, estado, idUsuarioCreacion, idUsuarioModificacion, fechaCreacion, fechaModificacion) 
                         VALUES 
                            ('$this->nombre', '$this->telefono', '$this->direccion', '$this->idCentroFormacion', '$this->estado',1,1,NOW(),NOW())
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE sede 
                            SET nombre = '$this->nombre',
                                telefono = '$this->telefono',
                                direccion = '$this->direccion',
                                idCentroFormacion = '$this->idCentroFormacion',
                                estado = '$this->estado',
                                idUsuarioModificacion = 1,
                                fechaModificacion = NOW()
                        WHERE idSede = '$this->idSede' 
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            sede
                        SET
                            estado = 'I'
                        WHERE 
                            idSede = $this->idSede";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                sede.*,
                                centro_formacion.nombre AS centro
                            FROM
                                 sede 
                            INNER JOIN centro_formacion ON centro_formacion.idCentroFormacion = sede.idCentroFormacion
                                 $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idSede !=''){
            $condicion=$condicion.$whereAnd." sede.idSede  = $this->idSede";
            $whereAnd = ' AND ';
            
        // }else{
        //     // if($this->idTipo !=''){
        //     //     $condicion=$condicion.$whereAnd." actividad.idTipo = '$this->idTipo'";
        //     //     $whereAnd = ' AND ';            
        //     // }
        //     if($this->estado!=''){          
        //                 $condicion=$condicion.$whereAnd." actividad.estado = '$this->estado'";
        //                 $whereAnd = ' AND ';                    
        //     }
        }       
        return $condicion;
           
    }

    public function consultarCentroFormacion(){

        $sentenciaSql = "SELECT * FROM centro_formacion WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idSede);
        unset($this->nombre);
        unset($this->telefono);
        unset($this->direccion);
        unset($this->idCentroFormacion);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }

    

    
}
?>
