<?php

require_once 'ConsultaGenericBase.php';
class Accion{
    
    private $fechaini;
    private $fechafin;



    public function __construct() {$this->conn = new Conexion();}
    
    
    
    public function consultar($valor, $idsede){
        //$fi = 7;
        switch ($valor) {
            case 1:
                $sql = "SELECT s.nombre AS sede, AVG(imc.imc) AS suma, f.ficha AS fe FROM imccontrol AS imc 
                    INNER JOIN aprendiz AS a ON imc.idAprendiz = a.idAprendiz
                    INNER JOIN aprendiz_ficha AS af ON a.idAprendiz = af.idAprendiz  
                    INNER JOIN ficha AS f ON af.idFicha = f.idFicha
                    INNER JOIN sede AS s ON f.idSede = s.idSede
                    INNER JOIN centro_formacion AS ct ON s.idCentroFormacion = ct.idCentroFormacion
                    WHERE ct.idCentroFormacion = :centro AND imc.fecha BETWEEN '$this->fechaini' AND '$this->fechafin'
                    GROUP BY fe
                                ";
                break;

            case 2:
                $sql = " SELECT  s.nombre AS sedes, t.nombre AS nombre, ev.fechaI AS fi, ev.fechaF AS fechafin FROM centro_formacion AS ct
                    INNER JOIN sede AS s ON ct.idCentroFormacion = s.idCentroFormacion

                    INNER JOIN evento AS ev ON s.idSede = ev.idSede
                    INNER JOIN tipo AS t ON ev.idTipo = t.idTipo
                    WHERE ct.idCentroFormacion = :centro AND ev.fechaI BETWEEN '$this->fechaini' AND '$this->fechafin'
                                ";
                break;

            case 3:
                $sql = " SELECT  s.nombre AS sedes, t.nombre AS nombre, CONCAT_WS(' ', ps.primerNombre, ps.primerApellido) AS nombreCom, punta
                            FROM centro_formacion AS ct
                            INNER JOIN sede AS s ON ct.idCentroFormacion = s.idCentroFormacion
                            INNER JOIN evento AS ev ON s.idSede = ev.idSede
                            INNER JOIN tipo AS t ON ev.idTipo = t.idTipo

                            INNER JOIN participante AS p ON ev.idEvento = p.idEvento
                            INNER JOIN aprendiz AS ap ON p.idAprendiz = ap.idAprendiz

                            INNER JOIN usuario AS us ON ap.idUsuario = us.idUsuario
                            INNER JOIN persona AS ps ON us.idPersona = ps.idPersona
                            JOIN (SELECT MAX(participante.puntaje) AS punta FROM participante  GROUP BY participante.idEvento) AS maximo
                            ON puntaje = maximo.punta

                            WHERE ct.idCentroFormacion = :centro AND ev.fechaI BETWEEN '$this->fechaini' AND '$this->fechafin'
                                ";
                break;

            case 4:
                $sql = " SELECT fi.ficha AS fichas, COUNT(DISTINCT p.idEvento) AS conteo
                            FROM participante AS p
                            INNER JOIN aprendiz AS a ON p.idAprendiz = a.idAprendiz
                            INNER JOIN aprendiz_ficha AS af ON a.idAprendiz = af.idAprendiz
                            INNER JOIN ficha AS fi ON af.idFicha = fi.idFicha
                            INNER JOIN sede AS s ON fi.idSede = s.idSede
                            INNER JOIN centro_formacion AS ct ON s.idCentroFormacion = ct.idCentroFormacion
                            INNER JOIN evento AS ev ON s.idSede = ev.idSede

                            WHERE ct.idCentroFormacion = :centro AND ev.fechaI BETWEEN '$this->fechaini' AND '$this->fechafin'
                            GROUP BY fichas
                                ";
                break;

            case 5:
                $sql = " SELECT COUNT(asis.idPer) AS conteo, MONTH(asis.fechaAsis) AS fechas, g.nombre AS gim
                    FROM asistenciagimnasio AS asis
                    INNER JOIN gimnasio AS g ON asis.idGim = g.idGimnasio
                    INNER JOIN sede AS s ON g.idSede = s.idSede
                    INNER JOIN centro_formacion AS ct ON s.idCentroFormacion = ct.idCentroFormacion

                    WHERE ct.idCentroFormacion = :centro AND asis.fechaAsis BETWEEN '$this->fechaini' AND '$this->fechafin'

                    GROUP BY fechas, asis.idGim
                                ";
                break;

            case 6:
                $sql = " SELECT COUNT(a.ina) AS conteo, MONTH(a.fechaAsis) AS fechas, g.nombre AS gim 
                    FROM asistenciagimnasio AS a
                    INNER JOIN gimnasio AS g ON a.idGim = g.idGimnasio
                    INNER JOIN sede AS s ON g.idSede = s.idSede
                    INNER JOIN centro_formacion AS ct ON s.idCentroFormacion = ct.idCentroFormacion

                    WHERE ct.idCentroFormacion = :centro AND a.fechaAsis BETWEEN '$this->fechaini' AND '$this->fechafin'

                    GROUP BY fechas, a.idGim
                                ";
                break;

            case 7:
                $sql = "SELECT u.usuario AS iden, p.primerNombre AS nombre, COUNT(ru.idUsuario) AS conteo from rutina AS r INNER JOIN rutina_usuario AS ru ON ru.idRutina = r.idRutina INNER JOIN usuario AS u ON u.idUsuario = ru.idUsuario INNER JOIN persona AS p ON p.idPersona = u.idPersona INNER JOIN aprendiz AS ap ON ap.idUsuario = u.idUsuario INNER JOIN aprendiz_ficha AS af ON af.idAprendiz = ap.idAprendiz INNER JOIN ficha AS f ON f.idFicha = af.idFicha INNER JOIN sede AS s ON s.idSede = f.idSede INNER JOIN centro_formacion AS ct ON ct.idCentroFormacion = s.idCentroFormacion
                    WHERE ct.idCentroFormacion = :centro AND ru.fechaCreacion BETWEEN '$this->fechaini' AND '$this->fechafin'

                    GROUP BY ru.idUsuario
                                ";
                break;                                 
            
            default:
                break;
        }
        
        //$condicion = $this->obtenerCondicion();
        
        $consulta = Conexionn::conectar()->prepare($sql);
        $consulta->bindParam(":centro", $idsede);
        $consulta->execute();
        $res = $consulta->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }    
    
    // Condicion funcion
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idActividad !=''){
            $condicion=$condicion.$whereAnd." actividad.idActividad  = $this->idActividad";
            $whereAnd = ' AND ';
        }       
        return $condicion;      
    }

    public function consultarCentroF(){

        $sentenciaSql = "SELECT * FROM regional";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarSedeId($id){

        $sentenciaSql = "SELECT idSede,nombre FROM sede WHERE idCentroFormacion = ".$id;
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarSedeIds($id){

       $res = ConsultaGenericBase::leer('centro_formacion', 'idRegional', $id, 'idCentroFormacion,nombre');

       return $res;
    }
    
    public function __destruct() {
        
    }

    /**
     * @param mixed $fechaini
     *
     * @return self
     */
    public function setFechaini($fechaini)
    {
        $this->fechaini = $fechaini;

        return $this;
    }

   
   

    /**
     * @param mixed $fechafin
     *
     * @return self
     */
    public function setFechafin($fechafin)
    {
        $this->fechafin = $fechafin;

        return $this;
    }
}
?>
