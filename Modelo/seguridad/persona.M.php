<?php 
	 include("../../Entorno/conexion.php");
	 class registro {

	 	private $identidad;
	 	public $prinombre;
		public $segnombre;
	 	public $priapellido;
	 	public $segapellido;
	 	public $rh;
	 	public $direccion;
	 	public $telefono;
	 	public $email;
	 	public $resultado;

	 	
	 	// public $tipopersona;

	 	public function getPrinombre(){
	 		 return $this->prinombre;
	 	}
	 	public function setPrinombre($prinombre){
	 		$this->prinombre = $prinombre;
	 	}
	 	public function getSegnombre(){
	 		return $this ->segnombre;
	 	}
	 	public function setSegnombre($segnombre){
	 		$this->segnombre = $segnombre;
	 	}
	 	public function getPriapellido(){
	 		return $this->priapellido;
	 	}
	 	public function setPriapellido($priapellido){
	 		$this->priapellido = $priapellido;
	 	}
	 	 	public function getSegapellido(){
	 		return $this->segapellido;
	 	}
	 	public function setSegapellido( $segapellido){
	 		$this->segapellido = $segapellido;
	 	}
	 	public function getIdentidad(){
	 		return $this->identidad;
	 	}
	 	public function setIdentidad($identidad){
	 		$this->identidad = $identidad;
	 	}
	 	public function getRh(){
	 		return $this->rh;
	 	}
	 	public function setRh($rh){
	 		$this->rh = $rh;
	 	}
	 	public function getDireccion(){
	 		return $this->direccion;
	 	}
	 	public function setDireccion($direccion){
	 		$this->direccion = $direccion;
	 	}
	 	public function getTelefono(){
	 		return $this->telefono;
	 	}
	 	public function setTelefono($telefono){
	 		$this->telefono = $telefono;
	 	}
	 	public function getEmail(){
	 		return $this->email;
	 	}
	 	public function setEmail($email){
	 		$this->email = $email;
	 	}
	 	public function guardar(){
				$registro = array();
				$registro['prinombre'] = $this ->prinombre;
				$registro['segnombre'] = $this ->segnombre;
				$registro['priapellido'] = $this ->priapellido;
				$registro['segapellido'] = $this ->segapellido;
				$registro['identidad']= $this ->identidad;
				$registro ['rh']= $this ->rh;
				$registro ['direccion']= $this ->direccion;
				$registro ['telefono']= $this ->telefono;
				$registro ['email']= $this ->email;

	 		$insertar= "INSERT INTO persona(
								 		primer_nombre,
								 		segundo_nombre,
								 		primer_apellido,
								 		segundo_apellido,
								 		identificacion,
								 		rh,
								 		direccion,
								 		telefono,
								 		email) 
	 								VALUES 
		 								('".$this->prinombre."',
		 								'".$this->segnombre."',
		 								'".$this->priapellido."',
		 								'".$this->segapellido."',
		 								'".$this->identidad."',
		 								'".$this->rh."',
		 								'".$this->direccion."',
		 								'".$this->telefono."',
		 								'".$this->email."')";

	 		$resultado = new Conexion();
	 		$resultado->preparar($insertar);
	 		$resultado->ejecutar();

	 		return "Ok";
		}

		public function consultar(){
			$resultado = new Conexion();
			$resultado->preparar("SELECT * FROM persona");
			$resultado->ejecutar();
			return $resultado->obtenerRegistros();
		}

	}
 
 ?>