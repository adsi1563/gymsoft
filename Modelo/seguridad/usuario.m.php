<?php
session_start();
class Usuario{
    
    private $idusuario;
    private $usuario;
    private $contrasenia;
    private $idpersona;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $rol;
    public  $conn=null;

    public function getIdusuario(){
		return $this->idusuario;
	}

	public function setIdusuario($idusuario){
		$this->idusuario = $idusuario;
	}

	public function getUsuario(){
		return $this->usuario;
	}

	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}

    public function getRol(){
        return $this->rol;
    }

    public function setRol($rol){
        $this->rol = $rol;
    }

	public function getContrasenia(){
		return $this->contrasenia;
	}

	public function setContrasenia($contrasenia){
		$this->contrasenia = $contrasenia;
	}

	public function getIdpersona(){
		return $this->idpersona;
	}

	public function setIdpersona($idpersona){
		$this->idpersona = $idpersona;
	}

	public function getEstado(){
		return $this->estado;
	}

	public function setEstado($estado){
		$this->estado = $estado;
	}

	public function getFechaCreacion(){
		return $this->fechaCreacion;
	}

	public function setFechaCreacion($fechaCreacion){
		$this->fechaCreacion = $fechaCreacion;
	}

	public function getFechaModificacion(){
		return $this->fechaModificacion;
	}

	public function setFechaModificacion($fechaModificacion){
		$this->fechaModificacion = $fechaModificacion;
	}

	public function getIdUsuarioCreacion(){
		return $this->idUsuarioCreacion;
	}

	public function setIdUsuarioCreacion($idUsuarioCreacion){
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}

	public function getIdUsuarioModificacion(){
		return $this->idUsuarioModificacion;
	}

	public function setIdUsuarioModificacion($idUsuarioModificacion){
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO usuario
                                (usuario,
                                contrasenia,
                                idPersona,
                                idRol,
                                estado,
                                idUsuarioCreacion,
                                idUsuarioModificacion) 
                             VALUES 
                                ('$this->usuario', 
                                '$this->contrasenia',
                                '$this->idpersona',
                                '$this->rol',
                                'A',
                                1,
                                1);";
                        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE usuario 
                            SET usuario = '$this->usuario',
                                contrasenia = '$this->contrasenia',
                                idPersona = '$this->idpersona',
                                estado = '$this->estado',
                                idUsuarioModificacion = 1
                        WHERE idUsuario = $this->idusuario  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            usuario 
                        SET
                           estado = 'I' 
                        WHERE 
                            idUsuario = $this->idusuario";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                usuario.idUsuario,
                                usuario.usuario,
                                CONCAT(persona.primerNombre,' ', persona.segundoNombre,' ',persona.primerApellido,' ', persona.segundoApellido) AS Nombre_Persona,
                                rol.descripcion AS Rol,
                                usuario.estado
                            FROM
                                 usuario 
                            INNER JOIN persona ON persona.idPersona = usuario.idPersona
                            INNER JOIN rol ON usuario.idRol = rol.idRol
                            $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idusuario !=''){
            $condicion=$condicion.$whereAnd." usuario.idUsuario  = $this->idusuario";
            $whereAnd = ' AND ';
            
        }
               
        return $condicion;
           
    }

    public function iniciarSesion(){
        $sentenciaSql = ("SELECT usuario.*,
                                CONCAT(primerNombre,' ',primerApellido) AS NombrePersona
                            FROM 
                                usuario 
                            INNER JOIN persona ON usuario.idPersona = persona.idPersona
                            WHERE  
                            usuario = '".$this->usuario."'");
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarPersona(){

        $sentenciaSql = "SELECT idPersona,CONCAT(primerNombre,' ',segundoNombre,' ',primerApellido,' ',segundoApellido) AS NombreCompleto,identificacion FROM persona WHERE idPersona=(SELECT MAX(idPersona) FROM persona)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function actualizar(){
        $sentenciaSql = "UPDATE 
                            usuario 
                        SET
                           contrasenia = '$this->contrasenia'
                        WHERE 
                            usuario = '$this->usuario'";  
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }

    
    public function __destruct() {
        unset($this->idusuario);
        unset($this->usuario);
        unset($this->contrasenia);
        unset($this->idpersona);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>