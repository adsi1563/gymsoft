<?php
class Accion{
    
    private $idUsuarioRol;
    private $idUsuario;
    private $idRol;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    /**
     * @return mixed
     */
    public function getIdUsuarioRol()
    {
        return $this->idUsuarioRol;
    }

    /**
     * @param mixed $idUsuarioRol
     *
     * @return self
     */
    public function setIdUsuarioRol($idUsuarioRol)
    {
        $this->idUsuarioRol = $idUsuarioRol;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     *
     * @return self
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->idRol;
    }

    /**
     * @param mixed $idRol
     *
     * @return self
     */
    public function setIdRol($idRol)
    {
        $this->idRol = $idRol;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "INSERT INTO usuario_rol
                            ( idUsuario,
                              idRol,
                              estado,
                              idUsuarioCreacion,
                              idUsuarioModificacion)
                    VALUES ('$this->idUsuario',
                            '$this->idRol',
                            '$this->estado',
                             1,
                             1);";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE `usuario_rol`
                        SET 
                        idUsuario = '$this->idUsuario',
                        idRol = '$this->idRol',
                        estado = '$this->estado',
                        idUsuarioModificacion = 1
                        WHERE idUsuarioRol = '$this->idUsuarioRol'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function eliminar(){
        $sentenciaSql = "UPDATE usuario_rol SET estado='I' WHERE idUsuarioRol = '$this->idUsuarioRol'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT usuario_rol.idUsuarioRol, 
                                usuario_rol.idUsuario,
                                usuario_rol.idRol,
                                usuario_rol.estado,
                                CONCAT(persona.primerNombre,' ',persona.segundoNombre,' ',persona.primerApellido,' ',persona.segundoApellido) AS nombreCompleto,
                                rol.descripcion AS descripcionRol,
                                usuario.usuario
                        FROM usuario_rol
                        INNER JOIN rol ON rol.idRol = usuario_rol.idRol 
                        INNER JOIN usuario ON usuario_rol.idUsuario = usuario.idUsuario
                        INNER JOIN persona ON persona.idPersona = usuario.idPersona
                        $condicion ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idUsuarioRol !=''){
            $condicion=$condicion.$whereAnd." usuario_rol.idUsuarioRol  = $this->idUsuarioRol";
            $whereAnd = ' AND ';   
        }
        return $condicion;
           
    }

    public function consultarRol(){

        $sentenciaSql = "SELECT idRol, descripcion FROM rol WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarUsuario($term){
        $stmt = new Conexion();
        $stmt->preparar("SELECT idUsuario, Nombre_Completo
                            FROM
                            instructornombre WHERE Nombre_Completo LIKE CONCAT('%','".$term."','%')");
        $stmt->ejecutar();
        return $stmt->obtenerRegistros();
        $stmt = null;
    }
    
    public function __destruct() {
        unset($this->idUsuarioRol);
        unset($this->idUsuario);
        unset($this->idRol);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
