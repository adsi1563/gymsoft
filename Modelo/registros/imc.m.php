<?php
class Accion{
    
    private $idImc;
    private $estatura;
    private $peso;
    private $calculoImc;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $calculoRutina;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idImc
    public function getIdImc(){return $this->idImc;}
    public function setIdImc($idImc){$this->idImc = $idImc;}
    
    //estatura
    public function getEstatura(){return $this->estatura;}
    public function setEstatura($estatura){$this->estatura = $estatura;}
    
    //peso
    public function getPeso(){return $this->peso;}
    public function setPeso($peso){$this->peso = $peso;}

     //calculoImc
     public function getCalculoImc(){return $this->calculoImc;}
     public function setCalculoImc($calculoImc){$this->calculoImc = $calculoImc;}
     
     //Estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}

    //idCalculoRutina
    public function getCalculoRutina(){return $this->calculoRutina;}
    public function setCalculoRutina($calculoRutina){$this->calculoRutina = $calculoRutina;}
    
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO imc 
                                (estatura, peso, calculoImc, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->estatura', '$this->peso', '$this->calculoImc', 'A',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE imc 
                            SET estatura = '$this->estatura', 
                                peso = '$this->peso',
                                calculoImc = '$this->calculoImc', 
                                estado = '$this->estado' 
                        WHERE idImc = $this->idImc
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            imc
                        SET
                            estado='I'
                        WHERE 
                            idImc = $this->idImc";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idImc
                                , estatura
                                , peso
                                , calculoImc
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 imc $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idImc !=''){
            $condicion=$condicion.$whereAnd." imc.idImc  = $this->idImc";
            $whereAnd = ' AND ';
        }       
        return $condicion;
           
    }

     public function consultarRutina(){

        $sentenciaSql = "SELECT idRutina, rutina FROM rutina_alimenticia WHERE imcMenor < $this->calculoRutina AND imcMayor > $this->calculoRutina";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idImc);
        unset($this->estatura);
        unset($this->peso);
        unset($this->calculoImc);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
