<?php
class Accion{

	private $idRutina;
    private $idImc;
	private $Rutina;
	private $Select;
	public $conn=null;

	public function getIdRutina(){return $this->idRutina;}
    public function setIdRutina($idRutina){$this->idRutina = $idRutina;return $this;}

    public function getIdImc(){return $this->idImc;}
    public function setIdImc($idImc){$this->idImc = $idImc;return $this;}

    public function getRutinaAlimenticia(){return $this->Rutina;}
    public function setRutinaAlimenticia($Rutina){$this->Rutina = $Rutina;return $this;
    }
 	public function getSelect(){return $this->Select;}
    public function setSelect($Select){$this->Select = $Select;}

    public function __construct() {$this->conn = new Conexion();}

        public function agregar(){
        $sentenciaSql = "
                             INSERT INTO rutina_alimenticia 
                                (rutina, imcMenor,imcMayor) 
                             VALUES 
                                ('$this->Rutina','$this->Select')
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE rutina_alimenticia 
                            SET rutina = '$this->Rutina',
                                imc = '$this->Select'
                        WHERE idRutina = $this->idRutina
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "DELETE FROM 
                            rutina_alimenticia
                        WHERE 
                            idRutina = $this->idRutina";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                             idRutina
                            , rutina
                            , CONCAT(imcMenor,' - ',imcMayor) AS IMC
                            FROM
                                 rutina_alimenticia $condicion                    
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idRutina !=''){
            $condicion=$condicion.$whereAnd." rutina_alimenticia.idRutina  = $this->idRutina";
            $whereAnd = ' AND ';
        }     
        return $condicion;  
    }

    public function consultarRutina(){

        $sentenciaSql = "SELECT * FROM rutina_alimenticia";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idRutina);
        unset($this->Rutina);
        unset($this->Select);
        unset($this->conn);
    }
}
?>

