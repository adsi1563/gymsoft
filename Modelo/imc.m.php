<?php
require_once 'conexion.php';
require_once '../controlador/imc.c.php';
    class Datos extends Conexion{

        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //|                                                                                               |
        //|                                       CREAR                                                   |
        //|                                                                                               |
        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

        public function create($peso,$altura)
        {
     
            $stmt = Conexion::connect()->prepare("INSERT INTO imc( estatura,peso) VALUES (:estatura, :peso)");
            $stmt->bindParam(":estatura",$altura);
            $stmt->bindParam(":peso",$peso);

            if($stmt->execute()){
                return "sucess";
            }else{

                return "error";
            }

        }

        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //|                                                                                               |
        //|                                       CONSULTAR                                               |
        //|                                                                                               |
        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

        public function read()
        {
            $stmt = Conexion::connect()->prepare("SELECT idImc,estatura,peso FROM imc");
            $stmt->execute();

		    return $stmt->fetchAll();

		    $stmt->close();

        }


        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //|                                                                                               |
        //|                                       BORRAR DE LA BD                                         |
        //|                                                                                               |
        //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

        public function delete($id)
        {
            $stmt = Conexion::connect()->prepare("DELETE FROM imc WHERE idImc = :id");
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);

            if($stmt->execute()){

                return "success";

            }

            else{

                return "error";

            }
        }












        
    }


?>