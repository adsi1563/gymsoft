<?php
class Accion{
    
    private $idequipo;
    private $nombremaquina;
    private $cantidad;
    private $serialmaquina;
    private $descripcion;
    private $fechaingreso;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idequipo
    public function getIdequipo(){return $this->idequipo;}
    public function setIdequipo($idequipo){$this->idequipo = $idequipo;}
    
    //nombremaquina
    public function getNombremaquina(){return $this->nombremaquina;}
    public function setNombremaquina($nombremaquina){$this->nombremaquina = $nombremaquina;}
    
    //cantidad
    public function getCantidad(){return $this->cantidad;}
    public function setCantidad($cantidad){$this->cantidad = $cantidad;}

    //serialmaquina
    public function getSerialmaquina(){return $this->serialmaquina;}
    public function setSerialmaquina($serialmaquina){$this->serialmaquina = $serialmaquina;}

    //descripcion
    public function getDescripcion(){return $this->descripcion;}
    public function setDescripcion($descripcion){$this->descripcion = $descripcion;}

    //fechaingreso
    public function getFechaingreso(){return $this->fechaingreso;}
    public function setFechaingreso($fechaingreso){$this->fechaingreso = $fechaingreso;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO equipo 
                                (nombre, cantidad, serialmaquina, descripcion, fecha_ingreso, id_usuario_creacion, id_usuario_modificacion) 
                             VALUES 
                                ('$this->nombremaquina', '$this->cantidad','$this->serialmaquina','$this->descripcion','$this->fechaingreso',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE equipo 
                            SET nombre = '$this->nombremaquina', 
                                cantidad = '$this->cantidad',
                                serialmaquina = '$this->serialmaquina',
                                descripcion = '$this->descripcion',
                                fecha_ingreso = '$this->fechaingreso'
                        WHERE   id_equipo = $this->idequipo  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "DELETE FROM 
                            equipo
                        WHERE 
                                id_equipo = $this->idequipo";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                id_equipo
                                , nombre
                                , cantidad
                                , serialmaquina
                                , descripcion
                                , fecha_ingreso
                                , fecha_creacion
                                , fecha_modificacion
                                , id_usuario_creacion
                                , id_usuario_modificacion
                            FROM
                                 equipo $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idequipo !=''){
            $condicion=$condicion.$whereAnd." equipo.id_equipo  = $this->idequipo";
            $whereAnd = ' AND ';
            
        }else{
            if($this->nombremaquina !=''){
                $condicion=$condicion.$whereAnd." equipo.nombre  like '$this->nombremaquina'";
                $whereAnd = ' AND ';            
            }
            if($this->cantidad!=''){          
                        $condicion=$condicion.$whereAnd." equipo.cantidad = '%$this->cantidad%'";
                        $whereAnd = ' AND ';                    
            }
            // if($this->serialmaquina!=''){          
            //             $condicion=$condicion.$whereAnd." equipo.serialmaquina = '%$this->serialmaquina%'";
            //             $whereAnd = ' AND ';                    
            // }
            // if($this->descripcion!=''){          
            //             $condicion=$condicion.$whereAnd." equipo.descripcion = '%$this->descripcion%'";
            //             $whereAnd = ' AND ';                    
            // }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idequipo);
        unset($this->nombremaquina);
        unset($this->cantidad);
        unset($this->serialmaquina);
        unset($this->descripcion);
        unset($this->fechaingreso);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}

?>
