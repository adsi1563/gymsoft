<?php
class Accion{
    
    private $idCentroFormacion;
    private $nombre;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $idAseguradora;
    private $idCiudad;
    private $idRegional;
    private $Estado;
    public $conn=null;
    
   
    //idCentroFormacion
    public function getIdCentroFormacion(){return $this->idCentroFormacion;}
    public function setIdCentroFormacion($idCentroFormacion){$this->idCentroFormacion = $idCentroFormacion;}

    //Nombre
    public function getNombre(){return $this->nombre;}
    public function setNombre($nombre){$this->nombre = $nombre;}

    //fechaCreacion
    public function getFechaCreacion(){return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion){$this->fechaCreacion = $fechaCreacion;}

    //fechaModificacion
    public function getFechaModificacion(){return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion){$this->fechaModificacion = $fechaModificacion;}

    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($idUsuarioCreacion){$this->idUsuarioCreacion = $idUsuarioCreacion;}

    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($idUsuarioModificacion){$this->idUsuarioModificacion = $idUsuarioModificacion;}

    //idCuidad
    public function getIdCiudad(){return $this->idCiudad;}
    public function setIdCiudad($idCiudad){$this->idCiudad = $idCiudad;}

    //idRegional
    public function getIdRegional(){return $this->idRegional;}
    public function setIdRegional($idRegional){$this->idRegional = $idRegional;}

    //idCuidad
    public function getIdAseguradora(){return $this->idAseguradora;}
    public function setIdAseguradora($idAseguradora){$this->idAseguradora = $idAseguradora;}

    public function getEstado(){return $this->Estado;}
    public function setEstado($Estado){$this->Estado = $Estado;}

    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO centro_formacion 
                                (nombre, idAseguradora, idCiudad, idRegional, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->nombre','$this->idAseguradora','$this->idCiudad','$this->idRegional','$this->Estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE centro_formacion 
                            SET nombre = '$this->nombre',
                                idAseguradora = '$this->idAseguradora',
                                idRegional = '$this->idRegional',
                                idCiudad = '$this->idCiudad',
                                estado = '$this->Estado' 
                        WHERE idCentroFormacion = $this->idCentroFormacion  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE
                            centro_formacion
                        SET
                           estado ='I' 
                        WHERE 
                            idCentroFormacion = $this->idCentroFormacion";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idCentroFormacion
                                , nombre
                                , idAseguradora
                                , idRegional
                                , idCiudad
                                , estado
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                                , fechaCreacion
                                , fechaModificacion
                            FROM
                                 centro_formacion $condicion                    
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idCentroFormacion !=''){
            $condicion=$condicion.$whereAnd." centro_formacion.idCentroFormacion  = $this->idCentroFormacion";
            $whereAnd = ' AND ';
            
        }
        return $condicion;
           
    }

    public function consultarAseguradora(){

        $sentenciaSql = "SELECT * FROM aseguradora WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarRegional(){

        $sentenciaSql = "SELECT * FROM regional WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarCiudad(){

        $sentenciaSql = "SELECT * FROM ciudad WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idCentroFormacion);
        unset($this->nombre);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->idAseguradora);
        unset($this->idCiudad);
        unset($this->idRegional);
        unset($this->Estado);
        unset($this->conn);
    }

}
?>
