<?php
class Accion{
    
    private $idPersona;
    private $PrimerNombre;
    private $SegundoNombre;
    private $PrimerApellido;
    private $SegundoApellido;
    private $Documento;
    private $Rh;
    private $Direccion;
    private $Telefono;
    private $Correo;
    private $IdTipoPersona;
    private $IdTipoIdentificacion;
    private $IdGimasio;
    private $IdEps;
    private $Estado;
    private $idCondicion;
    private $condicion;
    private $contrasenia;
    private $token1;
    private $token2;
    public $conn=null;

    public function getContrasenia(){return $this->contrasenia;}
    public function setContrasenia($contrasenia){$this->contrasenia = $contrasenia;return $this;}

    public function getToken1(){return $this->token1;}
    public function setToken1($token1){$this->token1 = $token1;return $this;}

    public function getToken2(){return $this->token2;}
    public function setToken2($token2){$this->token2 = $token2;return $this;}

    public function getIdCondicion(){return $this->idCondicion;}
    public function setIdCondicion($idCondicion){$this->idCondicion = $idCondicion;return $this;}

    public function getCondicion(){return $this->condicion;}
    public function setCondicion($condicion){$this->condicion = $condicion;return $this;}
    
    public function getIdPersona(){return $this->idPersona;}
    public function setIdPersona($idPersona){$this->idPersona = $idPersona;return $this;}

    public function getPrimerNombre(){return $this->PrimerNombre;}
    public function setPrimerNombre($PrimerNombre){$this->PrimerNombre = $PrimerNombre;return $this;
    }

    public function getSegundoNombre(){return $this->SegundoNombre;}
    public function setSegundoNombre($SegundoNombre){$this->SegundoNombre = $SegundoNombre;return $this;}

    public function getPrimerApellido(){return $this->PrimerApellido;}
    public function setPrimerApellido($PrimerApellido){$this->PrimerApellido = $PrimerApellido;return $this;}

    public function getSegundoApellido(){return $this->SegundoApellido;}
    public function setSegundoApellido($SegundoApellido){$this->SegundoApellido = $SegundoApellido;return $this;}

    public function getDocumento(){return $this->Documento;}
    public function setDocumento($Documento){$this->Documento = $Documento;return $this;}

    public function getRh(){return $this->Rh;}
    public function setRh($Rh){$this->Rh = $Rh;return $this;}

    public function getDireccion(){return $this->Direccion;}
    public function setDireccion($Direccion){$this->Direccion = $Direccion;return $this;}

    public function getTelefono(){return $this->Telefono;}
    public function setTelefono($Telefono){$this->Telefono = $Telefono;return $this;}

    public function getCorreo(){return $this->Correo;}
    public function setCorreo($Correo){$this->Correo = $Correo;return $this;}

    public function getIdTipoPersona(){return $this->IdTipoPersona;}
    public function setIdTipoPersona($IdTipoPersona){$this->IdTipoPersona = $IdTipoPersona;return $this;}

    public function getIdTipoIdentificacion(){return $this->IdTipoIdentificacion;}
    public function setIdTipoIdentificacion($IdTipoIdentificacion){$this->IdTipoIdentificacion = $IdTipoIdentificacion;return $this;}

    public function getIdGimasio(){return $this->IdGimasio;}
    public function setIdGimasio($IdGimasio){$this->IdGimasio = $IdGimasio;return $this;}

    public function getIdEps(){return $this->IdEps;}
    public function setIdEps($IdEps){$this->IdEps = $IdEps;return $this;}

    public function getEstado(){return $this->Estado;}
    public function setEstado($Estado){$this->Estado = $Estado;}

     public function __construct() {$this->conn = new Conexion();}

        public function agregar(){
        $sentenciaSql = "
                             INSERT INTO persona 
                                (primerNombre
                                , segundoNombre
                                , primerApellido
                                , segundoApellido
                                , identificacion
                                , rh
                                , direccion
                                , telefono
                                , email
                                , idTipoIdentificacion
                                , idGimnasio
                                , idEps
                                , idCondicion
                                , estado
                                , idUsuarioCreacion
                                , idUsuarioModificacion) 
                             VALUES 
                                ( '$this->PrimerNombre'
                                , '$this->SegundoNombre'
                                , '$this->PrimerApellido'
                                , '$this->SegundoApellido'
                                , '$this->Documento'
                                , '$this->Rh'
                                , '$this->Direccion'
                                , '$this->Telefono'
                                , '$this->Correo'
                                , '$this->IdTipoIdentificacion'
                                , '$this->IdGimasio'
                                , '$this->IdEps'
                                , '$this->Condicion'
                                , '$this->Estado'
                                , 1
                                , 1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE persona 
                            SET primerNombre = '$this->PrimerNombre',
                                segundoNombre = '$this->SegundoNombre',
                                primerApellido = '$this->PrimerApellido',
                                segundoApellido = '$this->SegundoApellido',
                                identificacion = '$this->Documento',
                                rh = '$this->Rh',
                                direccion = '$this->Direccion',
                                telefono = '$this->Telefono',
                                email = '$this->Correo',
                                idTipoIdentificacion = '$this->IdTipoIdentificacion',
                                idGimnasio = '$this->IdGimasio',
                                idEps = '$this->IdEps',
                                idUsuarioModificacion = 1,
                                estado = '$this->Estado'
                        WHERE idPersona = $this->idPersona
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            persona
                        SET
                            estado = 'I'
                        WHERE 
                            idPersona = $this->idPersona";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                             idPersona
                            , primerNombre
                            , segundoNombre
                            , primerApellido
                            , segundoApellido
                            , identificacion
                            , rh
                            , direccion
                            , telefono
                            , email
                            , idTipoIdentificacion
                            , idGimnasio
                            , idEps
                            , estado
                            FROM
                                 persona $condicion                    
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idPersona !=''){
            $condicion=$condicion.$whereAnd." persona.idPersona  = $this->idPersona";
            $whereAnd = ' AND ';
        }     
        return $condicion;  
    }

    public function consultarIdentificacion(){

        $sentenciaSql = "SELECT * FROM tipo_identificacion WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarEps(){

        $sentenciaSql = "SELECT * FROM eps WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarGimnasio(){

        $sentenciaSql = "SELECT * FROM gimnasio WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function readAutocompletar($term){
        $stmt = new Conexion();
        $stmt->preparar("SELECT IdPersona, Nombre_Completo
                            FROM
                            autocompletar WHERE Nombre_Completo LIKE CONCAT('%','".$term."','%')");
        $stmt->ejecutar();
        return $stmt->obtenerRegistros();
        $stmt = null;
    }

    public function consultarRol(){

        $sentenciaSql = "SELECT * FROM rol WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarCondiciones(){

        $sentenciaSql = "SELECT idGrupo,descripcion FROM grupo_enfermedades";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarEnfermedades(){

        $sentenciaSql = "SELECT * FROM condicion_preexistente WHERE idGrupo = $this->idCondicion";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarEmail(){

        $sentenciaSql = "SELECT persona.email
                            , CONCAT(persona.primerNombre,' ',persona.primerApellido) AS Nombre
                            , usuario.usuario
                            , usuario.token 
                        FROM usuario 
                        INNER JOIN persona ON usuario.idPersona = persona.idPersona
                        WHERE persona.estado='A' AND email = '$this->Correo'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function actualizar(){
        $sentenciaSql = "UPDATE 
                            usuario 
                        SET
                           contrasenia = '$this->contrasenia'
                        WHERE 
                            usuario = '$this->token2'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }

    public function consultarUsuario(){

        $sentenciaSql = "SELECT usuario,token FROM usuario WHERE usuario = '$this->token2' AND token = '$this->token1'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function actualizarToken(){
        $sentenciaSql = "UPDATE 
                            usuario 
                        SET
                           token = '$this->token1'
                        WHERE 
                            usuario = '$this->token2'"; 
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function __destruct() {
        unset($this->idPersona);
        unset($this->PrimerNombre);
        unset($this->SegundoNombre);
        unset($this->PrimerApellido);
        unset($this->SegundoApellido);
        unset($this->Documento);
        unset($this->Rh);
        unset($this->Direccion);
        unset($this->Telefono);
        unset($this->Correo);
        unset($this->IdTipoPersona);
        unset($this->IdTipoIdentificacion);
        unset($this->IdGimasio);
        unset($this->IdEps);
        unset($this->Estado);
        unset($this->conn);
    }
}
?>
