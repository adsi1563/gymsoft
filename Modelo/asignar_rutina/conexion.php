<?php

class Conexion
{
    public static function getConnection()
    {
        try {
            $user = "root";
            $password = "";
            $dbname = "proyectoformativo";
            $dsn = "mysql:host=localhost;dbname=$dbname";
            $dbh = new PDO($dsn, $user, $password);
            return $dbh;
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    
}

?>