<?php
require_once 'conexion.php';
    class Datos extends Conexion
    {
         /**
         * ESTA LA CONSULTA DONDE SE DEBE MOSTRAR LA INFO DEL USUARIO
         * ES DECIR EL IMC SU NOMBRE Y SU RUTINA
         */
        public static function consultarDatosUsuarioModel($tabla)
        {
            $stmt = Conexion::getConnection()->prepare("SELECT * FROM $tabla");
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            // Ejecutamos
            $stmt->execute();
            // Mostramos los resultados
            return $stmt;

        }
        /**
         * ESTA CONSULTA ES PARA MOSTRAR UN SELECT AL USER DE LOS USURIOS DISPONIBLES EN LA BD
         */
        public static function consultarUser($tabla)
        {
            $stmt = Conexion::getConnection()->prepare("SELECT * FROM $tabla");
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            // Ejecutamos
            $stmt->execute();
            // Mostramos los resultados
            return $stmt;
        }

        /**
         * ESTA INSERCIÓN ES PARA CREAR LA RUTINA EN LA BD
         * ES DECIR EL NOMBRE Y SU RUTINA
         */
        public static function  createRutinaModel($tabla, $datos)
        {
            $stmt = Conexion::getConnection()->prepare("INSERT INTO $tabla (idUsuario,hora,rutina)VALUES(?, ?,?)");
            $stmt->bindParam(1, $datos['usuario']);
            $stmt->bindParam(2, $datos['hora']);
            $stmt->bindParam(3, $datos['rutina']);
            // Ejecutamos
            if($stmt->execute()){
                return "succes";
            }else{
                return ":(";
            }
            // Mostramos los resultados
            
        }
        
    }
    
?>


