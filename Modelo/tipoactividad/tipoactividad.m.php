<?php
class Accion{
    
    private $idTipoActividad;
    private $Nombre;
    private $Descripcion;
    private $Tiempo;
    private $Estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public $conn=null;

    public function getIdTipoActividad(){return $this->idTipoActividad;}
    public function setIdTipoActividad($idTipoActividad){$this->idTipoActividad = $idTipoActividad;}

    public function getNombre(){return $this->Nombre;} 
    public function setNombre($Nombre){$this->Nombre = $Nombre;}

    public function getDescripcion(){return $this->Descripcion;}
    public function setDescripcion($Descripcion){$this->Descripcion = $Descripcion;}

    public function getTiempo(){return $this->Tiempo;}
    public function setTiempo($Tiempo){$this->Tiempo = $Tiempo;}

    public function getEstado(){return $this->Estado;}
    public function setEstado($Estado){$this->Estado = $Estado;}

     public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //

    public function __construct() {$this->conn = new Conexion();}

     public function agregar(){
        $sentenciaSql = "
                             INSERT INTO tipo 
                                (nombre, descripcion, tiempoActividad, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->Nombre','$this->Descripcion','$this->Tiempo','$this->Estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE tipo 
                            SET nombre = '$this->Nombre',
                                descripcion = '$this->Descripcion',
                                tiempoActividad = '$this->Tiempo',
                                estado = '$this->Estado'
                        WHERE idTipo = '$this->idTipoActividad'
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE
                            tipo
                        SET 
                           estado = 'I' 
                        WHERE 
                            idTipo = '$this->idTipoActividad'";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                            idTipo
                            , nombre
                            , descripcion
                            , tiempoActividad
                            , estado

                            FROM
                                 tipo $condicion                    
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idTipoActividad !=''){
            $condicion=$condicion.$whereAnd." tipo.idTipo  = $this->idTipoActividad";
            $whereAnd = ' AND ';
            
        }
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idTipoActividad);
        unset($this->Nombre);
        unset($this->Descripcion);
        unset($this->Tiempo);
        unset($this->Estado);
        unset($this->conn);
    }
}
?>