<?php
class Accion{
    
    private $idtipoidentificacion;
    private $tipoIdentificacion;
    private $Descripcion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    /**
     * @param mixed $idtipoidentificacion
     *
     * @return self
     */
    public function setIdtipoidentificacion($idtipoidentificacion)
    {
        $this->idtipoidentificacion = $idtipoidentificacion;

        return $this;
    }

    /**
     * @param mixed $tipoIdentificacion
     *
     * @return self
     */
    public function setTipoIdentificacion($tipoIdentificacion)
    {
        $this->tipoIdentificacion = $tipoIdentificacion;

        return $this;
    }

    /**
     * @param mixed $Descripcion
     *
     * @return self
     */
    public function setDescripcion($Descripcion)
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
    
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO tipo_identificacion 
                                (tipo, descripcion, idUsuarioCreacion, idUsuarioModificacion, fechaCreacion,fechaModificacion) 
                             VALUES 
                                ('$this->tipoIdentificacion', '$this->Descripcion',1,1,NOW(),NOW())
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE tipo_identificacion 
                            SET tipo = '$this->tipoIdentificacion', 
                                descripcion = '$this->Descripcion',
                                idUsuarioModificacion = 1,
                                fechaModificacion = NOW()
                        WHERE   idTipoIdentificacion = $this->idtipoidentificacion  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            tipo_identificacion
                        SET
                            estado='I'
                        WHERE 
                                idTipoIdentificacion = $this->idtipoidentificacion";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idTipoIdentificacion
                                , tipo
                                , descripcion
                            FROM
                                 tipo_identificacion $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idtipoidentificacion !=''){
            $condicion=$condicion.$whereAnd." tipo_identificacion.idTipoIdentificacion  = $this->idtipoidentificacion";
            $whereAnd = ' AND ';
            
        }else{
            if($this->tipoIdentificacion !=''){
                $condicion=$condicion.$whereAnd." tipo_identificacion.tipo  like '$this->tipoIdentificacion'";
                $whereAnd = ' AND ';            
            }
            if($this->Descripcion!=''){          
                        $condicion=$condicion.$whereAnd." tipo_identificacion.descripcion = '%$this->Descripcion%'";
                        $whereAnd = ' AND ';                    
            }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idtipoidentificacion);
        unset($this->tipoIdentificacion);
        unset($this->Descripcion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }

}

?>
