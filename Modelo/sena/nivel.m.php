<?php
class Accion{
    
    private $idNivel;
    private $estado;
    private $nombre;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
     /**
     * @return mixed
     */
    public function getIdNivel()
    {
        return $this->idNivel;
    }

    /**
     * @param mixed $idNivel
     *
     * @return self
     */
    public function setIdNivel($idNivel)
    {
        $this->idNivel = $idNivel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
       


        $sentenciaSql = "
                             INSERT INTO nivel_programa 
                                (descripcion,estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->nombre','$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE nivel_programa 
                            SET descripcion = '$this->nombre',
                                estado = '$this->estado',
                                idUsuarioModificacion = 1        
                        WHERE idNivelPrograma = $this->idNivel   
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            nivel_programa
                        SET
                            estado='I'
                        WHERE 
                            idNivelPrograma = $this->idNivel";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idNivelPrograma
                                , descripcion
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 nivel_programa  $condicion				
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        
        $whereAnd = " WHERE ";

        $condicion = "";

        if($this->idNivel !=''){

            
            $condicion=$condicion.$whereAnd." nivel_programa.idNivelPrograma  = $this->idNivel";
            $whereAnd = ' AND ';
            
        }else if($this->nombre!='') {


            
            if($this->nombre!=''){          
                        $condicion=$condicion.$whereAnd." nivel_programa.descripcion = '%$this->nombre%'";
                        $whereAnd = ' AND ';                    
            }
        }  else {

            $condicion = "";
        }     
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idNivel);
        unset($this->nombre);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
