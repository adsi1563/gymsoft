<?php
    require_once '../../Entorno/conexion.php';
    session_start();
    class ModeloInstructorFicha{
        public $instructor;
        public $ficha;

        public function getInstructor(){
             return $this->instructor;
        }
        public function setInstructor($instructor){
            $this->instructor = $instructor;
        }

        public function getFicha(){
             return $this->ficha;
        }
        public function setFicha($ficha){
            $this->ficha = $ficha;
        }

        static public function mdlConsultarTodoInstructorFicha(){
            $stmt = new Conexion();
            $stmt->preparar("SELECT * FROM instructor_ficha");
            $stmt->ejecutar();
            if($stmt->obtenerNumeroRegistros() > 0){
                return $stmt->obtenerRegistros();
            }
        }
        
        static public function mdlGuardarInstructorFicha(){
            $sql = "INSERT INTO instructor_ficha
                        (id_ficha,
                        id_instructor,
                        id_usuario_modificacion,
                        fecha_creacion,
                        fecha_modificacion)
                    VALUES ('".$this->ficha."',
                            '".$this->instructor."',
                            ".$_SESSION["idUsuario"].",
                            (SELECT NOW()),
                            (SELECT NOW()));";
            $stmt = new Conexion();
            $stmt->preparar($sql);
            if($stmt->ejecutar()){
                return "ok";
            }else{
                return "error";
            }
            $stmt = null;
        }
        
        static public function mdlModificarInstructorFicha($datos){
            $stmt = new Conexion();
            $stmt->preparar("UPDATE instructor_ficha
                            SET 
                            id_ficha = ".$this->ficha.",
                            id_instructor = ".$this->instructor.",
                            id_usuario_modificacion = "$_SESSION["idUsuario"].",
                            fecha_modificacion = (SELECT NOW())
                            WHERE id_marca = ".$datos["hiddenIdficha"]."
                            ");
            if($stmt->ejecutar()){
                return "ok";
            }else{
                return "error";
            }
            $stmt = null;
        }
        
        static public function mdlEliminarInstructorFicha($idEliminar){
            $stmt = new Conexion();
            $stmt->preparar("DELETE FROM instructor_ficha WHERE id_instructor_ficha = ".$idEliminar);
            if($stmt->ejecutar()){
                return "ok";
            }else{
                return "error";
            }
        }
    }
?>