<?php
class Accion{
    
    private $idAmbiente;
    private $codigo;
    private $descripcion;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idDepartamento
    public function getIdAmbiente(){return $this->idAmbiente;}
    public function setIdAmbiente($idAmbiente){$this->idAmbiente = $idAmbiente;}
    
    //Codigo
    public function getCodigo(){return $this->codigo;}
    public function setCodigo($codigo){$this->codigo = $codigo;}
    
    //descripcion
    public function getNombre(){return $this->descripcion;}
    public function setNombre($descripcion){$this->descripcion = $descripcion;}

    //estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}

    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO ambiente 
                                (codigo, descripcion, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->codigo', '$this->descripcion', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE ambiente 
                            SET codigo = '$this->codigo', 
                                descripcion = '$this->descripcion',
                                estado =  '$this->estado'
                        WHERE idAmbiente = $this->idAmbiente  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            ambiente
                        SET
                            estado='I'
                        WHERE 
                            idAmbiente = $this->idAmbiente";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condiciones = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idAmbiente
                                , codigo
                                , descripcion
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 ambiente $condiciones					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAmbiente !=''){
            $condicion=$condicion.$whereAnd." ambiente.idAmbiente  = $this->idAmbiente";
            $whereAnd = ' AND ';
            
        }else  if($this->codigo !='' && $this->descripcion!='') {
            if($this->codigo !=''){
                $condicion=$condicion.$whereAnd." ambiente.codigo  like '$this->codigo'";
                $whereAnd = ' AND ';            
            }
            if($this->descripcion!=''){          
                        $condicion=$condicion.$whereAnd." ambiente.descripcion = '%$this->descripcion%'";
                        $whereAnd = ' AND ';                    
            }
        } else { $condicion = "";   }      
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idAmbiente);
        unset($this->codigo);
        unset($this->nombre);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
