<?php
class Aprendiz{
    
    private $idAprendiz;
    private $usuario;
    private $imc;
    private $acudiente;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;

    /**
     * @return mixed
     */
    public function getIdAprendiz()
    {
        return $this->idAprendiz;
    }

    /**
     * @param mixed $idAprendiz
     *
     * @return self
     */
    public function setIdAprendiz($idAprendiz)
    {
        $this->idAprendiz = $idAprendiz;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     *
     * @return self
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImc()
    {
        return $this->imc;
    }

    /**
     * @param mixed $imc
     *
     * @return self
     */
    public function setImc($imc)
    {
        $this->imc = $imc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcudiente()
    {
        return $this->acudiente;
    }

    /**
     * @param mixed $acudiente
     *
     * @return self
     */
    public function setAcudiente($acudiente)
    {
        $this->acudiente = $acudiente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     *
     * @return self
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * @param mixed $fechaModificacion
     *
     * @return self
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @param mixed $idUsuarioCreacion
     *
     * @return self
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioModificacion()
    {
        return $this->idUsuarioModificacion;
    }

    /**
     * @param mixed $idUsuarioModificacion
     *
     * @return self
     */
    public function setIdUsuarioModificacion($idUsuarioModificacion)
    {
        $this->idUsuarioModificacion = $idUsuarioModificacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }


    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO aprendiz
                                (idUsuario,
                                idAcudiente,
                                idImc,
                                estado,
                                idUsuarioCreacion,
                                idUsuarioModificacion) 
                             VALUES 
                                ('$this->usuario', 
                                '$this->acudiente',  
                                '$this->imc',
                                'A',
                                1,
                                1);
                        ";
                        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE aprendiz 
                            SET idUsuario = '$this->usuario',
                                idAcudiente = '$this->acudiente',
                                idImc = '$this->imc',
                                estado = '$this->estado' 
                        WHERE idAprendiz = $this->idAprendiz
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                           aprendiz
                        SET
                           estado = 'I' 
                        WHERE 
                            idAprendiz = $this->idAprendiz";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                aprendiz.idAprendiz
                                , aprendiz.idUsuario
                                , aprendiz.idAcudiente
                                , aprendiz.idImc
                                , aprendiz.estado
                                , CONCAT(persona.primerNombre,' ', persona.segundoNombre,' ',persona.primerApellido,' ', persona.segundoApellido) AS nombrePersona
                                , CONCAT(acudiente.nombre,' ',acudiente.apellido) AS nombreAcudiente
                                , imc.calculoImc
                            FROM
                                 aprendiz 
                            INNER JOIN usuario ON usuario.idUsuario = aprendiz.idUsuario
                            INNER JOIN acudiente ON acudiente.idAcudiente = aprendiz.idAcudiente
                            INNER JOIN persona ON persona.idPersona = usuario.idPersona
                            INNER JOIN imc ON imc.idImc = aprendiz.idImc 
                            $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAprendiz !=''){
            $condicion=$condicion.$whereAnd." aprendiz.idAprendiz  = $this->idAprendiz";
            $whereAnd = ' AND ';    
        }   
        return $condicion;       
    }


    public function consultarImc(){

        $sentenciaSql = "SELECT idImc,calculoImc FROM imc WHERE idImc=(SELECT MAX(idImc) FROM imc)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarUsuario(){

        $sentenciaSql = "SELECT idUsuario,usuario FROM usuario WHERE idUsuario=(SELECT MAX(idUsuario) FROM usuario)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    public function consultarAcudiente(){

        $sentenciaSql = "SELECT idAcudiente,CONCAT(nombre,' ',apellido) AS Nombre FROM acudiente WHERE idAcudiente=(SELECT MAX(idAcudiente) FROM acudiente)";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }

    
    public function __destruct() {
        unset($this->idAprendiz);
        unset($this->usuario);
        unset($this->imc);
        unset($this->acudiente);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>