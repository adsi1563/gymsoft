<?php
class Accion{
    
    private $idProgramaFormacion;
    private $codigo;
    private $descripcion;
    private $duracion;
    private $nivelPrograma;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idProgrmadeformacion
    public function getIdProgramaFormacion(){return $this->idProgramaFormacion;}
    public function setIdProgramaFormacion($idProgramaFormacion){$this->idProgramaFormacion = $idProgramaFormacion;}
    
    //Codigo
    public function getCodigo(){return $this->codigo;}
    public function setCodigo($codigo){$this->codigo = $codigo;}
    
    //Descrpcion
    public function getDescripcion(){return $this->descripcion;}
    public function setDescripcion($descripcion){$this->descripcion = $descripcion;}

    //Duracion
    public function getDuracion(){return $this->duracion;}
    public function setDuracion($duracion){$this->duracion = $duracion;}

    //nivelPrograma
    public function getNivelPrograma(){return $this->nivelPrograma;}
    public function setNivelPrograma($nivelPrograma){$this->nivelPrograma = $nivelPrograma;}

    //estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($idUsuarioCreacion) { $this->idUsuarioCreacion =$idUsuarioCreacion;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($idUsuarioModificacion) { $this->idUsuarioModificacion =$idUsuarioModificacion;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO programa_formacion 
                                (codigo, descripcion, duracion, idNivelPrograma, estado,idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->codigo', '$this->descripcion', '$this->duracion', '$this->nivelPrograma', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE programa_formacion 
                            SET codigo = '$this->codigo', 
                                descripcion = '$this->descripcion',
                                duracion = '$this->duracion',
                                idNivelPrograma = '$this->nivelPrograma',
                                estado = '$this->estado'
                        WHERE idProgramaFormacion = $this->idProgramaFormacion  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE 
                            programa_formacion
                        SET
                            estado='I'
                        WHERE 
                            idProgramaFormacion = $this->idProgramaFormacion";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idProgramaFormacion
                                , codigo
                                , descripcion
                                , duracion
                                , idNivelPrograma
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 programa_formacion $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idProgramaFormacion !=''){
            $condicion=$condicion.$whereAnd." programa_formacion.idProgramaFormacion  = $this->idProgramaFormacion";
            $whereAnd = ' AND ';   
        }     
        return $condicion;
           
    }

    public function consultarNivelPrograma(){

        $sentenciaSql = "SELECT * FROM nivel_programa WHERE estado='A'";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function __destruct() {
        unset($this->idProgrmadeformacion);
        unset($this->codigo);
        unset($this->descripcion);
        unset($this->duracion);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
