<?php
class Accion{
    
    private $idAcudiente;
    private $nombre;
    private $apellido;
    private $telefono;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idAcudiente
    public function getIdAcudiente(){return $this->idAcudiente;}
    public function setIdAcudiente($idAcudiente){$this->idAcudiente = $idAcudiente;}
    
    //Nombre
    public function getNombre(){return $this->nombre;}
    public function setNombre($nombre){$this->nombre = $nombre;}
    
    //Apellido
    public function getApellido(){return $this->apellido;}
    public function setApellido($apellido){$this->apellido = $apellido;}

    //Telefono
    public function getTelefono(){return $this->telefono;}
    public function setTelefono($telefono){$this->telefono = $telefono;}

    //Estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($idUsuarioCreacion) { $this->idUsuarioCreacion =$idUsuarioCreacion;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($idUsuarioModificacion) { $this->idUsuarioModificacion =$idUsuarioModificacion;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO acudiente 
                                (nombre, apellido, telefono, estado, idUsuarioCreacion, idUsuarioModificacion) 
                             VALUES 
                                ('$this->nombre','$this->apellido','$this->telefono','A',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE acudiente 
                            SET nombre = '$this->nombre', 
                                apellido = '$this->apellido',
                                telefono = '$this->telefono',
                                estado = '$this->estado'
                        WHERE idAcudiente = '$this->idAcudiente'  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "UPDATE  
                            acudiente
                        SET
                            estado='I'
                        WHERE 
                            idAcudiente = '$this->idAcudiente'";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idAcudiente
                                , nombre
                                , apellido
                                , telefono
                                , estado
                                , fechaCreacion
                                , fechaModificacion
                                , idUsuarioCreacion
                                , idUsuarioModificacion
                            FROM
                                 acudiente $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAcudiente !=''){
            $condicion=$condicion.$whereAnd." acudiente.idAcudiente  = '$this->idAcudiente'";
            $whereAnd = ' AND ';
        }       
        return $condicion;
           
    }
    

    public function __destruct() {
        unset($this->idAcudiente);
        unset($this->nombre);
        unset($this->apellido);
        unset($this->telefono);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
        unset($this->stmt);
    }
}
?>
