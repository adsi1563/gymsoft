<?php
class Accion{
    
    private $idAccion;
    private $accion;
    
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idAccion
    public function getIdAccion(){return $this->idAccion;}
    public function setIdAccion($idAccion){$this->idAccion = $idAccion;}
    
    //accion
    public function getAccion(){return $this->accion;}
    public function setAccion($accion){$this->accion = $accion;}
    
        
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    //
    
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                             INSERT INTO accion 
                                (accion,id_usuario_creacion, id_usuario_modificacion) 
                             VALUES 
                                ('$this->accion', '$this->estado',1,1)
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "UPDATE accion 
                            SET accion = '$this->accion', 
                                 
                        WHERE id_accion = $this->idAccion  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "DELETE FROM 
                            accion 
                        WHERE 
                            id_accion = $this->idAccion";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                id_accion
                                , accion
                                
                                , fecha_creacion
                                , fecha_modificacion
                                , id_usuario_creacion
                                , id_usuario_modificacion
                            FROM
                                 accion $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAccion !=''){
            $condicion=$condicion.$whereAnd." accion.id_accion  = $this->idAccion";
            $whereAnd = ' AND ';
            
        }else{
            if($this->accion !=''){
                $condicion=$condicion.$whereAnd." accion.accion  like '%$this->accion%'";
                $whereAnd = ' AND ';            
            }
            if($this->estado!=''){          
                        $condicion=$condicion.$whereAnd." accion.estado = '$this->estado'";
                        $whereAnd = ' AND ';                    
            }
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idAccion);
        unset($this->accion);
        
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn);
    }
}
?>
