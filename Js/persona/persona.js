function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
        if( $('#txtPrimerNombre').val()== "",$('#txtSegundoNombre').val()== "",$('#txtPrimerApellido').val()== "",$('#txtSegundoApellido').val()== "",$('#txtDocumento').val()== "",$('#txtRh').val()== "",$('#txtDireccion').val()== "",$('#txtTelefono').val()== "",$('#txtCorreo').val()== ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdPersona').val();
    }
    var parametros = {
        "id" :id,
        "PrimerNombre" :$('#txtPrimerNombre').val(),
        "SegundoNombre" :$('#txtSegundoNombre').val(),
        "PrimerApellido" :$('#txtPrimerApellido').val(),
        "SegundoApellido" :$('#txtSegundoApellido').val(),
        "Documento" :$('#txtDocumento').val(),
        "Rh" :$('#rh').val(),
        "Direccion" :$('#txtDireccion').val(),
        "Telefono" :$('#txtTelefono').val(),
        "Correo" :$('#txtCorreo').val(),
        "Estado" :$('#estado').val(),
        "tipoIdentificacion" :$('#tipoIdentificacion').val(),
        "eps" :$('#eps').val(),
        "gimnasio" :$('#gimnasio').val(),
        "condicion":$('#condicion').val(),
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/persona/persona.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyPersona").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					//titleAttr: 'PDF'
					text: '<img src="../../img/pdf.svg" class="iconos">',
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					//titleAttr: 'EXCEL'
					text: '<img src="../../img/excel.svg" class="iconos">',
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					//titleAttr: 'COPIAR'
					text: '<img src="../../img/document.svg" class="iconos">',
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					//titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidIdPersona').val(data['id']);
				$('#txtPrimerNombre').val(data['PrimerNombre']);
				$('#txtSegundoNombre').val(data['SegundoNombre']);
				$('#txtPrimerApellido').val(data['PrimerApellido']);
				$('#txtSegundoApellido').val(data['SegundoApellido']);
				$('#txtDocumento').val(data['Documento']);
				$('#rh').val(data['Rh']);
				$('#txtDireccion').val(data['Direccion']);
				$('#txtTelefono').val(data['Telefono']);
				$('#txtCorreo').val(data['Correo']);
				$('#eps').val(data['Eps']);
				$('#gimnasio').val(data['Gimnasio']);
				$('#tipoIdentificacion').val(data['TipoIdentificacion']);
				$('#estado').val(data['Estado']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				window.location.href='../../vista/seguridad/usuario.v.php';
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });
    
}

$(document).ready(function() {
	tipoIdentificacion();
	eps();
	gimnasio();
	rol();
	condiciones();
	function rol() { 
	    var parametros = {
	        "accion": "CONSULTA_ROL"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/persona/persona.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#rol').html(response);
	        }
	    });   
	}

	function tipoIdentificacion() { 
	    var parametros = {
	        "accion": "CONSULTA_TIPO_IDENTIFICACION"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/persona/persona.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#tipoIdentificacion').html(response);
	        }
	    });   
	}

	function eps() { 
	    var parametros = {
	        "accion": "CONSULTA_EPS"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/persona/persona.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#eps').html(response);
	        }
	    });   
	}

	function gimnasio() { 
	    var parametros = {
	        "accion": "CONSULTA_GIMNASIO"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/persona/persona.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#gimnasio').html(response);
	        }
	    });   
	}

	function condiciones() { 
	    var parametros = {
	        "accion": "CONSULTA_CONDICIONES"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/persona/persona.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#condicionPre').html(response);
	        }
	    });
	}
});

function enfermedades() { 
	    var parametros = {
	        "accion": "CONSULTA_ENFERMEDADES",
	        "idCondicion": $('#condicionPre').val()
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/persona/persona.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#condicion').html(response);
	        }
	    });
	}

function selectRol() {
	
	var rol = $('#rol').val();
	sessionStorage.rol= rol;
	sessionStorage.borrado = "sessionStorage.clear()";
	
	switch(rol){
		case '1':
			sessionStorage.paso1 = '../seguridad/usuario.v.php';
			sessionStorage.paso2 = '../persona/persona.v.php';
		break;

		case '2':
			sessionStorage.paso1 = '../seguridad/usuario.v.php';
			sessionStorage.paso2 = '../persona/persona.v.php';
		break;

		case '3':
			sessionStorage.paso1 = '../seguridad/usuario.v.php';
			sessionStorage.paso2 = '../parametrizacion/entrenador.v.php';
			sessionStorage.paso3 = '../parametrizacion/entrenador_sede.v.php';
			sessionStorage.paso4 = '../persona/persona.v.php';
		break;

		case '4':
			sessionStorage.paso1 = '../seguridad/usuario.v.php';
			sessionStorage.paso2 = '../persona/persona.v.php';
		break;

		case '5':
			sessionStorage.paso1 = '../seguridad/usuario.v.php';
			sessionStorage.paso2 = '../sena/acudiente.v.php';
			sessionStorage.paso3 = '../registros/imc.v.php';
			sessionStorage.paso4 = '../sena/aprendiz.v.php';
			sessionStorage.paso5 = '../parametrizacion/aprendiz_ficha.v.php';
			sessionStorage.paso6 = '../persona/persona.v.php';

		break;

		case '6':
			sessionStorage.paso1 = '../seguridad/usuario.v.php';
			sessionStorage.paso2 = '../parametrizacion/instructor.v.php';
			sessionStorage.paso3 = '../parametrizacion/instructor_ficha.v.php';
			sessionStorage.paso4 = '../persona/persona.v.php';
		break;
	}

	$('#btnAdicionar').attr('onclick', 'Enviar("ADICIONAR",null),location.href="'+sessionStorage.paso1+'"');
}
