function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
        if( $('#txtNombre').val()== ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdCentroFormacion').val();
    }
    var parametros = {
        "id" :id,
        "nombre" :$('#txtNombre').val(),
        "Estado" :$('#estado').val(),
        "aseguradora" :$('#aseguradora').val(),
        "regional" :$('#regional').val(),
        "ciudad" :$('#ciudad').val(),
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/centroformacion/centroformacion.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyCentroFormacion").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					//titleAttr: 'PDF'
					text: '<img src="../../img/pdf.svg" class="iconos">',
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					//titleAttr: 'EXCEL'
					text: '<img src="../../img/excel.svg" class="iconos">',
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					//titleAttr: 'COPIAR'
					text: '<img src="../../img/document.svg" class="iconos">',
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					//titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidIdCentroFormacion').val(data['id']);
				$('#txtNombre').val(data['nombre']);
				$('#estado').val(data['estado']);
				$('#aseguradora').val(data['aseguradora']);
				$('#regional').val(data['regional']);
				$('#ciudad').val(data['ciudad']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });  
}

$(document).ready(function() {
	aseguradora();
	regional();
	ciudad();
	function aseguradora() { 
	    var parametros = {
	        "accion": "CONSULTA_ASEGURADORA"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/centroformacion/centroformacion.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#aseguradora').html(response);
	        }
	    });   
	}

	function regional() { 
	    var parametros = {
	        "accion": "CONSULTA_REGIONAL"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/centroformacion/centroformacion.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#regional').html(response);
	        }
	    });   
	}

	function ciudad() { 
	    var parametros = {
	        "accion": "CONSULTA_CIUDAD"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/centroformacion/centroformacion.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#ciudad').html(response);
	        }
	    });   
	}
});