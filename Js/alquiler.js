$(document).ready(function() {
	var ruta;
	var nombrePelicula;
	var formato;
	var tiempo;
	var porcentaje;
	var precioTiempo;
	var subtotal;
	var AlquilerPeliculas = new Array;

	// INICIO CALCULAR PRECIO
	$('#listaTiempo').on('change', function() {	
		porcentaje = $('#listaFormato option:selected').val();
		precioTiempo = $('#listaTiempo option:selected').val();
		subtotal = (parseInt(precioTiempo)*parseInt(porcentaje))/100+parseInt(precioTiempo);
		$('#valor').html("$ "+subtotal);
	})

	$('#listaFormato').on('change', function() {
		porcentaje = $('#listaFormato option:selected').val();
		precioTiempo = $('#listaTiempo option:selected').val();
		subtotal = (parseInt(precioTiempo)*parseInt(porcentaje))/100+parseInt(precioTiempo);
		$('#valor').html("$ "+subtotal);
	})
	// FIN CALCULAR PRECIO
	
	if (localStorage.getItem('peliculas')) {
		AlquilerPeliculas = JSON.parse(localStorage.getItem('peliculas'));
		$('#carrito').html(Object.keys(AlquilerPeliculas).length);
	}

	$(document).on('click','.btnPelicula',function(){

		ruta = $(this).parent('.card-body').prev('img').attr('src');
		nombrePelicula = $(this).parent().children('.card-title').text();
		console.log(ruta+nombrePelicula);

		document.getElementById('ImgPeliculaAlquiler').innerHTML="<img src='"+ruta+"' class='card-img-top imgPeliculaModal'>";
		document.getElementById('TextoPeliculaAlquiler').innerHTML= nombrePelicula;
		
		$.ajax({
			url: '../controlador/formato.read.php',
			type: 'POST',
			dataType: 'json',
			data: null,
		})
		.done(function(json) {
			console.log("success");
			console.log(json.mensaje);
			var listaFormato = "";
			$.each(json.arrayFormato, function(index, val) {
			 listaFormato += '<option value="'+val.porcentaje+'">'+val.formatoNombre+'</option>';
			});
			
			$('#listaFormato').html(listaFormato);
		})
		.fail(function() {
			console.log("error");
		})

		$.ajax({
			url: '../controlador/tiempo.read.php',
			type: 'POST',
			dataType: 'json',
			data: null,
		})
		.done(function(json) {
			console.log("success");
			console.log(json.mensaje);
			var listaTiempo= "";
			$.each(json.arrayTiempo, function(index, val) {
			 listaTiempo += '<option value="'+val.precio+'">'+val.descripcion+'</option>';
			});
			$('#listaTiempo').html(listaTiempo);
		})
		.fail(function() {
			console.log("error");
		})
			precio = subtotal = (parseInt("5000")*parseInt("0"))/100+parseInt("5000");
				$('#valor').html("$ "+subtotal);
	});
	
	$(document).on('click','.btnAgregar',function(){
		formato = $('#listaFormato option:selected');
		tiempo = $('#listaTiempo option:selected');
		alertify.confirm("Confirma la compra","Esta seguro de agregar la pelicula '"+nombrePelicula+"' al carrito por un timpo de: "+tiempo.text()+" con un valor de "+subtotal,
		  function(){
		  	let peliculas = {
		  		nombre: nombrePelicula,
		  		formato: formato.text(),
		  		tiempo: tiempo.text(),
		  		ruta: ruta,
		  		porcentaje: formato.val(),
		  		precioTiempo: tiempo.val(),
		  		subtotal: subtotal

		  	}
		  	alertify.success('Pelicula "'+nombrePelicula+'" Agregada al Carrito en formato '+formato.text());
		  	$('#modal1').modal('hide');
		  	AlquilerPeliculas.push(peliculas);
		  	localStorage.setItem("peliculas",JSON.stringify(AlquilerPeliculas));
		  	$('#carrito').html(Object.keys(AlquilerPeliculas).length);
		  },
		  function(){
		    alertify.error('Cancelado');
		  });
	})

	$(document).on('click','#btnCarrito',function(){
		var itemCarrito = "";
		var total = 0;
		$.each(AlquilerPeliculas, function(index,val){
			itemCarrito += "<div class='row align-items-center'>";
				itemCarrito += "<div class='col-6'>";
					itemCarrito +=	"<img src='"+val.ruta +"' class='card-img-top imgPeliculaAlquiler'></img>";
				itemCarrito += "</div>";
				itemCarrito += "<div class='col-3'>Titulo: "
					itemCarrito += "<span>"+val.nombre+"</span><br><br>";
				itemCarrito += "Formato: "
					itemCarrito += "<span>"+val.formato+"</span><br><br>";
				itemCarrito += "Tiempo: "
					itemCarrito += "<span><br>"+val.tiempo+"</span>";
				itemCarrito += "</div>";
				itemCarrito += "<div class='col-3'>"
					itemCarrito += "<span style='font-weight: bold; font-size: 18px'>$ "+val.subtotal+"</span>";
			itemCarrito += "</div>";
			total += val.subtotal;
		});
		$('#carritoModal').html(itemCarrito);
		$('#total').html(total);
	})

	$(document).on('click','#btnAlquiler', function() {
		$.ajax({
			url: '../controlador/alquiler.create.php',
			type: 'POST',
			dataType: 'json',
			data: {alquilerPeliculas: AlquilerPeliculas},
		})
		.done(function(json) {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
	})
});