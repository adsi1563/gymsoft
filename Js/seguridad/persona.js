// var urll='../Controlador/controlador.persona.php'
 // var parametros=null;

function ObtenerParametros(accion){
	var parametros = {
		"primernombre": $('#txtprinom').val(),
		"segundonombre":$('#txtsegnom').val(),
		"primerapellido":$('#txtprigapell').val(),
		"segundoapellido":$('#txtsegapell').val(),
		"identificion":$('#txtidentificacion').val(),
		"rh":$('#txtrh').val(),
		"direccion":$('#txtdireccion').val(),
		"telefono":$('#txttelefono').val(),
		"email":$('#txtemail').val(),
		"accion": accion
	}
	return parametros;
}


function guardar(){
	if ($('#txtprinom').val()== "" || $('#txtsegnom').val() == "" || $('#txtprigapell').val() == "" 
		|| $('#txtsegapell').val() == ""|| $('#txtidentificacion').val() == ""|| $('#txtrh').val() == ""|| $('#txtdireccion').val() == "" || $('#txttelefono').val() == "" || $('#txtemail').val() == "") {
		return alert("Hay datos en blanco Por favor revisar");
	}
	$.ajax({
		data: ObtenerParametros("GUARDAR"),
		url: '../../controlador/seguridad/persona.C.php',
		type: 'post',
		dataType: 'json',
		success: function (data) {}	
	});
}

function Consultar(){
	if ($('#txtidentificacion').val()== "" ) {
		return alert("Hay datos en blanco Por favor revisar");
	}
	$.ajax({
		data: ObtenerParametros("CONSULTAR"),
		url: '../../controlador/seguridad/persona.C.php',
		type: 'post',
		success: function (respuesta) {
			$('#resultado').html(respuesta);
		}	
	});
}

function soloLetras(e) 
	{
	    key = e.keyCode || e.which;
	    tecla = String.fromCharCode(key).toLowerCase();
	    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	    especiales = [8, 37, 39, 46];

	    tecla_especial = false
	    for(var i in especiales) {
	        if(key == especiales[i]) {
	            tecla_especial = true;
	            break;
	        }
	    }

	    if(letras.indexOf(tecla) == -1 && !tecla_especial)
	        return false;
	}