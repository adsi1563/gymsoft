$(document).on('click', '#cambiar', function() {
    var parametros = {
        "accion" : "ACTUALIZAR_CONTRASENIA",
        "token2" : $('#tkn2').val(),
        "contrasenia" : $('#txtContrasenia3').val()
    }

    $.ajax({
        data: parametros,
        url: 'controlador/recuperarPassword/recuperar.php',
        type: 'post',
        dataType: 'json',
        success: function(response){
            $('#comprueba').html(response['respuesta']);
        }
    });
});

$(document).on('click', '#regresar', function() {
    window.location.href = 'http://localhost/GymSoft';
});

$(document).on('click', '#cerrar', function() {
      $('#txtContrasenia2').val('');
      $('#txtContrasenia3').val('');
      $('#verificacion').html('');
      $('#txtContrasenia3').prop("disabled",true);  
});

$(document).on('keyup', '#txtContrasenia3', function(){
    if($('#txtContrasenia2').val() != $('#txtContrasenia3').val()){
        $('#verificacion').html('Las contraseñas no Coinciden');
        $('#verificacion').attr("class",'btn-danger text-center');
        $('#cambiar').prop("disabled",true);
    }else{
        if ($('#txtContrasenia2').val() == $('#txtContrasenia3').val()) {
            $('#verificacion').html('Las contraseñas Coinciden');
            $('#verificacion').attr("class",'btn-success text-center');
            $('#cambiar').prop("disabled",false);
        }
    }
});

$(document).on('keyup', '#txtContrasenia2', function(){
    var contra = $('#txtContrasenia2').val();
    if (contra.length == 0) {
        $('#verificacion').html(' ');
    }else{
         if(contra.length < 8){
        $('#verificacion').html('La contraseña debe tener minimo 8 Caracteres');
        $('#verificacion').attr("class",'btn-danger text-center');
        $('#txtContrasenia3').prop("disabled",true);
        }else{
            if (contra.length >= 8) {
                $('#verificacion').html(' ');
                $('#verificacion').attr("class",'');
                $('#txtContrasenia3').prop("disabled",false);
            }
        }
    }
});

$(document).ready(function() {
    iniciar();
    function iniciar() {
        var parametros = {
        "accion" : "CONSULTA_USUARIO",
        "token1" : $('#tkn1').val(),
        "token2" : $('#tkn2').val()
        }

        $.ajax({
            data: parametros,
            url: 'controlador/recuperarPassword/recuperar.php',
            type: 'post',
            dataType: 'json',
            success: function(response){
                $('#comprueba').html(response['respuesta']);
            }
        });
    }
});