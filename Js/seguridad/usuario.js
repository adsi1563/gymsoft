function Enviar(accion,id){

    if(id==null){
        id=$('#hidIdUsuario').val();
    }
    var parametros = {
        "id" :id,
        "usuario" :$('#txtusuario').val(),
        "contrasenia" :$('#txtContrasenia').val(),
        "idPersona" :$('#hiddenPersona').val(),
        //"estado" :$('#estado').val(),
        "rol": sessionStorage.rol,
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/seguridad/usuario.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyUsuarios").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 archivos Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					text: '<img src="../../img/pdf.svg" class="iconos">',
					titleAttr: 'PDF'
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					text: '<img src="../../img/excel.svg" class="iconos">',
					titleAttr: 'EXCEL'
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					text: '<img src="../../img/document.svg" class="iconos">',
					titleAttr: 'COPIAR'
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
                $('#hidIdUsuario').val(data['id']);
                $('#txtusuario').val(data['usuario']);
                $('#hiddenPersona').val(data['idPersona']);
                $('#txtPersona').val(data['nombre']);
				//$('#estado').val(data['estado']);
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });   
}

$(document).ready(function() {
	personaConsulta();
	cargue();
	function personaConsulta() { 
	    var parametros = {
	        "accion": "CONSULTA_PERSONA"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/seguridad/usuario.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#txtusuario').val(response['identificacion']);
	            $('#txtPersona').val(response['NombreCompleto']);
	            $('#hiddenPersona').val(response['idPersona']);

	        }
	    });   
	}

	function cargue() {
		$('#btnAdicionar').attr('onclick', 'Enviar("ADICIONAR",null),location.href="'+sessionStorage.paso2+'"');
	}
});
