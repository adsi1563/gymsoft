function iniciar(){
    var parametros = {
        "accion" : "INICIAR_SESION",
        "usuario" : $('#txtUsuario').val(),
        "contrasenia" : $('#txtContrasenia').val()
    }

    if($('#txtUsuario').val() == ""){
        alert("Faltan datos por llenar, por favor verifique.");
        return;
    }

    $.ajax({
        data: parametros,
        url: 'controlador/seguridad/usuario.c.php',
        type: 'post',
        dataType: 'json',
        success: function(response){
            if (response['nueva'] == 'nueva') {
                $('#nuevaContra').modal('toggle');
            }else{
                if (response['incorrecto'] == 1) {
                    alert('La contraseña es incorrecta, Verifique los datos.')
                }else{
                    eval(response);
                }
            }
        }
    });
}

$(window).keypress(function(e) {
    if(e.keyCode == 13) {
        iniciar();
    }
});

$(document).on('click', '#cerrar', function() {
      $('#txtContrasenia2').val('');
      $('#txtContrasenia3').val('');
      $('#verificacion').html('');
      $('#txtContrasenia3').prop("disabled",true);  
});

$(document).on('keyup', '#txtContrasenia3', function(){
    if($('#txtContrasenia2').val() != $('#txtContrasenia3').val()){
        $('#verificacion').html('Las contraseñas no Coinciden');
        $('#verificacion').attr("class",'btn-danger text-center');
        $('#actualizar').prop("disabled",true);
    }else{
        if ($('#txtContrasenia2').val() == $('#txtContrasenia3').val()) {
            $('#verificacion').html('Las contraseñas Coinciden');
            $('#verificacion').attr("class",'btn-success text-center');
            $('#actualizar').prop("disabled",false);
        }
    }
});

$(document).on('keyup', '#txtContrasenia2', function(){
    var contra = $('#txtContrasenia2').val();
    if(contra.length < 8){
        $('#verificacion').html('La contraseña debe tener minimo 8 Caracteres');
        $('#verificacion').attr("class",'btn-danger text-center');
        $('#txtContrasenia3').prop("disabled",true);
    }else{
        if (contra.length >= 8) {
            $('#verificacion').html(' ');
            $('#verificacion').attr("class",'');
            $('#txtContrasenia3').prop("disabled",false);
        }
    }
});

$(document).on('click', '#actualizar', function() {
    
    var parametros = {
        "accion" : "ACTUALIZAR_CONTRASENIA",
        "usuario" : $('#txtUsuario').val(),
        "contrasenia" : $('#txtContrasenia3').val()
    }

    $.ajax({
        data: parametros,
        url: 'controlador/seguridad/usuario.c.php',
        type: 'post',
        dataType: 'json',
        success: function(response){
            $('#txtContrasenia2').val('');
            $('#txtContrasenia3').val('');
            $('#verificacion').html('');
            $('#nuevaContra').modal('toggle');
            alert(response['respuesta']);
        }
    });       
});

$(document).on('click', '#recuperar', function() {
    $('#modalEnviar').modal('toggle'); 
});

$(document).on('keyup', '#emailRecuperar', function() {
    
    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if (regex.test($('#emailRecuperar').val().trim())) {
        $('#verificacionCorreo').html('Correo Correcto');
        $('#verificacionCorreo').attr("class",'btn-success text-center');
        $('#enviarEmail').prop("disabled",false);
    } else {
        $('#verificacionCorreo').html('Formato de correo no valido');
        $('#verificacionCorreo').attr("class",'btn-danger text-center');
        $('#enviarEmail').prop("disabled",true);
    }
});

$(document).on('click', '#cerrar2', function() {
      $('#verificacionCorreo').html('');
      $('#emailRecuperar').val('');
});

$(document).on('click', '#enviarEmail', function() {
    
    var parametros = {
        "accion" : "CONSULTAR_EMAIL",
        "email" : $('#emailRecuperar').val(),
    }

    $.ajax({
        data: parametros,
        url: 'controlador/recuperarPassword/recuperar.php',
        type: 'post',
        dataType: 'json',
        success: function(response){
            $('#verificacionCorreo').html('');
            $('#emailRecuperar').val('');
            $('#modalEnviar').modal('toggle'); 
            alert(response['respuesta']);
        }
    });       
});