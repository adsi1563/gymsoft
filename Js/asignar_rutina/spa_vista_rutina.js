;
((c,d) => {
    let getId = (id)=>{ return d.getElementById(id)}, router;
    getId('frmAsignarRutina').addEventListener('click', (e)=> {
        if(e.target.value === "cardio"){
            router = "cardio";
        }else if(e.target.value === "ganancia_muscular"){
            router = "ganancia-muscular";
        }else if(e.target.value === "tonificar"){
            router = "tonificar";
        }else if(e.target.value === "seleccione"){
            router = "seleccione";
        }
        /**
         * VALIDAR SI EL CHECK HA SIDO OPRIMIDO
         */
        let desactivarVistas = getId('desactivaEjercicios').checked;
        if(!desactivarVistas){
            if(router === "cardio" || router === "ganancia-muscular" || router === "tonificar" || router === "seleccione"){
                fetch(`../../Vista/asignar_rutina/modules/${router}.php`)
                    .then(res => res.text())
                    .then(response => {getId('vista').innerHTML = response;});
            }
        }

        /**
         * end validacin del chheck
         */


    })

})(console.log, document)
;

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *          enviar al controlador la accion de los botones 
 *          conultar y actualizar
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * 
 */
((c,d)=>{
    d.getElementById('btn').addEventListener('click',(e)=>{
        
        let controlador = new FormData();/**Este objeto se usa para meter la info */
        if(e.target.value == "Adicionar"){
            
            /**
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             * EN CASO DE USAR AJAX PARA GUARDAR LA INFORMACION VA AQUI!
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
             */
        }else{
             e.preventDefault();
              if(e.target.textContent === "Consultar"){
                  controlador.append('boton','consultar')
              } else if(e.target.textContent === "Actualizar"){
                  controlador.append('boton','actualizar');
              }
              fetch('modelo/consultar.php',{
                  method: 'post',
                  body: controlador
              }).then(res=> res.json())
                .then(response => {
                    c(response)
                })
              
        }

    })
})(console.log, document)
;

/**
 * CAMBIOS DEL SABADO AGREGAR LA VISTA DE LA CONSULTA
 * DE LOS APRENDICEZ Y SUS RUTINAS
 */

//mostrar la vista de consultar
let i = 0; //contador para validar el alert
((c, d)=>{
    let getQ = (q)=> {return d.querySelector(q)};
    getQ("#generar-vistas").addEventListener('click', (e)=>{
        e.preventDefault();
        if(e.target.id === "vista-consulta"){
            i++;
            if(i === 1){swal("Bienvenido :)","Ahora puedes ver la información de los usuarios","success");}
            fetch('../../vista/asignar_rutina/modules/generar_vista_consulta.php')
                .then(res => res.text())
                .then(res =>{
                    getQ('#vista').innerHTML = res;
                    /**
                     * Función de jquery para la tabla de resultado
                     */
                    $(document).ready(function() {
                        $('#tabla-user-rutina').DataTable();
                    } );
                    /**
                     * end funcion para la tabla
                     */
                }

             );
        }
        /**
        * starter if para generar la vista de la rutina rutina
        * 
        */
    
       if (e.target.id === "generar-vista-ejercicios") {
           /**
           * Modal n1
           */
           swal({
            title: "Bienvenido!",
            text: "Has entrado a tus recomendaciones!",
            icon: "success",
            button: "Gracias!",
          });
        
           /**
            * end modal
            */
           fetch('../../vista/asignar_rutina/modules/generar_vista_ejercicios.php')
                .then(res => res.text())
                .then(res => {
                    /**
                     * starter remover el nodo donde está el formulario
                     * y programar la configuracion de pantalla completa
                     */
                    let desactivaPantalla = getQ('#desactivaPantalla').checked;
                    if(desactivaPantalla){
                        let body = getQ('body');
                        //divBotonPantallaChica = getQ('#pantallaChica');
                        //divBotonPantallaChica.append('button');
                        
                        let main = getQ('#main');
                        let resul = body.removeChild(main);
                    }
                    getQ('#vista').innerHTML = res;
                    $( function() {
                        $( "#accordion" ).accordion();
                            /**
                             * end funcion del horario imc bajo 
                             */
                            $(()=>{ $("#peso-bajo").tabs();})/**imc bajo */
                            $(()=> { $("#peso-normal").tabs();})/*mostrar rutina con el imc normal */
                            $(() => { $("#peso-obeso").tabs();})   /**mostrar el imc sobrepeso obeso*/
                        /**
                         * end funcion del horario imc bajo END
                         */
                    });
                    /**
                     * end vista para 
                     * rutina según el imc
                     */
                });
       }
        /**
        * 
        * end if del generador de la vista de la rutina
        */
    });
   /**
   * acá finaliza el event listenner
   */

})(console.log, document)