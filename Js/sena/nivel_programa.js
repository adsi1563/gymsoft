function Enviar(accion,id){
    if(id==null){
        id=$('#hidIdAccion').val();
    }
    var parametros = {
        "id" :id,
        "descripcion" :$('#txtAccion').val(),
        
        "accion" : accion
    };

     $.ajax({
            data:  parametros, //datos que se van a enviar al ajax
            url:   '../../controlador/sena/nivel_programa.C.php', //archivo php que recibe los datos
            type:  'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
           
            success:  function (data) { //procesa y devuelve la respuesta
                
                if(data['accion']=='ADICIONAR'){
                    alert(data['respuesta']);
                }
                
                if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
                        $("#resultado").html(data['tablaRegistro']);
                }else{
                    $('#hidIdAccion').val(data['id']);
                    $('#txtAccion').val(data['accion_registro']);
                    
                }
                if(data['accion']=='MODIFICAR'){
                    alert(data['respuesta']);
                }
                if(data['accion']=='ELIMINAR'){
                    alert(data['respuesta']);
                }
            }
    });
    
}
