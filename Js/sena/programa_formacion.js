// var urll='../Controlador/controlador.persona.php'
 var parametros=null;

function ObtenerParametros(accion){
	var parametros = {
		"codigo": $('#txtCodigo').val(),
		"descripcion":$('#txtDescripcion').val(),
		"duracion":$('#txtDuracion').val(),
		"hideId":$('#txtHideid').val(),
		"accion": accion
	}
	return parametros;
}


function Guardar() {
	
	$.ajax({
		data: ObtenerParametros("GUARDAR"),
		url: '../../controlador/sena/programa_formacion.C.php',
		type: 'post',
		dataType: 'json',
		success: function (data) {
			
		}	
	});
}

function Consultar() {
	$.ajax({
		data: ObtenerParametros("CONSULTAR"),
		url: '../../controlador/sena/programa_formacion.C.php',
		type: 'post',
		success: function (respuesta) {
			$('#resultado').html(respuesta);
		}	
	});
}

function Editar(id) {
	var parametros = {
		"id": id,
		"accion": 'EDITAR',
		"codigo": $('#txtCodigo').val(),
		"descripcion":$('#txtDescripcion').val(),
		"duracion":$('#txtDuracion').val(),
		"hideId":$('#txtHideid').val(),
	}
	$.ajax({
		data: parametros,
		url: '../../controlador/sena/programa_formacion.C.php',
		type: 'post',
		dataType: 'json',
		success: function (respuesta) {
			$('#txtCodigo').val(respuesta[1]);
			$('#txtDescripcion').val(respuesta[2]);
			$('#txtDuracion').val(respuesta[3]);
			$('#txtHideid').val(respuesta[0]);
		}	
	});
}

function Eliminar(idEliminar) {
	var parametros = {
		"accion": 'ELIMINAR',
		"idEliminar": idEliminar
	}
	$.ajax({
		data: parametros,
		url: '../../controlador/sena/programa_formacion.C.php',
		type: 'post',
		dataType: 'json',
		success: function(respuesta){
            eval(respuesta);
		}	
	});
}



		function soloLetras(e) 
			{
			    key = e.keyCode || e.which;
			    tecla = String.fromCharCode(key).toLowerCase();
			    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
			    especiales = [8, 37, 39, 46];

			    tecla_especial = false
			    for(var i in especiales) {
			        if(key == especiales[i]) {
			            tecla_especial = true;
			            break;
			        }
			    }

			    if(letras.indexOf(tecla) == -1 && !tecla_especial)
			        return false;
			}

		function limpia() 
			{
			    var val = document.getElementById("txtNombremaquina").value;
			    var tam = val.length;
			    for(i = 0; i < tam; i++) {
			        if(!isNaN(val[i]))
			            document.getElementById("txtNombremaquina").value = '';
			    }
			}


		function maxLengthCheck(object)
		    {
		        if (object.value.length > object.maxLength)
		            object. value= object. value. slice (0, object. maxLength)
		    }
