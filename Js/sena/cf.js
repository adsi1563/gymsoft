 var parametros=null;

function ObtenerParametros(accion){
	var parametros = {
		"Nombre": $('#txtNombre').val(),
        "Ciudad": $('#txtCiudad').val(),
        "Aseguradora": $('#txtAseguradora').val(),
        "accion": accion
	}
	return parametros;
}

function guardar(){
	if($('#txtNombre').val()== "" || $('#txtCiudad').val()== "" || $('#txtAseguradora').val()== "" ) {
		return alert("Hay datos en blanco Por favor revisar");
	}
	$.ajax({
		data: ObtenerParametros("GUARDAR"),
		url: '../../controlador/sena/cf.C.php',
		type: 'post',
		dataType: 'json',
		success: function (data) {}	
	});
}

function Consultar(){
	if ($('#txtNombre').val()== "" || $('#txtCiudad').val()== "") {
		return alert("Hay datos en blanco Por favor revisar");
	}
	$.ajax({
		data: ObtenerParametros("CONSULTAR"),
		url: '../../controlador/sena/cf.C.php',
		type: 'post',
		success: function (respuesta) {
			$('#resultado').html(respuesta);
		}	
	});
}

//FUNCIONES DE VALIDACION
function soloLetras(e) 
	{
	    key = e.keyCode || e.which;
	    tecla = String.fromCharCode(key).toLowerCase();
	    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	    especiales = [8, 37, 39, 46];

	    tecla_especial = false
	    for(var i in especiales) {
	        if(key == especiales[i]) {
	            tecla_especial = true;
	            break;
	        }
	    }

	    if(letras.indexOf(tecla) == -1 && !tecla_especial)
	        return false;
	}

function limpia() 
	{
	    var val = document.getElementById("miInput").value;
	    var tam = val.length;
	    for(i = 0; i < tam; i++) {
	        if(!isNaN(val[i]))
	            document.getElementById("miInput").value = '';
	    }
	}
