var parametros = null;
var resultado = null;
var url = "../../controlador/sena/marca.C.php";

function consultar(){
    $.ajax({
        data: obtenerParametros("CONSULTA"),
        url: url,
        type: 'post',
        success: function(response){
            $('#datos').html(response);
        }
    });
}

function guardar(){
    if($('#txtinstructor').val() == "" || $('#ddlficha').val() == ""){
            return alert("Existen campos vacíos, por favor verifique.");
        }

    $.ajax({
        data: obtenerParametros("GUARDAR"),
        url: url,
        type: 'post',
        success: function(response){
            eval(response);
        }
    });
}

function modificar(){
    if($('#txtinstructor').val() == "" || $('#ddlficha').val() == ""){
        return alert("Existen campos vacíos, por favor verifique.");
        }

    $.ajax({
        data: obtenerParametros("MODIFICAR"),
        url: url,
        type: 'post',
        success: function(response){
            eval(response);
            nuevo();
        }
    });
}

function eliminar(idEliminar){
    var parametros = {
        "accion": "ELIMINAR",
        "hiddenIdPersona": idEliminar
    }

    $.ajax({
        data: parametros,
        url: url,
        type: "post",
        success: function(response){
            eval(response);
        }
    })
}

function obtenerParametros(accion){
    parametros = {
        "accion": accion,
        "instructor": $('#txtinstructor').val(),
        "ficha": $('#ddlficha').val()

    }
    return parametros;
}

function soloLetras(e) 
    {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = [8, 37, 39, 46];

        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }