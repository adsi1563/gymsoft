function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
        if( $('#hidIdUsuario').val()== "" || $('#hidIdAcudiente').val() == ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdAprendiz').val();
    }
    var parametros = {
        "id" :id,
        "usuario" :$('#hidIdUsuario').val(),
        "imc" :$('#hidIdImc').val(),
        "acudiente" :$('#hidIdAcudiente').val(),
        //"estado" :$('#estado').val(),
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/sena/aprendiz.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyAprendiz").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 archivos Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					text: '<img src="../../img/pdf.svg" class="iconos">',
					titleAttr: 'PDF'
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					text: '<img src="../../img/excel.svg" class="iconos">',
					titleAttr: 'EXCEL'
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					text: '<img src="../../img/document.svg" class="iconos">',
					titleAttr: 'COPIAR'
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
                $('#hidIdAprendiz').val(data['id']);
                $('#hidIdUsuario').val(data['idusuario']);
				$('#imc').val(data['idimc']);
                $('#hidIdAcudiente').val(data['idacudiente']);
				//$('#estado').val(data['estado']);
				$('#txtUsuario').val(data['nombrePersona']);
				$('#txtAcudiente').val(data['nombreAcudiente']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });  
}

$(document).ready(function() {
	imc();
	usuario();
	acudiente();
	cargue();
	
	function imc() { 
    var parametros = {
        "accion": "CONSULTA_IMC"
    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/sena/aprendiz.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#imc').val(response['calculoImc']);
	            $('#hidIdImc').val(response['idImc']);
	        }
	    });   
	}

	function usuario() { 
    var parametros = {
        "accion": "CONSULTA_USUARIO"
    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/sena/aprendiz.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#txtUsuario').val(response['usuario']);
	            $('#hidIdUsuario').val(response['idUsuario']);
	        }
	    });   
	}

	function acudiente() { 
    var parametros = {
        "accion": "CONSULTA_ACUDIENTE"
    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/sena/aprendiz.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#txtAcudiente').val(response['Nombre']);
	            $('#hidIdAcudiente').val(response['idAcudiente']);
	        }
	    });   
	}

	function cargue() {
		$('#btnAdicionar').attr('onclick', 'Enviar("ADICIONAR",null),location.href="'+sessionStorage.paso5+'"');
	}

});