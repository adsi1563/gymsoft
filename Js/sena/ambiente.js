

function Enviar(accion,id){

	if(id==null){
        id=$('#hidAmbiente').val();
    }

	var parametros = {
        "id" :id,
        "codigo" :$('#txtCodigo').val(),
        "descripcion" :$('#txtDescripcion').val(),
        "estado" :$('#selEstado').val(),
        "accion" : accion
    }

    

	if (accion == 'ADICIONAR') {
        if (validar()) {
        	return alert("faltan datos");
        }
    }

 

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/sena/ambiente.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyAmbiente").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					//titleAttr: 'PDF'
					text: '<img src="../../img/pdf.svg" class="iconos">',
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					//titleAttr: 'EXCEL'
					text: '<img src="../../img/excel.svg" class="iconos">',
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					//titleAttr: 'COPIAR'
					text: '<img src="../../img/document.svg" class="iconos">',
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					//titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidAmbiente').val(data['id']);
				$('#txtCodigo').val(data['codigo']);
				$('#txtDescripcion').val(data['descripcion']);
				$('#selEstado').val(data['estado']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });
    
}



function validar() {
	if($("#txtCodigo").val() == "" || 
        $("#txtDescripcion").val() == "" || 
        $("#selEstado").val() == -1)
    {
        $("#mensaje").html('<div class="alert alert-warning alert-dismissible fade show" role="alert"><span class="alert-inner--text">Existen campos vacíos, por favor verifique.</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button></div>');
        return true;
    } else {
    	return false;
    }
    
}
