

function fechaF(argument){

	var fechaFi= $('#fechaFin').val();
	var fechaIni= $('#fechaInicio').val();
	if( fechaFi < fechaIni){
		alert("No se puede seleccionar una fecha inferior");
		$('#fechaFin').val("");
	}
}


function Consultar(accion){

	if (!(typeof accion === 'undefined')) {

		if (accion === 'reporteAsisGym') {
			
			
			$('#nombreTabla').html('Asistencia');
		} else if (accion === 'reporteDesGym') {

			$('#nombreTabla').html('Deserciones');
		}

		if( $('#fechaInicio').val()== "" || $('#fechaFin').val() == ""){
            return alert("Hay datos en blanco Por favor revisar"); }
	}



    var parametros = {
 
        "idsede" :$('#idsede').val(),
        "idGimnasio" :$('#idsede').val(),
        "fechaInicio" :$('#fechaInicio').val(),
        "fechaFin" :$('#fechaFin').val(),
        
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/reportes/reporte.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		async: false,

		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='Consulta'){
				console.log(data);
			}
			
			if(data['accion']== accion){
					$("#tableBodyActividad").html(data['tablaRegistro']);
					
				$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					//titleAttr: 'PDF'
					text: '<img src="../../img/pdf.svg" class="iconos">',
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					//titleAttr: 'EXCEL'
					text: '<img src="../../img/excel.svg" class="iconos">',
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					//titleAttr: 'COPIAR'
					text: '<img src="../../img/document.svg" class="iconos">',
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					//titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}
			
		} 
    }); 
}

$(document).ready(function() {
	gimnasio();
	
	Consultar();
	
	cambio();

});

function cambio() {
	$('#idGimnasio').on('change', function(){
		
		var id = $('#idGimnasio').val();
		
		var parametros = {
			"accion": "ConsultaSede",
			'idCentro': id
		}
	    
	    $.ajax({
	      type: 'POST',
	      url: '../../controlador/reportes/reporte.c.php',
	      data: parametros,
	      dataType: "json",
	      success: function(response){
	            $('#idsede').html(response);
	        }
	    }) //fin de ajax
	    
	  });
}

function gimnasio() { 
	    var parametros = {
	        "accion": "Consulta_Centro_form"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/reportes/reporte.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#idGimnasio').html(response);
	        }
	    });   
	}