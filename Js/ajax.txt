var parametros = null;
var resultado = null;
var url = "../controlador/seguridad/persona.C.php";

function consultar(){
    $.ajax({
        data: obtenerParametros("CONSULTA"),
        url: url,
        type: 'post',
        success: function(response){
            $('#datos').html(response);
        }
    });
}

function consultarPorId(idPersona){
    var parametros = {
        "accion": "CONSULTA_ID",
        "idPersona": idPersona
    }
    $.ajax({
        data: parametros,
        url: url,
        type: 'post',
        success: function(response){
            var resultado = response;
            var datos = JSON.parse(response);
            $('#txtDocumento').val(datos[1]);
            $('#txtPrimerNombre').val(datos[2]);
            $('#txtSegundoNombre').val(datos[3]);
            $('#txtPrimerApellido').val(datos[4]);
            $('#txtSegundoApellido').val(datos[5]);
            $('#txtTelefono').val(datos[6]);
            $('#txtEmail').val(datos[7]);
            $('#ddlEstado').val(datos[9]);
            $('#hiddenIdPersona').val(datos[0]);
        }
    })
}

function guardar(){
    if($('#txtPrimerNombre').val() == "" ||$('#txtSegundoNombre').val() == "" ||$('#txtPrimerApellido').val() == "" ||
    $('#txtSegundoApellido').val() == "" ||$('#txtDocumento').val() == "" ||$('#txtTelefono').val() == "" ||
    $('#txtEmail').val() == "" ||$('ddlEstado').val() == "-1"){
            alert("Existen campos vacíos, por favor verifique.");
        }

    if($('#hiddenIdPersona').val() != ""){
        modificar();
        return;
    }

    $.ajax({
        data: obtenerParametros("GUARDAR"),
        url: url,
        type: 'post',
        success: function(response){
            eval(response);
        }
    });
}

function modificar(){
    if($('#txtPrimerNombre').val() == "" ||$('#txtSegundoNombre').val() == "" ||$('#txtPrimerApellido').val() == "" ||
    $('#txtSegundoApellido').val() == "" ||$('#txtDocumento').val() == "" ||$('#txtTelefono').val() == "" ||$('#txtEmail').val() == "" ||$('ddlEstado').val() == "-1"){
            alert("Existen campos vacíos, por favor verifique.");
        }

    $.ajax({
        data: obtenerParametros("MODIFICAR"),
        url: url,
        type: 'post',
        success: function(response){
            eval(response);
            nuevo();
        }
    });
}

function eliminar(idEliminar){
    var parametros = {
        "accion": "ELIMINAR",
        "hiddenIdPersona": idEliminar
    }

    $.ajax({
        data: parametros,
        url: url,
        type: "post",
        success: function(response){
            eval(response);
        }
    })
}

function obtenerParametros(accion){
    parametros = {
        "accion": accion,
        "primerNombre": $('#txtPrimerNombre').val(),
        "segundoNombre": $('#txtSegundoNombre').val(),
        "primerApellido": $('#txtPrimerApellido').val(),
        "segundoApellido": $('#txtSegundoApellido').val(),
        "documento": $('#txtDocumento').val(),
        "telefono": $('#txtTelefono').val(),
        "email": $('#txtEmail').val(),
        "estado": $('#ddlEstado').val(),
        "hiddenIdPersona": $('#hiddenIdPersona').val()
    }
    return parametros;
}