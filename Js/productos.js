$(document).ready(function() {
	read();
	$(document).on('click', '#btnAgregar', function() {
		var parametros = {
			"txtSerial" : $('#txtSerial').val(),
			"txtNombreProducto" : $('#txtNombreProducto').val(),
            "numValor" : $('#numValor').val(),
		}

		$.ajax({
			url: '../controlador/productos.registro.php',
			type: 'POST',
			dataType: 'json',
			data: parametros,
		})
		.done(function() {
			alert("Producto creado Exitosamente");
			$('#txtSerial').val(""),
			$('#txtNombreProducto').val(""),
            $('#numValor').val("")
		})
		.fail(function() {
			alert("No se pudo crear el Producto");
		})
	});

	/////////////////////// READ //////////////////////////////

	function read() {
		$.ajax({
			url: '../controlador/producto.read.php',
			type: 'POST',
			dataType: 'json',
			data: null,
		})
		.done(function(json) {
			console.log("success");
			// console.log(JSON[0].nombrePelicula)
			var tabla=null;

			$.each(json, function(index, val) {
				tabla += '<tr>';
				tabla += '<th scope="row">'+(index+1)+'</th>';
				tabla += '<td>'+val.serial+'</td>';
				tabla += '<td>'+val.nombreProducto+'</td>';
				tabla += '<td>'+val.valor+'</td>';
				tabla += '<td>'+val.fechaCreacion+'</td>';
				tabla += '<td>'+val.fechaModificacion+'</td>';
				tabla += '</tr>';
			});
			$('#tableBodyProductos').html(tabla);
			$('#tableProductos').dataTable({
				"language":{"url": "../lenguajes/Spanish.json",
				"buttons":{
					copyTitle: 'Peliculas Copiadas',
					copySuccess: {
						_: '%d Peliculas Copiadas',
						1: '1 Pelicula Copiada',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					text: '<img src="../ico/002-pdf.svg" width="20px">',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Reporte de peliculas en inventario',
					title: 'Inventario de Peliculas',
					exportOptions:{columns:[0,1,2,3,4,5]},
					titleAttr: 'PDF'
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					text: '<img src="../ico/001-excel.svg" width="20px">',
					autoFilter: true,
					sheetName: 'Exported data',
					titleAttr: 'EXCEL'
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					text: '<img src="../ico/004-document.svg" width="20px">',
					exportOptions: {
						columns:':visible'
					},
					titleAttr: 'COPIAR'
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../ico/003-funnel.svg" width="20px">',
					titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../ico/005-print.svg" width="20px">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});

		})
		.fail(function() {
			console.log("error");
		})
	}
});