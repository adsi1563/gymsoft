function fechaInicioLectiva(argument){
	var hoy= moment().format("YYYY-MM-DD");
	var fechaI= $('#fechaInicio').val();

	if(fechaI < hoy){
		alert("La fecha ingresada no puede ser menor a la actual: "+hoy);
		$('#fechaInicio').val("");
	}

}

function fechaLectiva(argument){

	var fechaF= $('#fechaFinLectiva').val();
	var fechaI= $('#fechaInicio').val();
	if( fechaF < fechaI){
		alert("No se puede seleccionar una fecha inferior a inicio de etapa lectiva");
		$('#fechaFinLectiva').val("");
	}
}

function fechaProductiva(argument){

	var fechaP= $('#fechaFinProductiva').val();
	var fechaLI= $('#fechaInicio').val();
	var fechaLF= $('#fechaFinLectiva').val();
	if( fechaP <= fechaLI){
		alert("No se puede seleccionar una fecha inferior a etapa lectiva");
		$('#fechaFinProductiva').val("");
	}
	if( fechaP <= fechaLF){
		alert("No se puede seleccionar una fecha inferior a etapa lectiva");
		$('#fechaFinProductiva').val("");
	}
}



function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
        if( $('#ficha').val() == "" || $('#fechaInicio').val() == ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdFicha').val();
    }
    var parametros = {
        "id":id,
        "ficha" :$('#ficha').val(),
        "fechaInicio" :$('#fechaInicio').val(),
        "fechaFinLectiva" :$('#fechaFinLectiva').val(),
        "fechaFinProductiva" :$('#fechaFinProductiva').val(),
        "Jornada" :$('#Jornada').val(),
        "Ambiente" :$('#Ambiente').val(),
        "ProgramaFormacion" :$('#ProgramaFormacion').val(),
        "estado" :$('#estado').val(),
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/parametrizacion/ficha.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyFicha").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
			

				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					//titleAttr: 'PDF'
					text: '<img src="../../img/pdf.svg" class="iconos">',
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					//titleAttr: 'EXCEL'
					text: '<img src="../../img/excel.svg" class="iconos">',
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					//titleAttr: 'COPIAR'
					text: '<img src="../../img/document.svg" class="iconos">',
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					//titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidIdFicha').val(data['id']);
				$('#ficha').val(data['ficha']);
				$('#fechaInicio').val(data['fechaInicio']);
				$('#fechaFinLectiva').val(data['fechaFinLectiva']);
				$('#fechaFinProductiva').val(data['fechaFinProductiva']);
				$('#Jornada').val(data['Jornada']);
				$('#Ambiente').val(data['Ambiente']);
				$('#ProgramaFormacion').val(data['ProgramaFormacion']);
				$('#estado').val(data['estado']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });
    
}

$(document).ready(function() {
	jornada();
	ambiente();
	programaFormacion();
	function jornada() { 
	    var parametros = {
	        "accion": "CONSULTA_JORNADA"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/parametrizacion/ficha.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#Jornada').html(response);
	        }
	    });   
	}

	function ambiente() { 
	    var parametros = {
	        "accion": "CONSULTA_AMBIENTE"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/parametrizacion/ficha.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#Ambiente').html(response);
	        }
	    });   
	}

	function programaFormacion() { 
	    var parametros = {
	        "accion": "CONSULTA_PROGRAMA"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/parametrizacion/ficha.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#ProgramaFormacion').html(response);
	        }
	    });   
	}
});
