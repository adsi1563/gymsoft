function fInicio(argument){
	var hoy= moment().format("YYYY-MM-DD");
	var fechaI= $('#fechaInicio').val();

	if(fechaI < hoy){
		alert("La fecha ingresada no puede ser menor a la actual: "+hoy);
		$('#fechaInicio').val("");
	}

}

function fFin(argument){

	var fechaF= $('#fechaFin').val();
	var fechaI= $('#fechaInicio').val();
	if( fechaF < fechaI){
		alert("No se puede seleccionar una fecha inferior");
		$('#fechaFin').val("");
	}
}



function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
        if( $('#fechaInicio').val()== "" || $('#fechaFin').val() == ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdActividad').val();
    }
    var parametros = {
        "idActividad" :id,
        "idGimnasio" :$('#idGimnasio').val(),
        "idTipo" :$('#idTipo').val(),
        "fechaInicio" :$('#fechaInicio').val(),
        "fechaFin" :$('#fechaFin').val(),
        "estado" :$('#estado').val(),
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/parametrizacion/actividad.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyActividad").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					//titleAttr: 'PDF'
					text: '<img src="../../img/pdf.svg" class="iconos">',
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					//titleAttr: 'EXCEL'
					text: '<img src="../../img/excel.svg" class="iconos">',
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					//titleAttr: 'COPIAR'
					text: '<img src="../../img/document.svg" class="iconos">',
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					//titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidIdActividad').val(data['idActividad']);
				$('#idGimnasio').val(data['idGimnasio']);
				$('#idTipo').val(data['idTipo']);
				$('#fechaInicio').val(data['fechaInicio']);
				$('#fechaFin').val(data['fechaFin']);
				$('#estado').val(data['estado']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });    
}

$(document).ready(function() {
	gimnasio();
	tipo();
	function gimnasio() { 
	    var parametros = {
	        "accion": "CONSULTA_GIMNASIO"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/parametrizacion/actividad.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#idGimnasio').html(response);
	        }
	    });   
	}

	function tipo() { 
	    var parametros = {
	        "accion": "CONSULTA_TIPO"
	    } 
	    $.ajax({
	        data: parametros,
	        url: '../../controlador/parametrizacion/actividad.c.php',
	        type: "post",
	        dataType: "json",
	        success: function(response){
	            $('#idTipo').html(response);
	        }
	    });   
	}
});
