
var parametros = null;
var resultado = null;
var url = '../../controlador/parametrizacion/entrenador.c.php';


function consultar(){
    $.ajax({
        data: obtenerParametros("CONSULTA"),
        url: url,
        type: 'post',
        success: function(response){
            $('#datos').html(response);
        }
    });
}

function consultarPorId(id_entrenador){
    var parametros = {
        "accion": "CONSULTA_ID",
        "id_entrenador": id_entrenador
    }
    $.ajax({
        data: parametros,
        url: url,
        type: 'post',
        success: function(response){
            var resultado = response;
            var datos = JSON.parse(response);
            $('#txtNombre').val(datos[2]);
            $('#ddlEstado').val(datos[4]);
            $('#hiddenIdEmpresaSector').val(datos[0]);
        }
    })
}

function guardar(){
    if($('#txtSector').val() == "" || $('ddlEstado').val() == "-1"){
            alert("Existen campos vacíos, por favor verifique.");
        }

    if($('#hiddenIdEmpresaSector').val() != ""){
        modificar();
        return;
    }

    $.ajax({
        data: obtenerParametros("GUARDAR"),
        url: url,
        type: 'post',
        success: function(response){
            eval(response);
        }
    });
}

function modificar(){
    if($('#txtSector').val() == "" || $('ddlEstado').val() == "-1"){
            alert("Existen campos vacíos, por favor verifique.");
        }
        alert("Datos modificados")

    $.ajax({
        data: obtenerParametros("MODIFICAR"),
        url: url,
        type: 'post',
        success: function(response){
            eval(response);
            nuevo();
        }
    });
}

function eliminar(idEliminar){
    var parametros = {
        "accion": "ELIMINAR",
        "idEmpresaSector": idEliminar
    }

    $.ajax({
        data: parametros,
        url: url,
        type: "post",
        success: function(response){
            eval(response);
        }
    })
}

function obtenerParametros(accion){
    parametros = {
        "accion": accion,
        "sector": $('#txtSector').val(),
        "estado": $('#ddlEstado').val(),
        "idEmpresaSector": $('#hiddenIdEmpresaSector').val()
    }
    return parametros;
}

function nuevo(){
    $('#txtSector').val("");
    $('#ddlEstado').val("");
    $('#hiddenIdEmpresaSector').val("");
}