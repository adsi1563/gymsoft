function Enviar(accion,id){

    if (accion == 'ADICIONAR') {
        if( $('#txtNit').val()== "" || $('#txtRazon').val() == "" || $('#txtDireccion').val() == "" || $('#txtRepresentante').val() == ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
       id=$('#hidIdAseguradora').val();
    }
    var parametros = {
        "id" :id,
        "nit" :$('#txtNit').val(),
		"razon" :$('#txtRazon').val(),
		"direccion" :$('#txtDireccion').val(),
        "representante" :$('#txtRepresentante').val(),
        "estado" :$('#est').val(),
        "accion" : accion
    };

     $.ajax({
        data:  parametros, //datos que se van a enviar al ajax
        url:   '../../controlador/parametrizacion/aseguradora.c.php', //archivo php que recibe los datos
        type:  'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        
        success:  function (data) { //procesa y devuelve la respuesta
            
            if(data['accion']=='ADICIONAR'){
                alert(data['respuesta']);
            }
            
            if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
                    $("#tableBodyDepartamento").html(data['tablaRegistro']);
                    $("#resultado").DataTable({
                "language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
                "buttons":{
                    copyTitle: 'Registros Copiados',
                    copySuccess: {
                        _: '%d Registros Copiados',
                        1: '1 Peliculas Copiado',
                    },
                    colvis: 'Visualizar',
                },
            },
                
                dom: 'Bfrtip',
                buttons:[
                ////////////////PDF////////////////
                {
                    extend: 'pdfHtml5',
                    orentation: 'landscape',
                    pageSize: 'LEGAL',
                    download: 'open',
                    messageTop: 'Representante Legal de la Aseguradora',
                    title: 'Reporte de Aseguradora',
                    exportOptions:{columns:[0,1,2,3]},
                    text: '<img src="../../img/pdf.svg" class="iconos">',
                    titleAttr: 'PDF'
                },
                ///////////EXCEL/////////////////
                {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data',
                    text: '<img src="../../img/excel.svg" class="iconos">',
                    titleAttr: 'EXCEL'
                },
                //////////COPIAR//////////////////////
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns:':visible'
                    },
                    text: '<img src="../../img/document.svg" class="iconos">',
                    titleAttr: 'COPIAR'
                },
                //////////////FILTRO///////////////////
                {
                    extend: 'colvis',
                    text: '<img src="../../img/funnel.svg" class="iconos">',
                    titleAttr: 'FILTRO'
                },
                {
                    extend: 'print',
                    text: '<img src="../../img/printer.svg" class="iconos">',
                    titleAttr: 'IMPRIMIR'
                }

                ],
            });
            }else{
                $('#hidIdAseguradora').val(data['id']);
                $('#txtNit').val(data['nit']);
				$('#txtRazon').val(data['razon']);
				$('#txtDireccion').val(data['direccion']);
                $('#txtRepresentante').val(data['representante']);
                $('#est').val(data['estado']);
                $('#btnAdicionar').attr("disabled" , "disabled");
            }
            if(data['accion']=='MODIFICAR'){
                alert(data['respuesta']);
                $('#btnAdicionar').removeAttr("disabled");
            }
            if(data['accion']=='ELIMINAR'){
                alert(data['respuesta']);
                $('#btnAdicionar').removeAttr("disabled");
            }
        }
    });
    
}
