guardar_localstorage();
obtener_localstorage();

function obtener_localstorage() {
	if (localStorage.getItem("datos")) {
		
		let nombre = JSON.parse(localStorage.getItem("datos"));
		var institucion = localStorage.getItem("institucion");
		console.log(nombre);
		console.log(institucion);
	}
}

function guardar_localstorage() {
	
	let persona = {
		nombre: "Fabian",
		edad: 30,
		correo: "fccano@misena.edu.co",
		fechaNacimiento: {
			dia: 17,
			mes: 07
		}
	}

	localStorage.setItem("datos",JSON.stringify(persona));
	localStorage["institucion"] = "Sena";
}