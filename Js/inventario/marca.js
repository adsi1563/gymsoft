function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
        if( $('#txtDescripcion').val()== "" || $('#dep').val() == ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdMarca').val();
    }
    var parametros = {
		"id" :id,
		"nombre" :$('#txtNombre').val(),
		"descripcion" :$('#txtDescripcion').val(),
		"marca" :$('#txtMarca').val(),
		"tipoElemento" :$('#txtTipoElemento').val(),
        "estado" :$('#dep').val(),
        "accion" : accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/inventario/marca.c.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableBodyDepartamento").html(data['tablaRegistro']);
					$("#resultado").DataTable({
				"language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiados',
						1: '1 Peliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Departamentos con sus Respectivos Codigos',
					title: 'Reporte de Departamentos Registrados',
					exportOptions:{columns:[0,1]},
					text: '<img src="../../img/pdf.svg" class="iconos">',
					titleAttr: 'PDF'
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					text: '<img src="../../img/excel.svg" class="iconos">',
					titleAttr: 'EXCEL'
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					text: '<img src="../../img/document.svg" class="iconos">',
					titleAttr: 'COPIAR'
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidIdMarca').val(data['id']);
				$('#txtNombre').val(data['nombre']);
				$('#txtDescripcion').val(data['descripcion']);
				$('#txtMarca').val(data['marca']);
				$('#txtTipoElemento').val(data['tipoElemento']);
				$('#dep').val(data['estado']);
				// $('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
	});
	$(document).ready(function() {
		nombre();	
		function nombre() { 
			var parametros = {
				"accion": "consultarNombre"
			} 
			$.ajax({
				data: parametros,
				url: '../../controlador/inventario/marca.c.php',
				type: "post",
				dataType: "json",
				success: function(response){
					$('#txtNombre').html(response);
				}
			});   
		}
	
	});
    
}
