
function fechaIngreso(argument){
	var hoy= moment().format("YYYY-MM-DD");
	var fechaIn= $('#dateIngreso').val();

	if(fechaIn < hoy){
		alert("La fecha ingresada no puede ser menor a la actual: "+hoy);
		$('#dateIngreso').val("");
	}

}


function Enviar(accion,id){

	$(document).on('change', '#manual', function() {
		var names = document.getElementById('manual').files[0].name;	
		$('#archivo').val(names);
	});

	if (accion == 'ADICIONAR') {
        if( $('#txtNombremaquina').val() == ""){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#txtHideidequipo').val();
    }
    var parametros = {
    	"id":id,
        "nombremaquina":$('#txtNombremaquina').val(),
		"manual":$('#archivo').val(),
		"estado":$('#estado').val(),
		"fechaingreso":$('#dateIngreso').val(),
		"accion": accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/inventario/controlador.equipo.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableEquipo").html(data['tablaRegistro']);
					$("#resultado").dataTable({
				"language":{"url": "../lenguajes/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiadas',
						1: '1 PPeliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Equipos de GYMSOFT  ',
					title: 'Reporte de equipos Registrados',
					exportOptions:{columns:[0,1]},
					text: '<img src="../../img/pdf.svg" class="iconos">',
					titleAttr: 'PDF'
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					text: '<img src="../../img/excel.svg" class="iconos">',
					titleAttr: 'EXCEL'
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					text: '<img src="../../img/document.svg" class="iconos">',
					titleAttr: 'COPIAR'
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#txtHideidequipo').val(data['id']);
				$('#txtNombremaquina').val(data['nombremaquina']);
				$('#estado').val(data['estado']);
				$('#manual').val(data['manual']);
				$('#dateIngreso').val(data['fechaingreso']);
				// $('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });
    
}

$(document).ready(function() {
	$(document).on('click','#btnAdicionar',function(e){
	e.preventDefault();	

	var formData = new FormData(document.getElementById("accion"));
	formData.append("dato","valor");

		$.ajax({
			url: '../../controlador/inventario/equipo.uploap.c.php',
			type: 'POST',
			dataType: 'JSON',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
		})

		.done(function(data) {
			console.log("success");
			alert(data);
		})
		.fail(function() {
			console.log("error");
		})
		
	})
})
           
//////fecha automatica al sistema
            $( document ).ready(function() {

                var now = new Date();

                var day = ("0" + now.getDate()).slice(-2);
                var month = ("0" + (now.getMonth() + 1)).slice(-2);

                var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
                $("#dateIngreso").val(today);
            });