// var urll='../Controlador/controlador_actividad.php'
 var parametros=null;

function ObtenerParametros(accion){
	var parametros = {
		"estado": $('#txtestado').val(),
		"fecha_inicio":$('#datefechainicio').val(),
		"fecha_final":$('#datefechafinal').val(),
		"accion": accion
	}
	return parametros;
}


function Guardar() {
	$.ajax({
		data: ObtenerParametros("GUARDAR"),
		url: '../../controlador/registros/sede.C.php',
		type: 'post',
		dataType: 'json',
		success: function (data) {}	
	});
}

function Consultar() {
	$.ajax({
		data: ObtenerParametros("CONSULTAR"),
		url: '../../controlador/registros/actividad.C.php',
		type: 'post',
		success: function (respuesta) {
			$('#resultado').html(respuesta);
		}	
	});
}

function soloLetras(e) 
	{
	    key = e.keyCode || e.which;
	    tecla = String.fromCharCode(key).toLowerCase();
	    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	    especiales = [8, 37, 39, 46];

	    tecla_especial = false
	    for(var i in especiales) {
	        if(key == especiales[i]) {
	            tecla_especial = true;
	            break;
	        }
	    }

	    if(letras.indexOf(tecla) == -1 && !tecla_especial)
	        return false;
	}

function limpia() 
	{
	    var val = document.getElementById("miInput").value;
	    var tam = val.length;
	    for(i = 0; i < tam; i++) {
	        if(!isNaN(val[i]))
	            document.getElementById("miInput").value = '';
	    }
	}

function maxLengthCheck(object)
    {
        if (object.value.length > object.maxLength)
            object. value= object. value. slice (0, object. maxLength)
    }