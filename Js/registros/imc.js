//funcion para calcular el indice de masa corporal
function suma(){
    //variabled para hacer el calculo traidas de la vista por metodo onblur y onKeyUp
        var sum1 = document.getElementById("txtEstatura");
        var sum2 = document.getElementById("txtPeso");
     //calculo de el resultado enviado al al input establecido para el resultado       
        var div = document.getElementById("txtCalculoImc");
     //operacion para calcular el imc        
        resultado = parseInt(sum2.value) / (parseInt(sum1.value) * parseInt(sum1.value))*10000;
        div.value= resultado.toFixed(2);

        RutinaAlimenticia(resultado);

        if(resultado<16){
          document.getElementById("txtCalculoImc2").value="Delgadez Severa";    
        }
        else if(resultado<17){
          document.getElementById("txtCalculoImc2").value="Delgadez Moderada";    
        }
        else if(resultado<18.5){
          document.getElementById("txtCalculoImc2").value="Delgadez Aceptable";   
        }
        else if(resultado<25){
          document.getElementById("txtCalculoImc2").value="Peso Normal";    
        }
        else if(resultado<30){
          document.getElementById("txtCalculoImc2").value="Sobrepeso";    
        }
        else if(resultado<35){
          document.getElementById("txtCalculoImc2").value="Obeso: Tipo I";    
        }
        else if(resultado<40){
          document.getElementById("txtCalculoImc2").value="Obeso: Tipo II";   
        }
        else if(resultado>=40){
          document.getElementById("txtCalculoImc2").value="Obeso: Tipo III";    
        }  
}
//modal iniciar al cargar pagina
$( document ).ready(function() {
    $('#imcmodal').modal('toggle')
});
     

function Enviar(accion,id){

    if (accion == 'ADICIONAR') {
        if( $('#txtEstatura').val()== "" || $('#txtPeso').val() == "" || $('#txtCalculoImc').val() == "" || $('#txtCalculoImc2').val() == "" ){
            return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
       id=$('#hidIdImc').val();
    }
    var parametros = {
        "id" :id,
        "estatura" :$('#txtEstatura').val(),
        "peso" :$('#txtPeso').val(),
        "calculoImc" :$('#txtCalculoImc').val(),
        "descripcionimc" :$('#txtCalculoImc2').val(),
        //"estado" :$('#est').val(),
        "accion" : accion
    };

     $.ajax({
        data:  parametros, //datos que se van a enviar al ajax
        url:   '../../controlador/registros/imc.c.php', //archivo php que recibe los datos
        type:  'post', //método para enviar los datos
        dataType: 'json',//Recibe el array desde php
        
        success:  function (data) { //procesa y devuelve la respuesta
            
            if(data['accion']=='ADICIONAR'){
                alert(data['respuesta']);
            }
            
            if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
                    $("#tableBodyImc").html(data['tablaRegistro']);
                    $("#resultado").DataTable({
                "language":{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
                "buttons":{
                    copyTitle: 'Registros Copiados',
                    copySuccess: {
                        _: '%d Registros Copiados',
                        1: '1 Registro Copiado',
                    },
                    colvis: 'Visualizar',
                },
            },
                
                dom: 'Bfrtip',
                buttons:[
                ////////////////PDF////////////////
                {
                    extend: 'pdfHtml5',
                    orentation: 'landscape',
                    pageSize: 'LEGAL',
                    download: 'open',
                    messageTop: 'imc Legal de la gimasio',
                    title: 'Reporte de imc',
                    exportOptions:{columns:[0,1,2,3]},
                    text: '<img src="../../img/pdf.svg" class="iconos">',
                    titleAttr: 'PDF'
                },
                ///////////EXCEL/////////////////
                {
                    extend: 'excelHtml5',
                    autoFilter: true,
                    sheetName: 'Exported data',
                    text: '<img src="../../img/excel.svg" class="iconos">',
                    titleAttr: 'EXCEL'
                },
                //////////COPIAR//////////////////////
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns:':visible'
                    },
                    text: '<img src="../../img/document.svg" class="iconos">',
                    titleAttr: 'COPIAR'
                },
                //////////////FILTRO///////////////////
                {
                    extend: 'colvis',
                    text: '<img src="../../img/funnel.svg" class="iconos">',
                    titleAttr: 'FILTRO'
                },
                {
                    extend: 'print',
                    text: '<img src="../../img/printer.svg" class="iconos">',
                    titleAttr: 'IMPRIMIR'
                }

                ],
            });
            }else{
                $('#hidIdImc').val(data['id']);
                $('#txtEstatura').val(data['estatura']);
                $('#txtPeso').val(data['peso']);
                $('#txtCalculoImc').val(data['calculoImc']);
                $('#txtCalculoImc2').val(data['descripcionimc']);
                //$('#est').val(data['estado']);
                // $('#btnAdicionar').attr("disabled" , "disabled");
            }
            if(data['accion']=='MODIFICAR'){
                alert(data['respuesta']);
                $('#btnAdicionar').removeAttr("disabled");
            }
            if(data['accion']=='ELIMINAR'){
                alert(data['respuesta']);
                $('#btnAdicionar').removeAttr("disabled");
            }
        }
    });
    
}

$(document).ready(function() {
    RutinaAlimenticia();
    
});

function RutinaAlimenticia(vare) {
                var parametros = {
                "calculoRutina": vare,
                "accion": "CONSULTA_RUTINA"
            } 
            $.ajax({
                data: parametros,
                url: '../../controlador/registros/imc.C.php',
                type: "post",
                dataType: "json",
                success: function(response){
                    $("#txtRutAli").val(response["rutina"]);
                }
            });
            
        
/*
          */ 
    }

$(document).ready(function() {
    $('#btnAdicionar').attr('onclick', 'Enviar("ADICIONAR",null),location.href="'+sessionStorage.paso4+'"');
});

