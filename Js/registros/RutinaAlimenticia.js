function Enviar(accion,id){

	if (accion == 'ADICIONAR') {
     if ($('#txtRutinaAlimenticia').val()== "") {
		return alert("Hay datos en blanco Por favor revisar");
        }
    }

    if(id==null){
        id=$('#hidIdRutina').val();
    }
    var parametros = {
    	"id":id,
        "RutinaAlimenticia": $('#txtRutinaAlimenticia').val(),
        "Select" :$('#selectImc').val(),
		"accion": accion
    };

     $.ajax({
		data:  parametros, //datos que se van a enviar al ajax
		url:   '../../controlador/registros/RutinaAlimenticia.C.php', //archivo php que recibe los datos
		type:  'post', //método para enviar los datos
		dataType: 'json',//Recibe el array desde php
		
		success:  function (data) { //procesa y devuelve la respuesta
			if(data['accion']=='ADICIONAR'){
				alert(data['respuesta']);
			}
			
			if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
					$("#tableRutinaAlimenticia").html(data['tablaRegistro']);
					$('#resultado').dataTable({
				"language":{"url": "../lenguajes/Spanish.json",
				"buttons":{
					copyTitle: 'Registros Copiados',
					copySuccess: {
						_: '%d Registros Copiadas',
						1: '1 PPeliculas Copiado',
					},
					colvis: 'Visualizar',
				},
			},
				
				dom: 'Bfrtip',
				buttons:[
				////////////////PDF////////////////
				{
					extend: 'pdfHtml5',
					orentation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					messageTop: 'Tipos de identificacion con sus Respectivos Codigos',
					title: 'Reporte de Tipos De Identidad Registrados',
					exportOptions:{columns:[0,1]},
					text: '<img src="../../img/pdf.svg" class="iconos">',
					titleAttr: 'PDF'
				},
				///////////EXCEL/////////////////
				{
					extend: 'excelHtml5',
					autoFilter: true,
					sheetName: 'Exported data',
					text: '<img src="../../img/excel.svg" class="iconos">',
					titleAttr: 'EXCEL'
				},
				//////////COPIAR//////////////////////
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns:':visible'
					},
					text: '<img src="../../img/document.svg" class="iconos">',
					titleAttr: 'COPIAR'
				},
				//////////////FILTRO///////////////////
				{
					extend: 'colvis',
					text: '<img src="../../img/funnel.svg" class="iconos">',
					titleAttr: 'FILTRO'
				},
				{
					extend: 'print',
					text: '<img src="../../img/printer.svg" class="iconos">',
					titleAttr: 'IMPRIMIR'
				}

				],
			});
			}else{
				$('#hidIdRutina').val(data['id']);
				$('#txtRutinaAlimenticia').val(data['RutinaAlimenticia']);
				$('#selectImc').val(data['id']);
				$('#btnAdicionar').attr("disabled" , "disabled");
			}
			if(data['accion']=='MODIFICAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
			if(data['accion']=='ELIMINAR'){
				alert(data['respuesta']);
				$('#btnAdicionar').removeAttr("disabled");
			}
		}
    });   
}

$(document).ready(function() {
    RutinaAlimenticia();
    function RutinaAlimenticia() { 
        var parametros = {
            "accion": "CONSULTA_RUTINA"
        } 
        $.ajax({
            data: parametros,
            url: '../../controlador/registros/RutinaAlimenticia.C.php',
            type: "post",
            dataType: "json",
            success: function(response){
                $('#selectImc').html(response);
            }
        });   
    }
});